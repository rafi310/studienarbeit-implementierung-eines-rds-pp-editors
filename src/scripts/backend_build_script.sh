#/bin/bash

####################################################################################################
# INSTRUCTIONS:                                                                                    #
#                                                                                                  #
#   0.) create the file "/lib/systemd/system/rdspp-backend.service" with the following content:    #
#                                                                                                  #
#       [Unit]                                                                                     #
#       Description=rdspp-backend                                                                  #
#                                                                                                  #
#       [Service]                                                                                  #
#       Type=simple                                                                                #
#       Restart=always                                                                             #
#       RestartSec=5s                                                                              #
#       ExecStart=/var/www/api.rdspp.de/main                                                       #
#                                                                                                  #
#       [Install]                                                                                  #
#       WantedBy=multi-user.target                                                                 #
#                                                                                                  #
#   1.) deploy ../backend/ to server (/var/www/api.rdspp.de/src)                                   #
#   2.) deploy this script to the same destination                                                 #
#   3.) run this script (sudo !!!)                                                                 #
#                                                                                                  #
####################################################################################################

DIR="/var/www/api.rdspp.de/src"

# go into dir with transfered files
cd "$DIR"

# install dependencies and build go application
go get
go build -o main

# replace current executable with the just compiled one
mv -f main ../
mv -f config.yml ../

# replace resources and other static files
rm -rf ../ressources/
mv -f ressources/ ../
cp api/api-spec.yaml static/swagger-ui/api-spec.yaml
rm -rf ../static/
mv -f static/ ../

# restart the service
sudo service rdspp-backend restart

# clean up
cd ..
# rm -rf "$DIR"
