import Vue from 'vue'
import VueMatomo from 'vue-matomo'

export default ({ app }) => {
  Vue.use(VueMatomo, {
    router: app.router,
    host: 'https://analytics.rdspp.de/',
    siteId: 1,
    domains: '*.rdspp.de',
    cookieDomain: '*.rdspp.de'
  })
}
