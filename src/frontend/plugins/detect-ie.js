import extend from '~/util/extend-vue-app'

export default function ({ app }) {
  extend(app, {
    beforeMount() {
      const isIE = /* @cc_on!@ */ false || !!document.documentMode
      if (isIE) {
        app.router.push(app.localePath('internet-explorer'))
      }
      app.router.beforeEach((to, from, next) => {
        if (to.path === app.localePath('internet-explorer')) {
          next()
        } else if (isIE) {
          next(app.localePath('internet-explorer'))
        } else {
          next()
        }
      })
    }
  })
}
