ENV=".env"

echo "export BUILD_DATE=$(date '+%F')" > $ENV
echo "export API_HOST=$API_HOST" >> $ENV
echo "export MAP_QUEST_API_KEY=$MAP_QUEST_API_KEY" >> $ENV
