export const I18N = {
  locales: [
    {
      code: 'de',
      iso: 'de-DE',
      name: 'Deutsch',
      file: 'de/global.json'
    },
    {
      code: 'en',
      iso: 'en-EN',
      name: 'English',
      file: 'en/global.json'
    }
  ],
  defaultLocale: 'de',
  strategy: 'prefix_except_default',
  lazy: true,
  langDir: 'locale/',
  vueI18n: {
    fallbackLocale: 'de',
    silentFallbackWarn: true
  },
  vuex: {
    moduleName: 'i18n',
    syncLocale: true,
    syncMessages: false,
    syncRouteParams: true
  },
  seo: false, // done in layout => better performance (https://nuxt-community.github.io/nuxt-i18n/seo.html#improving-performance)
  vueI18nLoader: true
}
