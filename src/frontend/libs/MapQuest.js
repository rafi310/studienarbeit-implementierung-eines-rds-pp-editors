class MapQuest {
  apiKey = ''
  axios = null

  constructor(apiKey, axios) {
    this.apiKey = apiKey
    this.axios = axios
    this.geocoding.apiKey = apiKey
    this.geocoding.axios = axios
  }

  load(callback) {
    // load mapquest css and prevent repeated loading via id
    if (!document.head.querySelector('#mapquest-css')) {
      const styles = document.createElement('link')
      styles.setAttribute('href', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-maps.css')
      styles.setAttribute('type', 'text/css')
      styles.setAttribute('rel', 'stylesheet')
      styles.setAttribute('id', 'mapquest-css')
      document.head.appendChild(styles)
    }

    // load mapquest js and prevent repeated loading via id
    if (!document.head.querySelector('#mapquest-js')) {
      const plugin = document.createElement('script')
      plugin.setAttribute('src', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest.js')
      plugin.addEventListener('load', () => {
        plugin.setAttribute('loaded', 'true')
        callback()
      })
      plugin.async = true
      plugin.setAttribute('id', 'mapquest-js')
      document.head.appendChild(plugin)
    } else if (document.head.querySelector('#mapquest-js').hasAttribute('loaded')) {
      callback()
    } else {
      document.head.querySelector('#mapquest-js').addEventListener('load', callback)
    }
  }

  geocoding = {
    apiKey: '',
    axios: null,
    reverse(lat, lng, locales, locale) {
      // update geocoding information via mapquest API
      const apiUrl = `https://www.mapquestapi.com/geocoding/v1/reverse?key=${this.apiKey}&lat=${lat}&lng=${lng}&thumbMaps=false&maxResults=1`
      // assemble Accept-Language http header from i18n module options
      let acceptLanguage = ''
      locales.forEach((l) => {
        if (l.code === locale) {
          acceptLanguage = `${l.iso},${l.code};q=0.9`
        }
      })
      let q = 0.8
      locales.forEach((l) => {
        if (q < 0) return
        if (l.code !== locale) {
          acceptLanguage += `,${l.iso};q=${q.toFixed(1)}`
          q -= 0.1
          acceptLanguage += `,${l.code};q=${q.toFixed(1)}`
          q -= 0.1
        }
      })
      // make API request
      return this.axios.get(apiUrl, {
        headers: {
          'Accept-Language': acceptLanguage
        }
      })
      /*
        The following would work too, but has a downside: you can't set the Accept-Language header
        which is needed by the MapQuest Geoconding API to return i18ned names. Thus the manual axis
        request above is used instead.

        this.L.mapquest
          .geocoding({ thumbMaps: false, maxResults: 1 })
          .geocoder.reverse([this.lat, this.lng])
          .then(({ results }) => {
            ...
          })

       */
    }
  }
}

export default MapQuest
