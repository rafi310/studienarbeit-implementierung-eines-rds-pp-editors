module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended'
  ],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'linebreak-style': ['warn', 'windows'], // CRLF (\r\n, windows-style) instead of LF (\n, unix-style)
    'import/no-webpack-loader-syntax': 'off'
  }
}
