export const state = () => ({
  selectedItem: {}
})

export const mutations = {
  SET_SELECTED(state, item) {
    state.selectedItem = item
  }
}

export const actions = {
  setSelected({ commit }, item) {
    commit('SET_SELECTED', item)
  }
}
