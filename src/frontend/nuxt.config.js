import { I18N } from './config/i18n'
import fa from './util/fontawesome'
require('dotenv').config()

export default {
  mode: 'universal',
  env: process.env,
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { httpEquiv: 'X-UA-Compatible', content: 'IE=edge,chrome=1' },
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['~assets/scss/buefy.scss'],
  /*
   ** @nuxtjs/style-resources configuration
   ** See https://github.com/nuxt-community/style-resources-module
   */
  styleResources: {
    scss: ['~/assets/scss/vars/all.scss', '~/assets/scss/mixins/all.scss']
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/global-components',
    '~/plugins/buefy',
    '~/plugins/vue-cookies',
    '~/plugins/detect-ie',
    { src: '~/plugins/vue-matomo', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/style-resources',
    'nuxt-fontawesome',
    'nuxt-i18n',
    [
      'vue-scrollto/nuxt',
      {
        duration: 1000,
        offset: -80
      }
    ]
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** nuxt-i18n module options
   ** See https://nuxt-community.github.io/nuxt-i18n/options-reference.html
   */
  i18n: I18N,
  /*
   * nuxt-fontawesome config
   */
  fontawesome: {
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: fa.fas
      },
      {
        set: '@fortawesome/free-brands-svg-icons',
        icons: fa.fab
      }
    ]
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.vue$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'vue-svg-inline-loader'
          }
        ]
      })
    }
  }
}
