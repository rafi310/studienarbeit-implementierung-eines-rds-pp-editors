module rdspp.de/src

go 1.13

require (
	github.com/antlr/antlr4 v0.0.0-20200225173536-225249fdaef5
	github.com/fatih/color v1.9.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/jasonwinn/geocoder v0.0.0-20190118154513-0a8a678400b8
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a // indirect
	github.com/looplab/fsm v0.1.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/secsy/goftp v0.0.0-20190720192957-f31499d7c79a
	github.com/spf13/viper v1.6.3
	github.com/zemirco/couchdb v0.0.0-20170316052722-83ed906ea1f0 // indirect
	github.com/zemirco/uid v0.0.0-20160129141151-3763f3c45832 // indirect
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
