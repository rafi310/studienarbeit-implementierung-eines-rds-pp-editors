package api

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"rdspp.de/src/logic/designation/denomination"
	"rdspp.de/src/logic/parser"
)

func parse_GET() {
	router.GET("/parser/parse", func(c *gin.Context) {
		q := c.Query("designation")
		if strings.Trim(q, " ") == "" {
			c.Status(http.StatusBadRequest)
			return
		}
		p := parser.NewRdsppParser()
		p.ParseDesignation(q)
		denomination.Denominate(p.Tree)
		c.JSON(http.StatusOK, gin.H{
			"query":  q,
			"tree": p.Tree,
			"error": p.Error,
			"hint": p.Hint,
		})
	})
}

func isValid_GET() {
	router.GET("/parser/is-valid", func(c *gin.Context) {
		q := c.Query("designation")
		p := parser.NewRdsppParser()
		c.JSON(http.StatusOK, p.IsValid(q))
	})
}

func isUnambiguous_GET() {
	router.GET("/parser/is-unambiguous", func(c *gin.Context) {
		q := c.Query("designation")
		p := parser.NewRdsppParser()
		c.JSON(http.StatusOK, p.IsUnambiguous(q))
	})
}

func hints_GET() {
	router.GET("/parser/hints", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"hints": denomination.Hints.Hints,
		})
	})
}
