package api

func serveStatic() {
	// ./ => index page
	router.StaticFile("/", "static/index.html")
	router.StaticFile("/index.css", "static/index.css")

	// ./spec => Swagger UI
	router.Static("spec", "static/swagger-ui/")

	// ./edit => Swagger Editor
	router.Static("edit", "static/swagger-editor/")
}
