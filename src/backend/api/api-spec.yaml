openapi: '3.0.2'
info:
  title: api.rdspp.de
  version: '1.0'
  description: This is the official API specification for api.rdspp.de
servers:
- url: https://api.rdspp.de/
- url: http://localhost:8080/
tags:
- name: assistant
  description: assistance to be able to create your own designation
- name: crud
  description: everything about saving and retrieving designations from a database
- name: parser
  description: everything about syntactical analysis and semantic interpretation of designations
- name: ftp
  description: uploading and retrieving documents
paths:
  /parser/hints:
    get:
      tags: [parser]
      summary: get all hints on how the different designation types are structured
      description: ''
      responses:
        200:
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Hint'
  /parser/is-valid:
    get:
      tags: [parser]
      summary: checks if a designation is valid
      description: ''
      parameters:
      - name: designation
        in: query
        required: true
        schema:
          type: string
      responses:
        200:
          description: request is always successful. whether the designation is valid or not is expressed in the response body
          content:
            application/json:
              schema:
                type: boolean
  /parser/is-unambiguous:
    get:
      tags: [parser]
      summary: checks if a designation is unambiguous/unique
      description: ''
      parameters:
      - name: designation
        in: query
        required: true
        schema:
          type: string
      responses:
        200:
          description: request is always successful. whether the designation is valid or not is expressed in the response body
          content:
            application/json:
              schema:
                type: boolean
  /parser/parse:
    get:
      tags: [parser]
      summary: parses and denominates a designation
      description: ''
      parameters:
      - name: designation
        in: query
        required: true
        schema:
          type: string
      responses:
        200:
          description: successfully parsed the designation
          content:
            application/json:
              schema:
                type: object
                properties:
                  query:
                    type: string
                  tree:
                    $ref: '#/components/schemas/DesignationTree'
                  error:
                    $ref: '#/components/schemas/Error'
                  hint:
                    $ref: '#/components/schemas/Hint'
        400:
          description: no designation parameter provided
  /crud/save:
    post:
      tags: [crud]
      summary: try to save a designation to the database
      description: ''
      requestBody:
        required: true
        content:
          application/json:
           schema:
            type: object
            required: [designation]
            properties:
              designation:
                type: string
              override:
                type: boolean
                default: false
              author:
                type: string
              comment:
                type: string
      responses:
        200:
          description: may be successful, but may also be aborted. in case the designation was not saved, the response body is non-empty
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BaseDesignationModel'
        400:
          description: either the request body is insufficient/wrong or the passed _designation_ is invalid/ambiguous
        500:
          description: database connection failed
  /crud/can-delete:
    get:
      tags: [crud]
      summary: check whether a designation is allowed to be deleted or not
      description: ''
      parameters:
      - name: designation
        in: query
        required: true
        schema:
          type: string
      responses:
        200:
          description: request is always ok
          content:
            application/json:
              schema:
                type: boolean
                description: true if the designation may be deleted, false otherwise
  /assistant:
    post:
      tags: [assistant]
      summary: Get new State with new Proposals
      description: ''
      requestBody: 
        required: true
        content:
          application/json:
           schema:
             $ref: '#/components/schemas/CommunicationData'
      responses:
        200:
          description: successfully got new state from assistant
          content:
            application/json:
              schema:
                type: object
                properties:
                  backendData:
                    $ref: '#/components/schemas/CommunicationData'
        400:
          description: could not find another state or parse the data input Data
  /assistant/plant:
    post:
      tags: [assistant]
      summary: Is called to create only a common designation (for plants)
      description: ''
      requestBody: 
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CommunicationData'
      responses:
        200:
          description: successfully got new state from assistant
          content:
            application/json:
              schema:
                type: object
                properties:
                  backendData:
                    $ref: '#/components/schemas/CommunicationData'
        400:
          description: could not find another state or parse the data input Data
  /ftp/is-document:
    get:
      tags: [ftp]
      summary: check whether a designation is a document designation or not
      description: '**IMPORTANT**: only unique document designations with preceding conjoint designation are accepted (example: #4842N00874E.DE_BW.HOR_1WN &ABC012)'
      parameters:
      - name: designation
        in: query
        required: true
        schema:
          type: string
      responses:
        200:
          description: request is ok
          content:
            application/json:
              schema:
                type: boolean
                description: true if it is a document designation, false otherwise
  /ftp/exists:
    get:
      tags: [ftp]
      summary: check whether a document file exists or not
      description: ''
      parameters:
      - name: designation
        in: query
        required: true
        schema:
          type: string
      responses:
        200:
          description: request is ok
          content:
            application/json:
              schema:
                oneOf:
                - type: boolean
                  description: false if the document file does not exist, otherwise an object with meta information
                - $ref: '#/components/schemas/DocumentFile'
        400:
          description: bad request. designation parameter was not provided or was not a valid RDS-PP designation
  /ftp/upload:
    post:
      tags: [ftp]
      summary: upload a file for a document designation
      description: ''
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              required: [designation]
              properties:
                designation:
                  type: string
                override:
                  type: boolean
                  default: false
                file:
                  type: string
                  format: binary
                  example: 'some_document.pdf'
                  description: max. file size is 32 MiB
      responses:
        200:
          description: ok. successful upload
        400:
          description: bad request. too few parameters were provided or the designation parameter was not a valid RDS-PP _document_ designation
        409:
          description: conflict. there already exists a file for the requested document designation and the override flag was false. in case you want to replace the file set override to true and retry the request
        413:
          description: request entity too large. the uploaded file is too large (max. 32 MiB)
        500:
          description: internal server error. FTP server was not available or some unknown error
  /ftp/download:
    get:
      tags: [ftp]
      summary: retrieve a file for a document designation
      description: ''
      parameters:
      - name: designation
        in: query
        required: true
        schema:
          type: string
      responses:
        200:
          description: ok. file was found
          content:
            application/octet-stream:
              schema:
                type: string
                format: bytes
                example: some_document.pdf
        400:
          description: bad request. designation parameter was not provided or was not a valid RDS-PP _document_ designation
        404:
          description: not found. there exists no file for the requested document designation
        408:
          description: request timeout. the connection to the FTP server timed out. the timeout duration is 5min
        500:
          description: internal server error. FTP server was not available or some unknown error

components:
  schemas:
    Denomination:
      type: object
      nullable: true
      properties:
        de:
          type: string
        en:
          type: string
    Examples:
      type: object
      properties:
        de:
          type: array
          nullable: true
          items:
            type: string
        en:
          type: array
          nullable: true
          items:
            type: string
    DesignationTree:
      type: object
      properties:
        designation:
          type: string
        denomination:
          $ref: '#/components/schemas/Denomination'
        examples:
          $ref: '#/components/schemas/Examples'
        sectionDenomination:
          $ref: '#/components/schemas/Denomination'
        childs:
          type: array
          items:
            $ref: '#/components/schemas/DesignationTree'
    Error:
      type: object
      properties:
        designation:
          type: string
          description: the erroneous input
        message:
          type: string
          description: unmodified parser error -> do not use this as error message because it ain't very user friendly, instead use _type_ to determine an appropriate error message
        index:
          type: integer
          description: the index of _text_ in _designation_ (0 based)
        text:
          type: string
          description: the mismatched character(s)
        type:
          type: integer
          enum: [0, 1, 2, 3]
          description: >
            meaning:
              * 0 - no viable alternative
              * 1 - mismatched input
              * 2 - extraneous input
              * 3 - missing token
        suggestions:
          type: array
          description: some possible alternatives to _text_ (no guarantee they are helpful)
          items:
            type: string
    Hint:
      type: object
      properties:
        name:
          $ref: '#/components/schemas/Denomination'
        breakdownLevels:
          type: array
          items:
            type: object
            properties:
              bl:
                type: integer
                description: >
                  meaning:
                   * &gt;= 0 - the breakdown level number  
                   * -1 - unnumbered breakdown level
              denomination:
                $ref: '#/components/schemas/Denomination'
              dataPositions:
                type: array
                description: ordered array of all data positions of the breakdown level
                items:
                  type: string
                  minLength: 1
                  description: a character or string representing/describing one data position
              explanations:
                type: array
                description: explanations for one or more data positions each
                items:
                  type: object
                  properties:
                    start:
                      type: integer
                      description: index of the first element in _dataPositions_ this explanations is for (0 based)
                    end:
                      type: integer
                      description: index of the last element in _dataPositions_ this explanations is for (0 based)
                    de:
                      type: string
                      description: explanantion in German
                    en:
                      type: string
                      description: explanation in English
    BaseDesignationModel:
      type: object
      properties:
        designation:
          type: string
        lastModified:
          type: string
          format: date-time
        author:
          type: string
          nullable: true
        comment:
          type: string
          nullable: true
    CommunicationData:
          type: object
          properties:
            designation:
              type: string
            history:
              type: array
              items:
                $ref: '#/components/schemas/CommunicationData'
            possibleFinalState:
              type: boolean
            nextStep:
              type: integer
              enum: [0, 1, 100]
            dataInputType:
              type: integer
              enum: [0, 1, 2,3, 4, 5, 6, 7, 8 ]
            data:
              type: array
              items:
                type: string
            dataInputLength:
              type: array
              items:
                type: integer
            actualDesignationType:
              type: integer
              enum: [0, 1, 100]
            proposalList:
              type: array
              items:
                $ref: '#/components/schemas/DataCode'
    DataCode:
      type: object
      properties:
        code:
          type: string
        denomination:
          $ref: '#/components/schemas/Denomination'
    DocumentFile:
      type: object
      properties:
        designation:
          type: string
        filename:
          type: string
        filesize:
          type: integer
        lastModified:
          type: string
          format: date-time
