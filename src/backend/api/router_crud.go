package api

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"rdspp.de/src/data/repository"
	"rdspp.de/src/data/repository/models"
	"rdspp.de/src/logic/designation/denomination"
	"rdspp.de/src/logic/parser"
)

type saveRequestJson struct {
	Designation string `form:"designation" json:"designation" binding:"required"`
	Override    bool   `form:"override" json:"override"`
	Author      string `form:"author" json:"author"`
	Comment     string `form:"comment" json:"comment"`
}

func save_POST() {
	router.POST("/crud/save", func(c *gin.Context) {
		var request saveRequestJson
		if c.BindJSON(&request) != nil {
			// insufficient request parameters
			c.Status(http.StatusBadRequest)
			return
		}
		repo, err := repository.NewDbRepository()
		if err != nil {
			// database connection failed
			c.Status(http.StatusInternalServerError)
			return
		}
		existing, err := repo.SaveDesignation(request.Designation, request.Override, &models.MetaInformation{
			Author:  request.Author,
			Comment: request.Comment,
		})
		if err != nil {
			// invalid input
			c.String(http.StatusBadRequest, err.Error())
			return
		}
		if existing != nil {
			// conflict with existing designation => require confimation to override first
			var existingMap map[string]interface{}
			bytes, _ := json.Marshal(existing)
			json.Unmarshal(bytes, &existingMap)
			existingMap["fullDesignation"] = request.Designation
			c.JSON(http.StatusConflict, existingMap)
		}
		c.Status(http.StatusOK)
	})
}

func saveCsv_POST() {
	router.POST("/crud/save-csv", func(c *gin.Context) {
		file, header, err := c.Request.FormFile("file")
		defer file.Close()

		// ensure a file was uploaded and has a correct MIME type
		if err != nil {
			c.Status(http.StatusBadRequest)
			return
		}
		switch header.Header.Get("content-type") {
		case "text/csv",
			"text/plain",
			"application/vnd.ms-excel":
			// accepted content types
		default:
			c.Status(http.StatusBadRequest)
			return
		}

		// read file content
		buf := bytes.NewBuffer(nil)
		io.Copy(buf, file)
		csv := string(buf.Bytes())

		// pass content to repository
		repo, err := repository.NewDbRepository()
		if err != nil {
			// database connection failed
			c.Status(http.StatusInternalServerError)
			return
		}
		err = repo.SaveCSV(&csv)
		if err != nil {
			c.String(http.StatusBadRequest, err.Error())
		} else {
			c.Status(http.StatusOK)
		}
	})
}

func getPlants_GET() {
	router.GET("/crud/get-plants", func(c *gin.Context) {
		repo, err := repository.NewDbRepository()
		if err != nil {
			// database connection failed
			c.Status(http.StatusInternalServerError)
			return
		}
		c.JSON(http.StatusOK, repo.GetPlants())
	})
}

func getSuccessors_GET() {
	router.GET("/crud/get-successors", func(c *gin.Context) {
		repo, err := repository.NewDbRepository()
		if err != nil {
			// database connection failed
			c.Status(http.StatusInternalServerError)
			return
		}
		d := c.Query("designation")
		p := c.Query("next")
		successors, err := repo.GetSuccessorsOf(d, p)
		if err != nil {
			c.String(http.StatusBadRequest, err.Error())
		} else {
			c.JSON(http.StatusOK, successors)
		}
	})
}
func deleteDesignation_GET() {
	router.GET("/crud/delete-designation", func(c *gin.Context) {
		repo, err := repository.NewDbRepository()
		if err != nil {
			// database connection failed
			c.Status(http.StatusInternalServerError)
			return
		}
		d := c.Query("designation")
		err = repo.Delete(d)
		if err != nil {
			c.String(http.StatusBadRequest, err.Error())
		} else {
			c.JSON(http.StatusOK, d)
		}
	})
}

func canDelete_GET() {
	router.GET("/crud/can-delete", func(c *gin.Context) {
		designation := c.Query("designation")
		repo, err := repository.NewDbRepository()
		if err != nil {
			// database connection failed
			c.Status(http.StatusInternalServerError)
			return
		}
		c.JSON(http.StatusOK, repo.CanDelete(designation))
	})
}

func canContinue_GET() {
	router.GET("/crud/can-continue", func(c *gin.Context) {
		designation := c.Query("designation")
		repo, err := repository.NewDbRepository()
		if err != nil {
			// database connection failed
			c.Status(http.StatusInternalServerError)
			return
		}
		c.JSON(http.StatusOK, repo.CanContinue(designation))
	})
}

func denominatePart_GET() {
	router.GET("/parser/denominate-part", func(c *gin.Context) {
		q := c.Query("designation")
		if strings.TrimSpace(q) == "" {
			c.Status(http.StatusBadRequest)
			return
		}
		p := parser.NewRdsppParser()
		p.ParseDesignation(q)
		denomination.Denominate(p.Tree)
		if p.Tree.Childs == nil || len(p.Tree.Childs) == 0 {
			c.Status(http.StatusOK)
		} else if len(p.Tree.Childs) == 1 {
			c.JSON(http.StatusOK, gin.H{"sectionDenomination": p.Tree.Childs[0].SectionDenomination})
		} else {
			last := p.Tree.GetLastChild()
			c.JSON(http.StatusOK, gin.H{
				"denomination": last.Denomination,
				"sectionDenomination": last.SectionDenomination,
				"examples":     last.Examples,
			})
		}
	})
}
