package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func initEndpoints() {
	// test endpoint
	ping_GET()

	// parser endpoints
	parse_GET()
	denominatePart_GET()
	isValid_GET()
	isUnambiguous_GET()
	hints_GET()

	// assistant endpoints
	assistant_POST()
	assistantPlant_POST()
	assistantRestore_GET()

	// crud endpoints (db access)
	save_POST()
	saveCsv_POST()
	getPlants_GET()
	getSuccessors_GET()
	canDelete_GET()
	canContinue_GET()
	deleteDesignation_GET()

	// ftp endpoints
	isDocument_GET()
	exists_GET()
	upload_POST()
	download_GET()
}

func ping_GET() {
	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
}
