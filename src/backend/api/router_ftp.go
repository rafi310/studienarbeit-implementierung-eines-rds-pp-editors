package api

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"rdspp.de/src/data/repository"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser"
)

func isDocument_GET() {
	router.GET("/ftp/is-document", func(c *gin.Context) {
		doc := c.Query("designation")
		rdsppParser := parser.NewRdsppParser()
		rdsppParser.ParseDesignation(doc)
		if !rdsppParser.IsValid(doc) || len(rdsppParser.Tree.Childs) < 2 {
			c.JSON(http.StatusOK, false)
			return
		}
		isUniqueDoc := (rdsppParser.Tree.Childs[0].Section == designation.CD_ConjointDesignation) &&
			(rdsppParser.Tree.Childs[len(rdsppParser.Tree.Childs)-1].Section == designation.D_DocumentDesignation)
		c.JSON(http.StatusOK, isUniqueDoc)
	})
}

func exists_GET() {
	router.GET("/ftp/exists", func(c *gin.Context) {
		doc := c.Query("designation")
		r, err := repository.NewFtpRepository()
		if err != nil || r == nil {
			c.String(http.StatusInternalServerError, err.Error())
			return
		}
		c.JSON(http.StatusOK, r.Exists(doc))
	})
}

func upload_POST() {
	router.POST("/ftp/upload", func(c *gin.Context) {
		doc := c.Request.FormValue("designation")
		override := false
		if c.Request.FormValue("override") == "true" {
			override = true
		}
		file, header, err := c.Request.FormFile("file")
		defer file.Close()
		if doc == "" || err != nil {
			c.Status(http.StatusBadRequest)
			return
		}

		// read file content
		buf := bytes.NewBuffer(nil)
		io.Copy(buf, file)
		rdsppFile := &repository.RdsppDocumentFile{
			Designation:  doc,
			Filename:     header.Filename,
			Filesize:     len(buf.Bytes()),
			LastModified: time.Now(),
			Bytes:        buf.Bytes(),
		}

		// upload file
		r, err := repository.NewFtpRepository()
		if err != nil || r == nil {
			c.String(http.StatusInternalServerError, err.Error())
			return
		}
		err = r.Upload(rdsppFile, override)
		switch err {
		case nil:
			c.Status(http.StatusOK)
		case repository.ERROR_UPLOAD_OVERRIDE_REQUIRED:
			c.String(http.StatusConflict, err.Error())
		default:
			c.Status(http.StatusInternalServerError)
		}
	})
}

func download_GET() {
	router.GET("/ftp/download", func(c *gin.Context) {
		doc := c.Query("designation")
		if doc == "" {
			c.Status(http.StatusBadRequest)
			return
		}
		r, err := repository.NewFtpRepository()
		if err != nil || r == nil {
			c.String(http.StatusInternalServerError, err.Error())
			return
		}
		resultChannel, err := r.Retrieve(doc)
		if err == nil {
			select {
			case file := <-resultChannel:
				header := c.Writer.Header()
				header["Content-Type"] = []string{http.DetectContentType(file.Bytes)}
				header["Content-Length"] = []string{fmt.Sprintf("%d", file.Filesize)}
				header["Content-Disposition"] = []string{fmt.Sprintf("attachment; name=%s; filename=%s; size=%d", file.Filename, file.Filename, file.Filesize)}
				c.Writer.Write(file.Bytes)
				c.Status(http.StatusOK)
			case <-time.After(300 * time.Second):
				c.Status(http.StatusRequestTimeout)
			}
		} else {
			c.String(http.StatusNotFound, err.Error())
		}
	})
}
