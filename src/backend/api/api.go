package api

import (
	"fmt"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"rdspp.de/src/crosscutting/config"
)

var router *gin.Engine

func Init() {
	time.Sleep(1 * time.Second)

	// new blank router
	if config.IsDebug {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	router = gin.New()

	// use middleware
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(corsMittleware())

	// init REST-API endpoints
	initEndpoints()

	// serve static files
	serveStatic()

	// start router
	router.Run(fmt.Sprintf("%s:%d", config.Server.Host, config.Server.Port))
}

// factory method fpr a gin middleware handler that is responsible for CORS (Cross-Origin Resource Sharing) restrictions
func corsMittleware() gin.HandlerFunc {
	return cors.New(cors.Config{
		AllowOrigins:     config.Server.Headers.AllowOrigins,
		AllowMethods:     config.Server.Headers.AllowMethods,
		AllowHeaders:     config.Server.Headers.AllowHeaders,
		ExposeHeaders:    config.Server.Headers.ExposeHeaders,
		AllowCredentials: config.Server.Headers.AllowCredentials,
		MaxAge:           time.Duration(config.Server.Headers.MaxAge) * time.Second,
	})
}
