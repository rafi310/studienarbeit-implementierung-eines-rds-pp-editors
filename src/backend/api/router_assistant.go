package api

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"rdspp.de/src/logic/assistant"
)

type InputValueCommonication struct {
	Communication assistant.CommunicationData `yaml:"code" json:"communication"`
}

//complete Assistant
func assistant_POST() {
	router.POST("/assistant", func(c *gin.Context) {
		communication := new(InputValueCommonication)
		err := c.BindJSON(&communication)
		if err != nil {
			println("failed parse gin router")
			c.AbortWithError(400, err)
			return
		}
		newCommunication := assistant.InputLogic(&communication.Communication, false)
		c.JSON(http.StatusOK, gin.H{
			"backendData": newCommunication,
		})
	})
}

// assistant just for common Designation
func assistantPlant_POST() {
	router.POST("/assistant/plant", func(c *gin.Context) {
		communication := new(InputValueCommonication)
		err := c.BindJSON(&communication)
		if err != nil {
			println("failed parse gin router")
			c.AbortWithError(400, err)
			return
		}
		newCommunication := assistant.InputLogic(&communication.Communication, true)
		c.JSON(http.StatusOK, gin.H{
			"backendData": newCommunication,
		})
	})
}

//make designation to assistant data
func assistantRestore_GET() {
	router.GET("/assistant/restore", func(c *gin.Context) {
		d := c.Query("designation")
		if d == "" {
			c.AbortWithError(400, fmt.Errorf("failed to restore the assistant history of nothing"))
		}
		communication := assistant.RestoreAssistantData(d)
		c.JSON(http.StatusOK, gin.H{
			"backendData": communication,
		})
	})
}
