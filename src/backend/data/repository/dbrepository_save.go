package repository

import (
	"fmt"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"rdspp.de/src/data/repository/models"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser"
)

func (repo *dbRespoitory) SaveCSV(csvContent *string) (err error) {
	err = nil

	rows := strings.Split(*csvContent, "\n")
	for _, row := range rows {
		if strings.TrimSpace(row) == "" {
			// ignore empty lines
			continue
		}
		cols := strings.Split(row, ",")
		for i := 0; i < len(cols); i++ {
			// trim whitespace an quotation marks
			cols[i] = strings.Trim(cols[i], " \t\r\n\"'")
		}
		// parse values (by order)
		designation := cols[0]
		meta := &models.MetaInformation{}
		if len(cols) > 1 {
			meta.Author = cols[1]
		}
		if len(cols) > 2 {
			meta.Comment = cols[2]
		}
		// save row
		_, err = repo.SaveDesignation(designation, true, meta)
		if err != nil {
			return
		}
	}

	return
}

// SaveDesignation stores the passed designation the the database.
// If the designation is not valid, an error is returned.
// If the designation already exists in the database and override is false, a BaseDesignationModel of the existing designation is returned.
// Else (nil, nil) is returned.
func (repo *dbRespoitory) SaveDesignation(
	rdsppDesignation string,
	override bool,
	meta *models.MetaInformation,
) (models.IDbModel, error) {
	// remove whitespace
	rdsppDesignation = strings.TrimSpace(rdsppDesignation)
	rdsppDesignation = strings.ReplaceAll(rdsppDesignation, " ", "")
	// validate designation
	rdsppParser := parser.NewRdsppParser()
	if !rdsppParser.IsValid(rdsppDesignation) {
		return nil, fmt.Errorf("invalid designation")
	}
	if !rdsppParser.IsUnambiguous(rdsppDesignation) {
		return nil, fmt.Errorf("ambiguous designation")
	}

	var (
		root = rdsppParser.Tree
		nChilds    = len(root.Childs)
		upsertedID = primitive.NilObjectID
		err        error
		plantFilter = bson.E{Key: models.GetKey(models.KEY_PLANT), Value: root.Childs[0].Designation}
	)

	if nChilds == 0 || nChilds > 3 {
		// the following is practically unreachable because the designation is valid and the parser should allow this
		return nil, fmt.Errorf("designation to long => file a bug report")
	}

	if !override {
		// check if saving can't be done without overriding/updating existing documents
		overrideRequired, base := isOverrideRequired(root, plantFilter)
		if overrideRequired {
			return base, nil
		}
	}

	if nChilds == 3 {
		switch root.Childs[2].Section {
		case designation.D_DocumentDesignation:
			upsertedID, err = saveDocument(root.Childs[2], meta, plantFilter)
			switch root.Childs[1].Section {
			case designation.F_FunctionDesignation,
				designation.OE_OperatingEquipmentDesignation:
				upsertedID, err = saveFunction(root.Childs[1], nil, upsertedID, nil, plantFilter)
			case designation.PI_PointOfInstallationDesignation:
				upsertedID, err = savePointOfInstallation(root.Childs[1], upsertedID, nil, plantFilter)
			case designation.SI_SiteOfInstallationDesignation:
				upsertedID, err = saveSiteOfInstallation(root.Childs[1], upsertedID, nil, plantFilter)
			}
		case designation.S_SignalDesignation,
			designation.T_TerminalDesignation:
			upsertedID, err = saveFunction(root.Childs[1], root.Childs[2], primitive.NilObjectID, meta, plantFilter)
		}
		if err != nil {
			return nil, err
		}
	}

	if nChilds == 2 {
		switch root.Childs[1].Section {
		case designation.D_DocumentDesignation:
			upsertedID, err = saveDocument(root.Childs[1], meta, plantFilter)
		case designation.F_FunctionDesignation,
			designation.OE_OperatingEquipmentDesignation:
			upsertedID, err = saveFunction(root.Childs[1], nil, primitive.NilObjectID, meta, plantFilter)
		case designation.PI_PointOfInstallationDesignation:
			upsertedID, err = savePointOfInstallation(root.Childs[1], primitive.NilObjectID, meta, plantFilter)
		case designation.SI_SiteOfInstallationDesignation:
			upsertedID, err = saveSiteOfInstallation(root.Childs[1], primitive.NilObjectID, meta, plantFilter)
		}
		if err != nil {
			return nil, err
		}
	}

	if nChilds > 0 {
		if nChilds > 1 {
			err = savePlant(root.Childs[0], root.Childs[1].Section, upsertedID, meta)
		} else {
			err = savePlant(root.Childs[0], designation.U_Undefined, primitive.NilObjectID, meta)
		}
		if err != nil {
			return nil, err
		}
	}

	// successfully save everything
	return nil, nil
}

// Saves a conjoint designation (# ...) represented by plantNode.
// The childType defines what kind of child to reference while upserting the document. It must by on of the following values:
//  - D_DocumentDesignation
//  - F_FunctionDesignation
//  - PI_PointOfInstallationDesignation
//  - SI_SiteOfInstallationDesignation
//  - U_Undefined (no child will be set)
// If childType is one of the former 4, then childID must by provided, otherwise is may be NilObjectID.
func savePlant(
	plantNode *designation.DesignationNode,
	childType designation.DesignationSection,
	childID primitive.ObjectID,
	meta *models.MetaInformation,
) (err error) {
	err = nil

	// search for possibly existing document
	plantDesignation := plantNode.Designation
	plant := &models.PlantModel{}
	findIn(plantCollection, plant, primitive.NilObjectID, plantDesignation)
	if !plant.ID.IsZero() && childID.IsZero() {
		// plant already exists and no data is updated, hence no upsert is required
		return
	}

	// set/override fields
	plant.Designation = plantDesignation
	if plant.ID.IsZero() {
		if childID.IsZero() {
			plant.Consume(meta)
		} else {
			plant.Author = meta.Author
		}
	}
	currentIDs := make([]primitive.ObjectID, 0)
	switch childType {
	case designation.D_DocumentDesignation:
		currentIDs = plant.DocumentsIDs
	case designation.F_FunctionDesignation,
		designation.OE_OperatingEquipmentDesignation:
		currentIDs = plant.FunctionChildrenIDs
	case designation.PI_PointOfInstallationDesignation:
		currentIDs = plant.PointOfInstallationChildrenIDs
	case designation.SI_SiteOfInstallationDesignation:
		currentIDs = plant.SiteOfInstallationChildrenIDs
	case designation.U_Undefined:
	default:
	}
	isNewChild := true
	for _, id := range currentIDs {
		if id.Hex() == childID.Hex() {
			isNewChild = false
			break
		}
	}
	if isNewChild {
		switch childType {
		case designation.D_DocumentDesignation:
			plant.DocumentsIDs = append(plant.DocumentsIDs, childID)
		case designation.F_FunctionDesignation,
			designation.OE_OperatingEquipmentDesignation:
			plant.FunctionChildrenIDs = append(plant.FunctionChildrenIDs, childID)
		case designation.PI_PointOfInstallationDesignation:
			plant.PointOfInstallationChildrenIDs = append(plant.PointOfInstallationChildrenIDs, childID)
		case designation.SI_SiteOfInstallationDesignation:
			plant.SiteOfInstallationChildrenIDs = append(plant.SiteOfInstallationChildrenIDs, childID)
		}
	}
	plant.UpdateTimestamp() // only on data update

	// keep existing Id or create new one
	if plant.GetID().IsZero() {
		plant.SetID(primitive.NewObjectID())
	}

	// insert/udpate document
	upsertInto(plantCollection, plant)
	return
}

// Saves a designation that has one of the following structures:
//   =FUNCTION
//   =FUNCTION ;SIGNAL
//   =FUNCTION -PRODUCT (operating equipment)
//   =FUNCTION -PRODUCT :TERMINAL
// FUNCTION and PRODUCT are provided by the required functionNode paramter.
// SIGNAL and TERMINAL must be provided via the signalOrTerminalNode paramter (may be nil).
// The documentId will be used to set a reference to a document at the inserted leaf node. It is only use if it ain't zero (primitive.NilObjectID).
// The meta paramter may be nil. If not nil, it's content is used to set fields of the leaf node's mongo-document.
func saveFunction(
	functionNode *designation.DesignationNode,
	signalOrTerminalNode *designation.DesignationNode,
	documentId primitive.ObjectID,
	meta *models.MetaInformation,
	plantFilter bson.E,
) (rootID primitive.ObjectID, err error) {
	rootID = primitive.NilObjectID
	err = nil

	var (
		parent             models.IDbHierarchicalModel
		tmp                = models.FunctionModel{}
		modelFactory       = models.FunctionModelFactory{}
		productDesignation = functionNode.GetPartialDesignationOf(designation.P_ProductClassDesignation)
	)

	// handle function designation
	rootID, parent, err = saveHierarchically(
		functionCollection,
		functionNode,
		_HIERARCHY_FUNCTION,
		modelFactory,
		signalOrTerminalNode == nil && productDesignation == "",
		meta,
		documentId,
		plantFilter,
	)
	if err != nil {
		return
	}

	// handle product designation
	var productIndex int
	if productDesignation != "" {
		findIn(functionCollection, &tmp, parent.GetID(), "", plantFilter)
		product := models.ProductModel{}
		product.Designation = productDesignation
		if signalOrTerminalNode == nil {
			product.Consume(meta)
			product.UpdateTimestamp()
		}
		index := -1
		for i := 0; i < len(tmp.Products); i++ {
			if tmp.Products[i].Designation == product.Designation {
				index = i
				break
			}
		}
		if index == -1 { // append
			product.ID = primitive.NewObjectID()
			product.UpdateTimestamp()
			product.Author = meta.Author
			tmp.Products = append(tmp.Products, product)
			productIndex = len(tmp.Products) - 1
		} else { // replace
			if signalOrTerminalNode == nil {
				product.ID = tmp.Products[index].ID
				product.Terminals = tmp.Products[index].Terminals
				product.DocumentsIDs = tmp.Products[index].DocumentsIDs
				product.PointOfInstallationID = tmp.Products[index].PointOfInstallationID
				product.SiteOfInstallationID = tmp.Products[index].SiteOfInstallationID
				tmp.Products[index] = product
			}
			productIndex = index
		}
		tmp.SetPlant(plantFilter.Value)
		err = upsertInto(functionCollection, &tmp)
		if err != nil {
			return
		}
	}

	// handle signal/terminal designation
	if signalOrTerminalNode != nil {
		findIn(functionCollection, &tmp, parent.GetID(), "", plantFilter)
		switch signalOrTerminalNode.Section {
		case designation.S_SignalDesignation:
			signal := models.SignalModel{}
			signal.ID = primitive.NewObjectID()
			signal.Designation = signalOrTerminalNode.Designation
			signal.Consume(meta)
			signal.UpdateTimestamp()
			index := -1
			for i := 0; i < len(tmp.Signals); i++ {
				if tmp.Signals[i].Designation == signal.Designation {
					index = i
					break
				}
			}
			if index == -1 { // append
				tmp.Signals = append(tmp.Signals, signal)
			} else { // update
				tmp.Signals[index] = signal
			}
		case designation.T_TerminalDesignation:
			terminal := models.TerminalModel{}
			terminal.ID = primitive.NewObjectID()
			terminal.Designation = signalOrTerminalNode.Designation
			terminal.Consume(meta)
			terminal.UpdateTimestamp()
			index := -1
			for i := 0; i < len(tmp.Products[productIndex].Terminals); i++ {
				if tmp.Products[productIndex].Terminals[i].Designation == terminal.Designation {
					index = i
					break
				}
			}
			if index == -1 { // append
				tmp.Products[productIndex].Terminals = append(tmp.Products[productIndex].Terminals, terminal)
			} else {
				tmp.Products[productIndex].Terminals[index] = terminal
			}
		}
		tmp.SetPlant(plantFilter.Value)
		err = upsertInto(functionCollection, &tmp)
	}

	return
}

func savePointOfInstallation(
	poiNode *designation.DesignationNode,
	documentId primitive.ObjectID,
	meta *models.MetaInformation,
	plantFilter bson.E,
) (rootID primitive.ObjectID, err error) {
	rootID = primitive.NilObjectID
	err = nil

	var (
		parent           models.IDbHierarchicalModel
		modelFactory     = models.PointOfInstallationModelFactory{}
		placeDesignation = poiNode.GetPartialDesignationOf(designation.PI_PointOfInstallation)
	)

	// handle function-part of designation
	rootID, parent, err = saveHierarchically(
		poiCollection,
		poiNode,
		_HIERARCHY_POI,
		modelFactory,
		placeDesignation != "",
		meta,
		documentId,
		plantFilter,
	)
	if err != nil {
		return
	}

	if placeDesignation != "" {
		placeDesignation = poiNode.GetPartialDesignationOf(designation.SEP_Seperator) + placeDesignation
		tmp := &models.PointOfInstallationModel{}
		findIn(poiCollection, tmp, parent.GetID(), "", plantFilter)
		place := models.PointOfInstallationPlaceModel{}
		place.ID = primitive.NewObjectID()
		place.Designation = placeDesignation
		place.Consume(meta)
		place.UpdateTimestamp()
		index := -1
		for i := 0; i < len(tmp.PointOfInstallationPlaces); i++ {
			if tmp.PointOfInstallationPlaces[i].Designation == place.Designation {
				index = i
				break
			}
		}
		if index == -1 { // append
			tmp.PointOfInstallationPlaces = append(tmp.PointOfInstallationPlaces, place)
		} else {
			tmp.PointOfInstallationPlaces[index] = place
		}
		tmp.SetPlant(plantFilter.Value)
		err = upsertInto(poiCollection, tmp)
	}

	return
}

func saveSiteOfInstallation(
	soiNode *designation.DesignationNode,
	documentId primitive.ObjectID,
	meta *models.MetaInformation,
	plantFilter bson.E,
) (rootID primitive.ObjectID, err error) {
	rootID = primitive.NilObjectID
	err = nil

	var (
		parent           models.IDbHierarchicalModel
		modelFactory     = models.SiteOfInstallationModelFactory{}
		placeDesignation string
	)
	placeDesignation = soiNode.GetPartialDesignationOf(designation.SI_SiteOfInstallation)
	if placeDesignation == "" {
		placeDesignation = soiNode.GetPartialDesignationOf(designation.CD_GeographicLocation)
	}

	// handle function-part of designation
	rootID, parent, err = saveHierarchically(
		soiCollection,
		soiNode,
		_HIERARCHY_SOI,
		modelFactory,
		placeDesignation != "",
		meta,
		documentId,
		plantFilter,
	)
	if err != nil {
		return
	}

	if placeDesignation != "" {
		placeDesignation = soiNode.GetPartialDesignationOf(designation.SEP_Seperator) + placeDesignation
		tmp := &models.SiteOfInstallationModel{}
		findIn(soiCollection, tmp, parent.GetID(), "", plantFilter)
		place := models.SiteOfInstallationPlaceModel{}
		place.ID = primitive.NewObjectID()
		place.Designation = placeDesignation
		place.Consume(meta)
		place.UpdateTimestamp()
		index := -1
		for i := 0; i < len(tmp.SiteOfInstallationPlaces); i++ {
			if tmp.SiteOfInstallationPlaces[i].Designation == place.Designation {
				index = i
				break
			}
		}
		if index == -1 { // append
			tmp.SiteOfInstallationPlaces = append(tmp.SiteOfInstallationPlaces, place)
		} else {
			tmp.SiteOfInstallationPlaces[index] = place
		}
		tmp.SetPlant(plantFilter.Value)
		err = upsertInto(soiCollection, tmp)
	}

	return
}

// Saves (parts of) the rootNode in upsertCollection.
// The provided hierarchy is use to select the subnodes of rootNode and saves them. The saved documents are then linked in the order of hierarchy.
// (hierarchy must have at least one element)
// The modelFactory is used to create the models used during upsert.
// setMetaOnLeaf determines whether the last upserted document should include the passed meta struct and the documentId.
// If setMetaOnLeaf is false, meta and documentId may be nil/NilObjectID.
func saveHierarchically(
	upsertCollection *mongo.Collection,
	rootNode *designation.DesignationNode,
	hierarchy []designation.DesignationSection,
	modelFactory models.IHierarchicalModelFactory,
	setMetaOnLeaf bool,
	meta *models.MetaInformation,
	documentId primitive.ObjectID,
	plantFilter bson.E,
) (rootID primitive.ObjectID, lastInserted models.IDbHierarchicalModel, err error) {
	rootID = primitive.NilObjectID
	lastInserted = modelFactory.New()
	err = nil

	var (
		levels                                         = len(hierarchy)     //
		tmp                models.IDbHierarchicalModel = modelFactory.New() // current model
		parent             models.IDbHierarchicalModel                      // previous model and thus the parent of tmp
		partialDesignation string                                           // designation of tmp
		firstUpsert        bool                        = true
	)

	if levels == 0 {
		err = fmt.Errorf("no hierarchy provided")
		return
	}

	for i := 0; i < levels; i++ {
		var (
			section  = hierarchy[i]
			doUpsert = false // upsert only if node does not exist or
			isLeaf   = false // if it is the leaf node and may may be changed
		)

		tmp = modelFactory.New()
		partialDesignation = rootNode.GetPartialDesignationOf(section)
		if partialDesignation == "" {
			// don't break here, because the section may be optional => continue instead
			continue
		}
		isLeaf = i == levels-1 || rootNode.GetPartialDesignationOf(hierarchy[i+1]) == ""
		if firstUpsert { // root node of tree
			partialDesignation = rootNode.GetPartialDesignationOf(designation.PRE_Prefix) + partialDesignation
			findIn(upsertCollection, tmp, primitive.NilObjectID, partialDesignation, plantFilter)
		} else { // some child or leaf node
			for _, childID := range parent.GetChildrenIDs() {
				// check if the node already has a mongo-document
				findIn(upsertCollection, tmp, childID, partialDesignation, plantFilter)
				if !tmp.GetID().IsZero() { // found it
					break
				}
			}
		}
		if tmp.GetID().IsZero() { // new => insert
			tmp.SetID(primitive.NewObjectID())
			if meta != nil {
				tmp.SetAuthor(meta.Author)
			}
			doUpsert = true
		}
		// else: exists and has ID => update only if leaf
		if isLeaf && setMetaOnLeaf { // is leaf => insert/upsert
			doUpsert = true
			tmp.Consume(meta)
			if !documentId.IsZero() {
				isNewDocument := true
				for _, d := range tmp.GetDocumentsIDs() {
					if d.Hex() == documentId.Hex() {
						isNewDocument = false
						break
					}
				}
				if isNewDocument {
					tmp.SetDocumentsIDs(append(tmp.GetDocumentsIDs(), documentId))
				}
			}
		}
		// else: no action required
		if doUpsert {
			// upsert mongo-document
			tmp.SetDesignation(partialDesignation)
			tmp.UpdateTimestamp()
			tmp.SetPlant(plantFilter.Value)
			err = upsertInto(upsertCollection, tmp)
			if err != nil {
				return
			}
			// add reference to upserted document to parent
			if !firstUpsert {
				parentTmp := modelFactory.New()
				findIn(upsertCollection, parentTmp, parent.GetID(), "", plantFilter)
				isNewChild := true
				for _, id := range parentTmp.GetChildrenIDs() {
					if id.Hex() == tmp.GetID().Hex() {
						isNewChild = false
						break
					}
				}
				if isNewChild {
					parentTmp.SetChildrenIDs(append(parentTmp.GetChildrenIDs(), tmp.GetID()))
					parentTmp.UpdateTimestamp()
					parentTmp.SetPlant(plantFilter.Value)
					err = upsertInto(upsertCollection, parentTmp)
					if err != nil {
						return
					}
				}
			}
		}
		if firstUpsert {
			rootID = tmp.GetID()
		}
		parent = tmp
		firstUpsert = false
	}

	if tmp.GetID().IsZero() {
		lastInserted = parent
	} else {
		lastInserted = tmp
	}
	return
}

func saveDocument(
	documentNode *designation.DesignationNode,
	meta *models.MetaInformation,
	plantFilter bson.E,
) (documentId primitive.ObjectID, err error) {
	documentId = primitive.NilObjectID
	err = nil

	dcc := documentNode.GetPartialDesignationOf(designation.PRE_Prefix) +
		documentNode.GetPartialDesignationOf(designation.D_DocumentDcc)
	pageCountNumber := documentNode.GetPartialDesignationOf(designation.SEP_Seperator) +
		documentNode.GetPartialDesignationOf(designation.D_DocumentPageCountNumber)

	// handle DCC (e.g. &ABC001)
	doc := &models.DocumentModel{}
	findIn(documentCollection, doc, primitive.NilObjectID, dcc, plantFilter)
	if doc.GetID().IsZero() { // new document
		doc.SetID(primitive.NewObjectID())
		doc.UpdateTimestamp()
		doc.Author = meta.Author
	}
	if pageCountNumber == "" { // is leaf?
		doc.Consume(meta)
		doc.UpdateTimestamp()
	}
	doc.SetDesignation(dcc)
	// TODO ftp upload
	// TODO doc.Title, doc.Description, doc.FileUrl

	// handle nested page count number (e.g. /A12)
	if pageCountNumber != "" {
		page := models.PageCountNumberModel{}
		index := -1
		for i := 0; i < len(doc.PageCountNumbers); i++ {
			if doc.PageCountNumbers[i].Designation == pageCountNumber {
				index = i
			}
		}
		page.SetDesignation(pageCountNumber)
		page.Consume(meta)
		page.UpdateTimestamp()
		if index == -1 { // new page => append
			page.SetID(primitive.NewObjectID())
			doc.PageCountNumbers = append(doc.PageCountNumbers, page)
		} else { // existing page => update
			page.SetID(doc.PageCountNumbers[index].GetID())
			doc.PageCountNumbers[index] = page
		}
	}

	// save
	documentId = doc.GetID()
	doc.SetPlant(plantFilter.Value)
	err = upsertInto(documentCollection, doc)
	return
}
