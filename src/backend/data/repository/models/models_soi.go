package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SiteOfInstallationModel struct {
	BaseHierarichalModel     `bson:",inline"`
	SiteOfInstallationPlaces []SiteOfInstallationPlaceModel `json:"siteOfInstallationPlaces" bson:"siteOfInstallationPlaces"`
}

func (m *SiteOfInstallationModel) ToBSON() bson.D {
	doc := m.BaseHierarichalModel.ToBSON()
	appendToDocument(&doc, "siteOfInstallationPlaces", m.SiteOfInstallationPlaces)
	return doc
}

func (m *SiteOfInstallationModel) Consume(meta *MetaInformation) {
	if meta == nil {
		return
	}
	m.BaseHierarichalModel.Consume(meta)
}

type SiteOfInstallationPlaceModel struct {
	BaseDesignationModel `bson:",inline"`
	DocumentIDs []primitive.ObjectID `json:"documents" bson:"documents"`
}

func (m *SiteOfInstallationPlaceModel) ToBSON() bson.D {
	doc := m.BaseDesignationModel.ToBSON()
	appendToDocument(&doc, "documents", m.DocumentIDs)
	return doc
}
