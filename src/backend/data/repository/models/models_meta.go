package models

type MetaInformation struct {
	Author  string
	Comment string
}
