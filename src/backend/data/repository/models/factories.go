package models

type IHierarchicalModelFactory interface {
	New() IDbHierarchicalModel
}

type FunctionModelFactory struct {
}

func (f FunctionModelFactory) New() IDbHierarchicalModel {
	return &FunctionModel{}
}

type PointOfInstallationModelFactory struct {
}

func (f PointOfInstallationModelFactory) New() IDbHierarchicalModel {
	return &PointOfInstallationModel{}
}

type SiteOfInstallationModelFactory struct {
}

func (f SiteOfInstallationModelFactory) New() IDbHierarchicalModel {
	return &SiteOfInstallationModel{}
}
