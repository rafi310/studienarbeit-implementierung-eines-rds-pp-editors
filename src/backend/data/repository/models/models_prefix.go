package models

import "rdspp.de/src/logic/designation/denomination"

type IPrefixModel interface {
}

type prefixModel struct {
	IgnoreInDesignation bool   `json:"ignoreInDesignation" bson:"-"`
	Designation         string `json:"designation" bson:"-"`
	Denomination        struct {
		DE string `json:"de" bson:"-"`
		EN string `json:"en" bson:"-"`
	} `json:"denomination" bson:"-"`
}

type PrefixType int

const (
	PREFIX_UNDEFINED PrefixType = iota
	PREFIX_NONE
	PREFIX_EQUALS
	PREFIX_PLUS
	PREFIX_PLUSPLUS
	PREFIX_MINUS
	PREFIX_COLON
	PREFIX_SEMICOLON
	PREFIX_AMPERSAND
	PREFIX_HASH
)

func NewPrefixType(prefix string) PrefixType {
	switch prefix {
	case "#":
		return PREFIX_HASH
	case "=":
		return PREFIX_EQUALS
	case "+":
		return PREFIX_PLUS
	case "++":
		return PREFIX_PLUSPLUS
	case "-":
		return PREFIX_MINUS
	case ":":
		return PREFIX_COLON
	case ";":
		return PREFIX_SEMICOLON
	case "&":
		return PREFIX_AMPERSAND
	case "":
		return PREFIX_NONE
	}
	return PREFIX_UNDEFINED
}

func NewPrefixModel(modelType PrefixType) IPrefixModel {
	model := new(prefixModel)
	model.IgnoreInDesignation = true
	switch modelType {
	case PREFIX_EQUALS:
		model.Designation = "="
		model.Denomination.DE = denomination.DesignationSections.Function.De
		model.Denomination.EN = denomination.DesignationSections.Function.En
	case PREFIX_PLUS:
		model.Designation = "+"
		model.Denomination.DE = denomination.DesignationSections.PointOfInstallation.De
		model.Denomination.EN = denomination.DesignationSections.PointOfInstallation.En
	case PREFIX_PLUSPLUS:
		model.Designation = "++"
		model.Denomination.DE = denomination.DesignationSections.SiteOfInstallation.De
		model.Denomination.EN = denomination.DesignationSections.SiteOfInstallation.En
	case PREFIX_MINUS:
		model.Designation = "-"
		model.Denomination.DE = denomination.DesignationSections.Product.De
		model.Denomination.EN = denomination.DesignationSections.Product.En
	case PREFIX_COLON:
		model.Designation = ":"
		model.Denomination.DE = denomination.DesignationSections.Terminal.De
		model.Denomination.EN = denomination.DesignationSections.Terminal.En
	case PREFIX_SEMICOLON:
		model.Designation = ";"
		model.Denomination.DE = denomination.DesignationSections.Signal.De
		model.Denomination.EN = denomination.DesignationSections.Signal.En
	case PREFIX_AMPERSAND:
		model.Designation = "&"
		model.Denomination.DE = denomination.DesignationSections.Document.De
		model.Denomination.EN = denomination.DesignationSections.Document.En
	case PREFIX_UNDEFINED:
		model.Designation = ""
	}

	return model
}
