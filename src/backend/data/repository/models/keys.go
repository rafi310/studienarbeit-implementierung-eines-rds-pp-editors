package models

type Key int

const (
	KEY_DESIGNATION Key = iota
	KEY_PLANT
	KEY_PAGE_COUNT_NUMBERS
)

func GetKey(keys ...Key) string {
	key := ""
	for i, k := range keys {
		if i > 0 {
			// append dot for nested keys (e.g.: documents.designation)
			key += "."
		}
		switch k {
		case KEY_DESIGNATION:
			key += "designation"
		case KEY_PLANT:
			key += "_plant"
		case KEY_PAGE_COUNT_NUMBERS:
			key += "pageCountNumbers"
		default:
			if i > 0 {
				// remove dot if no key was appended to prevent a mulit-dot (e.g.: docuemnts..designation)
				key = key[0 : len(key)-1]
			}
		}
	}
	return key
}
