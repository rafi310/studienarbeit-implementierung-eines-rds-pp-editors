package models

import (
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func appendToDocument(doc *bson.D, key string, value interface{}) {
	*doc = append(*doc, bson.E{Key: key, Value: value})
}

type IDbModel interface {
	GetID() primitive.ObjectID
	SetID(primitive.ObjectID)
	GetPlant() string
	SetPlant(interface{})
	GetDesignation() string
	SetDesignation(string)
	GetAuthor() string
	SetAuthor(string)
	UpdateTimestamp()
	ToBSON() bson.D
	Consume(*MetaInformation)
}

type IDbHierarchicalModel interface {
	IDbModel
	GetChildrenIDs() []primitive.ObjectID
	SetChildrenIDs([]primitive.ObjectID)
	GetDocumentsIDs() []primitive.ObjectID
	SetDocumentsIDs([]primitive.ObjectID)
}

type BaseDesignationModel struct {
	ID           primitive.ObjectID `json:"-" bson:"_id"`
	Plant        string             `json:"-" bson:"_plant"`
	Designation  string             `json:"designation" bson:"designation"`
	LastModified primitive.DateTime `json:"lastModified" bson:"lastModified"`
	Author       string             `json:"author" bson:"author"`
	Comment      string             `json:"comment" bson:"comment"`
}

func (m *BaseDesignationModel) GetID() primitive.ObjectID {
	return m.ID
}

func (m *BaseDesignationModel) SetID(id primitive.ObjectID) {
	m.ID = id
}

func (m *BaseDesignationModel) GetPlant() string {
	return m.Plant
}

func (m *BaseDesignationModel) SetPlant(plant interface{}) {
	m.Plant = fmt.Sprintf("%v", plant)
}

func (m *BaseDesignationModel) GetDesignation() string {
	return m.Designation
}

func (m *BaseDesignationModel) SetDesignation(designation string) {
	m.Designation = designation
}

func (m *BaseDesignationModel) GetAuthor() string {
	return m.Author
}

func (m *BaseDesignationModel) SetAuthor(author string) {
	m.Author = author
}

func (m *BaseDesignationModel) UpdateTimestamp() {
	m.LastModified = primitive.NewDateTimeFromTime(time.Now())
}

func (m *BaseDesignationModel) ToBSON() bson.D {
	doc := bson.D{}
	appendToDocument(&doc, "_plant", m.Plant)
	appendToDocument(&doc, "designation", m.Designation)
	appendToDocument(&doc, "lastModified", m.LastModified)
	appendToDocument(&doc, "author", m.Author)
	appendToDocument(&doc, "comment", m.Comment)
	return doc
}

func (m *BaseDesignationModel) Consume(meta *MetaInformation) {
	if meta == nil {
		return
	}
	m.Author = meta.Author
	m.Comment = meta.Comment
}

type BaseHierarichalModel struct {
	BaseDesignationModel `bson:",inline"`
	DocumentsIDs         []primitive.ObjectID `json:"-" bson:"documents"`
	ChildrenIDs          []primitive.ObjectID `json:"-" bson:"children"`
}

func (m *BaseHierarichalModel) ToBSON() bson.D {
	doc := m.BaseDesignationModel.ToBSON()
	appendToDocument(&doc, "documents", m.DocumentsIDs)
	appendToDocument(&doc, "children", m.ChildrenIDs)
	return doc
}

func (m *BaseHierarichalModel) Consume(meta *MetaInformation) {
	if meta == nil {
		return
	}
	m.BaseDesignationModel.Consume(meta)
}

func (m *BaseHierarichalModel) GetChildrenIDs() []primitive.ObjectID {
	return m.ChildrenIDs
}

func (m *BaseHierarichalModel) SetChildrenIDs(ids []primitive.ObjectID) {
	m.ChildrenIDs = ids
}

func (m *BaseHierarichalModel) GetDocumentsIDs() []primitive.ObjectID {
	return m.DocumentsIDs
}

func (m *BaseHierarichalModel) SetDocumentsIDs(ids []primitive.ObjectID) {
	m.DocumentsIDs = ids
}
