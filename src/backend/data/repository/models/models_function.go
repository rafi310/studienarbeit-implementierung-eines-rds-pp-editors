package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type FunctionModel struct {
	BaseHierarichalModel `bson:",inline"`
	Products             []ProductModel `json:"-" bson:"products"`
	Signals              []SignalModel  `json:"-" bson:"signals"`
}

func (m *FunctionModel) ToBSON() bson.D {
	doc := m.BaseHierarichalModel.ToBSON()
	appendToDocument(&doc, "products", m.Products)
	appendToDocument(&doc, "signals", m.Signals)
	return doc
}

func (m *FunctionModel) Consume(meta *MetaInformation) {
	if meta == nil {
		return
	}
	m.BaseHierarichalModel.Consume(meta)
}

type ProductModel struct {
	BaseDesignationModel  `bson:",inline"`
	Terminals             []TerminalModel          `json:"terminals" bson:"terminals"`
	DocumentsIDs          []primitive.ObjectID     `json:"-" bson:"documents"`
	PointOfInstallationID *primitive.ObjectID      `json:"-" bson:"pointOfInstallation"`
	SiteOfInstallationID  *primitive.ObjectID      `json:"-" bson:"siteOfInstallation"`
}

func (m *ProductModel) ToBSON() bson.D {
	doc := m.BaseDesignationModel.ToBSON()
	appendToDocument(&doc, "terminals", m.Terminals)
	appendToDocument(&doc, "documents", m.DocumentsIDs)
	appendToDocument(&doc, "pointOfInstallation", m.PointOfInstallationID)
	appendToDocument(&doc, "siteOfInstallation", m.SiteOfInstallationID)
	return doc
}

type TerminalModel struct {
	BaseDesignationModel `bson:",inline"`
}

func (m *TerminalModel) ToBSON() bson.D {
	return m.BaseDesignationModel.ToBSON()
}

type SignalModel struct {
	BaseDesignationModel `bson:",inline"`
}

func (m *SignalModel) ToBSON() bson.D {
	return m.BaseDesignationModel.ToBSON()
}
