package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PointOfInstallationModel struct {
	BaseHierarichalModel      `bson:",inline"`
	PointOfInstallationPlaces []PointOfInstallationPlaceModel `json:"pointOfInstallationPlaces" bson:"pointOfInstallationPlaces"`
}

func (m *PointOfInstallationModel) ToBSON() bson.D {
	doc := m.BaseHierarichalModel.ToBSON()
	appendToDocument(&doc, "pointOfInstallationPlaces", m.PointOfInstallationPlaces)
	return doc
}

func (m *PointOfInstallationModel) Consume(meta *MetaInformation) {
	if meta == nil {
		return
	}
	m.BaseHierarichalModel.Consume(meta)
}

type PointOfInstallationPlaceModel struct {
	BaseDesignationModel `bson:",inline"`
	DocumentIDs []primitive.ObjectID `json:"documents" bson:"documents"`
}

func (m *PointOfInstallationPlaceModel) ToBSON() bson.D {
	doc := m.BaseDesignationModel.ToBSON()
	appendToDocument(&doc, "documents", m.DocumentIDs)
	return doc
}
