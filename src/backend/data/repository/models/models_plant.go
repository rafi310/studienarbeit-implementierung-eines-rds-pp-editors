package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PlantModel struct {
	BaseDesignationModel           `bson:",inline"`           // <= process all keys as if they were part of the outer struct (for XResult.Decode(&interface{}) to work)
	DocumentsIDs                   []primitive.ObjectID       `json:"-" bson:"documents"` // BSON only (gets stored in database)
	FunctionChildrenIDs            []primitive.ObjectID       `json:"-" bson:"functionChildren"`
	PointOfInstallationChildrenIDs []primitive.ObjectID       `json:"-" bson:"pointOfInstallationChildren"`
	SiteOfInstallationChildrenIDs  []primitive.ObjectID       `json:"-" bson:"siteOfInstallationChildren"`
}

func (m *PlantModel) ToBSON() bson.D {
	doc := m.BaseDesignationModel.ToBSON()
	appendToDocument(&doc, "documents", m.DocumentsIDs)
	appendToDocument(&doc, "functionChildren", m.FunctionChildrenIDs)
	appendToDocument(&doc, "pointOfInstallationChildren", m.PointOfInstallationChildrenIDs)
	appendToDocument(&doc, "siteOfInstallationChildren", m.SiteOfInstallationChildrenIDs)
	return doc
}
