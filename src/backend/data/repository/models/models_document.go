package models

import "go.mongodb.org/mongo-driver/bson"

type DocumentModel struct {
	BaseDesignationModel `bson:",inline"`
	Title                string                 `json:"title" bson:"title"`
	Description          string                 `json:"description" bson:"description"`
	FileUrl              string                 `json:"fileUrl" bson:"fileUrl"`
	PageCountNumbers     []PageCountNumberModel `json:"pageCountNumbers" bson:"pageCountNumbers"`
}

func (m *DocumentModel) ToBSON() bson.D {
	doc := m.BaseDesignationModel.ToBSON()
	appendToDocument(&doc, "title", m.Title)
	appendToDocument(&doc, "description", m.Description)
	appendToDocument(&doc, "fileUrl", m.FileUrl)
	appendToDocument(&doc, "pageCountNumbers", m.PageCountNumbers)
	return doc
}

type PageCountNumberModel struct {
	BaseDesignationModel `bson:",inline"`
}

func (m *PageCountNumberModel) ToBSON() bson.D {
	return m.BaseDesignationModel.ToBSON()
}
