package repository

import (
	"context"
	"fmt"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"rdspp.de/src/crosscutting/logger"
	"rdspp.de/src/data/repository/models"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser"
)

func (repo *dbRespoitory) Delete(rdsppDesignation string) (err error) {
	err = nil

	rdsppDesignation = strings.TrimSpace(rdsppDesignation)
	rdsppDesignation = strings.ReplaceAll(rdsppDesignation, " ", "")
	// validate designation
	rdsppParser := parser.NewRdsppParser()
	if !rdsppParser.IsValid(rdsppDesignation) {
		//deletes by trimming all after CD and try to parse this
		tryDeleteSomething(rdsppDesignation)
		if err == nil {
			return
		}
	}
	if !rdsppParser.IsUnambiguous(rdsppDesignation) {
		return fmt.Errorf("ambiguous designation")
	}

	root := rdsppParser.Tree
	var (
		nChilds = len(root.Childs)
	)
	if nChilds == 3 || nChilds == 2 {
		switch root.Childs[1].Section {
		case designation.D_DocumentDesignation:
			plant := models.PlantModel{}
			findIn(plantCollection, &plant, primitive.NilObjectID, root.Childs[0].Designation)
			deletePlant(plant, *root.Childs[1])
		case designation.F_FunctionDesignation,
			designation.OE_OperatingEquipmentDesignation:
			if !delete(functionCollection, primitive.NilObjectID, rdsppDesignation, *root) {
				err = fmt.Errorf("failed to delete in functionCollection")
			}
		case designation.PI_PointOfInstallationDesignation:
			if !delete(poiCollection, primitive.NilObjectID, rdsppDesignation, *root) {
				err = fmt.Errorf("failed to delete in poiCollection")
			}
		case designation.SI_SiteOfInstallationDesignation:
			if !delete(soiCollection, primitive.NilObjectID, rdsppDesignation, *root) {
				err = fmt.Errorf("failed to delete in soiCollection")
			}
		}
		return
	}
	if nChilds > 0 {
		if !delete(plantCollection, primitive.NilObjectID, rdsppDesignation, *root) {
			err = fmt.Errorf("failed to delete in plantCollection")
		}
	}

	return
}

// main function to delete a designation
func delete(
	collection *mongo.Collection,
	id primitive.ObjectID,
	rdsppDesignation string,
	root designation.DesignationNode,
) bool {
	switch collection {
	case plantCollection:
		plantModel := &models.PlantModel{}
		findIn(collection, plantModel, id, rdsppDesignation)
		if plantModel.GetID().IsZero() {
			return false
		}
		deletePlant(*plantModel, designation.DesignationNode{})
	case functionCollection:
		functionModel := &models.FunctionModel{}
		leaf, err := getLastHirarchyElement(root.Childs[1], rdsppDesignation, functionCollection)
		findIn(collection, functionModel, leaf.GetID(), "")
		if len(root.Childs) > 2 || root.GetPartialDesignationOf(designation.P_ProductClassDesignation) != "" {
			if err != nil {
				return false
			}
			findIn(functionCollection, functionModel, leaf.GetID(), "")
			findElementInFunctionModel(*functionModel, rdsppDesignation, root)
		} else {
			if functionModel.GetID().IsZero() {
				return false
			}
			deleteFunctionRecursive(*functionModel)
			removeChildID(functionModel.GetID(), *functionCollection)
		}
	case soiCollection:
		leaf, err := getLastHirarchyElement(root.Childs[1], rdsppDesignation, soiCollection)
		if err != nil {
			return false
		}
		soiModel := models.SiteOfInstallationModel{}
		findIn(soiCollection, &soiModel, leaf.GetID(), "")
		//if 3 childs the last child have to be a document
		if len(root.Childs) == 3 {
			docs := getDocuments(soiModel.DocumentsIDs)
			pageCount := root.Childs[2].GetPartialDesignationOf(designation.D_DocumentPageCountNumber)
			if pageCount != "" {
				var docID primitive.ObjectID
				// just one document
				if len(docs) == 1 {
					soiModel.DocumentsIDs = nil
				}
				for i, doc := range docs {
					if doc.(*models.DocumentModel).Designation == root.Childs[2].GetPartialDesignationOf(designation.D_DocumentDcc) {
						docID = doc.(*models.DocumentModel).ID
						soiModel.DocumentsIDs[i] = soiModel.DocumentsIDs[len(soiModel.DocumentsIDs)-1]
						soiModel.DocumentsIDs = soiModel.DocumentsIDs[:len(soiModel.DocumentsIDs)-1]
						break
					}
				}
				upsertInto(soiCollection, &soiModel)
				deleteIn(documentCollection, docID)
			} else {
				documents := castToDocuments(docs)
				updateDocumentPC(pageCount, documents)
			}
		}
		placeDesignation := root.GetPartialDesignationOf(designation.SI_SiteOfInstallation)
		if soiModel.SiteOfInstallationPlaces != nil && placeDesignation != "" {
			for i, place := range soiModel.SiteOfInstallationPlaces {
				if place.Designation == "."+placeDesignation {
					if len(soiModel.SiteOfInstallationPlaces) == 1 {
						soiModel.SiteOfInstallationPlaces = nil
					} else {
						soiModel.SiteOfInstallationPlaces[i] = soiModel.SiteOfInstallationPlaces[len(soiModel.SiteOfInstallationPlaces)-1]
						soiModel.SiteOfInstallationPlaces = soiModel.SiteOfInstallationPlaces[:len(soiModel.SiteOfInstallationPlaces)-1]
					}
					upsertInto(soiCollection, &soiModel)
				}
			}
			return true
		}
		if soiModel.GetID().IsZero() {
			return false
		}
		//delete all designations under the current
		deleteSoiRecursive(soiModel)
		// deletes this ID from all other
		removeChildID(soiModel.GetID(), *soiCollection)

	case poiCollection:
		leaf, err := getLastHirarchyElement(root.Childs[1], rdsppDesignation, poiCollection)
		if err != nil {
			return false
		}
		poiModel := models.PointOfInstallationModel{}
		findIn(poiCollection, &poiModel, leaf.GetID(), "")
		//if 3 childs the last child have to be a document
		if len(root.Childs) == 3 {
			docs := getDocuments(poiModel.DocumentsIDs)
			pageCount := root.Childs[2].GetPartialDesignationOf(designation.D_DocumentPageCountNumber)
			if pageCount != "" {
				var docID primitive.ObjectID
				// just one document
				if len(docs) == 1 {
					poiModel.DocumentsIDs = nil
				}
				for i, doc := range docs {
					if doc.(*models.DocumentModel).Designation == root.Childs[2].GetPartialDesignationOf(designation.D_DocumentDcc) {
						docID = doc.(*models.DocumentModel).ID
						poiModel.DocumentsIDs[i] = poiModel.DocumentsIDs[len(poiModel.DocumentsIDs)-1]
						poiModel.DocumentsIDs = poiModel.DocumentsIDs[:len(poiModel.DocumentsIDs)-1]
						break
					}
				}
				upsertInto(soiCollection, &poiModel)
				deleteIn(documentCollection, docID)
			} else {
				documents := castToDocuments(docs)
				updateDocumentPC(pageCount, documents)
			}
		}
		placeDesignation := root.GetPartialDesignationOf(designation.PI_PointOfInstallation)
		if poiModel.PointOfInstallationPlaces != nil && placeDesignation != "" {
			for i, place := range poiModel.PointOfInstallationPlaces {
				if place.Designation == "."+placeDesignation {
					if len(poiModel.PointOfInstallationPlaces) == 1 {
						poiModel.PointOfInstallationPlaces = nil
					} else {
						poiModel.PointOfInstallationPlaces[i] = poiModel.PointOfInstallationPlaces[len(poiModel.PointOfInstallationPlaces)-1]
						poiModel.PointOfInstallationPlaces = poiModel.PointOfInstallationPlaces[:len(poiModel.PointOfInstallationPlaces)-1]
					}
					upsertInto(poiCollection, &poiModel)
				}
			}
			return true
		}
		if poiModel.GetID().IsZero() {
			return false
		}
		//delete all designations under the current
		deletePoiRecursive(poiModel)
		// deletes this ID from all other
		removeChildID(poiModel.GetID(), *poiCollection)
	}

	return true
}

// get Terminal etc. in function model and delete it
func findElementInFunctionModel(model models.FunctionModel, rdsppDesignation string, root designation.DesignationNode) {
	productDesignation := root.GetPartialDesignationOf(designation.P_ProductClassDesignation)
	signalDesignation := root.GetPartialDesignationOf(designation.S_SignalDesignation)
	documentDCC := root.GetPartialDesignationOf(designation.D_DocumentDcc)
	documentPCN := root.GetPartialDesignationOf(designation.D_DocumentPageCountNumber)
	terminalDesignation := root.GetPartialDesignationOf(designation.T_TerminalDesignation)
	if productDesignation != "" && model.Products != nil {
		for j, product := range model.Products {
			if productDesignation == product.Designation {
				// search terminal after product
				if terminalDesignation != "" && product.Terminals != nil {
					for i, terminal := range product.Terminals {
						if terminalDesignation == terminal.Designation {
							if len(product.Terminals) == 1 {
								model.Products[j].Terminals = nil
							} else {
								model.Products[j].Terminals[i] = model.Products[j].Terminals[len(product.Terminals)-1]
								model.Products[j].Terminals = model.Products[j].Terminals[:len(product.Terminals)-1]
							}
							upsertInto(functionCollection, &model)
							return
						}
					}
				}
				// search documents after product
				if product.DocumentsIDs != nil && documentDCC != "" {
					docs := getDocuments(product.DocumentsIDs)
					for i, doc := range docs {
						docModel := doc.(*models.DocumentModel)
						if documentDCC == docModel.Designation {
							if documentPCN != "" {
								documents := []models.DocumentModel{}
								documents = append(documents, *docModel)
								updateDocumentPC(documentPCN, documents)
								return
							}
							if len(docs) == 1 {
								model.Products[j].DocumentsIDs = nil
							} else {
								model.Products[j].DocumentsIDs[i] = model.Products[j].DocumentsIDs[len(docs)-1]
								model.Products[j].DocumentsIDs = model.Products[j].DocumentsIDs[:len(docs)-1]
							}
							upsertInto(functionCollection, &model)
							deleteIn(documentCollection, doc.(*models.DocumentModel).GetID())
							return
						}
					}
				}
				if len(model.Products) == 1 {
					model.Products = nil
				} else {
					model.Products[j] = model.Products[len(model.Products)-1]
					model.Products = model.Products[:len(model.Products)-1]
				}
				upsertInto(functionCollection, &model)
				return
			}
		}
	} else if model.Signals != nil && signalDesignation != "" {
		for i, signal := range model.Signals {
			if signalDesignation == signal.Designation {
				if len(model.Signals) == 1 {
					model.Signals = nil
				} else {
					model.Signals[i] = model.Signals[len(model.Signals)-1]
					model.Signals = model.Signals[:len(model.Signals)-1]
				}
				upsertInto(functionCollection, &model)
				return
			}
		}

	} else if model.DocumentsIDs != nil && documentDCC != "" {
		docs := getDocuments(model.DocumentsIDs)
		for i, doc := range docs {
			docModel := doc.(*models.DocumentModel)
			if docModel.Designation == "&"+documentDCC {
				if documentPCN != "" {
					documents := []models.DocumentModel{}
					documents = append(documents, *docModel)
					updateDocumentPC(documentPCN, documents)
					return
				}
				if len(model.DocumentsIDs) == 1 {
					model.DocumentsIDs = nil
				} else {
					model.DocumentsIDs[i] = model.DocumentsIDs[len(model.DocumentsIDs)-1]
					model.DocumentsIDs = model.DocumentsIDs[:len(model.DocumentsIDs)-1]
				}
				upsertInto(functionCollection, &model)
				deleteIn(documentCollection, doc.(*models.DocumentModel).GetID())
				return
			}
		}
	}
	return
}

// delete plant and all designations which are reliable to it
func deletePlant(model models.PlantModel, root designation.DesignationNode) {
	//delete just Document in plant (dont delete plant)
	if root.Childs != nil {
		documentDCC := root.GetPartialDesignationOf(designation.D_DocumentDcc)
		documentPCN := root.GetPartialDesignationOf(designation.D_DocumentPageCountNumber)
		docs := getDocuments(model.DocumentsIDs)
		for i, doc := range docs {
			docModel := doc.(*models.DocumentModel)
			if docModel.Designation == "&"+documentDCC {
				if len(docs) == 1 {
					model.DocumentsIDs = nil
				} else {
					model.DocumentsIDs[i] = model.DocumentsIDs[len(model.DocumentsIDs)-1]
					model.DocumentsIDs = model.DocumentsIDs[:len(model.DocumentsIDs)-1]
				}
				if documentPCN != "" {
					documents := []models.DocumentModel{}
					documents = append(documents, *docModel)
					updateDocumentPC(documentPCN, documents)
				} else {
					upsertInto(plantCollection, &model)
					deleteIn(documentCollection, doc.(*models.DocumentModel).GetID())
				}
				return
			}
		}
		return
	}
	if model.SiteOfInstallationChildrenIDs != nil {
		for _, soiChild := range model.SiteOfInstallationChildrenIDs {
			soiModel := models.SiteOfInstallationModel{}
			findIn(soiCollection, &soiModel, soiChild, "")
			deleteSoiRecursive(soiModel)
		}
	}
	if model.PointOfInstallationChildrenIDs != nil {
		for _, poiChild := range model.PointOfInstallationChildrenIDs {
			poiModel := models.PointOfInstallationModel{}
			findIn(poiCollection, &poiModel, poiChild, "")
			deletePoiRecursive(poiModel)
		}
	}
	if model.FunctionChildrenIDs != nil {
		for _, functionChild := range model.FunctionChildrenIDs {
			functionModel := models.FunctionModel{}
			findIn(functionCollection, &functionModel, functionChild, "")
			deleteFunctionRecursive(functionModel)
		}
	}
	if model.DocumentsIDs != nil {
		for _, document := range model.DocumentsIDs {
			deleteIn(documentCollection, document)
		}
	}
	deleteIn(plantCollection, model.GetID())
}

// deletes a soi element and all his childs
func deleteSoiRecursive(model models.SiteOfInstallationModel) {
	if model.GetChildrenIDs() != nil {
		soiModel := models.SiteOfInstallationModel{}
		for _, child := range model.GetChildrenIDs() {
			findIn(soiCollection, &soiModel, child, "")
			deleteSoiRecursive(soiModel)
		}
	}
	deleteIn(soiCollection, model.GetID())
}

// deletes a poi element and all his childs
func deletePoiRecursive(model models.PointOfInstallationModel) {
	if model.GetChildrenIDs() != nil {
		poiModel := models.PointOfInstallationModel{}
		for _, child := range model.GetChildrenIDs() {
			findIn(poiCollection, &poiModel, child, "")
			deletePoiRecursive(poiModel)
		}
	}
	deleteIn(poiCollection, model.GetID())
}

// deletes a function element and all his childs
func deleteFunctionRecursive(model models.FunctionModel) {
	if model.GetChildrenIDs() != nil {
		functionModel := models.FunctionModel{}
		for _, child := range model.GetChildrenIDs() {
			findIn(functionCollection, &functionModel, child, "")
			if !functionModel.GetID().IsZero() {
				deleteFunctionRecursive(functionModel)
			}
		}
	}
	if model.DocumentsIDs != nil {
		for _, docID := range model.DocumentsIDs {
			deleteIn(documentCollection, docID)
		}
	}
	deleteIn(functionCollection, model.GetID())
}

// removes id from all elements in collection
func removeChildID(id primitive.ObjectID, collection mongo.Collection) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	switch collection {
	// TODO add documentCollection
	case *functionCollection:
		cur, err := functionCollection.Find(ctx, bson.D{{}})
		if err != nil {
			logger.Error("gotNoFunctions")
		}
		defer cur.Close(ctx)
		for cur.Next(context.Background()) {

			model := new(models.FunctionModel)
			err := cur.Decode(&model)
			if err != nil {
				logger.Error("could not decode child")
			}
			for i, child := range model.ChildrenIDs {
				if child == id {
					if len(model.ChildrenIDs) == 1 {
						model.ChildrenIDs = nil
					} else {
						model.ChildrenIDs[i] = model.ChildrenIDs[len(model.ChildrenIDs)-1]
						model.ChildrenIDs = model.ChildrenIDs[:len(model.ChildrenIDs)-1]
					}
					upsertInto(functionCollection, model)
					break
				}
			}
		}
	case *soiCollection:
		cur, err := soiCollection.Find(ctx, bson.D{{}})
		if err != nil {
			logger.Error("gotNoSOIs")
		}
		defer cur.Close(ctx)
		for cur.Next(context.Background()) {

			model := new(models.SiteOfInstallationModel)
			err := cur.Decode(&model)
			if err != nil {
				logger.Error("could not decode child")
			}
			for i, child := range model.ChildrenIDs {
				if child == id {
					model.ChildrenIDs[i] = model.ChildrenIDs[len(model.ChildrenIDs)-1]
					model.ChildrenIDs = model.ChildrenIDs[:len(model.ChildrenIDs)-1]
					upsertInto(soiCollection, model)
					break
				}
			}
		}
	case *poiCollection:
		cur, err := poiCollection.Find(ctx, bson.D{{}})
		if err != nil {
			logger.Error("gotNoPOIs")
		}
		defer cur.Close(ctx)
		for cur.Next(context.Background()) {

			model := new(models.PointOfInstallationModel)
			err := cur.Decode(&model)
			if err != nil {
				logger.Error("could not decode child")
			}
			for i, child := range model.ChildrenIDs {
				if child == id {
					model.ChildrenIDs[i] = model.ChildrenIDs[len(model.ChildrenIDs)-1]
					model.ChildrenIDs = model.ChildrenIDs[:len(model.ChildrenIDs)-1]
					upsertInto(poiCollection, model)
					break
				}
			}
		}
	}
	//if id is in a plant
	plants := getAllPlants()
	for _, plant := range plants {
		var plantmodel *models.PlantModel = plant.(*models.PlantModel)
		for i, functions := range plantmodel.FunctionChildrenIDs {
			if functions == id {
				if len(plantmodel.FunctionChildrenIDs) == 1 {
					plantmodel.FunctionChildrenIDs = nil
				} else {
					plantmodel.FunctionChildrenIDs[i] = plantmodel.FunctionChildrenIDs[len(plantmodel.FunctionChildrenIDs)-1]
					plantmodel.FunctionChildrenIDs = plantmodel.FunctionChildrenIDs[:len(plantmodel.FunctionChildrenIDs)-1]
				}
				upsertInto(plantCollection, plantmodel)
				return
			}
			for i, sois := range plantmodel.SiteOfInstallationChildrenIDs {
				if sois == id {
					if len(plantmodel.SiteOfInstallationChildrenIDs) == 1 {
						plantmodel.SiteOfInstallationChildrenIDs = nil
					} else {
						plantmodel.SiteOfInstallationChildrenIDs[i] = plantmodel.SiteOfInstallationChildrenIDs[len(plantmodel.SiteOfInstallationChildrenIDs)-1]
						plantmodel.SiteOfInstallationChildrenIDs = plantmodel.SiteOfInstallationChildrenIDs[:len(plantmodel.SiteOfInstallationChildrenIDs)-1]
					}
					upsertInto(plantCollection, plantmodel)
					return
				}
			}
			for i, pois := range plantmodel.PointOfInstallationChildrenIDs {
				if pois == id {
					if len(plantmodel.PointOfInstallationChildrenIDs) == 1 {
						plantmodel.PointOfInstallationChildrenIDs = nil
					} else {
						plantmodel.PointOfInstallationChildrenIDs[i] = plantmodel.PointOfInstallationChildrenIDs[len(plantmodel.PointOfInstallationChildrenIDs)-1]
						plantmodel.PointOfInstallationChildrenIDs = plantmodel.PointOfInstallationChildrenIDs[:len(plantmodel.PointOfInstallationChildrenIDs)-1]
					}
					upsertInto(plantCollection, plantmodel)
					return
				}
			}
		}
	}

}

func getLastHirarchyElement(
	rootNode *designation.DesignationNode,
	rdsppDesignation string,
	collection *mongo.Collection,
) (leaf models.IDbHierarchicalModel, err error) {
	leaf = nil
	err = nil

	var (
		hierarchy    []designation.DesignationSection
		modelFactory models.IHierarchicalModelFactory
	)
	if collection == functionCollection {
		hierarchy = _HIERARCHY_FUNCTION
		modelFactory = models.FunctionModelFactory{}
	}
	if collection == poiCollection {
		hierarchy = _HIERARCHY_POI
		modelFactory = models.PointOfInstallationModelFactory{}
	}
	if collection == soiCollection {
		hierarchy = _HIERARCHY_SOI
		modelFactory = models.SiteOfInstallationModelFactory{}
	}
	var (
		levels             = len(hierarchy)
		partialDesignation string
		tmp                models.IDbHierarchicalModel = modelFactory.New() // current model
		parent             models.IDbHierarchicalModel
	)

	if levels == 0 {
		err = fmt.Errorf("no hierarchy provided")
		return
	}

	for i := 0; i < levels; i++ {
		var (
			section   = hierarchy[i]
			isLeaf    = false // if it is the leaf node and may may be changed
			LeafFound = false
		)

		partialDesignation = rootNode.GetPartialDesignationOf(section)
		if partialDesignation == "" {
			// don't break here, because the section may be optional => continue instead
			if len(rootNode.Designation) == 2 {
				partialDesignation = rootNode.GetPartialDesignationOf(designation.F_SystemA1)
			} else {
				continue
			}
		}
		isLeaf = i == levels-1 || rootNode.GetPartialDesignationOf(hierarchy[i+1]) == ""
		if section == hierarchy[0] { // root node of tree
			partialDesignation = rootNode.GetPartialDesignationOf(designation.PRE_Prefix) + partialDesignation
			findIn(collection, tmp, primitive.NilObjectID, partialDesignation)
		} else {
			if parent != nil {
				if parent.GetChildrenIDs() == nil {
					findIn(collection, tmp, primitive.NilObjectID, partialDesignation)
				} // some child or leaf node
				for _, childID := range parent.GetChildrenIDs() {
					// check if the node already has a mongo-document
					findIn(collection, tmp, childID, "")
					if !tmp.GetID().IsZero() && partialDesignation == tmp.GetDesignation() { // found it
						break
					}
				}
			}
		}
		if tmp.GetID().IsZero() { // new => insert ?????? do i need this??
			tmp.SetID(primitive.NewObjectID())
			LeafFound = true
		}
		if isLeaf {
			leaf = tmp
			return
		}
		//maybe useless?
		if LeafFound {
			parent = tmp
		}
		parent = tmp
	}
	err = fmt.Errorf("didnt find leaf element of designation")
	return
}
func deleteInDocumentCollection(root designation.DesignationNode) {
	docDesignation := root.GetPartialDesignationOf(designation.D_DocumentDcc)
	pcnDesignation := root.GetPartialDesignationOf(designation.D_DocumentPageCountNumber)
	docModel := models.DocumentModel{}
	findIn(documentCollection, &docModel, primitive.NilObjectID, docDesignation)
	if pcnDesignation == "" {
		deleteIn(documentCollection, docModel.GetID())
	} else {
		documents := []models.DocumentModel{}
		documents = append(documents, docModel)
		updateDocumentPC(pcnDesignation, documents)
	}

}
func updateDocumentPC(pageCount string, documents []models.DocumentModel) {
	for _, doc := range documents {
		for j, pc := range doc.PageCountNumbers {
			if pc.Designation == "/"+pageCount {
				if len(doc.PageCountNumbers) == 1 {
					doc.PageCountNumbers = nil
				} else {
					doc.PageCountNumbers[j] = doc.PageCountNumbers[len(doc.PageCountNumbers)-1]
					doc.PageCountNumbers = doc.PageCountNumbers[:len(doc.PageCountNumbers)-1]
				}
				upsertInto(documentCollection, &doc)
				return
			}
		}
	}
}
func castToDocuments(inter []interface{}) []models.DocumentModel {
	documents := make([]models.DocumentModel, len(inter))
	for i := range inter {
		documents[i] = inter[i].(models.DocumentModel)
	}
	return documents
}
func tryDeleteSomething(rdsppDesignation string) {
	rdsppParser := parser.NewRdsppParser()
	trimmed := ""
	for len(rdsppDesignation) > 0 && !rdsppParser.IsValid(rdsppDesignation) {
		trimmed = rdsppDesignation[len(rdsppDesignation)-1:] + trimmed
		rdsppDesignation = rdsppDesignation[:len(rdsppDesignation)-1]
	}
	var (
		root = rdsppParser.Tree
	)
	if len(root.Childs) == 1 {
		plant := &models.PlantModel{}
		findIn(plantCollection, plant, primitive.NilObjectID, rdsppDesignation)
		unknownDesignation := true
		if plant.ID.IsZero() {
			return
		}
		if strings.HasPrefix(trimmed, "++") {
			soiModel := models.SiteOfInstallationModel{}
			for unknownDesignation {
				findIn(soiCollection, &soiModel, primitive.NilObjectID, trimmed)
				if soiModel.Designation != "" {
					unknownDesignation = false
				}
			}
			deleteSoiRecursive(soiModel)
		} else if strings.HasPrefix(trimmed, "+") {
			poiModel := models.PointOfInstallationModel{}
			for unknownDesignation {
				findIn(poiCollection, &poiModel, primitive.NilObjectID, trimmed)
				if poiModel.Designation != "" {
					unknownDesignation = false
				}
			}
			deletePoiRecursive(poiModel)
		} else if strings.HasPrefix(trimmed, "=") {
			functionModel := models.FunctionModel{}
			for unknownDesignation {
				findIn(functionCollection, &functionModel, primitive.NilObjectID, trimmed)
				if functionModel.Designation != "" {
					unknownDesignation = false
				}
			}
			deleteFunctionRecursive(functionModel)
		} else {
			return
		}
	}
}
