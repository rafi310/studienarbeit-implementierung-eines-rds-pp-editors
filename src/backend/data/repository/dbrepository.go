package repository

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"rdspp.de/src/crosscutting/config"
	"rdspp.de/src/crosscutting/logger"
	"rdspp.de/src/data/repository/models"
)

var (
	client             *mongo.Client     = nil
	plantCollection    *mongo.Collection = nil
	functionCollection *mongo.Collection = nil
	poiCollection      *mongo.Collection = nil
	soiCollection      *mongo.Collection = nil
	documentCollection *mongo.Collection = nil
)

type IDbRespoitory interface {
	SaveDesignation(designation string, override bool, meta *models.MetaInformation) (models.IDbModel, error)
	SaveCSV(csvContent *string) error
	/*
		TODO update methods
		Rename(oldDesignation string, newDesignation string)
			=> check if newDesignation is valid? (recursive)
			=> old and new always must be parsable without errors
			=> old and new must consist of the same sections (F_Function, ...) in the same order
			=> designation type must not change
	*/
	Delete(designation string) error //Deletes designation, returns error == nil if successful
	Exists(rdsppDesignation string) bool
	CanDelete(designation string) bool
	CanContinue(designation string) bool
	GetPlants() []interface{}
	GetSuccessorsOf(designation string, nextPrefix string) ([]interface{}, error)
}

// Implements IDbRepository
type dbRespoitory struct {
}

func NewDbRepository() (IDbRespoitory, error) {
	r := new(dbRespoitory)
	if !r.verifyConnected() {
		return r, fmt.Errorf("connection to database server could not be established")
	}
	return r, nil
}

// Checks if already connected or a connection can be established.
// False is only returned if neither a connections exists nor one can be established.
func (repo dbRespoitory) verifyConnected() bool {
	var (
		ctx    context.Context
		cancel context.CancelFunc
		err    error
	)
	// check if connection might already be established
	if client != nil {
		// if so: try to ping the server
		err = client.Ping(ctx, nil)
		if err == nil {
			// connection is still OK
			return true
		}
		// existing connection was closed/not available anymore
		// => clean up client before trying to reestablish connection
		logger.Error(err.Error())
		ctx, cancel = context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		err = client.Disconnect(ctx)
		if err != nil {
			logger.Error(err.Error())
		}
	}
	// assemble connection options
	connectionString := fmt.Sprintf("mongodb://%s:%d", config.MongoDb.Host, config.MongoDb.Port)
	clientOptions := options.Client().ApplyURI(connectionString)
	// connect to database
	ctx, cancel = context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err = mongo.Connect(ctx, clientOptions)
	if err != nil {
		// connection attempt failed
		logger.Error(err.Error())
		return false
	}
	// connection established
	db := client.Database(config.MongoDb.DatabaseName)
	plantCollection = db.Collection("plant")
	functionCollection = db.Collection("function")
	poiCollection = db.Collection("pointOfInstallation")
	soiCollection = db.Collection("siteOfInstallation")
	documentCollection = db.Collection("document")
	return true
}
