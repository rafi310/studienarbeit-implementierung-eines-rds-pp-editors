package repository

import (
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"rdspp.de/src/data/repository/models"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser"
)

func (repo *dbRespoitory) Exists(rdsppDesignation string) bool {
	rdsppDesignation = parser.RemoveWhitespace(rdsppDesignation)
	rdsppParser := parser.NewRdsppParser()
	rdsppParser.ParseDesignation(rdsppDesignation)
	if !rdsppParser.IsValid(rdsppDesignation) ||
		rdsppParser.Tree == nil ||
		len(rdsppParser.Tree.Childs) == 0 {
		return false // invalid designation don't get stored in the database
	}
	overrideRequired, _ := isOverrideRequired(
		rdsppParser.Tree,
		bson.E{
			Key:   models.GetKey(models.KEY_PLANT),
			Value: rdsppParser.Tree.Childs[0].Designation,
		},
	)
	return overrideRequired
}

func (repo *dbRespoitory) CanDelete(rdsppDesignation string) bool {
	rdsppDesignation = parser.RemoveWhitespace(rdsppDesignation)
	rdsppParser := parser.NewRdsppParser()
	rdsppParser.ParseDesignation(rdsppDesignation)

	// valid designation can be deleted at any time
	if rdsppParser.IsValid(rdsppDesignation) {
		return true
	}

	// chack spezial cases of invalid PoI/SoI designations that may be deleted
	if strings.Contains(rdsppDesignation, "+") {
		hierarchy := []designation.DesignationSection{}
		// convert PoI/SoI designation into a function designation by replacing the prefix
		// this can be done, because both designation types have the same structure as a function designation
		// (at least their beginnning)
		var functionizedDesignation string
		if strings.Contains(rdsppDesignation, "++") {
			hierarchy = _HIERARCHY_SOI
			functionizedDesignation = strings.Replace(rdsppDesignation, "++", "=", 1)
		} else if strings.Contains(rdsppDesignation, "+") {
			hierarchy = _HIERARCHY_POI
			functionizedDesignation = strings.Replace(rdsppDesignation, "+", "=", 1)
		} else {
			return false
		}
		// reparse as function designation
		rdsppParser.ParseDesignation(functionizedDesignation)
		// find first and last section
		var startSection designation.DesignationSection
		var endSection designation.DesignationSection
		for i := 0; i < len(hierarchy); i++ {
			if rdsppParser.Tree.Childs[1].GetPartialDesignationOf(hierarchy[i]) != "" {
				startSection = hierarchy[i]
				break
			}
		}
		for i := len(hierarchy) - 1; i >= 0; i-- {
			if rdsppParser.Tree.Childs[1].GetPartialDesignationOf(hierarchy[i]) != "" {
				endSection = hierarchy[i]
				break
			}
		}
		// allow delete only on top-level of invalid tree
		if startSection == endSection && startSection == hierarchy[0] {
			return true
		}
	}

	return false
}

func (repo *dbRespoitory) CanContinue(rdsppDesignation string) bool {
	rdsppDesignation = parser.RemoveWhitespace(rdsppDesignation)
	rdsppParser := parser.NewRdsppParser()
	rdsppParser.ParseDesignation(rdsppDesignation)
	if !rdsppParser.IsValid(rdsppDesignation) {
		shortenedDesignation := rdsppDesignation
		for len(shortenedDesignation) > 0 {
			shortenedDesignation = shortenedDesignation[0 : len(shortenedDesignation)-1]
			if rdsppParser.IsValid(shortenedDesignation) {
				invalidPart := rdsppDesignation[len(shortenedDesignation):]
				return (len(shortenedDesignation) == 12 ||
					len(shortenedDesignation) == 14 ||
					len(shortenedDesignation) == 26) &&
					strings.HasPrefix(invalidPart, "+")
			}
		}
		return false
	}
	rdsppParser.ParseDesignation(rdsppDesignation)
	root := rdsppParser.Tree
	nChilds := len(root.Childs)
	switch nChilds {
	case 1:
		return true
	case 2:
		switch root.Childs[1].Section {
		case designation.D_DocumentDesignation:
			return root.Childs[1].GetPartialDesignationOf(designation.D_DocumentPageCountNumber) == ""
		case designation.F_FunctionDesignation,
			designation.OE_OperatingEquipmentDesignation,
			designation.PI_PointOfInstallationDesignation,
			designation.SI_SiteOfInstallationDesignation:
			return true
		}
	case 3:
		switch root.Childs[2].Section {
		case designation.D_DocumentDesignation:
			return root.Childs[2].GetPartialDesignationOf(designation.D_DocumentPageCountNumber) == ""
		case designation.S_SignalDesignation,
			designation.T_TerminalDesignation:
			return false
		}
	}
	return false
}
