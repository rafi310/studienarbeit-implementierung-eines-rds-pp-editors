package repository

import (
	"context"
	"fmt"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"rdspp.de/src/crosscutting/logger"
	"rdspp.de/src/data/repository/models"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser"
)

func (repo *dbRespoitory) GetPlants() (plants []interface{}) {
	plants = getAllPlants()

	return
}

// returns all plants in the database
func getAllPlants() (plants []interface{}) {
	plants = make([]interface{}, 0)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cur, err := plantCollection.Find(ctx, bson.D{{}})
	if err != nil {
		logger.Error("gotNoPlants")
	}
	defer cur.Close(ctx)
	for cur.Next(context.Background()) {
		plant := new(models.PlantModel)
		err := cur.Decode(&plant)
		if err != nil {
			logger.Error("could not decode plant")
		}
		plants = append(plants, plant)
	}

	return
}

func (repo *dbRespoitory) GetSuccessorsOf(rdsppDesignation string, nextPrefix string) (successors []interface{}, err error) {
	successors = make([]interface{}, 0)
	err = nil

	nextPrefixType := models.NewPrefixType(nextPrefix)

	rdsppDesignation = strings.TrimSpace(rdsppDesignation)
	rdsppDesignation = strings.ReplaceAll(rdsppDesignation, " ", "")

	// validate designation
	rdsppParser := parser.NewRdsppParser()
	if !rdsppParser.IsValid(rdsppDesignation) {
		designationIsInDb, s := getSuccessorsOfInvalidDesignation(rdsppDesignation)
		if !designationIsInDb {
			err = fmt.Errorf("requested designation is not available in the database")
		} else {
			successors = s
		}
		return
	}

	root := rdsppParser.Tree
	var (
		nChilds = len(root.Childs)
	)

	if nChilds == 1 {
		successors = getSuccessorsOfPlant(root.Childs[0], nextPrefixType)
	}

	if nChilds == 2 {
		switch root.Childs[1].Section {
		case designation.D_DocumentDesignation:
			successors = getSuccessorsOfDocument(root.Childs[1])

		case designation.F_FunctionDesignation,
			designation.OE_OperatingEquipmentDesignation:
			successors = getSuccessorsOfFunction(root.Childs[1], nextPrefixType)

		case designation.PI_PointOfInstallationDesignation:
			successors = getSuccessorsOfPoI(root.Childs[1], nextPrefixType)

		case designation.SI_SiteOfInstallationDesignation:
			successors = getSuccessorsOfSoI(root.Childs[1], nextPrefixType)
		}
	}

	if nChilds == 3 {
		switch root.Childs[2].Section {
		case designation.D_DocumentDesignation:
			successors = getSuccessorsOfDocument(root.Childs[2])

		case designation.S_SignalDesignation,
			designation.T_TerminalDesignation:
			getSuccessorsOfFunction(root.Childs[1], nextPrefixType)
		}
	}

	return
}

func getSuccessorsOfInvalidDesignation(rdsppDesignation string) (isInDb bool, successors []interface{}) {
	isInDb = false
	successors = make([]interface{}, 0)
	
	rdsppParser := parser.NewRdsppParser()
	trimmed := ""
	for len(rdsppDesignation) > 0 && !rdsppParser.IsValid(rdsppDesignation) {
		trimmed = rdsppDesignation[len(rdsppDesignation)-1:] + trimmed
		rdsppDesignation = rdsppDesignation[:len(rdsppDesignation)-1]
	}

	var (
		root = rdsppParser.Tree
		model = &models.BaseHierarichalModel{}
		collection *mongo.Collection
		plantChildIDs []primitive.ObjectID
	)
	if len(root.Childs) == 1 {
		plant := &models.PlantModel{}
		findIn(plantCollection, plant, primitive.NilObjectID, rdsppDesignation)
		if plant.ID.IsZero() {
			return
		}
		if strings.HasPrefix(trimmed, "++") {
			collection = soiCollection
			plantChildIDs = plant.SiteOfInstallationChildrenIDs
		} else if strings.HasPrefix(trimmed, "+") {
			collection = poiCollection
			plantChildIDs = plant.PointOfInstallationChildrenIDs
		} else if strings.HasPrefix(trimmed, "=") {
			collection = functionCollection
			plantChildIDs = plant.FunctionChildrenIDs
		} else {
			return
		}
		for _, plantChildID := range plantChildIDs {
			model = &models.BaseHierarichalModel{}
			findIn(collection, model, plantChildID, "")
			if model.ID.IsZero() {
				// in case of an ID that isn't available any more
				continue
			}
			if strings.HasPrefix(trimmed, model.Designation) {
				trimmed = strings.TrimPrefix(trimmed, model.Designation)
				parent := model
				for len(trimmed) > 0 {
					model = &models.BaseHierarichalModel{}
					for _, childID := range parent.ChildrenIDs {
						findIn(collection, model, childID, "")
						if !model.ID.IsZero() && strings.HasPrefix(trimmed, model.Designation) { // child found
							trimmed = strings.TrimPrefix(trimmed, model.Designation)
							parent = model
							break
						}
					}
					if model.ID.IsZero() { // no child found => designation is not in database
						break
					}
				}
				break
			}
		}
	}
	
	if len(trimmed) == 0 {
		for _, childID := range model.ChildrenIDs {
			child := &models.BaseHierarichalModel{}
			findIn(collection, child, childID, "")
			if !child.ID.IsZero() {
				successors = append(successors, child)
			}
		}
		if collection == soiCollection {
			soiModel := &models.SiteOfInstallationModel{}
			findIn(soiCollection, soiModel, model.GetID(), "")
			if !soiModel.ID.IsZero() {
				for _, place := range soiModel.SiteOfInstallationPlaces {
					successors = append(successors, place)
				}
			}
		} else if collection == poiCollection {
			poiModel := &models.PointOfInstallationModel{}
			findIn(poiCollection, poiModel, model.GetID(), "")
			if !poiModel.ID.IsZero() {
				for _, place := range poiModel.PointOfInstallationPlaces {
					successors = append(successors, place)
				}
			}
		}
		return true, successors
	} else {
		return false, nil
	}
}

func getSuccessorsOfPlant(plantNode *designation.DesignationNode, nextPrefix models.PrefixType) (successors []interface{}) {
	successors = make([]interface{}, 0)

	if nextPrefix == models.PREFIX_NONE {
		successors = append(successors, models.NewPrefixModel(models.PREFIX_EQUALS))
		successors = append(successors, models.NewPrefixModel(models.PREFIX_PLUS))
		successors = append(successors, models.NewPrefixModel(models.PREFIX_PLUSPLUS))
		successors = append(successors, models.NewPrefixModel(models.PREFIX_AMPERSAND))
		return
	}

	plant := &models.PlantModel{}
	findIn(plantCollection, plant, primitive.NilObjectID, plantNode.Designation)
	if plant.ID.IsZero() {
		return
	}

	if nextPrefix == models.PREFIX_AMPERSAND {
		successors = getDocuments(plant.DocumentsIDs)
	}

	if nextPrefix == models.PREFIX_EQUALS || nextPrefix == models.PREFIX_PLUS || nextPrefix == models.PREFIX_PLUSPLUS {
		var (
			childIDs []primitive.ObjectID
			collection *mongo.Collection
		)
		switch nextPrefix {
		case models.PREFIX_EQUALS:
			childIDs = plant.FunctionChildrenIDs
			collection = functionCollection
		case models.PREFIX_PLUS:
			childIDs = plant.PointOfInstallationChildrenIDs
			collection = poiCollection
		case models.PREFIX_PLUSPLUS:
			childIDs = plant.SiteOfInstallationChildrenIDs
			collection = soiCollection
		}
		for _, id := range childIDs {
			model := &models.BaseHierarichalModel{}
			findIn(collection, model, id, "")
			if !model.ID.IsZero() {
				successors = append(successors, model)
			}
		}
	}
	
	return
}

func getDocuments(documentIDs []primitive.ObjectID) (successors []interface{}) {
	successors = make([]interface{}, 0)

	for _, id := range documentIDs {
		documentModel := &models.DocumentModel{}
		findIn(documentCollection, documentModel, id, "")
		if !documentModel.ID.IsZero() {
			successors = append(successors, documentModel)
		}
	}
	return
}

func getSuccessorsOfDocument(docNode *designation.DesignationNode) (successors []interface{}) {
	successors = make([]interface{}, 0)

	if docNode.GetPartialDesignationOf(designation.D_DocumentPageCountNumber) != "" {
		return
	}
	dcc := docNode.GetPartialDesignationOf(designation.PRE_Prefix) +
		docNode.GetPartialDesignationOf(designation.D_DocumentDcc)
	document := &models.DocumentModel{}
	findIn(documentCollection, document, primitive.NilObjectID, dcc)
	if !document.ID.IsZero() {
		for _, page := range document.PageCountNumbers {
			successors = append(successors, page)
		}
	}
	return
}

func getSuccessorsOfFunction(functionNode *designation.DesignationNode, nextPrefix models.PrefixType) (successors []interface{}) {
	successors = make([]interface{}, 0)

	isLastReached, lastID, s, err := checkDesignationHierarchy(
		functionCollection,
		functionNode,
		_HIERARCHY_FUNCTION,
		models.FunctionModelFactory{},
	)
	if err != nil {
		return
	}
	if !isLastReached && nextPrefix == models.PREFIX_NONE {
		successors = s
		successors = append(successors, models.NewPrefixModel(models.PREFIX_AMPERSAND))
		return
	}

	function := &models.FunctionModel{}
	findIn(functionCollection, function, lastID, "")
	if function.ID.IsZero() {
		return
	}

	productDesignation := functionNode.GetPartialDesignationOf(designation.P_ProductClassDesignation)
	switch nextPrefix {
		case models.PREFIX_NONE:
			if productDesignation == "" {
				successors = append(successors, models.NewPrefixModel(models.PREFIX_MINUS))
				successors = append(successors, models.NewPrefixModel(models.PREFIX_SEMICOLON))
			} else {
				successors = append(successors, models.NewPrefixModel(models.PREFIX_COLON))
			}
			successors = append(successors, models.NewPrefixModel(models.PREFIX_AMPERSAND))
		
		case models.PREFIX_AMPERSAND:
			if productDesignation == "" {
				successors = getDocuments(function.DocumentsIDs)
			} else {
				for _, p := range function.Products {
					if p.Designation == productDesignation {
						successors = getDocuments(p.DocumentsIDs)
						break
					}
				}
			}
			
		case models.PREFIX_MINUS:
			for _, product := range function.Products {
				successors = append(successors, product)
			}
			
		case models.PREFIX_SEMICOLON:
			for _, signal := range function.Signals {
				successors = append(successors, signal)
			}
			
		case models.PREFIX_COLON:
			for _, p := range function.Products {
				if p.Designation == productDesignation {
					for _, terminal := range p.Terminals {
						successors = append(successors, terminal)
					}
					break
				}
			}
	}

	return
}

func getSuccessorsOfPoI(poiNode *designation.DesignationNode, nextPrefix models.PrefixType) (successors []interface{}) {
	successors = make([]interface{}, 0)

	isLastReached, lastID, s, err := checkDesignationHierarchy(
		poiCollection,
		poiNode,
		_HIERARCHY_POI,
		models.PointOfInstallationModelFactory{},
	)
	if err != nil {
		return
	}
	if !isLastReached && nextPrefix == models.PREFIX_NONE {
		successors = s
		p := parser.NewRdsppParser()
		if p.IsValid(poiNode.Designation) {
			successors = append(successors, models.NewPrefixModel(models.PREFIX_AMPERSAND))
		}
		return
	}

	poi := &models.PointOfInstallationModel{}
	findIn(poiCollection, poi, lastID, "")
	if poi.ID.IsZero() {
		return
	}

	place := poiNode.GetPartialDesignationOf(designation.PI_PointOfInstallation)
	if place == "" {
		for _, p := range poi.PointOfInstallationPlaces {
			successors = append(successors, p)
		}
	}
	
	switch nextPrefix {
	case models.PREFIX_NONE:
		successors = append(successors, models.NewPrefixModel(models.PREFIX_AMPERSAND))

	case models.PREFIX_AMPERSAND:
		if place != "" {
			for _, p := range poi.PointOfInstallationPlaces {
				if p.Designation == place {
					successors = getDocuments(p.DocumentIDs)
					break
				}
			}
		} else {
			successors = getDocuments(poi.DocumentsIDs)
		}
	}

	return
}

func getSuccessorsOfSoI(soiNode *designation.DesignationNode, nextPrefix models.PrefixType) (successors []interface{}) {
	successors = make([]interface{}, 0)

	isLastReached, lastID, s, err := checkDesignationHierarchy(
		soiCollection,
		soiNode,
		_HIERARCHY_SOI,
		models.SiteOfInstallationModelFactory{},
	)
	if err != nil {
		return
	}
	if !isLastReached && nextPrefix == models.PREFIX_NONE {
		successors = s
		p := parser.NewRdsppParser()
		if p.IsValid(soiNode.Designation) {
			successors = append(successors, models.NewPrefixModel(models.PREFIX_AMPERSAND))
		}
		return
	}

	soi := &models.SiteOfInstallationModel{}
	findIn(soiCollection, soi, lastID, "")
	if soi.ID.IsZero() {
		return
	}

	place := soiNode.GetPartialDesignationOf(designation.SI_SiteOfInstallation)
	if place == "" {
		for _, p := range soi.SiteOfInstallationPlaces {
			successors = append(successors, p)
		}
	}

	switch nextPrefix {
	case models.PREFIX_NONE:
		successors = append(successors, models.NewPrefixModel(models.PREFIX_AMPERSAND))

	case models.PREFIX_AMPERSAND:
		if place != "" {
			for _, p := range soi.SiteOfInstallationPlaces {
				if p.Designation == place {
					successors = getDocuments(p.DocumentIDs)
					break
				}
			}
		} else {
			successors = getDocuments(soi.DocumentsIDs)
		}
	}

	return
}

func checkDesignationHierarchy(
	collection *mongo.Collection,
	rootNode *designation.DesignationNode,
	hierarchy []designation.DesignationSection,
	modelFactory models.IHierarchicalModelFactory,
) (isLastReached bool, lastID primitive.ObjectID, successors []interface{}, err error) {
	isLastReached = false
	lastID = primitive.NilObjectID
	successors = make([]interface{}, 0)
	err = nil

	var (
		levels = len(hierarchy)
		partialDesignation string
		coveredDesignation string
		tmp models.IDbHierarchicalModel
		parent models.IDbHierarchicalModel = modelFactory.New()
		first = true
	)

	for i := 0; i < levels; i++ {
		var (
			section = hierarchy[i]
		)
		partialDesignation = rootNode.GetPartialDesignationOf(section)
		if partialDesignation == "" {
			if !first && coveredDesignation == rootNode.Designation {
				for _, id := range parent.GetChildrenIDs() {
					tmp = modelFactory.New()
					findIn(collection, tmp, id, "")
					successors = append(successors, tmp)
				}
				lastID = parent.GetID()
				return
			}
			continue
		}
		if first {
			partialDesignation = rootNode.GetPartialDesignationOf(designation.PRE_Prefix) + partialDesignation
			findIn(collection, parent, primitive.NilObjectID, partialDesignation)
			if parent.GetID().IsZero() {
				err = fmt.Errorf("designation is not in database")
				return
			}
			first = false
		}
		coveredDesignation += partialDesignation
		for _, id := range parent.GetChildrenIDs() {
			tmp = modelFactory.New()
			findIn(collection, tmp, id, "")
			if !tmp.GetID().IsZero() && tmp.GetDesignation() == partialDesignation {
				parent = tmp
				break
			}
		}
		if tmp.GetID().IsZero() {
			err = fmt.Errorf("designation is not in database")
			return
		}
	}

	isLastReached = true
	lastID = parent.GetID()
	return
}
