package repository

import (
	"context"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"rdspp.de/src/data/repository/models"
	"rdspp.de/src/logic/designation"
)

// Checks whether a designation can be saved with insertions only without overriding a existing designation.
// In case of designation trees (like a function designation) only the leave node is consideres since all
// intermediate nodes wouldn't be changed anyway.
func isOverrideRequired(root *designation.DesignationNode, plantFilter bson.E) (bool, models.IDbModel) {
	var (
		nChilds                 = len(root.Childs)
		model   models.IDbModel = nil
	)

	if nChilds == 1 {
		plant := &models.PlantModel{}
		findIn(plantCollection, plant, primitive.NilObjectID, root.Childs[0].Designation)
		model = plant
	}

	if nChilds == 2 {
		switch root.Childs[1].Section {
		case designation.D_DocumentDesignation:
			model = isOverrideRequiredDocument(root.Childs[1], plantFilter)

		case designation.F_FunctionDesignation,
			designation.OE_OperatingEquipmentDesignation:
			model = isOverrideRequiredFunction(root.Childs[1], nil, plantFilter)

		case designation.PI_PointOfInstallationDesignation:
			model = isOverrideRequiredPointOfInstallation(root.Childs[1], plantFilter)
		case designation.SI_SiteOfInstallationDesignation:
			model = isOverrideRequiredSiteOfInstallation(root.Childs[1], plantFilter)
		}
	}

	if nChilds == 3 {
		switch root.Childs[2].Section {
		case designation.D_DocumentDesignation:
			model = isOverrideRequiredDocument(root.Childs[2], plantFilter)

		case designation.S_SignalDesignation,
			designation.T_TerminalDesignation:
			model = isOverrideRequiredFunction(root.Childs[1], root.Childs[2], plantFilter)
		}
	}

	if model == nil || reflect.ValueOf(model).IsNil() || model.GetID().IsZero() {
		return false, nil
	}
	return true, model
}

// Don't use this method manually. Use the more general isOverrideRequired() instead.
func isOverrideRequiredDocument(documentNode *designation.DesignationNode, plantFilter bson.E) models.IDbModel {
	var (
		model models.IDbModel = nil
	)

	dcc := documentNode.GetPartialDesignationOf(designation.PRE_Prefix) +
		documentNode.GetPartialDesignationOf(designation.D_DocumentDcc)
	pageCountNumber := documentNode.GetPartialDesignationOf(designation.SEP_Seperator) +
		documentNode.GetPartialDesignationOf(designation.D_DocumentPageCountNumber)
	filters := []bson.E{
		plantFilter,
		{
			Key:   models.GetKey(models.KEY_DESIGNATION),
			Value: dcc,
		},
	}
	if pageCountNumber != "" {
		filters = append(filters, bson.E{
			Key:   models.GetKey(models.KEY_PAGE_COUNT_NUMBERS, models.KEY_DESIGNATION),
			Value: pageCountNumber,
		})
	}
	doc := &models.DocumentModel{}
	findIn(documentCollection, doc, primitive.NilObjectID, "", filters...)
	model = doc

	return model
}

// Don't use this method manually. Use the more general isOverrideRequired() instead.
func isOverrideRequiredFunction(
	functionNode *designation.DesignationNode,
	signalOrTerminal *designation.DesignationNode,
	plantFilter bson.E,
) models.IDbModel {
	var (
		model models.IDbModel = nil
	)
	model = isOverrideRequiredHierarchy(
		functionCollection,
		functionNode,
		_HIERARCHY_FUNCTION,
		models.FunctionModelFactory{},
		plantFilter,
	)
	if model != nil && !reflect.ValueOf(model).IsNil() { // then check product, signal and terminal as well
		var productModel *models.ProductModel = nil
		productDesignation := functionNode.GetPartialDesignationOf(designation.P_ProductClassDesignation)
		if productDesignation != "" {
			for _, p := range model.(*models.FunctionModel).Products { // try to find product
				if p.GetDesignation() == productDesignation {
					productModel = &p
					break
				} else {
					productModel = nil
				}
			}
		}

		if signalOrTerminal == nil {
			if productModel != nil { // product is leaf
				model = productModel
			}
		} else { // check signal/terminal as well
			switch signalOrTerminal.Section {
			case designation.S_SignalDesignation:
				signalDesignation := signalOrTerminal.GetPartialDesignationOf(designation.S_SignalDesignation)
				if signalDesignation == "" {
					return model
				}
				for _, s := range model.(*models.FunctionModel).Signals {
					if s.GetDesignation() == signalDesignation {
						return &s
					}
				}
				model = nil
			case designation.T_TerminalDesignation:
				if productModel == nil || reflect.ValueOf(productModel).IsNil() { // no product found, hence no (embedded) terminal can be found
					break
				}
				terminalDesignation := signalOrTerminal.GetPartialDesignationOf(designation.T_TerminalDesignation)
				if terminalDesignation == "" {
					return model
				}
				for _, t := range productModel.Terminals {
					if t.GetDesignation() == terminalDesignation {
						return &t
					}
				}
				model = nil
			}
		}
	}
	return model
}

// Don't use this method manually. Use the more general isOverrideRequired() instead.
func isOverrideRequiredPointOfInstallation(
	poiNode *designation.DesignationNode,
	plantFilter bson.E,
) models.IDbModel {
	var (
		model models.IDbModel = nil
	)
	model = isOverrideRequiredHierarchy(
		poiCollection,
		poiNode,
		_HIERARCHY_POI,
		models.PointOfInstallationModelFactory{},
		plantFilter,
	)
	if model != nil && !reflect.ValueOf(model).IsNil() {
		placeDesignation := poiNode.GetPartialDesignationOf(designation.PI_PointOfInstallation)
		if placeDesignation == "" {
			return model
		}
		for _, p := range model.(*models.PointOfInstallationModel).PointOfInstallationPlaces {
			if p.GetDesignation() == placeDesignation {
				return &p
			}
		}
		model = nil
	}
	return model
}

// Don't use this method manually. Use the more general isOverrideRequired() instead.
func isOverrideRequiredSiteOfInstallation(
	soiNode *designation.DesignationNode,
	plantFilter bson.E,
) models.IDbModel {
	var (
		model models.IDbModel = nil
	)
	model = isOverrideRequiredHierarchy(
		soiCollection,
		soiNode,
		_HIERARCHY_SOI,
		models.SiteOfInstallationModelFactory{},
		plantFilter,
	)
	if model != nil && !reflect.ValueOf(model).IsNil() {
		placeDesignation := soiNode.GetPartialDesignationOf(designation.SI_SiteOfInstallation)
		if placeDesignation == "" {
			return model
		}
		for _, p := range model.(*models.SiteOfInstallationModel).SiteOfInstallationPlaces {
			if p.GetDesignation() == placeDesignation {
				return &p
			}
		}
		model = nil
	}
	return model
}

// Don't use this method manually. Use the more general isOverrideRequired() instead.
func isOverrideRequiredHierarchy(
	collection *mongo.Collection,
	node *designation.DesignationNode,
	hierarchy []designation.DesignationSection,
	modelFactory models.IHierarchicalModelFactory,
	plantFilter bson.E,
) models.IDbModel {
	var (
		nHierarchies                 = len(hierarchy)
		model        models.IDbModel = nil
	)
	tmp := modelFactory.New()
MainLoop:
	for i, h := range hierarchy {
		if nHierarchies > 1 && i == nHierarchies-1 {
			break
		}
		var d string
		if i == 0 { // first
			d = node.GetPartialDesignationOf(designation.PRE_Prefix) +
				node.GetPartialDesignationOf(h)
			findIn(collection, tmp, primitive.NilObjectID, d, plantFilter)
			if tmp.GetID().IsZero() {
				tmp = nil
				break
			}
			if nHierarchies == 1 {
				break
			}
		}
		d = node.GetPartialDesignationOf(hierarchy[i+1])
		if d == "" {
			break
		}
		for _, childID := range tmp.GetChildrenIDs() {
			findIn(collection, tmp, childID, "", plantFilter)
			if tmp.GetDesignation() == d {
				continue MainLoop
			}
		}
		tmp = nil
		break
	}
	model = tmp
	return model
}

// Performs a FindOne operation on the passed collection.
// Either _id, rdsppDesignation or additionalFilters (or a combination of all) must be provided and
// at least one of them must be non-empty/zero since they are used as filters.
// If any further filters are required use the variadic additionalFilters parameter,
// where every element with a non-empty Key will be appended to the filter.
// The result is then being decoded into out, so make sure it's a pointer/reference.
func findIn(
	collection *mongo.Collection,
	out models.IDbModel,
	_id primitive.ObjectID,
	rdsppDesignation string,
	additionalFilters ...bson.E,
) {
	if _id.IsZero() && rdsppDesignation == "" && len(additionalFilters) == 0 {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.D{}
	if !_id.IsZero() {
		filter = append(filter, bson.E{Key: "_id", Value: _id})
	}
	if rdsppDesignation != "" {
		filter = append(filter, bson.E{Key: models.GetKey(models.KEY_DESIGNATION), Value: rdsppDesignation})
	}
	for _, f := range additionalFilters {
		if f.Key != "" {
			filter = append(filter, f)
		}
	}
	findResult := collection.FindOne(ctx, filter)
	findResult.Decode(out)
}

// Updates or inserts the provided model into the collection.
// If the models ID is zero, a new ID will be generated.
func upsertInto(
	collection *mongo.Collection,
	model models.IDbModel,
) error {
	_id := model.GetID()
	if _id.IsZero() {
		_id = primitive.NewObjectID()
	}
	upsertOptions := options.UpdateOptions{}
	upsertOptions.SetUpsert(true)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := collection.UpdateOne(
		ctx,
		bson.D{bson.E{Key: "_id", Value: _id}},
		bson.D{bson.E{Key: "$set", Value: model.ToBSON()}},
		&upsertOptions,
	)
	if err != nil {
		return err
	}
	return nil
}

// Deletes one element from a collection by its ID
func deleteIn(collection *mongo.Collection, id primitive.ObjectID) {
	if id == primitive.NilObjectID {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.D{}
	filter = append(filter, bson.E{Key: "_id", Value: id})
	collection.DeleteOne(ctx, filter)
}
