package repository

import "rdspp.de/src/logic/designation"

/*
	hierarchy in which the mongo-documents should be referenced:

	F_MainSystemA1					G
	└─>	F_MainSystemCounter			└─>	001
		└─>	F_SystemA1					└─>	M
			└─>	F_SystemA2					└─>	D
				└─>	F_SystemA3					└─>	K
					└─>	F_Subsystem					└─>	51
						└─>	F_BasicFunction				└─>	GP001

	(F_... represents a designation section an its corresponding mongo-document; └─> is a ObjectID reference)

	Embedded documents (e.g. T_Terminal or S_Signal) must be handled seperately.
*/
var (
	_HIERARCHY_FUNCTION = []designation.DesignationSection{
		designation.F_MainSystemA1,
		designation.F_MainSystemCounter,
		designation.F_SystemA1,
		designation.F_SystemA2,
		designation.F_SystemA3,
		designation.F_Subsystem,
		designation.F_BasicFunction,
	}
	_HIERARCHY_POI     = _HIERARCHY_FUNCTION
	_HIERARCHY_SOI     = _HIERARCHY_FUNCTION[0 : len(_HIERARCHY_FUNCTION)-1]
)
