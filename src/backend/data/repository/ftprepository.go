package repository

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/secsy/goftp"
	"rdspp.de/src/crosscutting/config"
	"rdspp.de/src/crosscutting/logger"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser"
)

var (
	ftpClient *goftp.Client

	forbiddenCharMap = map[string]string{
		"/": "<SLASH>",
	}

	ERROR_NO_CONNECTION            = fmt.Errorf("connection to ftp server could not be established")
	ERROR_UPLOAD_OVERRIDE_REQUIRED = fmt.Errorf("override required")
	ERROR_UPLOAD_WEIRD             = fmt.Errorf("i got no clue what happened ... you should probably file a bug report :)")
	ERROR_NOT_A_DOCUMENT           = fmt.Errorf("not a document designation")
	ERROR_NO_META                  = fmt.Errorf("meta information not found")
)

type IFtpRepository interface {
	EncodeFilename(rdsppDesignation string) (filename string)
	Exists(rdsppDesignation string) interface{}
	Retrieve(path string) (chan *RdsppDocumentFile, error)
	Upload(file *RdsppDocumentFile, override bool) error
}

// implements IFtpRepository
type ftpRepository struct {
}

type RdsppDocumentFile struct {
	Designation  string    `json:"designation"`
	Filename     string    `json:"name"`
	Filesize     int       `json:"size"`
	LastModified time.Time `json:"lastModified"`
	Bytes        []byte    `json:"-"`
}

func (f RdsppDocumentFile) IsMetaEmpty() bool {
	return f.Designation == "" && f.Filename == ""
}

func (f RdsppDocumentFile) IsDataEmpty() bool {
	return len(f.Bytes) == 0
}

func NewFtpRepository() (IFtpRepository, error) {
	r := new(ftpRepository)
	if !r.verifyConnected() {
		return r, ERROR_NO_CONNECTION
	}
	return r, nil
}

func (ftpRepository) verifyConnected() bool {
	// check if connection is already established
	if ftpClient != nil {
		// if so: try getting pwd
		_, err := ftpClient.Getwd()
		if err == nil {
			// connection is still ok
			return true
		}
		// client connection was closed/interruped => retry conneting
		ftpClient.Close()
		ftpClient = nil
	}

	// open connection
	ftpConfig := goftp.Config{
		User:     config.Ftp.Username,
		Password: config.Ftp.Password,
		TLSMode:  goftp.TLSExplicit,
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
			ClientAuth:         tls.RequestClientCert,
		},
		ConnectionsPerHost: 10,
		ServerLocation:     time.Local,
		Timeout:            5 * time.Second,
	}
	if config.IsDebug {
		ftpConfig.Logger = os.Stdout
	}
	c, err := goftp.DialConfig(ftpConfig, fmt.Sprintf("%s:%d", config.Ftp.Host, config.Ftp.Port))
	if err != nil {
		logger.Error(err.Error())
		return false
	}

	ftpClient = c
	return true
}

func (*ftpRepository) EncodeFilename(rdsppDesignation string) (filename string) {
	filename = parser.RemoveWhitespace(rdsppDesignation)
	for k, v := range forbiddenCharMap {
		filename = strings.ReplaceAll(filename, k, v)
	}
	return
}

func verifyDesignation(rdsppDesignation string) (bool, string) {
	// verify that rdsppDesignation is a doument designation
	rdsppDesignation = parser.RemoveWhitespace(rdsppDesignation)
	rdsppParser := parser.NewRdsppParser()
	rdsppParser.ParseDesignation(rdsppDesignation)
	ast := rdsppParser.Tree
	if !rdsppParser.IsValid(rdsppDesignation) ||
		len(ast.Childs) < 2 ||
		ast.Childs[len(ast.Childs)-1].Section != designation.D_DocumentDesignation {
		return false, ""
	}

	// remove page count number since it does not make sense to store a document with the designation of one of its pages
	ast.Childs[len(ast.Childs)-1].RemoveFirstChildBySection(designation.SEP_Seperator)
	ast.Childs[len(ast.Childs)-1].RemoveFirstChildBySection(designation.D_DocumentPageCountNumber)
	rdsppDesignation = ast.ToString()
	return true, rdsppDesignation
}

func (r *ftpRepository) Exists(rdsppDesignation string) interface{} {
	isDocumentDesignation, designationWithoutPage := verifyDesignation(rdsppDesignation)
	if !isDocumentDesignation {
		return false
	}
	rdsppDesignation = designationWithoutPage

	path := config.Ftp.RootDir + r.EncodeFilename(rdsppDesignation)
	files, _ := ftpClient.ReadDir(path)
	if len(files) == 1 {
		buf := bytes.NewBuffer(nil)
		ftpClient.Retrieve(path+".json", buf)
		rdsppFile := &RdsppDocumentFile{}
		json.Unmarshal(buf.Bytes(), rdsppFile)
		return rdsppFile
	}
	return false
}

func (r *ftpRepository) Upload(file *RdsppDocumentFile, override bool) error {
	isDocumentDesignation, designationWithoutPage := verifyDesignation(file.Designation)
	if !isDocumentDesignation {
		return ERROR_NOT_A_DOCUMENT
	}
	file.Designation = designationWithoutPage

	// ensure the directory exists
	dir, _ := ftpClient.ReadDir(config.Ftp.RootDir)
	if len(dir) == 0 {
		ftpClient.Mkdir(config.Ftp.RootDir)
	}

	// check if file already exists
	path := config.Ftp.RootDir + r.EncodeFilename(file.Designation)
	files, _ := ftpClient.ReadDir(path)
	if len(files) != 0 {
		dbRepo, _ := NewDbRepository()
		if dbRepo.Exists(file.Designation) && !override {
			return ERROR_UPLOAD_OVERRIDE_REQUIRED
		}
		// else:
		// - either the designation does not exists an thus the files may be deleted
		// - or override is true and thus the old files may be deleted before saving the old ones
		if len(files) == 1 {
			ftpClient.Delete(path)
			jsons, _ := ftpClient.ReadDir(path + ".json")
			if len(jsons) == 1 {
				ftpClient.Delete(path + ".json")
			}
		} else {
			return ERROR_UPLOAD_WEIRD
		}
	}

	// upload json file with meta information
	meta, _ := json.MarshalIndent(file, "", "  ")
	meta = append(meta, byte(10)) // append \n
	ftpClient.Store(path+".json", bytes.NewReader(meta))

	// actual upload
	ftpClient.Store(path, bytes.NewReader(file.Bytes))
	// ignore error from Store(...) since the upload somehow works despite the error

	return nil
}

func (r *ftpRepository) Retrieve(rdsppDesignation string) (out chan *RdsppDocumentFile, err error) {
	out = make(chan *RdsppDocumentFile, 1)
	err = nil

	isDocumentDesignation, designationWithoutPage := verifyDesignation(rdsppDesignation)
	if !isDocumentDesignation {
		err = ERROR_NOT_A_DOCUMENT
		return
	}
	rdsppDesignation = designationWithoutPage

	// retrieve meta information (.json file)
	filename := config.Ftp.RootDir + r.EncodeFilename(rdsppDesignation)
	rdsppFile := &RdsppDocumentFile{}
	buf1 := bytes.NewBuffer(nil)
	ftpClient.Retrieve(filename+".json", buf1)
	json.Unmarshal(buf1.Bytes(), rdsppFile)

	// retrieve actual document
	if !rdsppFile.IsMetaEmpty() {
		buf2 := bytes.NewBuffer(nil)
		ftpClient.Retrieve(filename, buf2)
		rdsppFile.Bytes = buf2.Bytes()
		out <- rdsppFile
	} else {
		err = ERROR_NO_META
	}
	return
}
