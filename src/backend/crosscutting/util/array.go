package util

func StringArrayContains(array []string, needle string) bool {
	for _, x := range array {
		if x == needle {
			return true
		}
	}
	return false
}

func IntArrayContains(array []int, needle int) bool {
	for _, x := range array {
		if x == needle {
			return true
		}
	}
	return false
}

func FloatArrayContains(array []float64, needle float64) bool {
	for _, x := range array {
		if x == needle {
			return true
		}
	}
	return false
}
