package config

import (
	"github.com/fatih/color"
	"github.com/spf13/viper"
)

// config struct for parsing config.yml
type config struct {
	Logging    LoggingConfig
	Server     ServerConfig
	MongoDb    MongoDbConfig
	Ftp        FtpConfig
	Ressources RessourcesConfig
	Api        ApiConfig
}

// all (statically) available configurations
var (
	IsDebug    bool
	Logging    LoggingConfig
	MongoDb    MongoDbConfig
	Server     ServerConfig
	Ftp        FtpConfig
	Ressources RessourcesConfig
	Api        ApiConfig
)

// Init is responsible for parsing a config.yml file in the root directory
// and making it's content available to the whole application
func Init(debug bool) {
	color.Cyan("[INFO]  processing config.yml")
	viper.SetConfigFile("config.yml")
	viper.AddConfigPath(".")
	var c config
	err := viper.ReadInConfig()
	if err != nil {
		color.Red("[ERROR] reading in config.yml failed:\n%s", err.Error())
	}
	err = viper.Unmarshal(&c)
	if err != nil {
		color.Red("[ERROR] unmarshalling config failed:\n%s", err.Error())
	}

	IsDebug = debug
	Logging = c.Logging
	Server = c.Server
	MongoDb = c.MongoDb
	Ftp = c.Ftp
	Ressources = c.Ressources
	Api = c.Api
}
