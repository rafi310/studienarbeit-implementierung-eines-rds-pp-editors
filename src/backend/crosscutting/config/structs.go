package config

type LoggingConfig struct {
	Filename string `yaml:"filename"`
}

type ServerConfig struct {
	Host    string `yaml:"host"`
	Port    int    `yaml:"port"`
	Headers struct {
		AllowHeaders     []string `yaml:"allowHeaders"`
		AllowOrigins     []string `yaml:"allowOrigins"`
		AllowMethods     []string `yaml:"allowMethods"`
		ExposeHeaders    []string `yaml:"exposeHeaders"`
		AllowCredentials bool     `yaml:"allowCredentials"`
		MaxAge           int      `yaml:"maxAge"`
	} `yaml:"headers"`
}

type MongoDbConfig struct {
	Host         string `yaml:"host"`
	Port         int    `yaml:"port"`
	DatabaseName string `yaml:"databaseName"`
}

type FtpConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	RootDir  string `yaml:"rootDir"`
}

type RessourcesConfig struct {
	Denomination struct {
		ConjointDenominaionFile  string `yaml:"conjointDenominaionFile"`
		DccFile                  string `yaml:"dccFile"`
		DesignationSectionsFile  string `yaml:"designationSectionsFile"`
		MainSystemsFile          string `yaml:"mainSystemsFile"`
		SystemsSubsystemsFile    string `yaml:"systemsSubsystemsFile"`
		BasicFunctionGeneralFile string `yaml:"basicFunctionGeneralFile"`
		BasicFunctionProductFile string `yaml:"basicFunctionProductFile"`
		SignalsFile              string `yaml:"signalsFile"`
		SpecialCasesFile         string `yaml:"specialCasesFile"`
		HintsFile                string `yaml:"hintsFile"`
		SelectionsFile           string `yaml:"selectionsFile"`
	} `yaml:"denomination"`
}

type ApiConfig struct {
	MapQuest struct {
		ApiKey    string `yaml:"apiKey"`
		ApiSecret string `yaml:"apiSecret"`
	} `yaml:"mapQuest"`
	Geonames struct {
		Username string `yaml:"username"`
	} `yaml:"geonames"`
}
