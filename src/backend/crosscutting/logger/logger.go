package logger

import (
	"log"
	"os"
	"time"

	"github.com/fatih/color"
	"rdspp.de/src/crosscutting/config"
)

var (
	printToFile    = true
	printToConsole = false
)

func Init() {
	logFile, err := os.OpenFile(config.Logging.Filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	// no 'defer logFile.Close()' because logging doesn't stop while application is running
	if err != nil {
		color.Red("[ERROR] logger: %s", err.Error())
	}
	log.SetOutput(logFile)
	log.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))
}

// SetLogOutputEnabled set where to print all following log messages:
// to a file, to the console, both or neither.
// default: file only
func SetLogOutputEnabled(file bool, console bool) {
	printToFile = file
	printToConsole = console
}

// Info logs a message with the severity [INFO]
func Info(msg string) {
	commonLog(msg, 0)
}

// Info logs a message with the severity [WARN]
func Warning(msg string) {
	commonLog(msg, 1)
}

// Info logs a message with the severity [ERROR]
func Error(msg string) {
	commonLog(msg, 2)
}

// Info logs a message with the severity [INFO]
// CONSOLE LOG ONLY
func InfoConsole(msg string) {
	f, c := printToFile, printToConsole
	SetLogOutputEnabled(false, true)
	commonLog(msg, 0)
	SetLogOutputEnabled(f, c)
}

// Info logs a message with the severity [WARN]
// CONSOLE LOG ONLY
func WarningConsole(msg string) {
	f, c := printToFile, printToConsole
	SetLogOutputEnabled(false, true)
	commonLog(msg, 1)
	SetLogOutputEnabled(f, c)
}

// Info logs a message with the severity [ERROR]
// CONSOLE LOG ONLY
func ErrorConsole(msg string) {
	f, c := printToFile, printToConsole
	SetLogOutputEnabled(false, true)
	commonLog(msg, 2)
	SetLogOutputEnabled(f, c)
}

// Info logs a message with the severity [INFO]
// FILE LOG ONLY
func InfoFile(msg string) {
	f, c := printToFile, printToConsole
	SetLogOutputEnabled(true, false)
	commonLog(msg, 0)
	SetLogOutputEnabled(f, c)
}

// Info logs a message with the severity [WARN]
// FILE LOG ONLY
func WarningFile(msg string) {
	f, c := printToFile, printToConsole
	SetLogOutputEnabled(true, false)
	commonLog(msg, 1)
	SetLogOutputEnabled(f, c)
}

// Info logs a message with the severity [ERROR]
// FILE LOG ONLY
func ErrorFile(msg string) {
	f, c := printToFile, printToConsole
	SetLogOutputEnabled(true, false)
	commonLog(msg, 2)
	SetLogOutputEnabled(f, c)
}

// internal function used by all public logging funtions
func commonLog(msg string, severity int) {
	// set prefix: [SEVERITY] + timestamp
	var prefix string
	switch severity {
	case 0:
		prefix = "[INFO]  "
	case 1:
		prefix = "[WARN]  "
	case 2:
		prefix = "[ERROR] "
	}
	prefix += time.Now().Format("2006-01-02 15:04:05") + " | "

	// log to file
	log.SetPrefix(prefix)
	if printToFile {
		log.Print(msg)
	}

	// log to console
	if printToConsole {
		switch severity {
		case 0:
			color.Cyan(prefix + msg)
		case 1:
			color.Yellow(prefix + msg)
		case 2:
			color.Red(prefix + msg)
		}
	}
}
