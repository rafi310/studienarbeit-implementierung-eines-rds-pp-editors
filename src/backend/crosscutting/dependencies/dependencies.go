package dependencies

import (
	"github.com/jasonwinn/geocoder"
	"rdspp.de/src/crosscutting/config"
)

func SetAllApiKeys() {
	geocoder.SetAPIKey(config.Api.MapQuest.ApiKey)
}
