# Swagger Editor

The static files are downloaded from the `dist/` directory of [https://github.com/swagger-api/swagger-editor](https://github.com/swagger-api/swagger-editor). A Git-Submodules is not necessary since we are only interessed in the dist-files and not in using part of the source code.

**LAST UPDATE: 2020-04-02**

The files were neither modified nor extended.
