# Swagger UI

The static files are downloaded from the `dist/` directory of [https://github.com/swagger-api/swagger-ui](https://github.com/swagger-api/swagger-ui). A Git-Submodules is not necessary since we are only interessed in the dist-files and not in using part of the source code.

**LAST UPDATE: 2020-04-02**

## Modifications

```html
<!-- index.html -->

<!-- ... -->
<head>
    <!-- ... -->
    <link rel="stylesheet" type="text/css" href="./custom.css" > <!-- <- add custom style here -->
    <!-- ... -->
</head>
<!-- ... -->

<!-- ... -->
<script>
    // ...
    const ui = SwaggerUIBundle({
        url: "./api-spec.yaml", // <- edit path to OpenAPI spec here
        // ...
    })
    // ...
</script>
<!-- ... -->
```

## Additionals Files

* `custom.css`  
  customized styling
* `api-spec.yaml`  
  copied from `<repo>/src/backend/api/api-spec.yaml`
