package assistant

import "rdspp.de/src/logic/designation/denomination"

func siteOfInstallationSelection() []DataCode {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == siteOfInstallationPlace || selection.Code == siteOfInstallationGeolocation {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}
func siteOfInstallationSubsystemSelection() []DataCode {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == siteOfInstallationSubsystem || selection.Code == siteOfInstallationNoSubsystem {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}
