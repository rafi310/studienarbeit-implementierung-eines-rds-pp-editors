package assistant

import (
	"rdspp.de/src/logic/designation/denomination"
)

func signalMainClass(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, mainSystem := range denomination.Signals.MainClass {
		communication.ProposalList = append(communication.ProposalList, mainSystem)
	}
}
func signalSubclass(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, subclass := range denomination.Signals.Subclass {
		communication.ProposalList = append(communication.ProposalList, subclass)
	}
}
func signalInformation(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, classes := range denomination.Signals.Class {
		if classes.MainClass == communication.ActualDesignation[len(communication.ActualDesignation)-2:] {
			for _, concretClasses := range classes.ConcreteClass {
				communication.ProposalList = append(communication.ProposalList, concretClasses)
			}
		}
	}
}
func signalDuration(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, duration := range denomination.Signals.Duration {
		communication.ProposalList = append(communication.ProposalList, duration)
	}
}
func signalType(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, types := range denomination.Signals.Types {
		communication.ProposalList = append(communication.ProposalList, types)
	}
}
