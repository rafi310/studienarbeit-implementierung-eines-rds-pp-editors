package assistant

import (
	"rdspp.de/src/logic/designation/denomination"
)

func documentTechnicalSector(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, firstChar := range denomination.Dcc.TechnicalSector {
		communication.ProposalList = append(communication.ProposalList, firstChar)
	}
}
func documentMainClass(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, secondChar := range denomination.Dcc.DocumentClass {

		communication.ProposalList = append(communication.ProposalList, DataCode{
			Code:         secondChar.Code,
			Denomination: secondChar.Denomination,
		})
	}
}
func documentSubClass(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, secondChar := range denomination.Dcc.DocumentClass {
		//get the right second char
		if secondChar.Code == communication.Data[0] {
			//add all subclasses from this second Char
			for _, subclass := range secondChar.Subclasses {
				communication.ProposalList = append(communication.ProposalList, DataCode{
					Code:         subclass.Code,
					Denomination: subclass.Denomination,
				})
			}
		}
	}
}
func documentNumbers(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, secondChar := range denomination.Dcc.DocumentClass {
		//get the right second char
		if secondChar.Code == string(communication.ActualDesignation[len(communication.ActualDesignation)-2]) {
			for _, subclass := range secondChar.Subclasses {
				//gets the right subclass
				if subclass.Code == string(communication.ActualDesignation[len(communication.ActualDesignation)-1]) {
					//add all numbers from the selected subclass
					for _, number := range subclass.Numbers {
						communication.ProposalList = append(communication.ProposalList, DataCode{
							Code:         number.Code,
							Denomination: number.Denomination,
						})
					}
				}
			}
		}
	}
}
func documentPageHelp(communication *CommunicationData){
	communication.ProposalList = nil
	for _, dcc := range denomination.Dcc.PageNumberAlpha{
		communication.ProposalList = append(communication.ProposalList, DataCode{
			Code: dcc.Code,
			Denomination: dcc.Denomination,
		})

	}
}
