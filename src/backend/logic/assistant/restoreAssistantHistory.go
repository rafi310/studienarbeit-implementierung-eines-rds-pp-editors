package assistant

import (
	"strings"

	"rdspp.de/src/crosscutting/logger"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser"
)

//RestoreAssistantData walks through the assistent with a given designation
func RestoreAssistantData(designation string) CommunicationData {
	designation = strings.Join(strings.Fields(designation), "")
	pars := parser.NewRdsppParser()
	pars.ParseDesignation(designation)
	assistantData := new(CommunicationData)
	assistantData = InputLogic(assistantData, false)
	for len(designation) > len(assistantData.ActualDesignation) {
		getInputDataByTypes(assistantData, pars.Tree)
		assistantData.NextStep = setNextStpeAtRightPosition(assistantData.NextStep)
		assistantData = InputLogic(assistantData, false)
	}
	return *assistantData
}

func setNextStpeAtRightPosition(state designation.DesignationSection) designation.DesignationSection {
	switch state {
	case designation.F_MainSystem:
		return designation.F_MainSystemA1
	case designation.F_SystemSubsystem:
		return designation.F_SystemA1
		// case designation.T_TerminalDesignation:
		// 	return designation.PRE_Prefix
	}
	return state
}

func getInputDataByTypes(assistant *CommunicationData, tree *designation.DesignationNode) {
	switch assistant.DataInputType {
	case Geolocation:
		input := ""
		getDataFromTree(tree, getActualStep(assistant.History, assistant.NextStep), &input, assistant.DataInputType)
		assistant.Data[0] = input
	case DataTypeList:
		input := ""
		getDataFromTree(tree, getActualStep(assistant.History, assistant.NextStep), &input, assistant.DataInputType)
		input = strings.Replace(input, "-", "", -1)
		assistant.Data[0] = input
	case InputNumber:
		input := ""
		getDataFromTree(tree, getActualStep(assistant.History, assistant.NextStep), &input, assistant.DataInputType)
		assistant.Data[0] = input
	case InputString:
		input := ""
		getDataFromTree(tree, getActualStep(assistant.History, assistant.NextStep), &input, assistant.DataInputType)
		assistant.Data[0] = input
	case InputStringNumber:
		input := ""
		getDataFromTree(tree, getActualStep(assistant.History, assistant.NextStep), &input, assistant.DataInputType)
		assistant.Data[0] = input
	case InputStringNumberList:
		input := ""
		getDataFromTree(tree, getActualStep(assistant.History, assistant.NextStep), &input, assistant.DataInputType)
		assistant.Data[0] = input
	case Prefix:
		designationLength := len(assistant.ActualDesignation)
		prefix, _ := getPrefixDataFromTree(*tree, designationLength)
		if assistant.Data == nil {
			assistant.Data = append(assistant.Data, prefix)
		} else {
			assistant.Data[0] = prefix
		}
	case Selection:
		proposal := assistant.ProposalList[0].Code
		nextStep := designation.PRE_Prefix
		_, state := getPrefixDataFromTree(*tree, len(assistant.ActualDesignation)-1)
		findSelectionInput(*tree, getActualStep(assistant.History[:len(assistant.History)-1], assistant.NextStep), state, proposal, &nextStep)

		assistant.NextStep = nextStep
	}
}

func getActualStep(history []CommunicationData, nextStep designation.DesignationSection) designation.DesignationSection {
	// if len(history) > 1{
	// 	if history[len(history)-1].NextStep == history[len(history)-2].NextStep{
	// 		return nextStep
	// 	}
	// }
	if history[len(history)-1].NextStep == designation.PRE_Prefix && history[len(history)-1].DataInputType == Prefix {
		switch nextStep {
		case designation.F_MainSystemCounter:
			return designation.F_MainSystemA1
		case designation.D_DocumentDccMainClass:
			return designation.D_DocumentDccTechnicalSector
		}
		return nextStep - 1
	}
	return history[len(history)-1].NextStep
}
func getSelectionStep(history []CommunicationData) designation.DesignationSection {
	if len(history) < 2 {
		return history[len(history)-1].NextStep
	}
	return history[len(history)-2].NextStep
}

//search recursive the state in a designationtree
func getDataFromTree(tree *designation.DesignationNode, state designation.DesignationSection, rdsppdesignation *string, inputType DataInputTypes) {
	if state != designation.CD_TownOrName {
		if tree.Section == state {
			*rdsppdesignation = tree.Designation
			return
		}
	} else {
		//i have two input arguments in this so i have to add the tree desigantion twice
		if tree.Section == state || tree.Section == state+1 {
			*rdsppdesignation = *rdsppdesignation + tree.Designation
		}
	}
	if tree.Childs != nil {
		for _, child := range tree.Childs {
			getDataFromTree(child, state, rdsppdesignation, inputType)
		}
	}
}

//returns the DesignationSection from the actual Designation (by the charLength of it)
func getPrefixDataFromTree(tree designation.DesignationNode, designationLength int) (string, designation.DesignationSection) {
	for i, designationType := range tree.Childs {
		if designationLength <= 0 || i == len(tree.Childs)-1 {
			return sectionToString(designationType.Section), designationType.Section
		}
		designationLength = designationLength - len(designationType.Designation)
	}
	return "0", 0
}

func findNextNode(tree designation.DesignationNode, state designation.DesignationSection, prefixState designation.DesignationSection, nextStep *designation.DesignationSection) {
	for _, child := range tree.Childs {
		if child.Section == prefixState || prefixState == 0 {
			flag := false
			rekursiveFindNextNode(*child, state, &flag, prefixState, nextStep)
		}
	}
}
func rekursiveFindNextNode(tree designation.DesignationNode, state designation.DesignationSection, actualStateFound *bool, prefixState designation.DesignationSection, nextStep *designation.DesignationSection) {
	if tree.Section == state || state == 0 {
		*actualStateFound = true
		return
	}
	if tree.Childs != nil {
		for _, child := range tree.Childs {
			if state == designation.U_Undefined {

				if *actualStateFound == true && child.Section != prefixState && child.Section != designation.PRE_Prefix {
					*nextStep = child.Section
					*actualStateFound = false
					return
				}
			} else if *actualStateFound == true && child.Section != prefixState && child.Section != designation.SEP_Seperator {
				*nextStep = child.Section
				*actualStateFound = false
				return
			}
			rekursiveFindNextNode(*child, state, actualStateFound, prefixState, nextStep)
		}
	}
}

func findSelectionInput(tree designation.DesignationNode, state designation.DesignationSection, prefixState designation.DesignationSection, switchProposal string, nextStep *designation.DesignationSection) {
	switch switchProposal {
	case "conjointDesignationGeo":
		*nextStep = designation.CD_GeographicLocation
	case "mainsystem":
		//check just one designation
		findNextNode(tree, state, prefixState, nextStep)
	case "productContinue":
		//check over designationtypes
		flag := false
		rekursiveFindNextNode(tree, state, &flag, prefixState, nextStep)
	case "terminalAssembly":
		//check just one designation
		findNextNode(tree, state, prefixState, nextStep)
	case "siteOfInstallationPlace":
		//check just one designation
		//designationtyp continues save after this selection
		findNextNode(tree, state, prefixState, nextStep)
	case "siteOfInstallationSubsystem":
		findNextNode(tree, state, prefixState, nextStep)

	case "pointOfInstallationBasicFunction":
		//check over designationtypes
		flag := false
		rekursiveFindNextNode(tree, state, &flag, prefixState, nextStep)
	case "pointOfInstallationPlace":
		//check over designationtypes
		flag := false
		rekursiveFindNextNode(tree, state, &flag, prefixState, nextStep)
	}
}
func sectionToString(section designation.DesignationSection) string {
	switch section {
	case designation.CD_ConjointDesignation:
		return "#"
	case designation.F_FunctionDesignation:
		return "="
	case designation.P_ProductClassDesignation:
		return "-"
	case designation.OE_OperatingEquipmentDesignation:
		return "=-"
	case designation.PI_PointOfInstallationDesignation:
		return "+"
	case designation.SI_SiteOfInstallationDesignation:
		return "++"
	case designation.T_TerminalDesignation:
		return ":"
	case designation.S_SignalDesignation:
		return ";"
	case designation.D_DocumentDesignation:
		return "&"
	case 0:
		logger.Error("wrong designationSection detected")
	}
	return "x"
}
