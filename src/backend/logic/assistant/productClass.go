package assistant

import "rdspp.de/src/logic/designation/denomination"

func productSelection() []DataCode {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == productContinue || selection.Code == productEnd {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}

func productLogic(communication *CommunicationData) {
	communication.ProposalList = nil
	if denomination.BasicFunctions.BasicFunctionsProducts.MainSystems != nil {
		for _, mainSystem := range denomination.BasicFunctions.BasicFunctionsProducts.MainSystems {
			for _, subsystem := range mainSystem.Subsystems {
				for _, basicFunction := range subsystem.BasicFunctions {
					for _, product := range basicFunction.Products {
						communication.ProposalList = append(communication.ProposalList, DataCode{
							Code:         product.Code,
							Denomination: product.Denomination,
						})
					}
				}
			}
		}
	}
}
