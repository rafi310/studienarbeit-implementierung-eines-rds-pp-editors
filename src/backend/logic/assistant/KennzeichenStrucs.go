package assistant

import (
	"rdspp.de/src/logic/designation"
)

//DataCode struct
type DataCode struct {
	Code         string `yaml:"code" json:"code"`
	Denomination struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"denomination" json:"denomination"`
}
type dataCodeDenomination struct {
	De string `yaml:"de" json:"de"`
	En string `yaml:"en" json:"en"`
}

//CommunicationData holds all communication between frontend and backend
type CommunicationData struct {
	History               []CommunicationData            `json:"history"` //denomination history
	PossibleFinalState    bool                           `json:"possibleFinalState"`
	NextStep              designation.DesignationSection `json:"nextStep"`        //actual state in the statemachine
	DataInputType         DataInputTypes                 `json:"dataInputType"`   //from backend which inputView should be presented
	DataInputLength       []int                          `json:"dataInputLength"` //from backend if inputView is string/numbers is the length set here
	Data                  []string                       `json:"data"`            //from frontend
	ActualDesignation     string                         `json:"actualDesignation"`
	ActualDesignationType designation.DesignationSection `json:"acutalState"`
	ProposalList          []DataCode                     `json:"proposalList"` //from backend
}

//DataInputTypes to enumerate the constants
type DataInputTypes int

const (
	undefined DataInputTypes = iota
	//Geolocation with two double values
	Geolocation
	//DataTypeList gets one Value from a selected list
	DataTypeList
	//InputString a
	InputString
	//InputNumber a
	InputNumber
	//InputStringNumber a
	InputStringNumber
	//Selection a
	Selection
	//Prefix a
	Prefix
	//InputStringNumberList input with string and number and unselectable proposal list
	InputStringNumberList
	// FinalState Last Assistant step
	FinalState
)

//Selections const strings which are selected
type Selections string

const (
	undefinedSelection                Selections = "undef"
	conjointDesignationGeo                       = "conjointDesignationGeo"
	conjointDesignationProject                   = "conjointDesignationProject"
	functionMainSystem                           = "mainsystem"
	functionSubSystem                            = "subsystem"
	productContinue                              = "productContinue"
	productEnd                                   = "productEnd"
	terminalAssembly                             = "terminalAssembly"
	terminalData                                 = "terminalData"
	siteOfInstallationSubsystem                  = "siteOfInstallationSubsystem"
	siteOfInstallationNoSubsystem                = "siteOfInstallationNoSubsystem"
	siteOfInstallationPlace                      = "siteOfInstallationPlace"
	siteOfInstallationGeolocation                = "siteOfInstallationGeolocation"
	pointOfInstallationBasicFunction             = "pointOfInstallationBasicFunction"
	pointOfInstallationGoToSelection2            = "pointOfInstallationGoToSelection2"
	pointOfInstallationPlace                     = "pointOfInstallationPlace"
	pointOfInstallationNoInput                   = "pointOfInstallationNoInput"
)
