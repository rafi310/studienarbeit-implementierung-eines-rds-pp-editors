package assistant

import "rdspp.de/src/logic/designation/denomination"

func terminalSelection() []DataCode {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == terminalAssembly || selection.Code == terminalData {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}
