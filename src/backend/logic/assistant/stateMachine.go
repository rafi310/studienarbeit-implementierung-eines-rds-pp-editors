package assistant

import "rdspp.de/src/logic/designation"

func handleNextStep(communication *CommunicationData, justPlants bool) {
	switch communication.ActualDesignationType {
	case designation.CD_ConjointDesignation:
		switch communication.NextStep {
		case designation.CD_ExistingDesignation:
			communication.DataInputType = DataTypeList
			communication.NextStep = designation.PRE_Prefix
			getAllPlantDesignations(communication)
			communication.PossibleFinalState = false
		case designation.CD_GeographicLocation:
			communication.DataInputType = Geolocation
			communication.NextStep = designation.CD_Country
			communication.PossibleFinalState = false
		case designation.CD_Country:
			if communication.ActualDesignation[len(communication.ActualDesignation)-1] != '#' {
				communication.ActualDesignation = communication.ActualDesignation + "."
			}
			communication.DataInputType = DataTypeList
			conjointDesignationCountry(communication)
			communication.NextStep = designation.CD_Region
			communication.PossibleFinalState = false
		case designation.CD_Region:
			communication.DataInputType = DataTypeList
			conjointDesignationRegion(communication)
			communication.NextStep = designation.CD_TownOrName
			communication.PossibleFinalState = false
		case designation.CD_TownOrName:
			if communication.ActualDesignation[len(communication.ActualDesignation)-6] != '.' {
				if communication.ActualDesignation[len(communication.ActualDesignation)-5] == '.' {
					position := len(communication.ActualDesignation) - 2
					communication.ActualDesignation = communication.ActualDesignation[0:position] + "_" + communication.ActualDesignation[position:len(communication.ActualDesignation)]
				}
				if communication.ActualDesignation[len(communication.ActualDesignation)-4] == '.' {
					position := len(communication.ActualDesignation) - 1
					communication.ActualDesignation = communication.ActualDesignation[0:position] + "__" + communication.ActualDesignation[position:len(communication.ActualDesignation)]
				}

			}
			communication.ActualDesignation = communication.ActualDesignation + "."
			communication.DataInputType = InputStringNumber
			communication.DataInputLength = []int{3, 2}
			communication.NextStep = designation.CD_PlantType
			communication.PossibleFinalState = false
		case designation.CD_PlantType:
			communication.DataInputType = DataTypeList
			conjointDesignationPlantType(communication)
			communication.NextStep = designation.CD_FacilityType
		case designation.CD_FacilityType:
			communication.PossibleFinalState = false
			communication.DataInputType = DataTypeList
			conjointDesignationFacilityType(communication)
			communication.NextStep = designation.PRE_Prefix
		case designation.PRE_Prefix:
			if !justPlants {
				communication.DataInputType = Prefix
				communication.ProposalList = getNextStates(communication.ActualDesignationType)
			} else {
				communication.DataInputType = FinalState
				communication.ProposalList = nil
			}
			communication.PossibleFinalState = true
		}
	case designation.F_FunctionDesignation:
		switch communication.NextStep {
		case designation.F_MainSystemA1:
			communication.DataInputType = DataTypeList
			functionLogicMainSystem(communication)
			communication.NextStep = designation.F_MainSystemCounter
			communication.PossibleFinalState = false
		case designation.F_MainSystemCounter:
			communication.DataInputType = InputNumber
			communication.DataInputLength = []int{3}
			communication.NextStep = designation.F_SystemA1
			communication.PossibleFinalState = false
		case designation.F_SystemA1:
			communication.DataInputType = DataTypeList
			functionLogicSystemA1(communication)
			communication.NextStep = designation.F_SystemA2
			communication.PossibleFinalState = false
		case designation.F_SystemA2:
			communication.DataInputType = DataTypeList
			functionLogicSystemA2(communication)
			communication.NextStep = designation.F_SystemA3
			communication.PossibleFinalState = false
		case designation.F_SystemA3:
			communication.DataInputType = DataTypeList
			functionLogicSystemA3(communication)
			communication.NextStep = designation.F_Subsystem
			communication.PossibleFinalState = false
		case designation.F_Subsystem:
			communication.DataInputType = DataTypeList
			functionLogicSubsystem(communication)
			communication.NextStep = designation.F_BasicFunction
			communication.PossibleFinalState = false
		case designation.F_BasicFunction:
			communication.DataInputType = DataTypeList
			functionLogicBasicFunction(communication)
			communication.NextStep = designation.PRE_Prefix
			communication.PossibleFinalState = false
		case designation.PRE_Prefix:
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}
	case designation.OE_OperatingEquipmentDesignation:
		switch communication.NextStep {
		case designation.F_MainSystemA1:
			communication.DataInputType = DataTypeList
			functionLogicMainSystem(communication)
			communication.NextStep = designation.F_MainSystemCounter
			communication.PossibleFinalState = false
		case designation.F_MainSystemCounter:
			communication.DataInputType = InputNumber
			communication.DataInputLength = []int{3}
			communication.NextStep = designation.F_SystemA1
			communication.PossibleFinalState = false
		case designation.F_SystemA1:
			communication.DataInputType = DataTypeList
			functionLogicSystemA1(communication)
			communication.NextStep = designation.F_SystemA2
			communication.PossibleFinalState = false
		case designation.F_SystemA2:
			communication.DataInputType = DataTypeList
			functionLogicSystemA2(communication)
			communication.NextStep = designation.F_SystemA3
			communication.PossibleFinalState = false
		case designation.F_SystemA3:
			communication.DataInputType = DataTypeList
			functionLogicSystemA3(communication)
			communication.NextStep = designation.F_Subsystem
			communication.PossibleFinalState = false
		case designation.F_Subsystem:
			communication.DataInputType = DataTypeList
			functionLogicSubsystem(communication)
			communication.NextStep = designation.F_BasicFunction
			communication.PossibleFinalState = false
		case designation.F_BasicFunction:
			communication.DataInputType = DataTypeList
			functionLogicBasicFunction(communication)
			communication.NextStep = designation.P_ProductClass
			communication.PossibleFinalState = false
		case designation.P_ProductClass:
			communication.ActualDesignation = communication.ActualDesignation + "-"
			communication.DataInputType = DataTypeList
			operatingEquipmentProduct(communication)
			communication.NextStep = designation.P_ProductClassConcrete
			communication.PossibleFinalState = false
		case designation.P_ProductClassConcrete:
			communication.DataInputType = Selection
			communication.ProposalList = productSelection()
			communication.PossibleFinalState = false
		case designation.PRE_Prefix:
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}
	case designation.P_ProductClassDesignation:
		switch communication.NextStep {
		case designation.P_ProductClass:
			communication.DataInputType = DataTypeList
			functionLogicBasicFunction(communication)
			communication.NextStep = designation.P_ProductClassConcrete
			communication.PossibleFinalState = false
		case designation.P_ProductClassConcrete:
			communication.DataInputType = Selection
			communication.ProposalList = productSelection()
			communication.PossibleFinalState = false
		case designation.PRE_Prefix:
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}

	case designation.PI_PointOfInstallationDesignation:
		switch communication.NextStep {
		case designation.F_MainSystemA1:
			communication.DataInputType = DataTypeList
			functionLogicMainSystem(communication)
			communication.NextStep = designation.F_MainSystemCounter
			communication.PossibleFinalState = false
		case designation.F_MainSystemCounter:
			communication.DataInputType = InputNumber
			communication.DataInputLength = []int{3}
			communication.NextStep = designation.F_SystemA1
			communication.PossibleFinalState = false
		case designation.F_SystemA1:
			communication.DataInputType = DataTypeList
			functionLogicSystemA1(communication)
			communication.NextStep = designation.F_SystemA2
			communication.PossibleFinalState = false
		case designation.F_SystemA2:
			communication.DataInputType = DataTypeList
			functionLogicSystemA2(communication)
			communication.NextStep = designation.F_SystemA3
			communication.PossibleFinalState = false
		case designation.F_SystemA3:
			communication.DataInputType = DataTypeList
			functionLogicSystemA3(communication)
			communication.NextStep = designation.F_Subsystem
			communication.PossibleFinalState = false
		case designation.F_Subsystem:
			communication.DataInputType = DataTypeList
			functionLogicSubsystem(communication)
			communication.NextStep = designation.PI_PointOfInstallationSelection1
			communication.PossibleFinalState = true
		case designation.PI_PointOfInstallationSelection1:
			communication.ProposalList = pointOfInstallationSelection1()
			communication.DataInputType = Selection
			communication.PossibleFinalState = false
		case designation.F_BasicFunction:
			communication.DataInputType = DataTypeList
			functionLogicBasicFunction(communication)
			communication.NextStep = designation.PI_PointOfInstallationSelection2
			communication.PossibleFinalState = true
		case designation.PI_PointOfInstallationSelection2:
			communication.ProposalList = pointOfInstallationSelection2()
			communication.DataInputType = Selection
			communication.PossibleFinalState = false
		case designation.PI_PointOfInstallation:
			communication.ActualDesignation = communication.ActualDesignation + "."
			communication.DataInputType = InputStringNumber
			communication.DataInputLength = []int{2, 3}
			communication.NextStep = designation.PRE_Prefix
			communication.PossibleFinalState = true
		case designation.PRE_Prefix:
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}
	case designation.SI_SiteOfInstallationDesignation:
		switch communication.NextStep {
		case designation.F_MainSystemA1:
			communication.DataInputType = DataTypeList
			functionLogicMainSystem(communication)
			communication.NextStep = designation.F_MainSystemCounter
			communication.PossibleFinalState = false
		case designation.F_MainSystemCounter:
			communication.DataInputType = InputNumber
			communication.DataInputLength = []int{3}
			communication.NextStep = designation.SI_SiteOfInstallationSubsystemSelection
			communication.PossibleFinalState = false
		case designation.SI_SiteOfInstallationSubsystemSelection:
			communication.DataInputType = Selection
			communication.ProposalList = siteOfInstallationSubsystemSelection()
			communication.PossibleFinalState = false
		case designation.F_SystemA1:
			communication.DataInputType = DataTypeList
			functionLogicSystemA1(communication)
			communication.NextStep = designation.F_SystemA2
			communication.PossibleFinalState = false
		case designation.F_SystemA2:
			communication.DataInputType = DataTypeList
			functionLogicSystemA2(communication)
			communication.NextStep = designation.F_SystemA3
			communication.PossibleFinalState = false
		case designation.F_SystemA3:
			communication.DataInputType = DataTypeList
			functionLogicSystemA3(communication)
			communication.NextStep = designation.F_Subsystem
			communication.PossibleFinalState = false
		case designation.F_Subsystem:
			communication.DataInputType = DataTypeList
			functionLogicSubsystem(communication)
			communication.NextStep = designation.SI_SiteOfInstallationSelection
			communication.PossibleFinalState = false
		case designation.SI_SiteOfInstallationSelection:
			communication.DataInputType = Selection
			communication.ProposalList = siteOfInstallationSelection()
			communication.PossibleFinalState = false
		case designation.SI_SiteOfInstallation:
			communication.ActualDesignation = communication.ActualDesignation + "."
			communication.DataInputType = InputStringNumber
			communication.DataInputLength = []int{4, 4}
			communication.NextStep = designation.PRE_Prefix
			communication.PossibleFinalState = false
		case designation.SI_SiteOfInstallationGeolocation:
			communication.ActualDesignation = communication.ActualDesignation + "."
			communication.DataInputType = Geolocation
			communication.NextStep = designation.PRE_Prefix
			communication.PossibleFinalState = false
		case designation.PRE_Prefix:
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}
	case designation.D_DocumentDesignation:
		switch communication.NextStep {
		case designation.D_DocumentDccTechnicalSector:
			communication.DataInputType = DataTypeList
			documentTechnicalSector(communication)
			communication.NextStep = designation.D_DocumentDccMainClass
			communication.PossibleFinalState = false
		case designation.D_DocumentDccMainClass:
			communication.DataInputType = DataTypeList
			documentMainClass(communication)
			communication.NextStep = designation.D_DocumentDccSubclass
			communication.PossibleFinalState = false
		case designation.D_DocumentDccSubclass:
			communication.DataInputType = DataTypeList
			documentSubClass(communication)
			communication.NextStep = designation.D_DocumentDccConcrete
			communication.PossibleFinalState = false
		case designation.D_DocumentDccConcrete:
			communication.DataInputType = InputNumber
			communication.DataInputLength = []int{3}
			communication.NextStep = designation.D_DocumentPageCountNumber
			communication.PossibleFinalState = false
		case designation.D_DocumentPageCountNumber:
			communication.DataInputType = InputStringNumberList
			documentPageHelp(communication)
			communication.DataInputLength = []int{1, 4}
			communication.NextStep = designation.PRE_Prefix
			communication.PossibleFinalState = true
		case designation.PRE_Prefix:
			communication.ActualDesignation = inserCharAtPosition(communication.ActualDesignation, "/", communication.Data, communication.DataInputType)
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}
	case designation.T_TerminalDesignation:
		switch communication.NextStep {
		case designation.T_TerminalAssembly:
			communication.DataInputType = InputStringNumber
			communication.DataInputLength = []int{9, 9}
			communication.NextStep = designation.T_TerminalPoint
			communication.PossibleFinalState = false
		case designation.T_TerminalPoint:
			if communication.ActualDesignation[len(communication.ActualDesignation)-1] != ':' {
				communication.ActualDesignation = communication.ActualDesignation + "."
			}
			communication.DataInputType = InputStringNumber
			communication.DataInputLength = []int{9, 9}
			communication.NextStep = designation.PRE_Prefix
			communication.PossibleFinalState = true
		case designation.PRE_Prefix:
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}
	case designation.S_SignalDesignation:
		switch communication.NextStep {
		case designation.S_SignalMainClass:
			communication.DataInputType = DataTypeList
			signalMainClass(communication)
			communication.NextStep = designation.S_SignalSubclass
			communication.PossibleFinalState = false
		case designation.S_SignalSubclass:
			communication.DataInputType = DataTypeList
			signalSubclass(communication)
			communication.NextStep = designation.S_SignalClassCounter
			communication.PossibleFinalState = false
		case designation.S_SignalClassCounter:
			communication.DataInputType = DataTypeList
			signalInformation(communication)
			communication.NextStep = designation.S_SignalDuration
			communication.PossibleFinalState = false
		case designation.S_SignalDuration:
			// communication.ActualDesignation = communication.ActualDesignation + "_"
			communication.DataInputType = DataTypeList
			signalDuration(communication)
			communication.NextStep = designation.S_SignalType
			communication.PossibleFinalState = true
		case designation.S_SignalType:
			communication.ActualDesignation = inserCharAtPosition(communication.ActualDesignation, "_", communication.Data, communication.DataInputType)
			communication.DataInputType = DataTypeList
			signalType(communication)
			communication.NextStep = designation.S_SignalInformationCounter
			communication.PossibleFinalState = false
		case designation.S_SignalInformationCounter:
			communication.DataInputType = InputNumber
			communication.DataInputLength = []int{2}
			communication.NextStep = designation.PRE_Prefix
			communication.PossibleFinalState = true
		case designation.PRE_Prefix:
			//End
			communication.PossibleFinalState = true
			communication.DataInputType = Prefix
			communication.ProposalList = getNextStates(communication.ActualDesignationType)
		}
	case designation.U_Undefined:
		//FirstCall
		communication.DataInputType = Prefix
		communication.ProposalList = getNextStates(designation.U_Undefined)
	}
}
func inserCharAtPosition(designation string, insert string, data []string, inputType DataInputTypes) string {
	position := 0
	if (inputType == InputStringNumber || inputType == InputStringNumberList) && len(data) > 1 {
		position = len(designation) - len(data[0]) - len(data[1])
	} else {
		position = len(designation) - len(data[0])
	}
	return designation[0:position] + insert + designation[position:len(designation)]
}
