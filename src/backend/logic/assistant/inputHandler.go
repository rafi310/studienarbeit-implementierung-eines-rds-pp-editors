package assistant

import (
	"rdspp.de/src/logic/designation"
)

//InputLogic first handler for UI input
//bool just plant is for the add operation in "All Plants"
func InputLogic(communication *CommunicationData, justPlants bool) *CommunicationData {
	if justPlants {
		if communication.ActualDesignationType == designation.U_Undefined {
			communication.ActualDesignationType = designation.CD_ConjointDesignation
			communication.ActualDesignation = "#"
			communication.NextStep = designation.CD_GeographicLocation
		} else if communication.ActualDesignationType != designation.CD_ConjointDesignation {
			return communication
		}
	}

	//Append this Data to History
	communication.History = append(communication.History, *communication)
	communication.History[len(communication.History)-1].History = nil
	//Append Data to Designation
	if communication.Data != nil {
		handleInputData(communication)
	}
	if communication.DataInputType == InputNumber || communication.DataInputType == InputString || communication.DataInputType == InputStringNumber {
		communication.ProposalList = nil
	}
	//StateMachine
	handleNextStep(communication, justPlants)
	return communication
}

func handleInputData(communication *CommunicationData) {
	if communication.DataInputType == Geolocation {
		if len(communication.Data) > 1 {
			coords := makeCoords(communication.Data)
			communication.ActualDesignation = communication.ActualDesignation + coords
			setDesignationIfPossible(communication)
		} else {
			communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
		}
		return
	}
	if communication.DataInputType == Selection {
		selectionSolver(communication)
		return
	}
	if communication.DataInputType == Prefix {
		getStateForPrefix(communication)

	} else {
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]

	}
}
func getStateForPrefix(communication *CommunicationData) {
	switch communication.Data[0] {
	case "#":
		communication.PossibleFinalState = false
		communication.DataInputType = Selection
		communication.ProposalList = conjointDesignationSelection()
		communication.ActualDesignationType = designation.CD_ConjointDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
		//fertig
	case "=":
		communication.PossibleFinalState = false
		communication.NextStep = designation.U_Undefined
		communication.ProposalList = functionSelection()
		communication.DataInputType = Selection
		communication.ActualDesignationType = designation.F_FunctionDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
	case "=-":
		communication.PossibleFinalState = false
		communication.NextStep = designation.U_Undefined
		communication.ProposalList = functionSelection()
		communication.DataInputType = Selection
		communication.ActualDesignationType = designation.OE_OperatingEquipmentDesignation
		communication.ActualDesignation = communication.ActualDesignation + "="
	case "-":
		communication.PossibleFinalState = false
		communication.NextStep = designation.P_ProductClass
		communication.ActualDesignationType = designation.P_ProductClassDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]

	case "+":
		communication.PossibleFinalState = false
		communication.NextStep = designation.U_Undefined
		communication.ProposalList = functionSelection()
		communication.DataInputType = Selection
		communication.ActualDesignationType = designation.PI_PointOfInstallationDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
	case "++":
		communication.PossibleFinalState = false
		communication.NextStep = designation.F_MainSystemA1
		communication.ActualDesignationType = designation.SI_SiteOfInstallationDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
		//fertig
	case "&":
		communication.PossibleFinalState = false
		communication.NextStep = designation.D_DocumentDccTechnicalSector
		communication.ActualDesignationType = designation.D_DocumentDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
	case ":":
		communication.PossibleFinalState = false
		communication.NextStep = designation.T_TerminalDesignation
		communication.DataInputType = Selection
		communication.ProposalList = terminalSelection()
		communication.ActualDesignationType = designation.T_TerminalDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
		//fertig
	case ";":
		communication.PossibleFinalState = false
		communication.NextStep = designation.S_SignalMainClass
		communication.ActualDesignationType = designation.S_SignalDesignation
		communication.ActualDesignation = communication.ActualDesignation + communication.Data[0]
	}
}

func selectionSolver(communication *CommunicationData) {
	switch communication.Data[0] {
	//#
	case conjointDesignationGeo:
		communication.NextStep = designation.CD_GeographicLocation
	case conjointDesignationProject:
		//communication.ActualDesignation = "#5163N00017W.GBCON.BEA_1WN"
		communication.NextStep = designation.CD_ExistingDesignation
	//=
	case functionMainSystem:
		communication.NextStep = designation.F_MainSystemA1
	case functionSubSystem:
		communication.NextStep = designation.F_SystemA1
	//-
	case productContinue:
		communication.NextStep = designation.P_ProductClass
	case productEnd:
		communication.NextStep = designation.PRE_Prefix
	//:
	case terminalAssembly:
		communication.NextStep = designation.T_TerminalAssembly
	case terminalData:
		communication.NextStep = designation.T_TerminalPoint
	//+
	case pointOfInstallationBasicFunction:
		communication.NextStep = designation.F_BasicFunction
	case pointOfInstallationGoToSelection2:
		communication.NextStep = designation.PI_PointOfInstallationSelection2
	case pointOfInstallationPlace:
		communication.NextStep = designation.PI_PointOfInstallation
	case pointOfInstallationNoInput:
		communication.NextStep = designation.PRE_Prefix
	//++
	case siteOfInstallationPlace:
		communication.NextStep = designation.SI_SiteOfInstallation
	case siteOfInstallationGeolocation:
		communication.NextStep = designation.SI_SiteOfInstallationGeolocation
	case siteOfInstallationSubsystem:
		communication.NextStep = designation.F_SystemA1
	case siteOfInstallationNoSubsystem:
		communication.NextStep = designation.SI_SiteOfInstallationSelection
	}
}
