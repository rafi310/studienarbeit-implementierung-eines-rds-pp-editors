package assistant

import (
	"math"
	"strconv"

	"rdspp.de/src/crosscutting/logger"
	"rdspp.de/src/data/repository"
	"rdspp.de/src/data/repository/models"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/designation/denomination"
	"rdspp.de/src/logic/geocoding"
)

func conjointDesignationSelection() (proposal []DataCode) {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == conjointDesignationGeo {
			selectionList = append(selectionList, selection)
		}
		if selection.Code == conjointDesignationProject {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}
func conjointDesignationCountry(communication *CommunicationData) {
	communication.ProposalList = nil
	if denomination.ConjointDesignation.Coutry != nil {
		for _, country := range denomination.ConjointDesignation.Coutry {
			localData := DataCode{
				Code:         country.Code,
				Denomination: country.Denomination,
			}
			communication.ProposalList = append(communication.ProposalList, localData)
		}
	}
}
func conjointDesignationRegion(communication *CommunicationData) {
	communication.ProposalList = nil
	for _, country := range denomination.ConjointDesignation.Coutry {
		//Check if the country is the selected country so we give just for this country proposals (is the desingation.Country the code?)
		if country.Regions != nil && country.Code == communication.Data[0] {
			for _, region := range country.Regions {
				localData := DataCode{
					Code:         region.Code,
					Denomination: region.Denomination,
				}
				communication.ProposalList = append(communication.ProposalList, localData)
			}
		}
	}
}
func conjointDesignationPlantType(communication *CommunicationData) {
	communication.ProposalList = nil
	if denomination.ConjointDesignation.Resource != nil {
		for _, resource := range denomination.ConjointDesignation.Resource {
			localData := DataCode{
				Code:         resource.Code,
				Denomination: resource.Denomination,
			}
			communication.ProposalList = append(communication.ProposalList, localData)
		}
	}
}
func conjointDesignationFacilityType(communication *CommunicationData) {
	communication.ProposalList = nil
	if denomination.ConjointDesignation.Resource != nil {
		for _, resource := range denomination.ConjointDesignation.Resource {
			//Check if the country is the selected country so we give just for this country proposals (is the desingation.Country the code?)
			if resource.Character != nil && resource.Code == communication.Data[0] {
				for _, infrastructure := range resource.Character {
					localData := DataCode{
						Code:         infrastructure.Code,
						Denomination: infrastructure.Denomination,
					}
					communication.ProposalList = append(communication.ProposalList, localData)
				}
			}
		}
	}
}
func getCountryFromCoords(communication *CommunicationData) (string, error) {
	latitude, longitude := stringToFloat(communication.Data)
	country, err := geocoding.GetCountryCode_ISO3166_1(latitude, longitude)
	return country, err
}
func getRegionFromCoords(communication *CommunicationData) (string, error) {
	latitude, longitude := stringToFloat(communication.Data)
	region, err := geocoding.GetRegionCode_ISO3166_2(latitude, longitude)
	return region, err
}
func setDesignationIfPossible(communication *CommunicationData) {
	country, err := getCountryFromCoords(communication)
	if err == nil {
		communication.ActualDesignation = communication.ActualDesignation + "." + country
		communication.NextStep = designation.CD_Region

		region, err := getRegionFromCoords(communication)
		if err == nil {
			communication.ActualDesignation = communication.ActualDesignation + region
			communication.NextStep = designation.CD_TownOrName
		}
	}
}

func makeCoords(data []string) string {
	latitude, longitude, data := parseCoordsToFloat(data)
	nsValue := strconv.Itoa(int(math.Round(latitude * 100)))
	ewValue := strconv.Itoa(int(math.Round(longitude * 100)))
	for len(nsValue) < 4 {
		nsValue = "0" + nsValue
	}
	for len(ewValue) < 5 {
		ewValue = "0" + ewValue
	}
	finalCoordinatesString := nsValue + data[2] + ewValue + data[3]
	return finalCoordinatesString
}
func parseCoordsToFloat(data []string) (lat float64, lgn float64, dataOut []string) {
	latitude, err := strconv.ParseFloat(data[0], 64)
	if err != nil {
		logger.Error("Coordinates parsing failed")
		return
	}
	if latitude > 0 {
		data = append(data, "N")
	} else {
		data = append(data, "S")
		latitude *= (-1)
	}
	longitude, err := strconv.ParseFloat(data[1], 64)
	if err != nil {
		logger.Error("Coordinates parsing failed")
		return
	}
	if longitude > 0 {
		data = append(data, "E")
	} else {
		data = append(data, "W")
		longitude *= (-1)
	}
	return latitude, longitude, data
}

func stringToFloat(data []string) (lat float64, lgn float64) {
	latitude, err := strconv.ParseFloat(data[0], 64)
	if err != nil {
		logger.Error("Coordinates parsing failed")
		return
	}
	longitude, err := strconv.ParseFloat(data[1], 64)
	if err != nil {
		logger.Error("Coordinates parsing failed")
		return
	}

	return latitude, longitude
}

func getAllPlantDesignations(communication *CommunicationData) {
	communication.ProposalList = nil
	repo, err := repository.NewDbRepository()
	if err != nil {
		// database connection failed
		logger.Error("DatabaseConnection Failed")
		return
	}
	plants := repo.GetPlants()
	for _, plant := range plants {
		data := DataCode{}
		var plantmodel *models.PlantModel = plant.(*models.PlantModel)
		data.Denomination.De = plantmodel.Designation[1:]
		data.Denomination.De = plantmodel.Designation[1:]
		data.Code = plantmodel.Designation[1:]
		communication.ProposalList = append(communication.ProposalList, data)
	}
}
