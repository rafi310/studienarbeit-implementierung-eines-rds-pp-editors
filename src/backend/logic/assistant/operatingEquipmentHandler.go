package assistant

import (
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/designation/denomination"
)

func operatingEquipmentProduct(communication *CommunicationData) {
	communication.ProposalList = nil

	selectedMainSystem, selectedSubSystem, selectedBasicFunction := getPossibleSelections(*communication)

	if denomination.BasicFunctions.BasicFunctionsProducts.MainSystems != nil {
		for _, mainSystem := range denomination.BasicFunctions.BasicFunctionsProducts.MainSystems {
			if mainSystem.Code == selectedMainSystem || selectedMainSystem == "" {
				for _, subsystem := range mainSystem.Subsystems {
					if subsystem.Code == selectedSubSystem {
						for _, basicFunction := range subsystem.BasicFunctions {
							if basicFunction.Code == selectedBasicFunction {
								for _, product := range basicFunction.Products {
									communication.ProposalList = append(communication.ProposalList, DataCode{
										Code:         product.Code,
										Denomination: product.Denomination,
									})
								}
							}
						}
					}
				}
			}
		}
	}
}

func getPossibleSelections(communication CommunicationData) (string, string, string) {
	mainSystem := ""
	subSystem := ""
	basicFunction := ""
	for _, communicationOld := range communication.History {
		switch communicationOld.NextStep {
		case designation.F_MainSystemCounter:
			mainSystem = mainSystem + communicationOld.Data[0]
		case designation.F_SystemA2:
			subSystem = subSystem + communicationOld.Data[0]
		case designation.F_SystemA3:
			subSystem = subSystem + communicationOld.Data[0]
		case designation.F_Subsystem:
			subSystem = subSystem + communicationOld.Data[0]
		case designation.F_BasicFunction:
			subSystem = subSystem + communicationOld.Data[0]
		case designation.P_ProductClass:
			basicFunction = communicationOld.Data[0]
		}
	}
	return mainSystem, subSystem, basicFunction
}
