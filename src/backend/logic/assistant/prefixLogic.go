package assistant

import (
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/designation/denomination"
)

var (
	conjointDesignation DataCode
	function            DataCode
	product             DataCode
	operatingEquipment  DataCode
	pointOfInstallation DataCode
	siteOfInstallation  DataCode
	document            DataCode
	terminal            DataCode
	signal              DataCode
)

//Init DataCodes for varibles have to call after denomination.Init()
func Init() {

	conjointDesignation = DataCode{
		Code:         denomination.DesignationSections.Prefix.Hash,
		Denomination: denomination.DesignationSections.ConjointDesignation,
	}

	function = DataCode{
		Code:         denomination.DesignationSections.Prefix.Equals,
		Denomination: denomination.DesignationSections.Function,
	}
	product = DataCode{
		Code:         denomination.DesignationSections.Prefix.Minus,
		Denomination: denomination.DesignationSections.Product,
	}
	operatingEquipment = DataCode{
		Code:         denomination.DesignationSections.Prefix.EqualsMinus,
		Denomination: denomination.DesignationSections.OperatingEquipment,
	}
	pointOfInstallation = DataCode{
		Code:         denomination.DesignationSections.Prefix.Plus,
		Denomination: denomination.DesignationSections.PointOfInstallation,
	}
	siteOfInstallation = DataCode{
		Code:         denomination.DesignationSections.Prefix.PlusPlus,
		Denomination: denomination.DesignationSections.SiteOfInstallation,
	}
	terminal = DataCode{
		Code:         denomination.DesignationSections.Prefix.Colon,
		Denomination: denomination.DesignationSections.Terminal,
	}
	document = DataCode{
		Code:         denomination.DesignationSections.Prefix.Ampersand,
		Denomination: denomination.DesignationSections.Document,
	}
	signal = DataCode{
		Code:         denomination.DesignationSections.Prefix.Semicolon,
		Denomination: denomination.DesignationSections.Signal,
	}
}
func getNextStates(acutalState designation.DesignationSection) []DataCode {
	stateList := []DataCode{}
	switch acutalState {
	case designation.U_Undefined:
		stateList = append(stateList, conjointDesignation)
		stateList = append(stateList, function)
		stateList = append(stateList, operatingEquipment)
		stateList = append(stateList, product)
		stateList = append(stateList, pointOfInstallation)
		stateList = append(stateList, siteOfInstallation)
		stateList = append(stateList, document)
		stateList = append(stateList, terminal)
		stateList = append(stateList, signal)
	case designation.CD_ConjointDesignation:
		stateList = append(stateList, function)
		stateList = append(stateList, operatingEquipment)
		stateList = append(stateList, document)
		stateList = append(stateList, pointOfInstallation)
		stateList = append(stateList, siteOfInstallation)
	case designation.F_FunctionDesignation:
		//stateList = append(stateList, designation.P_ProductClassDesignation)
		stateList = append(stateList, document)
		stateList = append(stateList, signal)
	case designation.OE_OperatingEquipmentDesignation:
		stateList = append(stateList, document)
		stateList = append(stateList, terminal)
	case designation.P_ProductClassDesignation:
	case designation.PI_PointOfInstallationDesignation:
		stateList = append(stateList, document)
	case designation.SI_SiteOfInstallationDesignation:
		stateList = append(stateList, document)
	case designation.D_DocumentDesignation:
	case designation.T_TerminalDesignation:
	case designation.S_SignalDesignation:
	}
	return stateList
}
