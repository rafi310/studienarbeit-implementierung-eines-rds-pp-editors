package assistant

import (
	"rdspp.de/src/logic/designation/denomination"
)

func functionSelection() []DataCode {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == functionMainSystem || selection.Code == functionSubSystem {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}

func functionLogicMainSystem(communication *CommunicationData) {
	communication.ProposalList = nil
	if denomination.MainSystem != nil {
		for _, system := range denomination.MainSystem.System {
			communication.ProposalList = append(communication.ProposalList, DataCode{
				Code:         system.Code,
				Denomination: system.Denomination,
			})
		}
	}
}
func functionLogicSystemA1(communication *CommunicationData) {
	communication.ProposalList = nil
	if denomination.SystemsSubsystems.Systems != nil {
		for _, system := range denomination.SystemsSubsystems.Systems {
			if !stringInSlice(string(communication.ActualDesignation[len(communication.ActualDesignation)-1]), []string{"=", "+", "++"}) {
				mainSystem := communication.ActualDesignation[len(communication.ActualDesignation)-4]
				//funktioniert das?
				if stringInSlice(string(mainSystem), system.Mainsystems) {
					communication.ProposalList = append(communication.ProposalList, DataCode{
						Code:         system.Code,
						Denomination: system.Denomination,
					})
				}
			} else {
				communication.ProposalList = append(communication.ProposalList, DataCode{
					Code:         system.Code,
					Denomination: system.Denomination,
				})
			}
		}
	}
}

func functionLogicSystemA2(communication *CommunicationData) {
	communication.ProposalList = nil
	systemA1selected := communication.ActualDesignation[len(communication.ActualDesignation)-1]
	if denomination.SystemsSubsystems.Systems != nil {
		for _, system := range denomination.SystemsSubsystems.Systems {
			if system.Code == string(systemA1selected) {
				for _, systemA2 := range system.Systems {
					communication.ProposalList = append(communication.ProposalList, DataCode{
						Code:         systemA2.Code,
						Denomination: systemA2.Denomination,
					})
				}
			}
		}
	}
}
func functionLogicSystemA3(communication *CommunicationData) {
	communication.ProposalList = nil
	systemA1selected := communication.ActualDesignation[len(communication.ActualDesignation)-2]
	systemA2selected := communication.ActualDesignation[len(communication.ActualDesignation)-1]
	if denomination.SystemsSubsystems.Systems != nil {
		for _, system := range denomination.SystemsSubsystems.Systems {
			if system.Code == string(systemA1selected) {
				for _, systemA2 := range system.Systems {
					if systemA2.Code == string(systemA2selected) {
						for _, systemA3 := range systemA2.Systems {
							communication.ProposalList = append(communication.ProposalList, DataCode{
								Code:         systemA3.Code,
								Denomination: systemA3.Denomination,
							})
						}
					}
				}
			}
		}
	}
}
func functionLogicSubsystem(communication *CommunicationData) {
	communication.ProposalList = nil
	systemA1selected := communication.ActualDesignation[len(communication.ActualDesignation)-3]
	systemA2selected := communication.ActualDesignation[len(communication.ActualDesignation)-2]
	systemA3selected := communication.ActualDesignation[len(communication.ActualDesignation)-1]
	if denomination.SystemsSubsystems.Systems != nil {
		for _, system := range denomination.SystemsSubsystems.Systems {
			if system.Code == string(systemA1selected) {
				for _, systemA2 := range system.Systems {
					if systemA2.Code == string(systemA2selected) {
						for _, systemA3 := range systemA2.Systems {
							if systemA3.Code == string(systemA3selected) {
								for _, subsystem := range systemA3.Subsystems {
									communication.ProposalList = append(communication.ProposalList, DataCode{
										Code:         subsystem.Code,
										Denomination: subsystem.Denomination,
									})
								}
							}
						}
					}
				}
			}
		}
	}
}

func functionLogicBasicFunction(communication *CommunicationData) {
	communication.ProposalList = nil
	selectedMainSystem, selectedSubsystem, _ := getPossibleSelections(*communication)
	//all basic functions if prefix - is selected
	if selectedMainSystem == "" && selectedSubsystem == "" {
		for _, mainSystem := range denomination.BasicFunctions.BasicFunctionsProducts.MainSystems {
			for _, subsystem := range mainSystem.Subsystems {
				for _, basicFunction := range subsystem.BasicFunctions {
					//add subsystems to proposalList
					communication.ProposalList = append(communication.ProposalList, DataCode{
						Code:         basicFunction.Code,
						Denomination: basicFunction.Denomination,
					})

				}
			}
		}
		return
	}

	if denomination.BasicFunctions.BasicFunctionsProducts.MainSystems != nil {
		for _, mainSystem := range denomination.BasicFunctions.BasicFunctionsProducts.MainSystems {
			//get right mainSystem
			if mainSystem.Code == selectedMainSystem || selectedMainSystem == "" {
				for _, subsystem := range mainSystem.Subsystems {
					//get right subsystem
					if subsystem.Code == selectedSubsystem {
						for _, basicFunction := range subsystem.BasicFunctions {
							//add subsystems to proposalList
							communication.ProposalList = append(communication.ProposalList, DataCode{
								Code:         basicFunction.Code,
								Denomination: basicFunction.Denomination,
							})

						}
					}
				}
			}
		}
	}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
