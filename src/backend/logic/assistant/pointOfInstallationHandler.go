package assistant

import "rdspp.de/src/logic/designation/denomination"

func pointOfInstallationSelection1() []DataCode {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == pointOfInstallationBasicFunction || selection.Code == pointOfInstallationGoToSelection2 {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}

func pointOfInstallationSelection2() []DataCode {
	var selectionList []DataCode
	for _, selection := range denomination.Selections.Selections {
		if selection.Code == pointOfInstallationPlace || selection.Code == pointOfInstallationNoInput {
			selectionList = append(selectionList, selection)
		}
	}
	return selectionList
}
