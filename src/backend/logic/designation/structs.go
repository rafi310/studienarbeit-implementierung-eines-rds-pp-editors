package designation

type DesignationNode struct {
	Designation  string `json:"designation"`
	Denomination struct {
		DE string `json:"de"`
		EN string `json:"en"`
	} `json:"denomination"`
	Examples struct {
		DE []string `json:"de"`
		EN []string `json:"en"`
	} `json:"examples"`
	Section             DesignationSection `json:"-"`
	SectionDenomination struct {
		DE string `json:"de"`
		EN string `json:"en"`
	} `json:"sectionDenomination"`
	ContainsError bool               `json:"containsError"`
	Childs        []*DesignationNode `json:"childs"`
}

// memcpy like cloning
func (node *DesignationNode) Clone(other *DesignationNode) {
	node.Designation = other.Designation
	node.Denomination.DE = other.Denomination.DE
	node.Denomination.EN = other.Denomination.EN
	node.Section = other.Section
	node.SectionDenomination.DE = other.SectionDenomination.DE
	node.SectionDenomination.EN = other.SectionDenomination.EN
	if other.Childs != nil && len(other.Childs) > 0 {
		for _, c := range other.Childs {
			nc := new(DesignationNode)
			nc.Clone(c)
			node.Childs = append(node.Childs, nc)
		}
	}
}

func (node *DesignationNode) GetCountOf(section DesignationSection) int {
	if node == nil {
		return 0
	}
	if node.Section == section {
		return 1
	}
	n := 0
	for _, child := range node.Childs {
		n += child.GetCountOf(section)
	}
	return n
}

// Search for a child of node that matches the paramter section.
// Returns the designation of the matching child or an empty string if no child matched.
func (node *DesignationNode) GetPartialDesignationOf(section DesignationSection) string {
	if node == nil {
		// no node means no designation :)
		return ""
	}
	if node.Section == section {
		// recursion: base case
		return node.Designation
	}
	for _, child := range node.Childs {
		// recursion
		d := child.GetPartialDesignationOf(section)
		if d != "" {
			// found it
			return d
		}
		// else: continue search
	}
	// in case no child matched
	return ""
}

func (node *DesignationNode) ToString() string {
	if node == nil {
		return ""
	}
	if node.Childs == nil || len(node.Childs) == 0 {
		return node.Designation
	}
	combinedString := ""
	for _, child := range node.Childs {
		combinedString += child.ToString()
	}
	return combinedString
}

func (node *DesignationNode) RemoveFirstChildBySection(section DesignationSection) bool {
	if node.Childs == nil || len(node.Childs) == 0 {
		return false
	}
	for i, child := range node.Childs {
		if child.Section == section {
			node.Childs = append(node.Childs[:i], node.Childs[i+1:]...)
			return true
		}
		found := child.RemoveFirstChildBySection(section)
		if found {
			return true
		}
	}
	return false
}

func (node *DesignationNode) GetLastChild() *DesignationNode {
	if node.Childs == nil || len(node.Childs) == 0 {
		return node
	}
	return node.Childs[len(node.Childs)-1].GetLastChild()
}

func (node *DesignationNode) SetErroneousRecursive(errorPos []int) {
	if len(errorPos) == 0 {
		return
	}
	node.setErroneousRecursive(errorPos, 0, len(node.Designation))
}

func (node *DesignationNode) setErroneousRecursive(errorPos []int, start int, end int) {
	for _, pos := range errorPos {
		if start <= pos && pos < end {
			node.ContainsError = true
			break
		}
	}
	if node.Childs == nil || len(node.Childs) == 0 {
		return
	}
	offset := start
	for _, c := range node.Childs {
		n := len(c.Designation)
		for offset < len(node.Designation) && node.Designation[offset] == ' ' {
			offset++
		}
		c.setErroneousRecursive(errorPos, offset, offset + len(c.Designation))
		offset += n
	}
}

type DesignationTuple struct {
	Designation string
	Type        DesignationSection
}

//UIDataList struct with informations for the UI
type UIDataList struct {
	SuccessorStates []DesignationSection
	MainNode        DesignationNode
}

//BackendDataList struct
type BackendDataList struct {
	SelectedState DesignationSection
	MainNode      DesignationNode
}
