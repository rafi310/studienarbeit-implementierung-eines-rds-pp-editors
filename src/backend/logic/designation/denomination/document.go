package denomination

import (
	"rdspp.de/src/crosscutting/util"
	"rdspp.de/src/logic/designation"
)

func addDocumentTechnicalSector(node *designation.DesignationNode, data *tmpRecursionData) {
	sector := node.Designation
	if len(sector) != 1 { // can't really happend due to parser rules
		return
	}
	for _, s := range Dcc.TechnicalSector {
		if s.Code == sector {
			node.Denomination.DE = s.Denomination.De
			node.Denomination.EN = s.Denomination.En
			break
		}
	}
}

func addDocumentMainClass(node *designation.DesignationNode, data *tmpRecursionData) {
	mainClass := node.Designation
	if len(mainClass) != 1 { // can't really happend due to parser rules
		return
	}
	data.documentMainClass = mainClass
	for _, c := range Dcc.DocumentClass {
		if c.Code == mainClass {
			node.Denomination.DE = c.Denomination.De
			node.Denomination.EN = c.Denomination.En
			break
		}
	}
}

func addDocumentSubclass(node *designation.DesignationNode, data *tmpRecursionData) {
	subclass := node.Designation
	if len(subclass) != 1 { // can't really happend due to parser rules
		return
	}
	data.documentSubclass = subclass
	mainClass := data.documentMainClass
	for _, c := range Dcc.DocumentClass {
		if c.Code == mainClass {
			for _, s := range c.Subclasses {
				if s.Code == subclass {
					node.Denomination.DE = s.Denomination.De
					node.Denomination.EN = s.Denomination.En
					node.Examples.DE = s.Examples.De
					node.Examples.EN = s.Examples.En
					break
				}
			}
			break
		}
	}
}

func addDocumentConcrete(node *designation.DesignationNode, data *tmpRecursionData) {
	concrete := node.Designation
	if len(concrete) != 3 { // can't really happend due to parser rules
		return
	}
	mainClass := data.documentMainClass
	subclass := data.documentSubclass
	for _, c := range Dcc.DocumentClass {
		if c.Code == mainClass {
			for _, s := range c.Subclasses {
				if s.Code == subclass {
					for _, n := range s.Numbers {
						if n.Code == concrete {
							node.Denomination.DE = n.Denomination.De
							node.Denomination.EN = n.Denomination.En
							break
						}
					}
					break
				}
			}
			break
		}
	}
}

func addDocumentPageNumberAlpha(node *designation.DesignationNode, data *tmpRecursionData) {
	designation := node.Designation
	n := len(designation)
	m := len(Dcc.PageNumberAlpha)
	added := make([]string, 0)
	for i := 0; i < n; i++ {
		alpha := designation[i : i+1]
		for k := 0; k < m; k++ {
			a := Dcc.PageNumberAlpha[k]
			if a.Code == alpha {
				if n == 1 {
					node.Denomination.DE = a.Denomination.De
					node.Denomination.EN = a.Denomination.En
				} else {
					if !util.StringArrayContains(added, a.Code) {
						added = append(added, a.Code)
						node.Denomination.DE += a.Code + ": " + a.Denomination.De
						node.Denomination.EN += a.Code + ": " + a.Denomination.En
						if i < n-1 {
							node.Denomination.DE += "\n"
							node.Denomination.EN += "\n"
						}
					}
				}
				break
			}
			if k == m-1 && !util.StringArrayContains(added, alpha) {
				added = append(added, alpha)
				node.Denomination.DE += alpha + ": undefiniert\n"
				node.Denomination.EN += alpha + ": undefined\n"
			}
		}
	}
}
