package denomination

import (
	"rdspp.de/src/logic/designation"
)

type tmpRecursionData struct {
	country               string
	plantType             string
	mainSystemA1          string
	systemA1              string
	systemA2              string
	systemA3              string
	subsystem             string
	basicFunctionA1       string
	basicFunctionA2       string
	basicFunctionConcrete string
	productClassA1        string
	productClassA2        string
	pruductClassConcrete  string
	documentMainClass     string
	documentSubclass      string
}

func Denominate(tree *designation.DesignationNode) {
	data := new(tmpRecursionData)
	denominate(tree, data)

}

func denominate(node *designation.DesignationNode, data *tmpRecursionData) {
	// recursion base case
	if node == nil {
		return
	}

	addDenomination(node, data)
	addSectionDenomination(node)

	// recursion
	if node.Childs != nil {
		for _, child := range node.Childs {
			denominate(child, data)
		}
	}
}

func addDenomination(node *designation.DesignationNode, data *tmpRecursionData) {
	switch node.Section {
	case designation.D_Designation: // no denomination
	case designation.PRE_Prefix: // no denomination
	case designation.SEP_Seperator: // no denomination
	case designation.CD_ConjointDesignation: // no denomination
	case designation.CD_GeographicLocation:
		addGeographicLocation(node, data)
	case designation.CD_Latitude:
		addLatitude(node, data)
	case designation.CD_Longitude:
		addLongitude(node, data)
	case designation.CD_ProjectSpecificInformation: // no denomination
	case designation.CD_Country:
		addCountry(node, data)
	case designation.CD_Region:
		addRegion(node, data)
	case designation.CD_TownOrName:
		addTownOrName(node, data)
	case designation.CD_ProjectCounter: // no denomination
	case designation.CD_PlantType:
		addPlantType(node, data)
	case designation.CD_FacilityType:
		addFavilityType(node, data)
	case designation.F_FunctionDesignation: // no denomination
	case designation.F_MainSystem: // no denomination
	case designation.F_MainSystemA1:
		addMainSystemA1(node, data)
	case designation.F_SystemSubsystem: // no denomination
	case designation.F_SystemA1:
		addSystemA1(node, data)
	case designation.F_MainSystemCounter: // no denomination
	case designation.F_SystemA2:
		addSystemA2(node, data)
	case designation.F_SystemA3:
		addSystemA3(node, data)
	case designation.F_Subsystem:
		addSubsystem(node, data)
	case designation.F_BasicFunction: // no denomination
	case designation.F_BasicFunctionA1:
		addBasicFunctionA1(node, data)
	case designation.F_BasicFunctionA2:
		addBasicFunctionA2(node, data)
	case designation.F_BasicFunctionConcrete:
		addBasicFunctionConcrete(node, data)
	case designation.P_ProductClassDesignation: // no denomination
	case designation.P_ProductClass: // no denomination
	case designation.P_ProductClassA1:
		addProductClassA1(node, data)
	case designation.P_ProductClassA2:
		addProductClassA2(node, data)
	case designation.P_ProductClassConcrete:
		addProductClassConcrete(node, data)
	case designation.OE_OperatingEquipmentDesignation: // no denomination
	case designation.PI_PointOfInstallationDesignation: // no denomination
	case designation.PI_PointOfInstallation:
		// TODO
	case designation.PI_Column:
		// TODO
	case designation.PI_Row:
		// TODO
	case designation.SI_SiteOfInstallationDesignation: // no denomination
	case designation.SI_SiteOfInstallation:
	case designation.T_TerminalDesignation: // no denomination
	case designation.T_TerminalPoint:
		// TODO
	case designation.S_SignalDesignation: // no denomination
	case designation.S_SignalClass:
		addSignalClass(node, data)
	case designation.S_SignalMainClass:
		addSignalMainClass(node, data)
	case designation.S_SignalSubclass:
		addSignalSubclass(node, data)
	case designation.S_SignalClassCounter: // no denomination
	case designation.S_SignalInformation: // no denomination
	case designation.S_SignalDuration:
		addSignalDuration(node, data)
	case designation.S_SignalType:
		addSignalType(node, data)
	case designation.S_SignalInformationCounter: // no denomination
	case designation.D_DocumentDesignation: // no denomination
	case designation.D_DocumentDcc: // no denomination
	case designation.D_DocumentDccTechnicalSector:
		addDocumentTechnicalSector(node, data)
	case designation.D_DocumentDccMainClass:
		addDocumentMainClass(node, data)
	case designation.D_DocumentDccSubclass:
		addDocumentSubclass(node, data)
	case designation.D_DocumentDccConcrete:
		addDocumentConcrete(node, data)
	case designation.D_DocumentPageCountNumber: // no denomination
	case designation.D_DocumentPageNumberAlpha:
		addDocumentPageNumberAlpha(node, data)
	case designation.D_DocumentPageNumber: // no denomination
	case designation.U_Undefined: // no denomination
	default:
	}
}

func addSectionDenomination(node *designation.DesignationNode) {
	res := DesignationSections
	switch node.Section {
	case designation.D_Designation:
		node.SectionDenomination.DE = res.Designation.De
		node.SectionDenomination.EN = res.Designation.En
	case designation.PRE_Prefix:
		node.SectionDenomination.DE = res.Prefix.De
		node.SectionDenomination.EN = res.Prefix.En
	case designation.SEP_Seperator:
		node.SectionDenomination.DE = res.Separator.De
		node.SectionDenomination.EN = res.Separator.En
	case designation.CD_ProjectCounter,
		designation.F_MainSystemCounter,
		designation.S_SignalClassCounter,
		designation.S_SignalInformationCounter:
		node.SectionDenomination.DE = res.Counter.De
		node.SectionDenomination.EN = res.Counter.En
	case designation.CD_ConjointDesignation:
		node.SectionDenomination.DE = res.ConjointDesignation.De
		node.SectionDenomination.EN = res.ConjointDesignation.En
	case designation.CD_GeographicLocation:
		node.SectionDenomination.DE = res.GeographicLocation.De
		node.SectionDenomination.EN = res.GeographicLocation.En
	case designation.CD_Latitude:
		node.SectionDenomination.DE = res.Latitude.De
		node.SectionDenomination.EN = res.Latitude.En
	case designation.CD_Longitude:
		node.SectionDenomination.DE = res.Longitude.De
		node.SectionDenomination.EN = res.Longitude.En
	case designation.CD_ProjectSpecificInformation:
		node.SectionDenomination.DE = res.ProjectSpecificInformation.De
		node.SectionDenomination.EN = res.ProjectSpecificInformation.En
	case designation.CD_Country:
		node.SectionDenomination.DE = res.Country.De
		node.SectionDenomination.EN = res.Country.En
	case designation.CD_Region:
		node.SectionDenomination.DE = res.Region.De
		node.SectionDenomination.EN = res.Region.En
	case designation.CD_TownOrName:
		node.SectionDenomination.DE = res.TownOrName.De
		node.SectionDenomination.EN = res.TownOrName.En
	case designation.CD_PlantType:
		node.SectionDenomination.DE = res.PlantType.De
		node.SectionDenomination.EN = res.PlantType.En
	case designation.CD_FacilityType:
		node.SectionDenomination.DE = res.FacilityType.De
		node.SectionDenomination.EN = res.FacilityType.En
	case designation.F_FunctionDesignation:
		node.SectionDenomination.DE = res.Function.De
		node.SectionDenomination.EN = res.Function.En
	case designation.F_MainSystem:
		node.SectionDenomination.DE = res.MainSystem.De
		node.SectionDenomination.EN = res.MainSystem.En
	case designation.F_MainSystemA1:
		node.SectionDenomination.DE = res.MainSystemA1.De
		node.SectionDenomination.EN = res.MainSystemA1.En
	case designation.F_SystemSubsystem:
		node.SectionDenomination.DE = res.SystemSubsystem.De
		node.SectionDenomination.EN = res.SystemSubsystem.En
	case designation.F_SystemA1:
		node.SectionDenomination.DE = res.SystemA1.De
		node.SectionDenomination.EN = res.SystemA1.En
	case designation.F_SystemA2:
		node.SectionDenomination.DE = res.SystemA2.De
		node.SectionDenomination.EN = res.SystemA2.En
	case designation.F_SystemA3:
		node.SectionDenomination.DE = res.SystemA3.De
		node.SectionDenomination.EN = res.SystemA3.En
	case designation.F_Subsystem:
		node.SectionDenomination.DE = res.Subsystem.De
		node.SectionDenomination.EN = res.Subsystem.En
	case designation.F_BasicFunction:
		node.SectionDenomination.DE = res.BasicFunction.De
		node.SectionDenomination.EN = res.BasicFunction.En
	case designation.F_BasicFunctionA1:
		node.SectionDenomination.DE = res.BasicFunctionA1.De
		node.SectionDenomination.EN = res.BasicFunctionA1.En
	case designation.F_BasicFunctionA2:
		node.SectionDenomination.DE = res.BasicFunctionA2.De
		node.SectionDenomination.EN = res.BasicFunctionA2.En
	case designation.F_BasicFunctionConcrete:
		node.SectionDenomination.DE = res.BasicFunctionConcrete.De
		node.SectionDenomination.EN = res.BasicFunctionConcrete.En
	case designation.P_ProductClassDesignation:
		node.SectionDenomination.DE = res.Product.De
		node.SectionDenomination.EN = res.Product.En
	case designation.P_ProductClass:
		node.SectionDenomination.DE = res.ProductClass.De
		node.SectionDenomination.EN = res.ProductClass.En
	case designation.P_ProductClassA1:
		node.SectionDenomination.DE = res.ProductClassA1.De
		node.SectionDenomination.EN = res.ProductClassA1.En
	case designation.P_ProductClassA2:
		node.SectionDenomination.DE = res.ProductClassA2.De
		node.SectionDenomination.EN = res.ProductClassA2.En
	case designation.P_ProductClassConcrete:
		node.SectionDenomination.DE = res.ProductClassConcrete.De
		node.SectionDenomination.EN = res.ProductClassConcrete.En
	case designation.OE_OperatingEquipmentDesignation:
		node.SectionDenomination.DE = res.OperatingEquipment.De
		node.SectionDenomination.EN = res.OperatingEquipment.En
	case designation.PI_PointOfInstallationDesignation:
		node.SectionDenomination.DE = res.PointOfInstallation.De
		node.SectionDenomination.EN = res.PointOfInstallation.En
	case designation.PI_PointOfInstallation:
		node.SectionDenomination.DE = res.PointOfInstallationPlace.De
		node.SectionDenomination.EN = res.PointOfInstallationPlace.En
	case designation.PI_Column:
		node.SectionDenomination.DE = res.Column.De
		node.SectionDenomination.EN = res.Column.En
	case designation.PI_Row:
		node.SectionDenomination.DE = res.Row.De
		node.SectionDenomination.EN = res.Row.En
	case designation.SI_SiteOfInstallationDesignation:
		node.SectionDenomination.DE = res.SiteOfInstallation.De
		node.SectionDenomination.EN = res.SiteOfInstallation.En
	case designation.SI_SiteOfInstallation:
		node.SectionDenomination.DE = res.SiteOfInstallationPlace.De
		node.SectionDenomination.EN = res.SiteOfInstallationPlace.En
	case designation.T_TerminalDesignation:
		node.SectionDenomination.DE = res.Terminal.De
		node.SectionDenomination.EN = res.Terminal.En
	case designation.T_TerminalPoint:
		node.SectionDenomination.DE = res.TerminalPoint.De
		node.SectionDenomination.EN = res.TerminalPoint.En
	case designation.S_SignalDesignation:
		node.SectionDenomination.DE = res.Signal.De
		node.SectionDenomination.EN = res.Signal.En
	case designation.S_SignalClass:
		node.SectionDenomination.DE = res.SignalClass.De
		node.SectionDenomination.EN = res.SignalClass.En
	case designation.S_SignalMainClass:
		node.SectionDenomination.DE = res.SignalMainClass.De
		node.SectionDenomination.EN = res.SignalMainClass.En
	case designation.S_SignalSubclass:
		node.SectionDenomination.DE = res.SignalSubclass.De
		node.SectionDenomination.EN = res.SignalSubclass.En
	case designation.S_SignalInformation:
		node.SectionDenomination.DE = res.SignalInformation.De
		node.SectionDenomination.EN = res.SignalInformation.En
	case designation.S_SignalDuration:
		node.SectionDenomination.DE = res.SignalDuration.De
		node.SectionDenomination.EN = res.SignalDuration.En
	case designation.S_SignalType:
		node.SectionDenomination.DE = res.SignalType.De
		node.SectionDenomination.EN = res.SignalType.En
	case designation.D_DocumentDesignation:
		node.SectionDenomination.DE = res.Document.De
		node.SectionDenomination.EN = res.Document.En
	case designation.D_DocumentDcc:
		node.SectionDenomination.DE = res.Dcc.De
		node.SectionDenomination.EN = res.Dcc.En
	case designation.D_DocumentDccTechnicalSector:
		node.SectionDenomination.DE = res.TechnicalSector.De
		node.SectionDenomination.EN = res.TechnicalSector.En
	case designation.D_DocumentDccMainClass:
		node.SectionDenomination.DE = res.DccMainClass.De
		node.SectionDenomination.EN = res.DccMainClass.En
	case designation.D_DocumentDccSubclass:
		node.SectionDenomination.DE = res.DccSubclass.De
		node.SectionDenomination.EN = res.DccSubclass.En
	case designation.D_DocumentDccConcrete:
		node.SectionDenomination.DE = res.DccConcrete.De
		node.SectionDenomination.EN = res.DccConcrete.En
	case designation.D_DocumentPageCountNumber:
		node.SectionDenomination.DE = res.PageCountNumber.De
		node.SectionDenomination.EN = res.PageCountNumber.En
	case designation.D_DocumentPageNumberAlpha:
		node.SectionDenomination.DE = res.PageNumberAlpha.De
		node.SectionDenomination.EN = res.PageNumberAlpha.En
	case designation.D_DocumentPageNumber:
		node.SectionDenomination.DE = res.PageNumber.De
		node.SectionDenomination.EN = res.PageNumber.En
	case designation.U_Undefined:
	default:
	}
}
