package denomination

import "rdspp.de/src/logic/designation"

func addSignalClass(node *designation.DesignationNode, data *tmpRecursionData) {
	class := node.Designation
	if len(class) != 5 { // can't really happend due to parser rules
		return
	}
	mainClass := class[0:2]
	concreteClass := class[2:]
	for _, m := range Signals.Class {
		if m.MainClass == mainClass {
			for _, c := range m.ConcreteClass {
				if c.Code == concreteClass {
					node.Denomination.DE = c.Denomination.De
					node.Denomination.EN = c.Denomination.En
					break
				}
			}
			break
		}
	}
}

func addSignalMainClass(node *designation.DesignationNode, data *tmpRecursionData) {
	class := node.Designation
	for _, c := range Signals.MainClass {
		if c.Code == class {
			node.Denomination.DE = c.Denomination.De
			node.Denomination.EN = c.Denomination.En
			break
		}
	}
}

func addSignalSubclass(node *designation.DesignationNode, data *tmpRecursionData) {
	class := node.Designation
	for _, c := range Signals.Subclass {
		if c.Code == class {
			node.Denomination.DE = c.Denomination.De
			node.Denomination.EN = c.Denomination.En
			break
		}
	}
}

func addSignalDuration(node *designation.DesignationNode, data *tmpRecursionData) {
	duration := node.Designation
	for _, d := range Signals.Duration {
		if d.Code == duration {
			node.Denomination.DE = d.Denomination.De
			node.Denomination.EN = d.Denomination.En
			break
		}
	}
}

func addSignalType(node *designation.DesignationNode, data *tmpRecursionData) {
	sType := node.Designation
	for _, t := range Signals.Types {
		if t.Code == sType {
			node.Denomination.DE = t.Denomination.De
			node.Denomination.EN = t.Denomination.En
			break
		}
	}
}
