package denomination

import (
	"regexp"
	"strconv"
	"strings"
)

// Checks if the reference string contains wildcards ('n' or 'm[a;b]') and whether the are applicable to match the counter.
func doesCounterMatch(reference string, counter string) (bool, int) {
	// no wildcard
	if !strings.Contains(reference, "n") &&
		!strings.Contains(reference, "m") {
		return counter == reference, -1
	}

	// wildcard 'n'
	var r string
	if strings.Contains(reference, "n") {
		for i := 0; i < 10; i++ {
			r = strings.ReplaceAll(reference, "n", strconv.Itoa(i))
			if counter == r {
				return true, i
			}
		}
	}

	// wildcard 'm[a;b]'
	// a and b are the lower and upper limit of an numeric interval
	regex, _ := regexp.Compile(`m\[\d+\;\d+]`)
	if regex.MatchString(reference) {
		lower := reference[strings.Index(reference, "[")+1 : strings.Index(reference, ";")]
		upper := reference[strings.Index(reference, ";")+1 : strings.Index(reference, "]")]
		loweri, _ := strconv.Atoi(lower)
		upperi, _ := strconv.Atoi(upper)
		counteri, err := strconv.Atoi(counter)
		if err != nil ||
			counteri < loweri ||
			counteri > upperi {
			return false, -1
		}
		return true, counteri
	}

	// normally not possible
	return false, -1
}

// Replaces a counter wildcard in the denomination string s with the counter i.
func replaceCounter(s string, i int) string {
	// wildcard '<n>'
	if strings.Contains(s, "<n>") {
		return strings.ReplaceAll(s, "<n>", strconv.Itoa(i))
	}

	// wildcard '<m[+-a]>'
	// +-a is a delta that needs to be added/substracted from i
	regex, _ := regexp.Compile(`<m\[(\+|-)\d+\]>`)
	if regex.MatchString(s) {
		m := regex.FindString(s)
		sign := m[3:4]
		delta := m[4:strings.Index(m, "]")]
		deltai, _ := strconv.Atoi(delta)
		switch sign {
		case "+":
			return regex.ReplaceAllString(s, strconv.Itoa(i + deltai))
		case "-":
			return regex.ReplaceAllString(s, strconv.Itoa(i - deltai))
		default:
			// can't happen due to regex checks
		}
	}

	// no wildcard
	return s
}
