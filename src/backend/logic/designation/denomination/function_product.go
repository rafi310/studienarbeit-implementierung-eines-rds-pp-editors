package denomination

import (
	"rdspp.de/src/crosscutting/util"
	"rdspp.de/src/logic/designation"
)

func addMainSystemA1(node *designation.DesignationNode, data *tmpRecursionData) {
	system := node.Designation
	if len(system) != 1 { // can't really happend due to parser rules
		return
	}
	data.mainSystemA1 = system
	for _, s := range MainSystem.System {
		if s.Code == system {
			node.Denomination.DE = s.Denomination.De
			node.Denomination.EN = s.Denomination.En
			break
		}
	}
}

func addSystemA1(node *designation.DesignationNode, data *tmpRecursionData) {
	system := node.Designation
	if len(system) != 1 { // can't really happend due to parser rules
		return
	}
	mainSystem := data.mainSystemA1
	a1 := system
	data.systemA1 = a1
	for _, s := range SystemsSubsystems.Systems {
		if (util.StringArrayContains(s.Mainsystems, mainSystem) ||
			mainSystem == "") &&
			s.Code == a1 {
			node.Denomination.DE = s.Denomination.De
			node.Denomination.EN = s.Denomination.En
			break
		}
	}
}

func addSystemA2(node *designation.DesignationNode, data *tmpRecursionData) {
	system := node.Designation
	if len(system) != 1 { // can't really happend due to parser rules
		return
	}
	mainSystem := data.mainSystemA1
	a1 := data.systemA1
	a2 := system
	data.systemA2 = a2
	for _, s := range SystemsSubsystems.Systems {
		if (util.StringArrayContains(s.Mainsystems, mainSystem) ||
			mainSystem == "") &&
			s.Code == a1 {
			for _, t := range s.Systems {
				if t.Code == a2 {
					node.Denomination.DE = t.Denomination.De
					node.Denomination.EN = t.Denomination.En
					break
				}
			}
			break
		}
	}
}

func addSystemA3(node *designation.DesignationNode, data *tmpRecursionData) {
	system := node.Designation
	if len(system) != 1 { // can't really happend due to parser rules
		return
	}
	mainSystem := data.mainSystemA1
	a1 := data.systemA1
	a2 := data.systemA2
	a3 := system
	data.systemA3 = a3
	for _, s := range SystemsSubsystems.Systems {
		if (util.StringArrayContains(s.Mainsystems, mainSystem) ||
			mainSystem == "") &&
			s.Code == a1 {
			for _, t := range s.Systems {
				if t.Code == a2 {
					for _, u := range t.Systems {
						if u.Code == a3 {
							node.Denomination.DE = u.Denomination.De
							node.Denomination.EN = u.Denomination.En
							break
						}
					}
					break
				}
			}
			break
		}
	}
}

func addSubsystem(node *designation.DesignationNode, data *tmpRecursionData) {
	subsystem := node.Designation
	if len(subsystem) != 2 { // can't really happend due to parser rules
		return
	}
	mainSystem := data.mainSystemA1
	a1 := data.systemA1
	a2 := data.systemA2
	a3 := data.systemA3
	data.subsystem = subsystem
	for _, s := range SystemsSubsystems.Systems {
		if (util.StringArrayContains(s.Mainsystems, mainSystem) ||
			mainSystem == "") &&
			s.Code == a1 {
			for _, t := range s.Systems {
				if t.Code == a2 {
					for _, u := range t.Systems {
						if u.Code == a3 {
							for _, v := range u.Subsystems {
								matches, i := doesCounterMatch(v.Code, subsystem)
								if matches {
									node.Denomination.DE = replaceCounter(v.Denomination.De, i)
									node.Denomination.EN = replaceCounter(v.Denomination.En, i)
									break
								}
							}
							break
						}
					}
					break
				}
			}
			break
		}
	}
}

func addBasicFunctionA1(node *designation.DesignationNode, data *tmpRecursionData) {
	_addBasicFunctionOrProductA1(node, data, true)
}

func addBasicFunctionA2(node *designation.DesignationNode, data *tmpRecursionData) {
	_addBasicFunctionOrProductA2(node, data, true)
}

func addBasicFunctionConcrete(node *designation.DesignationNode, data *tmpRecursionData) {
	_addBasicFunctionOrProductConcrete(node, data, true)
}

func addProductClassA1(node *designation.DesignationNode, data *tmpRecursionData) {
	_addBasicFunctionOrProductA1(node, data, false)
}

func addProductClassA2(node *designation.DesignationNode, data *tmpRecursionData) {
	_addBasicFunctionOrProductA2(node, data, false)
}

func addProductClassConcrete(node *designation.DesignationNode, data *tmpRecursionData) {
	_addBasicFunctionOrProductConcrete(node, data, false)
}

func _addBasicFunctionOrProductA1(node *designation.DesignationNode, data *tmpRecursionData, basicFunction bool) {
	a1 := node.Designation
	if len(a1) != 1 { // can't really happend due to parser rules
		return
	}
	if basicFunction {
		data.basicFunctionA1 = a1
	} else {
		data.productClassA1 = a1
	}
	for _, f := range BasicFunctions.General.BasicFunctions {
		if f.Code == a1 {
			node.Denomination.DE = f.Denomination.De
			node.Denomination.EN = f.Denomination.En
			node.Examples.DE = f.Examples.De
			node.Examples.EN = f.Examples.En
			break
		}
	}
}

func _addBasicFunctionOrProductA2(node *designation.DesignationNode, data *tmpRecursionData, basicFunction bool) {
	a2 := node.Designation
	if len(a2) != 1 { // can't really happend due to parser rules
		return
	}
	var a1 string
	if basicFunction {
		a1 = data.basicFunctionA1
		data.basicFunctionA2 = a2
	} else {
		a1 = data.productClassA1
		data.productClassA2 = a2
	}
	for _, f := range BasicFunctions.General.BasicFunctions {
		if f.Code == a1 {
			for _, g := range f.BasicFunctions {
				if g.Code == a2 {
					node.Denomination.DE = g.Denomination.De
					node.Denomination.EN = g.Denomination.En
					node.Examples.DE = g.Examples.De
					node.Examples.EN = g.Examples.En
					break
				}
			}
			break
		}
	}
}

func _addBasicFunctionOrProductConcrete(node *designation.DesignationNode, data *tmpRecursionData, basiscFunction bool) {
	var function string
	if basiscFunction {
		function = data.basicFunctionA1 + data.basicFunctionA2 + node.Designation
		data.basicFunctionConcrete = function
	} else {
		function = data.basicFunctionConcrete
	}
	if len(function) != 5 { // can't really happend due to parser rules
		return
	}
	mainSystemA1 := data.mainSystemA1
	subsystem := data.systemA1 + data.systemA2 + data.systemA3 + data.subsystem
	for _, m := range BasicFunctions.BasicFunctionsProducts.MainSystems {
		if m.Code == mainSystemA1 || mainSystemA1 == "" {
			for _, s := range m.Subsystems {
				if s.Code == subsystem {
					for _, b := range s.BasicFunctions {
						if b.Code == function {
							if basiscFunction {
								node.Denomination.DE = b.Denomination.De
								node.Denomination.EN = b.Denomination.En
							} else {
								product := data.productClassA1 + data.productClassA2 + node.Designation
								data.basicFunctionConcrete = product
								for _, p := range b.Products {
									if p.Code == product {
										node.Denomination.DE = p.Denomination.De
										node.Denomination.EN = p.Denomination.En
										break
									}
								}
							}
							break
						}
					}
					break
				}
			}
			if mainSystemA1 != "" {
				break
			}
		}
	}
}
