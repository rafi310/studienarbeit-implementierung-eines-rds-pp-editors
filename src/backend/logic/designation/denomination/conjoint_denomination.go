package denomination

import (
	"fmt"
	"strconv"
	"strings"

	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/geocoding"
)

func addGeographicLocation(node *designation.DesignationNode, data *tmpRecursionData) {
	if len(node.Designation) != 11 { // can't really happen due to parser rules
		return
	}
	var (
		lat         float64
		lng         float64
		preDecimal  int
		postDecimal int
	)

	// convert lat-lon string to decimals
	preDecimal, _ = strconv.Atoi(node.Designation[0:2])
	postDecimal, _ = strconv.Atoi(node.Designation[2:4])
	lat = float64(preDecimal) + float64(postDecimal)/100
	preDecimal, _ = strconv.Atoi(node.Designation[5:8])
	postDecimal, _ = strconv.Atoi(node.Designation[8:10])
	lng = float64(preDecimal) + float64(postDecimal)/100

	// reverse geocode
	address := geocoding.GetAddress(lat, lng)
	node.Denomination.DE = "Adresse: " + address
	node.Denomination.EN = "Address: " + address
}

func addLatitude(node *designation.DesignationNode, data *tmpRecursionData) {
	latitude := node.Designation
	if len(latitude) != 5 { // can't really happen due to parser rules
		return
	}
	preDecimal := latitude[0:2]
	postDecimal := latitude[2:4]
	cardinalDirection := latitude[4:]
	latitude = fmt.Sprintf("%s.%s° %s", preDecimal, postDecimal, cardinalDirection)
	node.Denomination.DE = latitude
	node.Denomination.EN = latitude
}

func addLongitude(node *designation.DesignationNode, data *tmpRecursionData) {
	longitude := node.Designation
	if len(longitude) != 6 { // can't really happen due to parser rules
		return
	}
	preDecimal := longitude[0:3]
	postDecimal := longitude[3:5]
	cardinalDirection := longitude[5:]
	longitude = fmt.Sprintf("%s.%s° %s", preDecimal, postDecimal, cardinalDirection)
	node.Denomination.DE = longitude
	node.Denomination.EN = longitude
}

func addCountry(node *designation.DesignationNode, data *tmpRecursionData) {
	country := node.Designation
	data.country = country
	for _, c := range ConjointDesignation.Coutry {
		if c.Code == country {
			node.Denomination.DE = c.Denomination.De
			node.Denomination.EN = c.Denomination.En
			break
		}
	}
}

func addRegion(node *designation.DesignationNode, data *tmpRecursionData) {
	if data.country == "" {
		return
	}
	region := strings.ReplaceAll(node.Designation, "_", "")
	if region == SpecialCases.ConjointDesignation.Region.Supergrid.Code {
		// special case: regionless supergrid
		node.Denomination.DE = SpecialCases.ConjointDesignation.Region.Supergrid.De
		node.Denomination.EN = SpecialCases.ConjointDesignation.Region.Supergrid.En
		return
	}
	for _, c := range ConjointDesignation.Coutry {
		if c.Code == data.country {
			for _, r := range c.Regions {
				if r.Code == region {
					node.Denomination.DE = r.Denomination.De
					node.Denomination.EN = r.Denomination.En
					break
				}
			}
			break
		}
	}
}

func addTownOrName(node *designation.DesignationNode, data *tmpRecursionData) {
	node.Denomination.DE = SpecialCases.ConjointDesignation.TownOrName.De
	node.Denomination.EN = SpecialCases.ConjointDesignation.TownOrName.En
}

func addPlantType(node *designation.DesignationNode, data *tmpRecursionData) {
	pType := node.Designation
	data.plantType = pType
	for _, r := range ConjointDesignation.Resource {
		if r.Code == pType {
			node.Denomination.DE = r.Denomination.De
			node.Denomination.EN = r.Denomination.En
			break
		}
	}
}

func addFavilityType(node *designation.DesignationNode, data *tmpRecursionData) {
	if data.plantType == "" {
		return
	}
	fType := node.Designation
	for _, r := range ConjointDesignation.Resource {
		if r.Code == data.plantType {
			for _, c := range r.Character {
				if c.Code == fType {
					node.Denomination.DE = c.Denomination.De
					node.Denomination.EN = c.Denomination.En
					break
				}
			}
			break
		}
	}
}
