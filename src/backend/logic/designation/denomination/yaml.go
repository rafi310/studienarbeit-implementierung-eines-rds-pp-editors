package denomination

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
	"rdspp.de/src/crosscutting/config"
	"rdspp.de/src/crosscutting/logger"
)

var (
	ConjointDesignation *ConjointDesignationDonamination
	Signals             *SignalDonomination
	Function            *FunctionDonomination
	Dcc                 *DccDonomination
	DesignationSections *DesignationSectionsDenomination
	SpecialCases        *SpecialCasesDenomination
	MainSystem          *MainSystemDenomination
	SystemsSubsystems   *SystemsSubsystemsDenomination
	BasicFunctions      *BasicFunctionDenomination
	Hints               *HintDenominations
	Selections          *SelectionsDenomination
)

func Init() {
	ConjointDesignation = new(ConjointDesignationDonamination)
	Signals = new(SignalDonomination)
	Function = new(FunctionDonomination)
	Dcc = new(DccDonomination)
	DesignationSections = new(DesignationSectionsDenomination)
	SpecialCases = new(SpecialCasesDenomination)
	MainSystem = new(MainSystemDenomination)
	SystemsSubsystems = new(SystemsSubsystemsDenomination)
	BasicFunctions = new(BasicFunctionDenomination)
	basicFunctionsGeneral := new(BasicFunctionGeneralDenomination)
	basicFunctionsProducts := new(BasicFunctionsProductsDenomination)
	Hints = new(HintDenominations)
	Selections = new(SelectionsDenomination)

	rDenom := config.Ressources.Denomination
	var (
		filename string
		yamlFile []byte
		err      error
	)

	filename = rDenom.ConjointDenominaionFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, ConjointDesignation)
	checkErr(err, filename, 2)

	filename = rDenom.DccFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, Dcc)
	checkErr(err, filename, 2)

	filename = rDenom.DesignationSectionsFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, DesignationSections)
	checkErr(err, filename, 2)

	filename = rDenom.MainSystemsFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, MainSystem)
	checkErr(err, filename, 2)

	filename = rDenom.SystemsSubsystemsFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, SystemsSubsystems)
	checkErr(err, filename, 2)

	filename = rDenom.BasicFunctionGeneralFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, basicFunctionsGeneral)
	checkErr(err, filename, 2)

	filename = rDenom.BasicFunctionProductFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, basicFunctionsProducts)
	checkErr(err, filename, 2)

	filename = rDenom.SignalsFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, Signals)
	checkErr(err, filename, 2)

	filename = rDenom.SpecialCasesFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, SpecialCases)
	checkErr(err, filename, 2)

	filename = rDenom.HintsFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, Hints)
	checkErr(err, filename, 2)

	filename = rDenom.SelectionsFile
	yamlFile, err = ioutil.ReadFile(filename)
	checkErr(err, filename, 1)
	err = yaml.Unmarshal(yamlFile, Selections)
	checkErr(err, filename, 2)

	BasicFunctions.General = basicFunctionsGeneral
	BasicFunctions.BasicFunctionsProducts = basicFunctionsProducts
}

func checkErr(err error, filename string, i int) {
	if err != nil {
		switch i {
		case 1:
			logger.Error("reading ressource file failed: " + filename)
		case 2:
			logger.Error("unmarschal of ressource file failed: " + filename)
		}
	}
}
