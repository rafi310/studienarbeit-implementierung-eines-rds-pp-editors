package denomination

//DesignationSectionsDenomination struct
type DesignationSectionsDenomination struct {
	Designation struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"designation"`
	Prefix struct {
		De          string `yaml:"de"`
		En          string `yaml:"en"`
		Hash        string `yaml:"hash"`
		Plus        string `yaml:"plus"`
		PlusPlus    string `yaml:"plusplus"`
		Minus       string `yaml:"minus"`
		Equals      string `yaml:"equals"`
		EqualsMinus string `yaml:"equalsminus"`
		Semicolon   string `yaml:"semicolon"`
		Colon       string `yaml:"colon"`
		Ampersand   string `yaml:"ampersand"`
	} `yaml:"prefix"`
	Separator struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"separator"`
	Counter struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"counter"`
	ConjointDesignation struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"conjointDesignation"`
	GeographicLocation struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"geographicLocation"`
	Latitude struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"latitude"`
	Longitude struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"longitude"`
	ProjectSpecificInformation struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"projectSpecificInformation"`
	Country struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"country"`
	Region struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"region"`
	TownOrName struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"townOrName"`
	PlantType struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"plantType"`
	FacilityType struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"facilityType"`
	Function struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"function"`
	MainSystem struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"mainSystem"`
	MainSystemA1 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"mainSystemA1"`
	SystemSubsystem struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"systemSubsystem"`
	SystemA1 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"systemA1"`
	SystemA2 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"systemA2"`
	SystemA3 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"systemA3"`
	Subsystem struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"subsystem"`
	BasicFunction struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"basicFunction"`
	BasicFunctionA1 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"basicFunctionA1"`
	BasicFunctionA2 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"basicFunctionA2"`
	BasicFunctionConcrete struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"basicFunctionConcrete"`
	Product struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"product"`
	ProductClass struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"productClass"`
	ProductClassA1 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"productClassA1"`
	ProductClassA2 struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"productClassA2"`
	ProductClassConcrete struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"productClassConcrete"`
	OperatingEquipment struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"operatingEquipment"`
	PointOfInstallation struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"pointOfInstallation"`
	PointOfInstallationPlace struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"pointOfInstallationPlace"`
	Column struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"column"`
	Row struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"row"`
	SiteOfInstallation struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"siteOfInstallation"`
	SiteOfInstallationPlace struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"siteOfInstallationPlace"`
	Terminal struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"terminal"`
	TerminalPoint struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"terminalPoint"`
	Signal struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"signal"`
	SignalClass struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"signalClass"`
	SignalMainClass struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"signalMainClass"`
	SignalSubclass struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"signalSubclass"`
	SignalInformation struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"signalInformation"`
	SignalDuration struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"signalDuration"`
	SignalType struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"signalType"`
	Document struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"document"`
	Dcc struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"dcc"`
	TechnicalSector struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"technicalSector"`
	DccMainClass struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"dccMainClass"`
	DccSubclass struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"dccSubclass"`
	DccConcrete struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"dccConcrete"`
	PageCountNumber struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"pageCountNumber"`
	PageNumberAlpha struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"pageNumberAlpha"`
	PageNumber struct {
		De string `yaml:"de"`
		En string `yaml:"en"`
	} `yaml:"pageNumber"`
	End struct {
		De string `yaml:"de" json:"de"`
		En string `yaml:"en" json:"en"`
	} `yaml:"end"`
}

//ConjointDesignationDonamination  struct
type ConjointDesignationDonamination struct {
	Resource []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
		Character []struct {
			Code         string `yaml:"code" json:"code"`
			Denomination struct {
				De string `yaml:"de" json:"de"`
				En string `yaml:"en" json:"en"`
			} `yaml:"denomination" json:"denomination"`
		}
	}
	Coutry []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
		Regions []struct {
			Code         string `yaml:"code" json:"code"`
			Denomination struct {
				De string `yaml:"de" json:"de"`
				En string `yaml:"en" json:"en"`
			} `yaml:"denomination" json:"denomination"`
		}
	}
}

//SignalDonomination saves signal values
type SignalDonomination struct {
	MainClass []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `yaml:"mainClass"`
	Subclass []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `yaml:"subclass"`
	Class []struct {
		MainClass     string `yaml:"mainClass"`
		ConcreteClass []struct {
			Code         string `yaml:"code" json:"code"`
			Denomination struct {
				De string `yaml:"de" json:"de"`
				En string `yaml:"en" json:"en"`
			} `yaml:"denomination" json:"denomination"`
		} `yaml:"concreteClass"`
	} `yaml:"class"`
	Duration []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `yaml:"duration"`
	Types []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `yaml:"types"`
}

//DccDonomination struct
type DccDonomination struct {
	PageNumberAlpha []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `yaml:"pageNumberAlpha" json:"pageNumberAlpha"`
	TechnicalSector []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `yaml:"technicalSector" json:"technicalSector"`
	DocumentClass []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
		Subclasses []struct {
			Code         string `yaml:"code" json:"code"`
			Denomination struct {
				De string `yaml:"de" json:"de"`
				En string `yaml:"en" json:"en"`
			} `yaml:"denomination" json:"denomination"`
			Examples struct {
				De []string `yaml:"de" json:"de"`
				En []string `yaml:"en" json:"en"`
			} `yaml:"examples" json:"examples"`
			Numbers []struct {
				Code         string `yaml:"code" json:"code"`
				Denomination struct {
					De string `yaml:"de" json:"de"`
					En string `yaml:"en" json:"en"`
				} `yaml:"denomination" json:"denomination"`
			} `yaml:"numbers" json:"numbers"`
		} `yaml:"subclasses" json:"subclasses"`
	} `yaml:"documentClass" json:"documentClass"`
}

type MainSystemDenomination struct {
	System []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `json:"system"`
}

//SelectionsDenomination struct
type SelectionsDenomination struct {
	Selections []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `yaml:"selections" json:"selections"`
}

//FunctionDonomination struct
type FunctionDonomination struct {
	Systems []struct {
		Code         string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
		Systems []struct {
			Code         string `yaml:"code" json:"code"`
			Denomination struct {
				De string `yaml:"de" json:"de"`
				En string `yaml:"en" json:"en"`
			} `yaml:"denomination" json:"denomination"`
			Subsystems []struct {
				Code         string `yaml:"code" json:"code"`
				Denomination struct {
					De string `yaml:"de" json:"de"`
					En string `yaml:"en" json:"en"`
				} `yaml:"denomination" json:"denomination"`
				Basicfunctions []struct {
					Code         string `yaml:"code" json:"code"`
					Denomination struct {
						De string `yaml:"de" json:"de"`
						En string `yaml:"en" json:"en"`
					} `yaml:"denomination" json:"denomination"`
					Products []struct {
						Code         string `yaml:"code" json:"code"`
						Denomination struct {
							De string `yaml:"de" json:"de"`
							En string `yaml:"en" json:"en"`
						} `yaml:"denomination" json:"denomination"`
					}
				}
			}
		}
		Subsystems []struct {
			Code         string `yaml:"code" json:"code"`
			Denomination struct {
				De string `yaml:"de" json:"de"`
				En string `yaml:"en" json:"en"`
			} `yaml:"denomination" json:"denomination"`
			Basicfunctions []struct {
				Code         string `yaml:"code" json:"code"`
				Denomination struct {
					De string `yaml:"de" json:"de"`
					En string `yaml:"en" json:"en"`
				} `yaml:"denomination" json:"denomination"`
				Products []struct {
					Code         string `yaml:"code" json:"code"`
					Denomination struct {
						De string `yaml:"de" json:"de"`
						En string `yaml:"en" json:"en"`
					} `yaml:"denomination" json:"denomination"`
				}
			}
		}
	}
}

//SpecialCasesDenomination struct
type SpecialCasesDenomination struct {
	ConjointDesignation struct {
		Region struct {
			Supergrid struct {
				Code string `yaml:"code"`
				De   string `yaml:"de"`
				En   string `yaml:"en"`
			} `yaml:"supergrid"`
		} `yaml:"region"`
		TownOrName struct {
			De string `yaml:"de"`
			En string `yaml:"en"`
		} `yaml:"townOrName"`
	} `yaml:"conjointDesignation"`
}

type SystemsSubsystemsDenomination struct {
	Systems []struct {
		Code         string   `yaml:"code" json:"code"`
		Mainsystems  []string `yaml:"mainsystems"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
		Systems []struct {
			Code         string `yaml:"code" json:"code"`
			Denomination struct {
				De string `yaml:"de" json:"de"`
				En string `yaml:"en" json:"en"`
			} `yaml:"denomination" json:"denomination"`
			Systems []struct {
				Code         string `yaml:"code" json:"code"`
				Denomination struct {
					De string `yaml:"de" json:"de"`
					En string `yaml:"en" json:"en"`
				} `yaml:"denomination" json:"denomination"`
				Subsystems []struct {
					Code         string `yaml:"code" json:"code"`
					Denomination struct {
						De string `yaml:"de" json:"de"`
						En string `yaml:"en" json:"en"`
					} `yaml:"denomination" json:"denomination"`
				} `yaml:"subsystems"`
			} `yaml:"systems"`
		} `yaml:"systems"`
	} `yaml:"systems"`
}

type BasicFunctionDenomination struct {
	General                *BasicFunctionGeneralDenomination
	BasicFunctionsProducts *BasicFunctionsProductsDenomination
}

type BasicFunctionGeneralDenomination struct {
	BasicFunctions []struct {
		Code         string `yaml:"code"`
		Denomination struct {
			De string `yaml:"de"`
			En string `yaml:"en"`
		} `yaml:"denomination"`
		Examples struct {
			De []string `yaml:"de"`
			En []string `yaml:"en"`
		} `yaml:"examples"`
		BasicFunctions []struct {
			Code         string `yaml:"code"`
			Denomination struct {
				De string `yaml:"de"`
				En string `yaml:"en"`
			} `yaml:"denomination"`
			Examples struct {
				De []string `yaml:"de"`
				En []string `yaml:"en"`
			} `yaml:"examples"`
		} `yaml:"basicFunctions"`
	} `yaml:"basicFunctions"`
}

type BasicFunctionsProductsDenomination struct {
	MainSystems []struct {
		Code       string `yaml:"code"`
		Subsystems []struct {
			Code           string `yaml:"code"`
			BasicFunctions []struct {
				Code         string `yaml:"code" json:"code"`
				Denomination struct {
					De string `yaml:"de" json:"de"`
					En string `yaml:"en" json:"en"`
				} `yaml:"denomination" json:"denomination"`
				Products []struct {
					Code         string `yaml:"code" json:"code"`
					Denomination struct {
						De string `yaml:"de" json:"de"`
						En string `yaml:"en" json:"en"`
					} `yaml:"denomination" json:"denomination"`
				} `yaml:"products"`
			} `yaml:"basicFunctions"`
		} `yaml:"subsystems"`
	} `yaml:"mainSystems"`
}

type HintDenomination struct {
	Name struct {
		DE string `yaml:"de" json:"de"`
		EN string `yaml:"en" json:"en"`
	} `yaml:"name" json:"name"`
	BreakdownLevels []struct {
		BL           int `yaml:"bl" json:"bl"`
		Denomination struct {
			DE string `yaml:"de" json:"de"`
			EN string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
		DataPositions []string `yaml:"dataPositions" json:"dataPositions"`
		Explanations  []struct {
			Start int    `yaml:"start" json:"start"`
			End   int    `yaml:"end" json:"end"`
			DE    string `yaml:"de" json:"de"`
			EN    string `yaml:"en" json:"en"`
		} `yaml:"explanations" json:"explanations"`
		Optional bool `yaml:"optional" json:"optional"`
	} `yaml:"breakdownLevels" json:"breakdownLevels"`
}

// Equals compares where two HintDenomination's are equal regarding the values of their field
// (value only check; no reference check)
func (hint *HintDenomination) Equals(other *HintDenomination) bool {
	if hint.Name.DE != other.Name.DE ||
		hint.Name.EN != hint.Name.EN ||
		len(hint.BreakdownLevels) != len(other.BreakdownLevels) {
		return false
	}
	for b := 0; b < len(hint.BreakdownLevels); b++ {
		if hint.BreakdownLevels[b].BL != other.BreakdownLevels[b].BL ||
			hint.BreakdownLevels[b].Denomination.DE != other.BreakdownLevels[b].Denomination.DE ||
			hint.BreakdownLevels[b].Denomination.EN != other.BreakdownLevels[b].Denomination.EN ||
			hint.BreakdownLevels[b].Optional != other.BreakdownLevels[b].Optional ||
			len(hint.BreakdownLevels[b].DataPositions) != len(other.BreakdownLevels[b].DataPositions) ||
			len(hint.BreakdownLevels[b].Explanations) != len(other.BreakdownLevels[b].Explanations) {
			return false
		}
		for i := 0; i < len(hint.BreakdownLevels[b].DataPositions); i++ {
			if hint.BreakdownLevels[b].DataPositions[i] != other.BreakdownLevels[b].DataPositions[i] {
				return false
			}
		}
		for j := 0; j < len(hint.BreakdownLevels[b].Explanations); j++ {
			if hint.BreakdownLevels[b].Explanations[j].DE != other.BreakdownLevels[b].Explanations[j].DE ||
				hint.BreakdownLevels[b].Explanations[j].EN != other.BreakdownLevels[b].Explanations[j].EN ||
				hint.BreakdownLevels[b].Explanations[j].Start != other.BreakdownLevels[b].Explanations[j].Start ||
				hint.BreakdownLevels[b].Explanations[j].End != other.BreakdownLevels[b].Explanations[j].End {
				return false
			}
		}
	}
	return true
}

type HintDenominations struct {
	Hints []HintDenomination `yaml:"hints"`
}
