package designation

type DesignationSection int

func (s DesignationSection) ToInt() int {
	return int(s)
}

const (
	// undefinierter Kennzeichenabschnitt
	U_Undefined DesignationSection = iota

	// Kennzeichen
	D_Designation

	// Vorzeichen
	PRE_Prefix

	// Trennzeichen
	SEP_Seperator

	// # Gemeinsame Zuordnung
	CD_ConjointDesignation
	CD_ExistingDesignation
	CD_GeographicLocation
	CD_Latitude
	CD_Longitude
	CD_ProjectSpecificInformation
	CD_Country
	CD_Region
	CD_TownOrName
	CD_ProjectCounter
	CD_PlantType
	CD_FacilityType

	// = Funktion
	F_FunctionDesignation
	F_MainSystem
	F_MainSystemA1
	F_MainSystemCounter
	F_SystemSubsystem
	F_SystemA1
	F_SystemA2
	F_SystemA3
	F_Subsystem
	F_BasicFunction
	F_BasicFunctionA1
	F_BasicFunctionA2
	F_BasicFunctionConcrete

	// - Produktklasse
	P_ProductClassDesignation
	P_ProductClass
	P_ProductClassA1
	P_ProductClassA2
	P_ProductClassConcrete

	// =- Betriebsmittel
	OE_OperatingEquipmentDesignation

	// + Einbauort
	PI_PointOfInstallationDesignation
	PI_PointOfInstallationSelection1
	PI_PointOfInstallationSelection2
	PI_PointOfInstallation
	PI_Column
	PI_Row

	// ++ Aufstellungsort
	SI_SiteOfInstallationDesignation
	SI_SiteOfInstallationSubsystemSelection
	SI_SiteOfInstallationSelection
	SI_SiteOfInstallationGeolocation
	SI_SiteOfInstallation

	// : Anschluss
	T_TerminalDesignation
	T_TerminalAssembly
	T_TerminalPoint

	// ; Signal
	S_SignalDesignation
	S_SignalClass
	S_SignalMainClass
	S_SignalSubclass
	S_SignalClassCounter
	S_SignalInformation
	S_SignalDuration
	S_SignalType
	S_SignalInformationCounter

	// & Dokument
	D_DocumentDesignation
	D_DocumentDcc
	D_DocumentDccTechnicalSector
	D_DocumentDccMainClass
	D_DocumentDccSubclass
	D_DocumentDccConcrete
	D_DocumentPageCountNumber
	D_DocumentPageNumberAlpha
	D_DocumentPageNumber

	// Terminate
	E_End
)
