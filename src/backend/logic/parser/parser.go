package parser

import (
	"fmt"
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"rdspp.de/src/crosscutting/config"
	"rdspp.de/src/crosscutting/util"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/designation/denomination"
	"rdspp.de/src/logic/parser/parser"
)

type RdsppParser struct {
	designation     string
	currentNode     *designation.DesignationNode
	parserRuleStack []int
	parentsStack    []*designation.DesignationNode

	// Deprecated
	Tokens string
	// Deprecated
	AST string
	// Deprecated
	ListenerWalk string
	Tree         *designation.DesignationNode
	allErrorPos  []int
	Error        *ErrorInformation
	Hint         *denomination.HintDenomination
}

func RemoveWhitespace(designation string) string {
	return strings.ToUpper(strings.TrimSpace(strings.ReplaceAll(designation, " ", "")))
}

func NewRdsppParser() *RdsppParser {
	return new(RdsppParser)
}

func (rdsppParser *RdsppParser) ParseDesignation(rdsppDesignation string) {
	rdsppParser.designation = strings.ToUpper(rdsppDesignation)
	rdsppParser.getTokens()
	rdsppParser.getAST()
	rdsppParser.walk()
	if rdsppParser.Error != nil {
		rdsppParser.tryToGetPartialTree()
	}
}

func (rdsppParser *RdsppParser) IsValid(rdsppDesignation string) bool {
	if rdsppParser.designation != rdsppDesignation || rdsppParser.Tree == nil {
		rdsppParser.Error = nil
		rdsppParser.Hint = nil
		rdsppParser.ParseDesignation(rdsppDesignation)
	}
	return rdsppParser.Error == nil
}

func (rdsppParser *RdsppParser) IsUnambiguous(rdsppDesignation string) bool {
	// run parser
	if rdsppParser.designation != rdsppDesignation || rdsppParser.Tree == nil {
		rdsppParser.Error = nil
		rdsppParser.Hint = nil
		rdsppParser.ParseDesignation(rdsppDesignation)
	}
	tree := rdsppParser.Tree

	// designation must be valid and tree have at least one child
	if rdsppParser.Error != nil ||
		tree.Childs == nil ||
		len(tree.Childs) < 1 {
		return false
	}

	// test if the node type of the direct childs of root matches sections
	test := func(root *designation.DesignationNode, sections ...int) bool {
		if len(root.Childs) != len(sections) {
			return false
		}
		for i := 0; i < len(sections); i++ {
			if root.Childs[i].Section.ToInt() != sections[i] {
				return false
			}
		}
		return true
	}

	// short vars
	conjoint := designation.CD_ConjointDesignation.ToInt()
	document := designation.D_DocumentDesignation.ToInt()
	function := designation.F_FunctionDesignation.ToInt()
	operatingEquipment := designation.OE_OperatingEquipmentDesignation.ToInt()
	pointOfInstallation := designation.PI_PointOfInstallationDesignation.ToInt()
	siteOfInstallation := designation.SI_SiteOfInstallationDesignation.ToInt()
	signal := designation.S_SignalDesignation.ToInt()
	terminal := designation.T_TerminalDesignation.ToInt()

	// test all unambiquous combinations (corresponds to designation-rule in RDSPP.g4)
	return false ||
		test(tree, conjoint) ||
		test(tree, conjoint, document) ||
		test(tree, conjoint, function) ||
		test(tree, conjoint, function, document) ||
		test(tree, conjoint, function, signal) ||
		test(tree, conjoint, operatingEquipment) ||
		test(tree, conjoint, operatingEquipment, document) ||
		test(tree, conjoint, operatingEquipment, terminal) ||
		test(tree, conjoint, pointOfInstallation) ||
		test(tree, conjoint, pointOfInstallation, document) ||
		test(tree, conjoint, siteOfInstallation) ||
		test(tree, conjoint, siteOfInstallation, document)
}

func (rdsppParser *RdsppParser) getTokens() {
	is := antlr.NewInputStream(rdsppParser.designation)
	lexer := parser.NewRDSPPLexer(is)
	for {
		t := lexer.NextToken()
		if t.GetTokenType() == antlr.TokenEOF {
			break
		}
		rdsppParser.Tokens += fmt.Sprintf("%s ─> %s\n", t.GetText(), lexer.SymbolicNames[t.GetTokenType()])
	}
}

func (rdsppParser *RdsppParser) getAST() {
	is := antlr.NewInputStream(rdsppParser.designation)
	lexer := parser.NewRDSPPLexer(is)
	tokenStream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	antlrParser := parser.NewRDSPPParser(tokenStream)
	antlrParser.RemoveErrorListeners()

	ctx := antlrParser.Start()
	recognizer := ctx.GetParser()
	rdsppParser.AST = ctx.ToStringTree(recognizer.GetRuleNames(), recognizer)
	rdsppParser.AST = indentLispTree(rdsppParser.AST)
}

func (rdsppParser *RdsppParser) getASTPartially(startSection designation.DesignationSection) {
	is := antlr.NewInputStream(rdsppParser.designation)
	lexer := parser.NewRDSPPLexer(is)
	tokenStream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	antlrParser := parser.NewRDSPPParser(tokenStream)
	antlrParser.RemoveErrorListeners()
	var (
		startRuleCtx antlr.ParserRuleContext
		recognizer   antlr.Parser
	)
	switch startSection {
	case designation.CD_ConjointDesignation:
		ctx := antlrParser.StartOnlyConjointDesignation()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.F_FunctionDesignation:
		ctx := antlrParser.StartOnlyFunction()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.OE_OperatingEquipmentDesignation:
		ctx := antlrParser.StartOnlyOperatingEquipment()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.PI_PointOfInstallationDesignation:
		ctx := antlrParser.StartOnlyPointOfInstallation()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.SI_SiteOfInstallationDesignation:
		ctx := antlrParser.StartOnlySiteOfInstallation()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.P_ProductClassDesignation:
		ctx := antlrParser.StartOnlyProduct()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.T_TerminalDesignation:
		ctx := antlrParser.StartOnlyTerminal()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.S_SignalDesignation:
		ctx := antlrParser.StartOnlySignal()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	case designation.D_DocumentDesignation:
		ctx := antlrParser.StartOnlyDocument()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	default:
		ctx := antlrParser.Start()
		recognizer = ctx.GetParser()
		startRuleCtx = ctx
	}
	rdsppParser.AST = startRuleCtx.ToStringTree(recognizer.GetRuleNames(), recognizer)
	rdsppParser.AST = indentLispTree(rdsppParser.AST)
}

func (rdsppParser *RdsppParser) walk() {
	is := antlr.NewInputStream(rdsppParser.designation)
	lexer := parser.NewRDSPPLexer(is)
	tokenStream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	antlrParser := parser.NewRDSPPParser(tokenStream)
	if !config.IsDebug {
		// prevent printing every error to console in production
		antlrParser.RemoveErrorListeners()
	}
	antlrParser.AddErrorListener(&errorListener{rdsppParser: rdsppParser})
	rdsppParser.ListenerWalk = `                                Step    Designation
--------------------------------------------------------------------------------------------------------------------------------
`
	antlr.NewParseTreeWalker().Walk(&treeListener{rdsppParser: rdsppParser}, antlrParser.Start())
}

func (rdsppParser *RdsppParser) walkPartially(startSection designation.DesignationSection) {
	is := antlr.NewInputStream(rdsppParser.designation)
	lexer := parser.NewRDSPPLexer(is)
	tokenStream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	antlrParser := parser.NewRDSPPParser(tokenStream)
	if !config.IsDebug {
		// prevent printing every error to console in production
		antlrParser.RemoveErrorListeners()
	}
	antlrParser.AddErrorListener(&errorListener{rdsppParser: rdsppParser})
	var (
		startRuleCtx antlr.ParserRuleContext
		rootRule     int
	)
	switch startSection {
	case designation.CD_ConjointDesignation:
		startRuleCtx = antlrParser.StartOnlyConjointDesignation()
		rootRule = parser.RDSPPParserRULE_conjointDesignation
	case designation.F_FunctionDesignation:
		startRuleCtx = antlrParser.StartOnlyFunction()
		rootRule = parser.RDSPPParserRULE_function
	case designation.OE_OperatingEquipmentDesignation:
		startRuleCtx = antlrParser.StartOnlyOperatingEquipment()
		rootRule = parser.RDSPPParserRULE_operatingEquipment
	case designation.PI_PointOfInstallationDesignation:
		startRuleCtx = antlrParser.StartOnlyPointOfInstallation()
		rootRule = parser.RDSPPParserRULE_pointOfInstallation
	case designation.SI_SiteOfInstallationDesignation:
		startRuleCtx = antlrParser.StartOnlySiteOfInstallation()
		rootRule = parser.RDSPPParserRULE_siteOfInstallation
	case designation.P_ProductClassDesignation:
		startRuleCtx = antlrParser.StartOnlyProduct()
		rootRule = parser.RDSPPParserRULE_product
	case designation.T_TerminalDesignation:
		startRuleCtx = antlrParser.StartOnlyTerminal()
		rootRule = parser.RDSPPParserRULE_terminal
	case designation.S_SignalDesignation:
		startRuleCtx = antlrParser.StartOnlySignal()
		rootRule = parser.RDSPPParserRULE_signal
	case designation.D_DocumentDesignation:
		startRuleCtx = antlrParser.StartOnlyDocument()
		rootRule = parser.RDSPPParserRULE_document
	default:
		startRuleCtx = antlrParser.Start()
		rootRule = parser.RDSPPParserRULE_designation
	}
	antlr.NewParseTreeWalker().Walk(
		&treeListener{rdsppParser: rdsppParser, rootRule: rootRule},
		startRuleCtx,
	)
}

func (rdsppParser *RdsppParser) tryToGetPartialTree() {
	rdsppParser.Tree.ContainsError = true
	// split designation into its parts
	designationParts := splitErroneousDesignationByPrefix(rdsppParser.designation, rdsppParser.allErrorPos)
	if len(designationParts) == 0 {
		return
	}

	// identify erroneous part
	var erroneousPartIndexes []int
	cummulatedLength := 0
	for i, p := range designationParts {
		cummulatedLength += len(p.Designation)
		if rdsppParser.Error.Index < cummulatedLength {
			erroneousPartIndexes = append(erroneousPartIndexes, i)
		}
	}

	// set top-level childs part by part
	rdsppParser.Tree.Childs = make([]*designation.DesignationNode, len(designationParts))
	for i, p := range designationParts {
		helperParser1 := NewRdsppParser()
		helperParser1.designation = strings.ToUpper(p.Designation)
		helperParser1.getASTPartially(p.Type)
		helperParser1.walkPartially(p.Type)
		if util.IntArrayContains(erroneousPartIndexes, i) {
			if i == erroneousPartIndexes[0] && helperParser1.Error != nil {
				// use (probably) better error message
				rdsppParser.Error.Message = helperParser1.Error.Message
				rdsppParser.Error.Type = helperParser1.Error.Type
				rdsppParser.Error.Suggestions = helperParser1.Error.Suggestions
			}
			if helperParser1.Tree != nil {
				// if the erroneous part could be partially parsed this can be used
				rdsppParser.Tree.Childs[i] = helperParser1.Tree
			} else {
				// otherwise we create a new node that only describes the kind of designation
				rdsppParser.Tree.Childs[i] = &designation.DesignationNode{Section: p.Type}
			}
			helperParser2 := NewRdsppParser()
			helperParser2.designation = p.Designation
			helperParser2.walk()
			rdsppParser.Tree.Childs[i].SetErroneousRecursive(helperParser2.allErrorPos)
			rdsppParser.Tree.Childs[i].ContainsError = len(helperParser2.allErrorPos) > 0
		} else {
			// a non-erroneous part can be taken as-is
			rdsppParser.Tree.Childs[i] = helperParser1.Tree
		}
	}
	return
}

func splitErroneousDesignationByPrefix(rdsppDesignation string, allErrorPos []int) []designation.DesignationTuple {
	parts := make([]designation.DesignationTuple, 0)
	var (
		n               = len(rdsppDesignation)
		currectPartType designation.DesignationSection
		part            string
		partStart       int
		ignoreNext      bool
		concludePart    = func() {
			if len(part) > 0 {
				part = strings.TrimSpace(part)
				parts = append(parts, designation.DesignationTuple{
					Designation: part,
					Type:        currectPartType,
				})
				part = ""
			}
		}
	)
	for i, c := range rdsppDesignation {
		if ignoreNext {
			ignoreNext = false
			continue
		}
		switch c {
		case '#':
			concludePart()
			currectPartType = designation.CD_ConjointDesignation
			part = "#"
			partStart = i
		case '=':
			concludePart()
			currectPartType = designation.F_FunctionDesignation
			part = "="
			partStart = i
		case '-':
			if currectPartType == designation.F_FunctionDesignation && len(part) > 0 { // operation equipment (=-)
				// check if errorPos is inside the operating equipment designation
				// -> if yes: take function (=) and product (-) seperately
				// -> if no: take them together (=-)
				isInside := false
				for _, errorPos := range allErrorPos {
					if isInside {
						break
					}
					if errorPos < partStart { // errorPos is outside
						continue
					} else {
						for k := i + 1; k < n; k++ {
							if rdsppDesignation[k] == ':' || rdsppDesignation[k] == '&' {
								if errorPos <= k { // errorPos is inside
									isInside = true
								} // else: errorPos is outside
								break
							}
							if k == n-1 { // errorPos is inside
								isInside = true
							}
						}
					}
				}
				if isInside {
					concludePart()
					currectPartType = designation.P_ProductClassDesignation
					part = "-"
					partStart = i
				} else {
					currectPartType = designation.OE_OperatingEquipmentDesignation
					part += "-"
				}
			} else { // only product (-)
				concludePart()
				currectPartType = designation.P_ProductClassDesignation
				part = "-"
				partStart = i
			}
		case '+':
			concludePart()
			if i < n-1 && rdsppDesignation[i+1] == '+' {
				currectPartType = designation.SI_SiteOfInstallationDesignation
				part = "++"
				ignoreNext = true
			} else {
				currectPartType = designation.PI_PointOfInstallationDesignation
				part = "+"
			}
			partStart = i
		case ':':
			concludePart()
			currectPartType = designation.T_TerminalDesignation
			part = ":"
			partStart = i
		case ';':
			concludePart()
			currectPartType = designation.S_SignalDesignation
			part = ";"
			partStart = i
		case '&':
			concludePart()
			currectPartType = designation.D_DocumentDesignation
			part = "&"
			partStart = i
		default:
			part += string(c)
		}
	}
	concludePart()
	return parts
}
