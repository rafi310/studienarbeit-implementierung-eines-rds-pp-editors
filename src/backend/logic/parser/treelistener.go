package parser

import (
	"fmt"
	"regexp"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"rdspp.de/src/logic/designation"
	"rdspp.de/src/logic/parser/parser"
)

type treeListener struct {
	*parser.BaseRDSPPListener
	rdsppParser *RdsppParser
	rootRule    int
}

var ruleNames []string

func (listener *treeListener) EnterStart(c *parser.StartContext) {
	ruleNames = c.GetParser().GetRuleNames()
}

func (listener *treeListener) VisitTerminal(t antlr.TerminalNode) {
	tokenType := t.GetSymbol().GetTokenType()
	var ruleName string
	switch tokenType {
	case parser.RDSPPParserHASH,
		parser.RDSPPParserEQUALS,
		parser.RDSPPParserPLUS,
		parser.RDSPPParserPLUSPLUS,
		parser.RDSPPParserMINUS,
		parser.RDSPPParserCOLON,
		parser.RDSPPParserSEMICOLON,
		parser.RDSPPParserAMPERSAND:
		ruleName = "prefix"
	case parser.RDSPPParserDOT,
		parser.RDSPPParserSLASH:
		ruleName = "separator"
	default:
		return
	}
	listener.rdsppParser.ListenerWalk += fmt.Sprintf(" >  %32s    %s\n", ruleName, t.GetText())

	// add node (leave) and directly go back to parent, because this is a terminal and thus has no childs
	listener.rdsppParser.setChild(ruleName, t.GetText())
	listener.rdsppParser.goToParentNode()
}

func (listener *treeListener) EnterEveryRule(c antlr.ParserRuleContext) {
	rule := c.GetRuleIndex()
	var ruleName string
	switch rule {
	case parser.RDSPPParserRULE_start,
		parser.RDSPPParserRULE_startOnlyConjointDesignation,
		parser.RDSPPParserRULE_startOnlyFunction,
		parser.RDSPPParserRULE_startOnlyOperatingEquipment,
		parser.RDSPPParserRULE_startOnlyPointOfInstallation,
		parser.RDSPPParserRULE_startOnlySiteOfInstallation,
		parser.RDSPPParserRULE_startOnlyProduct,
		parser.RDSPPParserRULE_startOnlySignal,
		parser.RDSPPParserRULE_startOnlyTerminal,
		parser.RDSPPParserRULE_startOnlyDocument:
		// ruleNames get initializes in EnterStart (see above) and is thus not available yet
		ruleName = "start"
	case parser.RDSPPParserRULE_systemSubsystemPart:
		ruleName = ruleNames[parser.RDSPPParserRULE_systemSubsystem]
	default:
		ruleName = ruleNames[rule]
	}
	listener.rdsppParser.ListenerWalk += fmt.Sprintf(" >  %32s    %s\n", ruleName, c.GetText())

	switch rule {
	case parser.RDSPPParserRULE_start,
		parser.RDSPPParserRULE_startOnlyConjointDesignation,
		parser.RDSPPParserRULE_startOnlyFunction,
		parser.RDSPPParserRULE_startOnlyOperatingEquipment,
		parser.RDSPPParserRULE_startOnlyPointOfInstallation,
		parser.RDSPPParserRULE_startOnlySiteOfInstallation,
		parser.RDSPPParserRULE_startOnlyProduct,
		parser.RDSPPParserRULE_startOnlySignal,
		parser.RDSPPParserRULE_startOnlyTerminal,
		parser.RDSPPParserRULE_startOnlyDocument,
		parser.RDSPPParserRULE_alpha,
		parser.RDSPPParserRULE_underscore_alpha:
		break

	case listener.rootRule,
		parser.RDSPPParserRULE_designation:
		listener.rdsppParser.setRoot(ruleName)
		listener.rdsppParser.parserRuleStack = append(listener.rdsppParser.parserRuleStack, rule)

	case parser.RDSPPParserRULE_systemSubsystem:
		if listener.rdsppParser.parserRuleStack[len(listener.rdsppParser.parserRuleStack)-1] == parser.RDSPPParserRULE_systemSubsystemPart {
			// ignore
		} else {
			listener.rdsppParser.setChild(ruleName, c.GetText())
		}

	default:
		listener.rdsppParser.setChild(ruleName, c.GetText())
	}
	listener.rdsppParser.parserRuleStack = append(listener.rdsppParser.parserRuleStack, rule)
}

func (listener *treeListener) ExitEveryRule(c antlr.ParserRuleContext) {
	ruleName := ruleNames[c.GetRuleIndex()]
	listener.rdsppParser.ListenerWalk += fmt.Sprintf("<   %32s    %s\n", ruleName, c.GetText())

	switch c.GetRuleIndex() {
	case parser.RDSPPParserRULE_start,
		parser.RDSPPParserRULE_startOnlyConjointDesignation,
		parser.RDSPPParserRULE_startOnlyFunction,
		parser.RDSPPParserRULE_startOnlyOperatingEquipment,
		parser.RDSPPParserRULE_startOnlyPointOfInstallation,
		parser.RDSPPParserRULE_startOnlySiteOfInstallation,
		parser.RDSPPParserRULE_startOnlyProduct,
		parser.RDSPPParserRULE_startOnlySignal,
		parser.RDSPPParserRULE_startOnlyTerminal,
		parser.RDSPPParserRULE_startOnlyDocument,
		parser.RDSPPParserRULE_alpha,
		parser.RDSPPParserRULE_underscore_alpha:
		break

	case listener.rootRule,
		parser.RDSPPParserRULE_designation:
		listener.rdsppParser.currentNode = nil

	case parser.RDSPPParserRULE_systemSubsystem:
		if listener.rdsppParser.parserRuleStack[len(listener.rdsppParser.parserRuleStack)-2] == parser.RDSPPParserRULE_systemSubsystemPart {
			// ignore
		} else {
			listener.rdsppParser.goToParentNode()
		}
		break

	default:
		listener.rdsppParser.goToParentNode()
	}
	listener.rdsppParser.parserRuleStack = listener.rdsppParser.parserRuleStack[:len(listener.rdsppParser.parserRuleStack)-1]
}

func (rdsppParser *RdsppParser) setRoot(ruleName string) {
	p := rdsppParser
	t := new(designation.DesignationNode)
	t.Section = rulenameToSectionCode(ruleName)
	t.Designation = p.designation
	rdsppParser.Tree = t
	p.currentNode = t
}

func (rdsppParser *RdsppParser) setChild(ruleName, text string) {
	p := rdsppParser
	n := new(designation.DesignationNode)
	n.Section = rulenameToSectionCode(ruleName)
	text = regexp.MustCompile("<.*>").ReplaceAllString(text, "") // replace e.g. <missing XY> which ANTLR sometimes puts in a nodes text for some reason
	n.Designation = text
	p.parentsStack = append(p.parentsStack, p.currentNode)
	if p.currentNode.Childs == nil {
		p.currentNode.Childs = make([]*designation.DesignationNode, 0)
	}
	p.currentNode.Childs = append(p.currentNode.Childs, n)
	p.currentNode = n
}

func (rdsppParser *RdsppParser) goToParentNode() {
	p := rdsppParser
	lastIndex := len(p.parentsStack) - 1
	if lastIndex >= 0 {
		p.currentNode = p.parentsStack[lastIndex]
		p.parentsStack = p.parentsStack[:lastIndex]
	}
}

func rulenameToSectionCode(rule string) designation.DesignationSection {
	switch rule {
	case "designation":
		return designation.D_Designation
	case "prefix":
		return designation.PRE_Prefix
	case "separator":
		return designation.SEP_Seperator
	case "conjointDesignation":
		return designation.CD_ConjointDesignation
	case "geographicLocation":
		return designation.CD_GeographicLocation
	case "latitude":
		return designation.CD_Latitude
	case "longitude":
		return designation.CD_Longitude
	case "projectSpecificInformation":
		return designation.CD_ProjectSpecificInformation
	case "country":
		return designation.CD_Country
	case "region":
		return designation.CD_Region
	case "townOrName":
		return designation.CD_TownOrName
	case "projectCounter":
		return designation.CD_ProjectCounter
	case "plantType":
		return designation.CD_PlantType
	case "facilityType":
		return designation.CD_FacilityType
	case "function":
		return designation.F_FunctionDesignation
	case "mainSystem":
		return designation.F_MainSystem
	case "mainSystemA1":
		return designation.F_MainSystemA1
	case "mainSystemCounter":
		return designation.F_MainSystemCounter
	case "systemSubsystem":
		return designation.F_SystemSubsystem
	case "systemA1":
		return designation.F_SystemA1
	case "systemA2":
		return designation.F_SystemA2
	case "systemA3":
		return designation.F_SystemA3
	case "subsystem":
		return designation.F_Subsystem
	case "basicFunction":
		return designation.F_BasicFunction
	case "basicFunctionA1":
		return designation.F_BasicFunctionA1
	case "basicFunctionA2":
		return designation.F_BasicFunctionA2
	case "basicFunctionConcrete":
		return designation.F_BasicFunctionConcrete
	case "product":
		return designation.P_ProductClassDesignation
	case "productClass":
		return designation.P_ProductClass
	case "productClassA1":
		return designation.P_ProductClassA1
	case "productClassA2":
		return designation.P_ProductClassA2
	case "productClassConcrete":
		return designation.P_ProductClassConcrete
	case "operatingEquipment":
		return designation.OE_OperatingEquipmentDesignation
	case "pointOfInstallation":
		return designation.PI_PointOfInstallationDesignation
	case "pointOfInstallationPlace":
		return designation.PI_PointOfInstallation
	case "column":
		return designation.PI_Column
	case "row":
		return designation.PI_Row
	case "siteOfInstallation":
		return designation.SI_SiteOfInstallationDesignation
	case "siteOfInstallationPlace":
		return designation.SI_SiteOfInstallation
	case "terminal":
		return designation.T_TerminalDesignation
	case "terminalPoint":
		return designation.T_TerminalPoint
	case "signal":
		return designation.S_SignalDesignation
	case "signalClass":
		return designation.S_SignalClass
	case "signalMainClass":
		return designation.S_SignalMainClass
	case "signalSubclass":
		return designation.S_SignalSubclass
	case "signalClassCounter":
		return designation.S_SignalClassCounter
	case "signalInformation":
		return designation.S_SignalInformation
	case "signalDuration":
		return designation.S_SignalDuration
	case "signalType":
		return designation.S_SignalType
	case "signalInformationCounter":
		return designation.S_SignalInformationCounter
	case "document":
		return designation.D_DocumentDesignation
	case "dcc":
		return designation.D_DocumentDcc
	case "technicalSector":
		return designation.D_DocumentDccTechnicalSector
	case "dccMainClass":
		return designation.D_DocumentDccMainClass
	case "dccSubclass":
		return designation.D_DocumentDccSubclass
	case "dccConcrete":
		return designation.D_DocumentDccConcrete
	case "pageCountNumber":
		return designation.D_DocumentPageCountNumber
	case "pageNumberAlpha":
		return designation.D_DocumentPageNumberAlpha
	case "pageNumber":
		return designation.D_DocumentPageNumber
	default:
		return designation.U_Undefined
	}
}
