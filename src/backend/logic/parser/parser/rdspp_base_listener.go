// Code generated from RDSPP.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // RDSPP

import "github.com/antlr/antlr4/runtime/Go/antlr"

// BaseRDSPPListener is a complete listener for a parse tree produced by RDSPPParser.
type BaseRDSPPListener struct{}

var _ RDSPPListener = &BaseRDSPPListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseRDSPPListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseRDSPPListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseRDSPPListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseRDSPPListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterStart is called when production start is entered.
func (s *BaseRDSPPListener) EnterStart(ctx *StartContext) {}

// ExitStart is called when production start is exited.
func (s *BaseRDSPPListener) ExitStart(ctx *StartContext) {}

// EnterStartOnlyConjointDesignation is called when production startOnlyConjointDesignation is entered.
func (s *BaseRDSPPListener) EnterStartOnlyConjointDesignation(ctx *StartOnlyConjointDesignationContext) {
}

// ExitStartOnlyConjointDesignation is called when production startOnlyConjointDesignation is exited.
func (s *BaseRDSPPListener) ExitStartOnlyConjointDesignation(ctx *StartOnlyConjointDesignationContext) {
}

// EnterStartOnlyFunction is called when production startOnlyFunction is entered.
func (s *BaseRDSPPListener) EnterStartOnlyFunction(ctx *StartOnlyFunctionContext) {}

// ExitStartOnlyFunction is called when production startOnlyFunction is exited.
func (s *BaseRDSPPListener) ExitStartOnlyFunction(ctx *StartOnlyFunctionContext) {}

// EnterStartOnlyOperatingEquipment is called when production startOnlyOperatingEquipment is entered.
func (s *BaseRDSPPListener) EnterStartOnlyOperatingEquipment(ctx *StartOnlyOperatingEquipmentContext) {
}

// ExitStartOnlyOperatingEquipment is called when production startOnlyOperatingEquipment is exited.
func (s *BaseRDSPPListener) ExitStartOnlyOperatingEquipment(ctx *StartOnlyOperatingEquipmentContext) {}

// EnterStartOnlyPointOfInstallation is called when production startOnlyPointOfInstallation is entered.
func (s *BaseRDSPPListener) EnterStartOnlyPointOfInstallation(ctx *StartOnlyPointOfInstallationContext) {
}

// ExitStartOnlyPointOfInstallation is called when production startOnlyPointOfInstallation is exited.
func (s *BaseRDSPPListener) ExitStartOnlyPointOfInstallation(ctx *StartOnlyPointOfInstallationContext) {
}

// EnterStartOnlySiteOfInstallation is called when production startOnlySiteOfInstallation is entered.
func (s *BaseRDSPPListener) EnterStartOnlySiteOfInstallation(ctx *StartOnlySiteOfInstallationContext) {
}

// ExitStartOnlySiteOfInstallation is called when production startOnlySiteOfInstallation is exited.
func (s *BaseRDSPPListener) ExitStartOnlySiteOfInstallation(ctx *StartOnlySiteOfInstallationContext) {}

// EnterStartOnlyProduct is called when production startOnlyProduct is entered.
func (s *BaseRDSPPListener) EnterStartOnlyProduct(ctx *StartOnlyProductContext) {}

// ExitStartOnlyProduct is called when production startOnlyProduct is exited.
func (s *BaseRDSPPListener) ExitStartOnlyProduct(ctx *StartOnlyProductContext) {}

// EnterStartOnlyTerminal is called when production startOnlyTerminal is entered.
func (s *BaseRDSPPListener) EnterStartOnlyTerminal(ctx *StartOnlyTerminalContext) {}

// ExitStartOnlyTerminal is called when production startOnlyTerminal is exited.
func (s *BaseRDSPPListener) ExitStartOnlyTerminal(ctx *StartOnlyTerminalContext) {}

// EnterStartOnlySignal is called when production startOnlySignal is entered.
func (s *BaseRDSPPListener) EnterStartOnlySignal(ctx *StartOnlySignalContext) {}

// ExitStartOnlySignal is called when production startOnlySignal is exited.
func (s *BaseRDSPPListener) ExitStartOnlySignal(ctx *StartOnlySignalContext) {}

// EnterStartOnlyDocument is called when production startOnlyDocument is entered.
func (s *BaseRDSPPListener) EnterStartOnlyDocument(ctx *StartOnlyDocumentContext) {}

// ExitStartOnlyDocument is called when production startOnlyDocument is exited.
func (s *BaseRDSPPListener) ExitStartOnlyDocument(ctx *StartOnlyDocumentContext) {}

// EnterDesignation is called when production designation is entered.
func (s *BaseRDSPPListener) EnterDesignation(ctx *DesignationContext) {}

// ExitDesignation is called when production designation is exited.
func (s *BaseRDSPPListener) ExitDesignation(ctx *DesignationContext) {}

// EnterAlpha is called when production alpha is entered.
func (s *BaseRDSPPListener) EnterAlpha(ctx *AlphaContext) {}

// ExitAlpha is called when production alpha is exited.
func (s *BaseRDSPPListener) ExitAlpha(ctx *AlphaContext) {}

// EnterUnderscore_alpha is called when production underscore_alpha is entered.
func (s *BaseRDSPPListener) EnterUnderscore_alpha(ctx *Underscore_alphaContext) {}

// ExitUnderscore_alpha is called when production underscore_alpha is exited.
func (s *BaseRDSPPListener) ExitUnderscore_alpha(ctx *Underscore_alphaContext) {}

// EnterUnderscore is called when production underscore is entered.
func (s *BaseRDSPPListener) EnterUnderscore(ctx *UnderscoreContext) {}

// ExitUnderscore is called when production underscore is exited.
func (s *BaseRDSPPListener) ExitUnderscore(ctx *UnderscoreContext) {}

// EnterConjointDesignation is called when production conjointDesignation is entered.
func (s *BaseRDSPPListener) EnterConjointDesignation(ctx *ConjointDesignationContext) {}

// ExitConjointDesignation is called when production conjointDesignation is exited.
func (s *BaseRDSPPListener) ExitConjointDesignation(ctx *ConjointDesignationContext) {}

// EnterGeographicLocation is called when production geographicLocation is entered.
func (s *BaseRDSPPListener) EnterGeographicLocation(ctx *GeographicLocationContext) {}

// ExitGeographicLocation is called when production geographicLocation is exited.
func (s *BaseRDSPPListener) ExitGeographicLocation(ctx *GeographicLocationContext) {}

// EnterLatitude is called when production latitude is entered.
func (s *BaseRDSPPListener) EnterLatitude(ctx *LatitudeContext) {}

// ExitLatitude is called when production latitude is exited.
func (s *BaseRDSPPListener) ExitLatitude(ctx *LatitudeContext) {}

// EnterLongitude is called when production longitude is entered.
func (s *BaseRDSPPListener) EnterLongitude(ctx *LongitudeContext) {}

// ExitLongitude is called when production longitude is exited.
func (s *BaseRDSPPListener) ExitLongitude(ctx *LongitudeContext) {}

// EnterProjectSpecificInformation is called when production projectSpecificInformation is entered.
func (s *BaseRDSPPListener) EnterProjectSpecificInformation(ctx *ProjectSpecificInformationContext) {}

// ExitProjectSpecificInformation is called when production projectSpecificInformation is exited.
func (s *BaseRDSPPListener) ExitProjectSpecificInformation(ctx *ProjectSpecificInformationContext) {}

// EnterCountry is called when production country is entered.
func (s *BaseRDSPPListener) EnterCountry(ctx *CountryContext) {}

// ExitCountry is called when production country is exited.
func (s *BaseRDSPPListener) ExitCountry(ctx *CountryContext) {}

// EnterRegion is called when production region is entered.
func (s *BaseRDSPPListener) EnterRegion(ctx *RegionContext) {}

// ExitRegion is called when production region is exited.
func (s *BaseRDSPPListener) ExitRegion(ctx *RegionContext) {}

// EnterTownOrName is called when production townOrName is entered.
func (s *BaseRDSPPListener) EnterTownOrName(ctx *TownOrNameContext) {}

// ExitTownOrName is called when production townOrName is exited.
func (s *BaseRDSPPListener) ExitTownOrName(ctx *TownOrNameContext) {}

// EnterProjectCounter is called when production projectCounter is entered.
func (s *BaseRDSPPListener) EnterProjectCounter(ctx *ProjectCounterContext) {}

// ExitProjectCounter is called when production projectCounter is exited.
func (s *BaseRDSPPListener) ExitProjectCounter(ctx *ProjectCounterContext) {}

// EnterPlantType is called when production plantType is entered.
func (s *BaseRDSPPListener) EnterPlantType(ctx *PlantTypeContext) {}

// ExitPlantType is called when production plantType is exited.
func (s *BaseRDSPPListener) ExitPlantType(ctx *PlantTypeContext) {}

// EnterFacilityType is called when production facilityType is entered.
func (s *BaseRDSPPListener) EnterFacilityType(ctx *FacilityTypeContext) {}

// ExitFacilityType is called when production facilityType is exited.
func (s *BaseRDSPPListener) ExitFacilityType(ctx *FacilityTypeContext) {}

// EnterFunction is called when production function is entered.
func (s *BaseRDSPPListener) EnterFunction(ctx *FunctionContext) {}

// ExitFunction is called when production function is exited.
func (s *BaseRDSPPListener) ExitFunction(ctx *FunctionContext) {}

// EnterMainSystem is called when production mainSystem is entered.
func (s *BaseRDSPPListener) EnterMainSystem(ctx *MainSystemContext) {}

// ExitMainSystem is called when production mainSystem is exited.
func (s *BaseRDSPPListener) ExitMainSystem(ctx *MainSystemContext) {}

// EnterMainSystemA1 is called when production mainSystemA1 is entered.
func (s *BaseRDSPPListener) EnterMainSystemA1(ctx *MainSystemA1Context) {}

// ExitMainSystemA1 is called when production mainSystemA1 is exited.
func (s *BaseRDSPPListener) ExitMainSystemA1(ctx *MainSystemA1Context) {}

// EnterMainSystemCounter is called when production mainSystemCounter is entered.
func (s *BaseRDSPPListener) EnterMainSystemCounter(ctx *MainSystemCounterContext) {}

// ExitMainSystemCounter is called when production mainSystemCounter is exited.
func (s *BaseRDSPPListener) ExitMainSystemCounter(ctx *MainSystemCounterContext) {}

// EnterSystemSubsystem is called when production systemSubsystem is entered.
func (s *BaseRDSPPListener) EnterSystemSubsystem(ctx *SystemSubsystemContext) {}

// ExitSystemSubsystem is called when production systemSubsystem is exited.
func (s *BaseRDSPPListener) ExitSystemSubsystem(ctx *SystemSubsystemContext) {}

// EnterSystemSubsystemPart is called when production systemSubsystemPart is entered.
func (s *BaseRDSPPListener) EnterSystemSubsystemPart(ctx *SystemSubsystemPartContext) {}

// ExitSystemSubsystemPart is called when production systemSubsystemPart is exited.
func (s *BaseRDSPPListener) ExitSystemSubsystemPart(ctx *SystemSubsystemPartContext) {}

// EnterSystemA1 is called when production systemA1 is entered.
func (s *BaseRDSPPListener) EnterSystemA1(ctx *SystemA1Context) {}

// ExitSystemA1 is called when production systemA1 is exited.
func (s *BaseRDSPPListener) ExitSystemA1(ctx *SystemA1Context) {}

// EnterSystemA2 is called when production systemA2 is entered.
func (s *BaseRDSPPListener) EnterSystemA2(ctx *SystemA2Context) {}

// ExitSystemA2 is called when production systemA2 is exited.
func (s *BaseRDSPPListener) ExitSystemA2(ctx *SystemA2Context) {}

// EnterSystemA3 is called when production systemA3 is entered.
func (s *BaseRDSPPListener) EnterSystemA3(ctx *SystemA3Context) {}

// ExitSystemA3 is called when production systemA3 is exited.
func (s *BaseRDSPPListener) ExitSystemA3(ctx *SystemA3Context) {}

// EnterSubsystem is called when production subsystem is entered.
func (s *BaseRDSPPListener) EnterSubsystem(ctx *SubsystemContext) {}

// ExitSubsystem is called when production subsystem is exited.
func (s *BaseRDSPPListener) ExitSubsystem(ctx *SubsystemContext) {}

// EnterBasicFunction is called when production basicFunction is entered.
func (s *BaseRDSPPListener) EnterBasicFunction(ctx *BasicFunctionContext) {}

// ExitBasicFunction is called when production basicFunction is exited.
func (s *BaseRDSPPListener) ExitBasicFunction(ctx *BasicFunctionContext) {}

// EnterBasicFunctionA1 is called when production basicFunctionA1 is entered.
func (s *BaseRDSPPListener) EnterBasicFunctionA1(ctx *BasicFunctionA1Context) {}

// ExitBasicFunctionA1 is called when production basicFunctionA1 is exited.
func (s *BaseRDSPPListener) ExitBasicFunctionA1(ctx *BasicFunctionA1Context) {}

// EnterBasicFunctionA2 is called when production basicFunctionA2 is entered.
func (s *BaseRDSPPListener) EnterBasicFunctionA2(ctx *BasicFunctionA2Context) {}

// ExitBasicFunctionA2 is called when production basicFunctionA2 is exited.
func (s *BaseRDSPPListener) ExitBasicFunctionA2(ctx *BasicFunctionA2Context) {}

// EnterBasicFunctionConcrete is called when production basicFunctionConcrete is entered.
func (s *BaseRDSPPListener) EnterBasicFunctionConcrete(ctx *BasicFunctionConcreteContext) {}

// ExitBasicFunctionConcrete is called when production basicFunctionConcrete is exited.
func (s *BaseRDSPPListener) ExitBasicFunctionConcrete(ctx *BasicFunctionConcreteContext) {}

// EnterProduct is called when production product is entered.
func (s *BaseRDSPPListener) EnterProduct(ctx *ProductContext) {}

// ExitProduct is called when production product is exited.
func (s *BaseRDSPPListener) ExitProduct(ctx *ProductContext) {}

// EnterProductClass is called when production productClass is entered.
func (s *BaseRDSPPListener) EnterProductClass(ctx *ProductClassContext) {}

// ExitProductClass is called when production productClass is exited.
func (s *BaseRDSPPListener) ExitProductClass(ctx *ProductClassContext) {}

// EnterProductClassA1 is called when production productClassA1 is entered.
func (s *BaseRDSPPListener) EnterProductClassA1(ctx *ProductClassA1Context) {}

// ExitProductClassA1 is called when production productClassA1 is exited.
func (s *BaseRDSPPListener) ExitProductClassA1(ctx *ProductClassA1Context) {}

// EnterProductClassA2 is called when production productClassA2 is entered.
func (s *BaseRDSPPListener) EnterProductClassA2(ctx *ProductClassA2Context) {}

// ExitProductClassA2 is called when production productClassA2 is exited.
func (s *BaseRDSPPListener) ExitProductClassA2(ctx *ProductClassA2Context) {}

// EnterProductClassConcrete is called when production productClassConcrete is entered.
func (s *BaseRDSPPListener) EnterProductClassConcrete(ctx *ProductClassConcreteContext) {}

// ExitProductClassConcrete is called when production productClassConcrete is exited.
func (s *BaseRDSPPListener) ExitProductClassConcrete(ctx *ProductClassConcreteContext) {}

// EnterOperatingEquipment is called when production operatingEquipment is entered.
func (s *BaseRDSPPListener) EnterOperatingEquipment(ctx *OperatingEquipmentContext) {}

// ExitOperatingEquipment is called when production operatingEquipment is exited.
func (s *BaseRDSPPListener) ExitOperatingEquipment(ctx *OperatingEquipmentContext) {}

// EnterPointOfInstallation is called when production pointOfInstallation is entered.
func (s *BaseRDSPPListener) EnterPointOfInstallation(ctx *PointOfInstallationContext) {}

// ExitPointOfInstallation is called when production pointOfInstallation is exited.
func (s *BaseRDSPPListener) ExitPointOfInstallation(ctx *PointOfInstallationContext) {}

// EnterPointOfInstallationPlace is called when production pointOfInstallationPlace is entered.
func (s *BaseRDSPPListener) EnterPointOfInstallationPlace(ctx *PointOfInstallationPlaceContext) {}

// ExitPointOfInstallationPlace is called when production pointOfInstallationPlace is exited.
func (s *BaseRDSPPListener) ExitPointOfInstallationPlace(ctx *PointOfInstallationPlaceContext) {}

// EnterColumn is called when production column is entered.
func (s *BaseRDSPPListener) EnterColumn(ctx *ColumnContext) {}

// ExitColumn is called when production column is exited.
func (s *BaseRDSPPListener) ExitColumn(ctx *ColumnContext) {}

// EnterRow is called when production row is entered.
func (s *BaseRDSPPListener) EnterRow(ctx *RowContext) {}

// ExitRow is called when production row is exited.
func (s *BaseRDSPPListener) ExitRow(ctx *RowContext) {}

// EnterSiteOfInstallation is called when production siteOfInstallation is entered.
func (s *BaseRDSPPListener) EnterSiteOfInstallation(ctx *SiteOfInstallationContext) {}

// ExitSiteOfInstallation is called when production siteOfInstallation is exited.
func (s *BaseRDSPPListener) ExitSiteOfInstallation(ctx *SiteOfInstallationContext) {}

// EnterSiteOfInstallationPlace is called when production siteOfInstallationPlace is entered.
func (s *BaseRDSPPListener) EnterSiteOfInstallationPlace(ctx *SiteOfInstallationPlaceContext) {}

// ExitSiteOfInstallationPlace is called when production siteOfInstallationPlace is exited.
func (s *BaseRDSPPListener) ExitSiteOfInstallationPlace(ctx *SiteOfInstallationPlaceContext) {}

// EnterSignal is called when production signal is entered.
func (s *BaseRDSPPListener) EnterSignal(ctx *SignalContext) {}

// ExitSignal is called when production signal is exited.
func (s *BaseRDSPPListener) ExitSignal(ctx *SignalContext) {}

// EnterSignalClass is called when production signalClass is entered.
func (s *BaseRDSPPListener) EnterSignalClass(ctx *SignalClassContext) {}

// ExitSignalClass is called when production signalClass is exited.
func (s *BaseRDSPPListener) ExitSignalClass(ctx *SignalClassContext) {}

// EnterSignalMainClass is called when production signalMainClass is entered.
func (s *BaseRDSPPListener) EnterSignalMainClass(ctx *SignalMainClassContext) {}

// ExitSignalMainClass is called when production signalMainClass is exited.
func (s *BaseRDSPPListener) ExitSignalMainClass(ctx *SignalMainClassContext) {}

// EnterSignalSubclass is called when production signalSubclass is entered.
func (s *BaseRDSPPListener) EnterSignalSubclass(ctx *SignalSubclassContext) {}

// ExitSignalSubclass is called when production signalSubclass is exited.
func (s *BaseRDSPPListener) ExitSignalSubclass(ctx *SignalSubclassContext) {}

// EnterSignalClassCounter is called when production signalClassCounter is entered.
func (s *BaseRDSPPListener) EnterSignalClassCounter(ctx *SignalClassCounterContext) {}

// ExitSignalClassCounter is called when production signalClassCounter is exited.
func (s *BaseRDSPPListener) ExitSignalClassCounter(ctx *SignalClassCounterContext) {}

// EnterSignalInformation is called when production signalInformation is entered.
func (s *BaseRDSPPListener) EnterSignalInformation(ctx *SignalInformationContext) {}

// ExitSignalInformation is called when production signalInformation is exited.
func (s *BaseRDSPPListener) ExitSignalInformation(ctx *SignalInformationContext) {}

// EnterSignalDuration is called when production signalDuration is entered.
func (s *BaseRDSPPListener) EnterSignalDuration(ctx *SignalDurationContext) {}

// ExitSignalDuration is called when production signalDuration is exited.
func (s *BaseRDSPPListener) ExitSignalDuration(ctx *SignalDurationContext) {}

// EnterSignalType is called when production signalType is entered.
func (s *BaseRDSPPListener) EnterSignalType(ctx *SignalTypeContext) {}

// ExitSignalType is called when production signalType is exited.
func (s *BaseRDSPPListener) ExitSignalType(ctx *SignalTypeContext) {}

// EnterSignalInformationCounter is called when production signalInformationCounter is entered.
func (s *BaseRDSPPListener) EnterSignalInformationCounter(ctx *SignalInformationCounterContext) {}

// ExitSignalInformationCounter is called when production signalInformationCounter is exited.
func (s *BaseRDSPPListener) ExitSignalInformationCounter(ctx *SignalInformationCounterContext) {}

// EnterTerminal is called when production terminal is entered.
func (s *BaseRDSPPListener) EnterTerminal(ctx *TerminalContext) {}

// ExitTerminal is called when production terminal is exited.
func (s *BaseRDSPPListener) ExitTerminal(ctx *TerminalContext) {}

// EnterTerminalPoint is called when production terminalPoint is entered.
func (s *BaseRDSPPListener) EnterTerminalPoint(ctx *TerminalPointContext) {}

// ExitTerminalPoint is called when production terminalPoint is exited.
func (s *BaseRDSPPListener) ExitTerminalPoint(ctx *TerminalPointContext) {}

// EnterDocument is called when production document is entered.
func (s *BaseRDSPPListener) EnterDocument(ctx *DocumentContext) {}

// ExitDocument is called when production document is exited.
func (s *BaseRDSPPListener) ExitDocument(ctx *DocumentContext) {}

// EnterDcc is called when production dcc is entered.
func (s *BaseRDSPPListener) EnterDcc(ctx *DccContext) {}

// ExitDcc is called when production dcc is exited.
func (s *BaseRDSPPListener) ExitDcc(ctx *DccContext) {}

// EnterTechnicalSector is called when production technicalSector is entered.
func (s *BaseRDSPPListener) EnterTechnicalSector(ctx *TechnicalSectorContext) {}

// ExitTechnicalSector is called when production technicalSector is exited.
func (s *BaseRDSPPListener) ExitTechnicalSector(ctx *TechnicalSectorContext) {}

// EnterDccMainClass is called when production dccMainClass is entered.
func (s *BaseRDSPPListener) EnterDccMainClass(ctx *DccMainClassContext) {}

// ExitDccMainClass is called when production dccMainClass is exited.
func (s *BaseRDSPPListener) ExitDccMainClass(ctx *DccMainClassContext) {}

// EnterDccSubclass is called when production dccSubclass is entered.
func (s *BaseRDSPPListener) EnterDccSubclass(ctx *DccSubclassContext) {}

// ExitDccSubclass is called when production dccSubclass is exited.
func (s *BaseRDSPPListener) ExitDccSubclass(ctx *DccSubclassContext) {}

// EnterDccConcrete is called when production dccConcrete is entered.
func (s *BaseRDSPPListener) EnterDccConcrete(ctx *DccConcreteContext) {}

// ExitDccConcrete is called when production dccConcrete is exited.
func (s *BaseRDSPPListener) ExitDccConcrete(ctx *DccConcreteContext) {}

// EnterPageCountNumber is called when production pageCountNumber is entered.
func (s *BaseRDSPPListener) EnterPageCountNumber(ctx *PageCountNumberContext) {}

// ExitPageCountNumber is called when production pageCountNumber is exited.
func (s *BaseRDSPPListener) ExitPageCountNumber(ctx *PageCountNumberContext) {}

// EnterPageNumberAlpha is called when production pageNumberAlpha is entered.
func (s *BaseRDSPPListener) EnterPageNumberAlpha(ctx *PageNumberAlphaContext) {}

// ExitPageNumberAlpha is called when production pageNumberAlpha is exited.
func (s *BaseRDSPPListener) ExitPageNumberAlpha(ctx *PageNumberAlphaContext) {}

// EnterPageNumber is called when production pageNumber is entered.
func (s *BaseRDSPPListener) EnterPageNumber(ctx *PageNumberContext) {}

// ExitPageNumber is called when production pageNumber is exited.
func (s *BaseRDSPPListener) ExitPageNumber(ctx *PageNumberContext) {}
