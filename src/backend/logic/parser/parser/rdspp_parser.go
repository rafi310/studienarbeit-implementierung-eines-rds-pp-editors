// Code generated from RDSPP.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // RDSPP

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 41, 532,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4, 18, 9,
	18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 4, 23, 9, 23,
	4, 24, 9, 24, 4, 25, 9, 25, 4, 26, 9, 26, 4, 27, 9, 27, 4, 28, 9, 28, 4,
	29, 9, 29, 4, 30, 9, 30, 4, 31, 9, 31, 4, 32, 9, 32, 4, 33, 9, 33, 4, 34,
	9, 34, 4, 35, 9, 35, 4, 36, 9, 36, 4, 37, 9, 37, 4, 38, 9, 38, 4, 39, 9,
	39, 4, 40, 9, 40, 4, 41, 9, 41, 4, 42, 9, 42, 4, 43, 9, 43, 4, 44, 9, 44,
	4, 45, 9, 45, 4, 46, 9, 46, 4, 47, 9, 47, 4, 48, 9, 48, 4, 49, 9, 49, 4,
	50, 9, 50, 4, 51, 9, 51, 4, 52, 9, 52, 4, 53, 9, 53, 4, 54, 9, 54, 4, 55,
	9, 55, 4, 56, 9, 56, 4, 57, 9, 57, 4, 58, 9, 58, 4, 59, 9, 59, 4, 60, 9,
	60, 4, 61, 9, 61, 4, 62, 9, 62, 4, 63, 9, 63, 4, 64, 9, 64, 4, 65, 9, 65,
	4, 66, 9, 66, 4, 67, 9, 67, 4, 68, 9, 68, 4, 69, 9, 69, 4, 70, 9, 70, 4,
	71, 9, 71, 4, 72, 9, 72, 3, 2, 3, 2, 3, 2, 3, 3, 3, 3, 3, 3, 3, 4, 3, 4,
	3, 4, 3, 5, 3, 5, 3, 5, 3, 6, 3, 6, 3, 6, 3, 7, 3, 7, 3, 7, 3, 8, 3, 8,
	3, 8, 3, 9, 3, 9, 3, 9, 3, 10, 3, 10, 3, 10, 3, 11, 3, 11, 3, 11, 3, 12,
	3, 12, 5, 12, 177, 10, 12, 3, 12, 3, 12, 5, 12, 181, 10, 12, 3, 12, 3,
	12, 3, 12, 5, 12, 186, 10, 12, 3, 12, 5, 12, 189, 10, 12, 3, 12, 3, 12,
	3, 12, 5, 12, 194, 10, 12, 3, 12, 5, 12, 197, 10, 12, 3, 12, 3, 12, 5,
	12, 201, 10, 12, 3, 12, 5, 12, 204, 10, 12, 3, 12, 3, 12, 5, 12, 208, 10,
	12, 3, 12, 3, 12, 3, 12, 5, 12, 213, 10, 12, 3, 13, 3, 13, 3, 14, 3, 14,
	3, 15, 3, 15, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 5, 16, 226, 10, 16, 5,
	16, 228, 10, 16, 3, 17, 3, 17, 3, 17, 3, 18, 3, 18, 3, 18, 3, 18, 3, 18,
	3, 18, 3, 19, 3, 19, 3, 19, 3, 19, 3, 19, 3, 19, 3, 19, 3, 20, 3, 20, 3,
	20, 3, 20, 3, 20, 3, 20, 3, 20, 3, 20, 3, 21, 3, 21, 3, 21, 5, 21, 257,
	10, 21, 3, 21, 3, 21, 3, 21, 5, 21, 262, 10, 21, 3, 22, 3, 22, 3, 22, 3,
	22, 5, 22, 268, 10, 22, 3, 22, 3, 22, 3, 22, 3, 22, 5, 22, 274, 10, 22,
	3, 22, 3, 22, 3, 22, 5, 22, 279, 10, 22, 3, 23, 3, 23, 3, 23, 5, 23, 284,
	10, 23, 3, 23, 3, 23, 3, 23, 5, 23, 289, 10, 23, 3, 23, 3, 23, 3, 23, 5,
	23, 294, 10, 23, 3, 24, 3, 24, 5, 24, 298, 10, 24, 3, 24, 3, 24, 3, 25,
	3, 25, 3, 26, 3, 26, 3, 27, 3, 27, 3, 27, 3, 27, 3, 27, 3, 27, 5, 27, 312,
	10, 27, 3, 27, 3, 27, 5, 27, 316, 10, 27, 5, 27, 318, 10, 27, 3, 28, 3,
	28, 3, 28, 3, 29, 3, 29, 3, 30, 3, 30, 3, 30, 3, 30, 3, 31, 3, 31, 3, 31,
	3, 31, 3, 31, 3, 32, 3, 32, 3, 32, 3, 32, 3, 32, 3, 32, 3, 32, 3, 32, 3,
	32, 5, 32, 343, 10, 32, 3, 33, 3, 33, 3, 34, 3, 34, 3, 35, 3, 35, 3, 36,
	3, 36, 3, 36, 3, 37, 3, 37, 3, 37, 3, 37, 3, 38, 3, 38, 3, 39, 3, 39, 3,
	40, 3, 40, 3, 40, 3, 40, 3, 41, 3, 41, 6, 41, 368, 10, 41, 13, 41, 14,
	41, 369, 3, 42, 3, 42, 3, 42, 3, 42, 3, 43, 3, 43, 3, 44, 3, 44, 3, 45,
	3, 45, 3, 45, 3, 45, 3, 46, 3, 46, 5, 46, 386, 10, 46, 3, 46, 3, 46, 3,
	46, 3, 46, 3, 47, 3, 47, 5, 47, 394, 10, 47, 3, 47, 3, 47, 5, 47, 398,
	10, 47, 3, 47, 3, 47, 5, 47, 402, 10, 47, 3, 48, 3, 48, 3, 48, 3, 49, 3,
	49, 3, 49, 3, 50, 3, 50, 3, 50, 3, 50, 3, 51, 3, 51, 3, 51, 5, 51, 417,
	10, 51, 3, 51, 3, 51, 3, 51, 5, 51, 422, 10, 51, 3, 52, 3, 52, 3, 52, 3,
	52, 5, 52, 428, 10, 52, 5, 52, 430, 10, 52, 5, 52, 432, 10, 52, 3, 52,
	3, 52, 3, 52, 3, 52, 5, 52, 438, 10, 52, 5, 52, 440, 10, 52, 5, 52, 442,
	10, 52, 3, 53, 3, 53, 3, 53, 3, 53, 3, 53, 5, 53, 449, 10, 53, 3, 54, 3,
	54, 3, 54, 3, 54, 3, 55, 3, 55, 3, 56, 3, 56, 3, 57, 3, 57, 3, 57, 3, 57,
	3, 58, 3, 58, 3, 58, 3, 58, 3, 59, 3, 59, 3, 60, 3, 60, 3, 61, 3, 61, 3,
	61, 3, 62, 3, 62, 3, 62, 3, 62, 5, 62, 478, 10, 62, 3, 63, 3, 63, 6, 63,
	482, 10, 63, 13, 63, 14, 63, 483, 3, 64, 3, 64, 3, 64, 3, 64, 5, 64, 490,
	10, 64, 3, 65, 3, 65, 3, 65, 3, 65, 3, 65, 3, 66, 3, 66, 3, 67, 3, 67,
	3, 68, 3, 68, 3, 69, 3, 69, 3, 69, 3, 69, 3, 70, 5, 70, 508, 10, 70, 3,
	70, 3, 70, 3, 71, 3, 71, 3, 71, 3, 71, 5, 71, 516, 10, 71, 5, 71, 518,
	10, 71, 5, 71, 520, 10, 71, 3, 72, 3, 72, 3, 72, 3, 72, 5, 72, 526, 10,
	72, 5, 72, 528, 10, 72, 5, 72, 530, 10, 72, 3, 72, 2, 2, 73, 2, 4, 6, 8,
	10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44,
	46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80,
	82, 84, 86, 88, 90, 92, 94, 96, 98, 100, 102, 104, 106, 108, 110, 112,
	114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140, 142,
	2, 5, 3, 2, 3, 26, 4, 2, 15, 15, 19, 19, 4, 2, 7, 7, 23, 23, 2, 532, 2,
	144, 3, 2, 2, 2, 4, 147, 3, 2, 2, 2, 6, 150, 3, 2, 2, 2, 8, 153, 3, 2,
	2, 2, 10, 156, 3, 2, 2, 2, 12, 159, 3, 2, 2, 2, 14, 162, 3, 2, 2, 2, 16,
	165, 3, 2, 2, 2, 18, 168, 3, 2, 2, 2, 20, 171, 3, 2, 2, 2, 22, 212, 3,
	2, 2, 2, 24, 214, 3, 2, 2, 2, 26, 216, 3, 2, 2, 2, 28, 218, 3, 2, 2, 2,
	30, 220, 3, 2, 2, 2, 32, 229, 3, 2, 2, 2, 34, 232, 3, 2, 2, 2, 36, 238,
	3, 2, 2, 2, 38, 245, 3, 2, 2, 2, 40, 256, 3, 2, 2, 2, 42, 267, 3, 2, 2,
	2, 44, 283, 3, 2, 2, 2, 46, 297, 3, 2, 2, 2, 48, 301, 3, 2, 2, 2, 50, 303,
	3, 2, 2, 2, 52, 317, 3, 2, 2, 2, 54, 319, 3, 2, 2, 2, 56, 322, 3, 2, 2,
	2, 58, 324, 3, 2, 2, 2, 60, 328, 3, 2, 2, 2, 62, 342, 3, 2, 2, 2, 64, 344,
	3, 2, 2, 2, 66, 346, 3, 2, 2, 2, 68, 348, 3, 2, 2, 2, 70, 350, 3, 2, 2,
	2, 72, 353, 3, 2, 2, 2, 74, 357, 3, 2, 2, 2, 76, 359, 3, 2, 2, 2, 78, 361,
	3, 2, 2, 2, 80, 365, 3, 2, 2, 2, 82, 371, 3, 2, 2, 2, 84, 375, 3, 2, 2,
	2, 86, 377, 3, 2, 2, 2, 88, 379, 3, 2, 2, 2, 90, 383, 3, 2, 2, 2, 92, 391,
	3, 2, 2, 2, 94, 403, 3, 2, 2, 2, 96, 406, 3, 2, 2, 2, 98, 409, 3, 2, 2,
	2, 100, 413, 3, 2, 2, 2, 102, 423, 3, 2, 2, 2, 104, 443, 3, 2, 2, 2, 106,
	450, 3, 2, 2, 2, 108, 454, 3, 2, 2, 2, 110, 456, 3, 2, 2, 2, 112, 458,
	3, 2, 2, 2, 114, 462, 3, 2, 2, 2, 116, 466, 3, 2, 2, 2, 118, 468, 3, 2,
	2, 2, 120, 470, 3, 2, 2, 2, 122, 473, 3, 2, 2, 2, 124, 481, 3, 2, 2, 2,
	126, 485, 3, 2, 2, 2, 128, 491, 3, 2, 2, 2, 130, 496, 3, 2, 2, 2, 132,
	498, 3, 2, 2, 2, 134, 500, 3, 2, 2, 2, 136, 502, 3, 2, 2, 2, 138, 507,
	3, 2, 2, 2, 140, 511, 3, 2, 2, 2, 142, 521, 3, 2, 2, 2, 144, 145, 5, 22,
	12, 2, 145, 146, 7, 2, 2, 3, 146, 3, 3, 2, 2, 2, 147, 148, 5, 30, 16, 2,
	148, 149, 7, 2, 2, 3, 149, 5, 3, 2, 2, 2, 150, 151, 5, 52, 27, 2, 151,
	152, 7, 2, 2, 3, 152, 7, 3, 2, 2, 2, 153, 154, 5, 90, 46, 2, 154, 155,
	7, 2, 2, 3, 155, 9, 3, 2, 2, 2, 156, 157, 5, 92, 47, 2, 157, 158, 7, 2,
	2, 3, 158, 11, 3, 2, 2, 2, 159, 160, 5, 100, 51, 2, 160, 161, 7, 2, 2,
	3, 161, 13, 3, 2, 2, 2, 162, 163, 5, 80, 41, 2, 163, 164, 7, 2, 2, 3, 164,
	15, 3, 2, 2, 2, 165, 166, 5, 122, 62, 2, 166, 167, 7, 2, 2, 3, 167, 17,
	3, 2, 2, 2, 168, 169, 5, 104, 53, 2, 169, 170, 7, 2, 2, 3, 170, 19, 3,
	2, 2, 2, 171, 172, 5, 126, 64, 2, 172, 173, 7, 2, 2, 3, 173, 21, 3, 2,
	2, 2, 174, 213, 5, 30, 16, 2, 175, 177, 5, 30, 16, 2, 176, 175, 3, 2, 2,
	2, 176, 177, 3, 2, 2, 2, 177, 178, 3, 2, 2, 2, 178, 213, 5, 126, 64, 2,
	179, 181, 5, 30, 16, 2, 180, 179, 3, 2, 2, 2, 180, 181, 3, 2, 2, 2, 181,
	182, 3, 2, 2, 2, 182, 185, 5, 52, 27, 2, 183, 186, 5, 104, 53, 2, 184,
	186, 5, 126, 64, 2, 185, 183, 3, 2, 2, 2, 185, 184, 3, 2, 2, 2, 185, 186,
	3, 2, 2, 2, 186, 213, 3, 2, 2, 2, 187, 189, 5, 30, 16, 2, 188, 187, 3,
	2, 2, 2, 188, 189, 3, 2, 2, 2, 189, 190, 3, 2, 2, 2, 190, 193, 5, 90, 46,
	2, 191, 194, 5, 122, 62, 2, 192, 194, 5, 126, 64, 2, 193, 191, 3, 2, 2,
	2, 193, 192, 3, 2, 2, 2, 193, 194, 3, 2, 2, 2, 194, 213, 3, 2, 2, 2, 195,
	197, 5, 30, 16, 2, 196, 195, 3, 2, 2, 2, 196, 197, 3, 2, 2, 2, 197, 198,
	3, 2, 2, 2, 198, 200, 5, 92, 47, 2, 199, 201, 5, 126, 64, 2, 200, 199,
	3, 2, 2, 2, 200, 201, 3, 2, 2, 2, 201, 213, 3, 2, 2, 2, 202, 204, 5, 30,
	16, 2, 203, 202, 3, 2, 2, 2, 203, 204, 3, 2, 2, 2, 204, 205, 3, 2, 2, 2,
	205, 207, 5, 100, 51, 2, 206, 208, 5, 126, 64, 2, 207, 206, 3, 2, 2, 2,
	207, 208, 3, 2, 2, 2, 208, 213, 3, 2, 2, 2, 209, 213, 5, 80, 41, 2, 210,
	213, 5, 122, 62, 2, 211, 213, 5, 104, 53, 2, 212, 174, 3, 2, 2, 2, 212,
	176, 3, 2, 2, 2, 212, 180, 3, 2, 2, 2, 212, 188, 3, 2, 2, 2, 212, 196,
	3, 2, 2, 2, 212, 203, 3, 2, 2, 2, 212, 209, 3, 2, 2, 2, 212, 210, 3, 2,
	2, 2, 212, 211, 3, 2, 2, 2, 213, 23, 3, 2, 2, 2, 214, 215, 9, 2, 2, 2,
	215, 25, 3, 2, 2, 2, 216, 217, 7, 27, 2, 2, 217, 27, 3, 2, 2, 2, 218, 219,
	7, 27, 2, 2, 219, 29, 3, 2, 2, 2, 220, 227, 7, 34, 2, 2, 221, 228, 5, 38,
	20, 2, 222, 225, 5, 32, 17, 2, 223, 224, 7, 32, 2, 2, 224, 226, 5, 38,
	20, 2, 225, 223, 3, 2, 2, 2, 225, 226, 3, 2, 2, 2, 226, 228, 3, 2, 2, 2,
	227, 221, 3, 2, 2, 2, 227, 222, 3, 2, 2, 2, 228, 31, 3, 2, 2, 2, 229, 230,
	5, 34, 18, 2, 230, 231, 5, 36, 19, 2, 231, 33, 3, 2, 2, 2, 232, 233, 7,
	31, 2, 2, 233, 234, 7, 31, 2, 2, 234, 235, 7, 31, 2, 2, 235, 236, 7, 31,
	2, 2, 236, 237, 9, 3, 2, 2, 237, 35, 3, 2, 2, 2, 238, 239, 7, 31, 2, 2,
	239, 240, 7, 31, 2, 2, 240, 241, 7, 31, 2, 2, 241, 242, 7, 31, 2, 2, 242,
	243, 7, 31, 2, 2, 243, 244, 9, 4, 2, 2, 244, 37, 3, 2, 2, 2, 245, 246,
	5, 40, 21, 2, 246, 247, 5, 42, 22, 2, 247, 248, 7, 32, 2, 2, 248, 249,
	5, 44, 23, 2, 249, 250, 5, 46, 24, 2, 250, 251, 5, 48, 25, 2, 251, 252,
	5, 50, 26, 2, 252, 39, 3, 2, 2, 2, 253, 257, 5, 24, 13, 2, 254, 257, 7,
	28, 2, 2, 255, 257, 7, 29, 2, 2, 256, 253, 3, 2, 2, 2, 256, 254, 3, 2,
	2, 2, 256, 255, 3, 2, 2, 2, 257, 261, 3, 2, 2, 2, 258, 262, 5, 24, 13,
	2, 259, 262, 7, 28, 2, 2, 260, 262, 7, 29, 2, 2, 261, 258, 3, 2, 2, 2,
	261, 259, 3, 2, 2, 2, 261, 260, 3, 2, 2, 2, 262, 41, 3, 2, 2, 2, 263, 268,
	5, 24, 13, 2, 264, 268, 7, 28, 2, 2, 265, 268, 7, 29, 2, 2, 266, 268, 5,
	26, 14, 2, 267, 263, 3, 2, 2, 2, 267, 264, 3, 2, 2, 2, 267, 265, 3, 2,
	2, 2, 267, 266, 3, 2, 2, 2, 268, 273, 3, 2, 2, 2, 269, 274, 5, 24, 13,
	2, 270, 274, 7, 28, 2, 2, 271, 274, 7, 29, 2, 2, 272, 274, 5, 26, 14, 2,
	273, 269, 3, 2, 2, 2, 273, 270, 3, 2, 2, 2, 273, 271, 3, 2, 2, 2, 273,
	272, 3, 2, 2, 2, 274, 278, 3, 2, 2, 2, 275, 279, 5, 24, 13, 2, 276, 279,
	7, 28, 2, 2, 277, 279, 7, 29, 2, 2, 278, 275, 3, 2, 2, 2, 278, 276, 3,
	2, 2, 2, 278, 277, 3, 2, 2, 2, 279, 43, 3, 2, 2, 2, 280, 284, 5, 24, 13,
	2, 281, 284, 7, 28, 2, 2, 282, 284, 7, 29, 2, 2, 283, 280, 3, 2, 2, 2,
	283, 281, 3, 2, 2, 2, 283, 282, 3, 2, 2, 2, 284, 288, 3, 2, 2, 2, 285,
	289, 5, 24, 13, 2, 286, 289, 7, 28, 2, 2, 287, 289, 7, 29, 2, 2, 288, 285,
	3, 2, 2, 2, 288, 286, 3, 2, 2, 2, 288, 287, 3, 2, 2, 2, 289, 293, 3, 2,
	2, 2, 290, 294, 5, 24, 13, 2, 291, 294, 7, 28, 2, 2, 292, 294, 7, 29, 2,
	2, 293, 290, 3, 2, 2, 2, 293, 291, 3, 2, 2, 2, 293, 292, 3, 2, 2, 2, 294,
	45, 3, 2, 2, 2, 295, 298, 7, 31, 2, 2, 296, 298, 5, 26, 14, 2, 297, 295,
	3, 2, 2, 2, 297, 296, 3, 2, 2, 2, 298, 299, 3, 2, 2, 2, 299, 300, 7, 31,
	2, 2, 300, 47, 3, 2, 2, 2, 301, 302, 5, 24, 13, 2, 302, 49, 3, 2, 2, 2,
	303, 304, 5, 24, 13, 2, 304, 51, 3, 2, 2, 2, 305, 306, 7, 35, 2, 2, 306,
	318, 5, 56, 29, 2, 307, 308, 7, 35, 2, 2, 308, 318, 5, 54, 28, 2, 309,
	311, 7, 35, 2, 2, 310, 312, 5, 54, 28, 2, 311, 310, 3, 2, 2, 2, 311, 312,
	3, 2, 2, 2, 312, 313, 3, 2, 2, 2, 313, 315, 5, 62, 32, 2, 314, 316, 5,
	72, 37, 2, 315, 314, 3, 2, 2, 2, 315, 316, 3, 2, 2, 2, 316, 318, 3, 2,
	2, 2, 317, 305, 3, 2, 2, 2, 317, 307, 3, 2, 2, 2, 317, 309, 3, 2, 2, 2,
	318, 53, 3, 2, 2, 2, 319, 320, 5, 56, 29, 2, 320, 321, 5, 58, 30, 2, 321,
	55, 3, 2, 2, 2, 322, 323, 5, 24, 13, 2, 323, 57, 3, 2, 2, 2, 324, 325,
	7, 31, 2, 2, 325, 326, 7, 31, 2, 2, 326, 327, 7, 31, 2, 2, 327, 59, 3,
	2, 2, 2, 328, 329, 5, 64, 33, 2, 329, 330, 5, 66, 34, 2, 330, 331, 5, 68,
	35, 2, 331, 332, 5, 70, 36, 2, 332, 61, 3, 2, 2, 2, 333, 343, 5, 60, 31,
	2, 334, 335, 5, 64, 33, 2, 335, 336, 5, 66, 34, 2, 336, 337, 5, 68, 35,
	2, 337, 343, 3, 2, 2, 2, 338, 339, 5, 64, 33, 2, 339, 340, 5, 66, 34, 2,
	340, 343, 3, 2, 2, 2, 341, 343, 5, 64, 33, 2, 342, 333, 3, 2, 2, 2, 342,
	334, 3, 2, 2, 2, 342, 338, 3, 2, 2, 2, 342, 341, 3, 2, 2, 2, 343, 63, 3,
	2, 2, 2, 344, 345, 5, 24, 13, 2, 345, 65, 3, 2, 2, 2, 346, 347, 5, 24,
	13, 2, 347, 67, 3, 2, 2, 2, 348, 349, 5, 24, 13, 2, 349, 69, 3, 2, 2, 2,
	350, 351, 7, 31, 2, 2, 351, 352, 7, 31, 2, 2, 352, 71, 3, 2, 2, 2, 353,
	354, 5, 74, 38, 2, 354, 355, 5, 76, 39, 2, 355, 356, 5, 78, 40, 2, 356,
	73, 3, 2, 2, 2, 357, 358, 5, 24, 13, 2, 358, 75, 3, 2, 2, 2, 359, 360,
	5, 24, 13, 2, 360, 77, 3, 2, 2, 2, 361, 362, 7, 31, 2, 2, 362, 363, 7,
	31, 2, 2, 363, 364, 7, 31, 2, 2, 364, 79, 3, 2, 2, 2, 365, 367, 7, 38,
	2, 2, 366, 368, 5, 82, 42, 2, 367, 366, 3, 2, 2, 2, 368, 369, 3, 2, 2,
	2, 369, 367, 3, 2, 2, 2, 369, 370, 3, 2, 2, 2, 370, 81, 3, 2, 2, 2, 371,
	372, 5, 84, 43, 2, 372, 373, 5, 86, 44, 2, 373, 374, 5, 88, 45, 2, 374,
	83, 3, 2, 2, 2, 375, 376, 5, 24, 13, 2, 376, 85, 3, 2, 2, 2, 377, 378,
	5, 24, 13, 2, 378, 87, 3, 2, 2, 2, 379, 380, 7, 31, 2, 2, 380, 381, 7,
	31, 2, 2, 381, 382, 7, 31, 2, 2, 382, 89, 3, 2, 2, 2, 383, 385, 7, 35,
	2, 2, 384, 386, 5, 54, 28, 2, 385, 384, 3, 2, 2, 2, 385, 386, 3, 2, 2,
	2, 386, 387, 3, 2, 2, 2, 387, 388, 5, 60, 31, 2, 388, 389, 5, 72, 37, 2,
	389, 390, 5, 80, 41, 2, 390, 91, 3, 2, 2, 2, 391, 393, 7, 36, 2, 2, 392,
	394, 5, 54, 28, 2, 393, 392, 3, 2, 2, 2, 393, 394, 3, 2, 2, 2, 394, 395,
	3, 2, 2, 2, 395, 397, 5, 60, 31, 2, 396, 398, 5, 72, 37, 2, 397, 396, 3,
	2, 2, 2, 397, 398, 3, 2, 2, 2, 398, 401, 3, 2, 2, 2, 399, 400, 7, 32, 2,
	2, 400, 402, 5, 94, 48, 2, 401, 399, 3, 2, 2, 2, 401, 402, 3, 2, 2, 2,
	402, 93, 3, 2, 2, 2, 403, 404, 5, 96, 49, 2, 404, 405, 5, 98, 50, 2, 405,
	95, 3, 2, 2, 2, 406, 407, 5, 24, 13, 2, 407, 408, 5, 24, 13, 2, 408, 97,
	3, 2, 2, 2, 409, 410, 7, 31, 2, 2, 410, 411, 7, 31, 2, 2, 411, 412, 7,
	31, 2, 2, 412, 99, 3, 2, 2, 2, 413, 414, 7, 37, 2, 2, 414, 416, 5, 54,
	28, 2, 415, 417, 5, 60, 31, 2, 416, 415, 3, 2, 2, 2, 416, 417, 3, 2, 2,
	2, 417, 418, 3, 2, 2, 2, 418, 421, 7, 32, 2, 2, 419, 422, 5, 102, 52, 2,
	420, 422, 5, 32, 17, 2, 421, 419, 3, 2, 2, 2, 421, 420, 3, 2, 2, 2, 422,
	101, 3, 2, 2, 2, 423, 431, 5, 24, 13, 2, 424, 429, 5, 24, 13, 2, 425, 427,
	5, 24, 13, 2, 426, 428, 5, 24, 13, 2, 427, 426, 3, 2, 2, 2, 427, 428, 3,
	2, 2, 2, 428, 430, 3, 2, 2, 2, 429, 425, 3, 2, 2, 2, 429, 430, 3, 2, 2,
	2, 430, 432, 3, 2, 2, 2, 431, 424, 3, 2, 2, 2, 431, 432, 3, 2, 2, 2, 432,
	433, 3, 2, 2, 2, 433, 441, 7, 31, 2, 2, 434, 439, 7, 31, 2, 2, 435, 437,
	7, 31, 2, 2, 436, 438, 7, 31, 2, 2, 437, 436, 3, 2, 2, 2, 437, 438, 3,
	2, 2, 2, 438, 440, 3, 2, 2, 2, 439, 435, 3, 2, 2, 2, 439, 440, 3, 2, 2,
	2, 440, 442, 3, 2, 2, 2, 441, 434, 3, 2, 2, 2, 441, 442, 3, 2, 2, 2, 442,
	103, 3, 2, 2, 2, 443, 444, 7, 40, 2, 2, 444, 448, 5, 106, 54, 2, 445, 446,
	5, 28, 15, 2, 446, 447, 5, 114, 58, 2, 447, 449, 3, 2, 2, 2, 448, 445,
	3, 2, 2, 2, 448, 449, 3, 2, 2, 2, 449, 105, 3, 2, 2, 2, 450, 451, 5, 108,
	55, 2, 451, 452, 5, 110, 56, 2, 452, 453, 5, 112, 57, 2, 453, 107, 3, 2,
	2, 2, 454, 455, 5, 24, 13, 2, 455, 109, 3, 2, 2, 2, 456, 457, 5, 24, 13,
	2, 457, 111, 3, 2, 2, 2, 458, 459, 7, 31, 2, 2, 459, 460, 7, 31, 2, 2,
	460, 461, 7, 31, 2, 2, 461, 113, 3, 2, 2, 2, 462, 463, 5, 116, 59, 2, 463,
	464, 5, 118, 60, 2, 464, 465, 5, 120, 61, 2, 465, 115, 3, 2, 2, 2, 466,
	467, 5, 24, 13, 2, 467, 117, 3, 2, 2, 2, 468, 469, 5, 24, 13, 2, 469, 119,
	3, 2, 2, 2, 470, 471, 7, 31, 2, 2, 471, 472, 7, 31, 2, 2, 472, 121, 3,
	2, 2, 2, 473, 474, 7, 39, 2, 2, 474, 477, 5, 124, 63, 2, 475, 476, 7, 32,
	2, 2, 476, 478, 5, 124, 63, 2, 477, 475, 3, 2, 2, 2, 477, 478, 3, 2, 2,
	2, 478, 123, 3, 2, 2, 2, 479, 482, 5, 24, 13, 2, 480, 482, 7, 31, 2, 2,
	481, 479, 3, 2, 2, 2, 481, 480, 3, 2, 2, 2, 482, 483, 3, 2, 2, 2, 483,
	481, 3, 2, 2, 2, 483, 484, 3, 2, 2, 2, 484, 125, 3, 2, 2, 2, 485, 486,
	7, 41, 2, 2, 486, 489, 5, 128, 65, 2, 487, 488, 7, 33, 2, 2, 488, 490,
	5, 138, 70, 2, 489, 487, 3, 2, 2, 2, 489, 490, 3, 2, 2, 2, 490, 127, 3,
	2, 2, 2, 491, 492, 5, 130, 66, 2, 492, 493, 5, 132, 67, 2, 493, 494, 5,
	134, 68, 2, 494, 495, 5, 136, 69, 2, 495, 129, 3, 2, 2, 2, 496, 497, 5,
	24, 13, 2, 497, 131, 3, 2, 2, 2, 498, 499, 5, 24, 13, 2, 499, 133, 3, 2,
	2, 2, 500, 501, 5, 24, 13, 2, 501, 135, 3, 2, 2, 2, 502, 503, 7, 31, 2,
	2, 503, 504, 7, 31, 2, 2, 504, 505, 7, 31, 2, 2, 505, 137, 3, 2, 2, 2,
	506, 508, 5, 140, 71, 2, 507, 506, 3, 2, 2, 2, 507, 508, 3, 2, 2, 2, 508,
	509, 3, 2, 2, 2, 509, 510, 5, 142, 72, 2, 510, 139, 3, 2, 2, 2, 511, 519,
	5, 24, 13, 2, 512, 517, 5, 24, 13, 2, 513, 515, 5, 24, 13, 2, 514, 516,
	5, 24, 13, 2, 515, 514, 3, 2, 2, 2, 515, 516, 3, 2, 2, 2, 516, 518, 3,
	2, 2, 2, 517, 513, 3, 2, 2, 2, 517, 518, 3, 2, 2, 2, 518, 520, 3, 2, 2,
	2, 519, 512, 3, 2, 2, 2, 519, 520, 3, 2, 2, 2, 520, 141, 3, 2, 2, 2, 521,
	529, 7, 31, 2, 2, 522, 527, 7, 31, 2, 2, 523, 525, 7, 31, 2, 2, 524, 526,
	7, 31, 2, 2, 525, 524, 3, 2, 2, 2, 525, 526, 3, 2, 2, 2, 526, 528, 3, 2,
	2, 2, 527, 523, 3, 2, 2, 2, 527, 528, 3, 2, 2, 2, 528, 530, 3, 2, 2, 2,
	529, 522, 3, 2, 2, 2, 529, 530, 3, 2, 2, 2, 530, 143, 3, 2, 2, 2, 52, 176,
	180, 185, 188, 193, 196, 200, 203, 207, 212, 225, 227, 256, 261, 267, 273,
	278, 283, 288, 293, 297, 311, 315, 317, 342, 369, 385, 393, 397, 401, 416,
	421, 427, 429, 431, 437, 439, 441, 448, 477, 481, 483, 489, 507, 515, 517,
	519, 525, 527, 529,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'A'", "'B'", "'C'", "'D'", "'E'", "'F'", "'G'", "'H'", "'J'", "'K'",
	"'L'", "'M'", "'N'", "'P'", "'Q'", "'R'", "'S'", "'T'", "'U'", "'V'", "'W'",
	"'X'", "'Y'", "'Z'", "'_'", "'I'", "'O'", "", "", "'.'", "'/'", "'#'",
	"'='", "'+'", "'++'", "'-'", "':'", "';'", "'&'",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "", "", "", "", "", "WHITESPACE", "NUMBER", "DOT",
	"SLASH", "HASH", "EQUALS", "PLUS", "PLUSPLUS", "MINUS", "COLON", "SEMICOLON",
	"AMPERSAND",
}

var ruleNames = []string{
	"start", "startOnlyConjointDesignation", "startOnlyFunction", "startOnlyOperatingEquipment",
	"startOnlyPointOfInstallation", "startOnlySiteOfInstallation", "startOnlyProduct",
	"startOnlyTerminal", "startOnlySignal", "startOnlyDocument", "designation",
	"alpha", "underscore_alpha", "underscore", "conjointDesignation", "geographicLocation",
	"latitude", "longitude", "projectSpecificInformation", "country", "region",
	"townOrName", "projectCounter", "plantType", "facilityType", "function",
	"mainSystem", "mainSystemA1", "mainSystemCounter", "systemSubsystem", "systemSubsystemPart",
	"systemA1", "systemA2", "systemA3", "subsystem", "basicFunction", "basicFunctionA1",
	"basicFunctionA2", "basicFunctionConcrete", "product", "productClass",
	"productClassA1", "productClassA2", "productClassConcrete", "operatingEquipment",
	"pointOfInstallation", "pointOfInstallationPlace", "column", "row", "siteOfInstallation",
	"siteOfInstallationPlace", "signal", "signalClass", "signalMainClass",
	"signalSubclass", "signalClassCounter", "signalInformation", "signalDuration",
	"signalType", "signalInformationCounter", "terminal", "terminalPoint",
	"document", "dcc", "technicalSector", "dccMainClass", "dccSubclass", "dccConcrete",
	"pageCountNumber", "pageNumberAlpha", "pageNumber",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type RDSPPParser struct {
	*antlr.BaseParser
}

func NewRDSPPParser(input antlr.TokenStream) *RDSPPParser {
	this := new(RDSPPParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "RDSPP.g4"

	return this
}

// RDSPPParser tokens.
const (
	RDSPPParserEOF        = antlr.TokenEOF
	RDSPPParserT__0       = 1
	RDSPPParserT__1       = 2
	RDSPPParserT__2       = 3
	RDSPPParserT__3       = 4
	RDSPPParserT__4       = 5
	RDSPPParserT__5       = 6
	RDSPPParserT__6       = 7
	RDSPPParserT__7       = 8
	RDSPPParserT__8       = 9
	RDSPPParserT__9       = 10
	RDSPPParserT__10      = 11
	RDSPPParserT__11      = 12
	RDSPPParserT__12      = 13
	RDSPPParserT__13      = 14
	RDSPPParserT__14      = 15
	RDSPPParserT__15      = 16
	RDSPPParserT__16      = 17
	RDSPPParserT__17      = 18
	RDSPPParserT__18      = 19
	RDSPPParserT__19      = 20
	RDSPPParserT__20      = 21
	RDSPPParserT__21      = 22
	RDSPPParserT__22      = 23
	RDSPPParserT__23      = 24
	RDSPPParserT__24      = 25
	RDSPPParserT__25      = 26
	RDSPPParserT__26      = 27
	RDSPPParserWHITESPACE = 28
	RDSPPParserNUMBER     = 29
	RDSPPParserDOT        = 30
	RDSPPParserSLASH      = 31
	RDSPPParserHASH       = 32
	RDSPPParserEQUALS     = 33
	RDSPPParserPLUS       = 34
	RDSPPParserPLUSPLUS   = 35
	RDSPPParserMINUS      = 36
	RDSPPParserCOLON      = 37
	RDSPPParserSEMICOLON  = 38
	RDSPPParserAMPERSAND  = 39
)

// RDSPPParser rules.
const (
	RDSPPParserRULE_start                        = 0
	RDSPPParserRULE_startOnlyConjointDesignation = 1
	RDSPPParserRULE_startOnlyFunction            = 2
	RDSPPParserRULE_startOnlyOperatingEquipment  = 3
	RDSPPParserRULE_startOnlyPointOfInstallation = 4
	RDSPPParserRULE_startOnlySiteOfInstallation  = 5
	RDSPPParserRULE_startOnlyProduct             = 6
	RDSPPParserRULE_startOnlyTerminal            = 7
	RDSPPParserRULE_startOnlySignal              = 8
	RDSPPParserRULE_startOnlyDocument            = 9
	RDSPPParserRULE_designation                  = 10
	RDSPPParserRULE_alpha                        = 11
	RDSPPParserRULE_underscore_alpha             = 12
	RDSPPParserRULE_underscore                   = 13
	RDSPPParserRULE_conjointDesignation          = 14
	RDSPPParserRULE_geographicLocation           = 15
	RDSPPParserRULE_latitude                     = 16
	RDSPPParserRULE_longitude                    = 17
	RDSPPParserRULE_projectSpecificInformation   = 18
	RDSPPParserRULE_country                      = 19
	RDSPPParserRULE_region                       = 20
	RDSPPParserRULE_townOrName                   = 21
	RDSPPParserRULE_projectCounter               = 22
	RDSPPParserRULE_plantType                    = 23
	RDSPPParserRULE_facilityType                 = 24
	RDSPPParserRULE_function                     = 25
	RDSPPParserRULE_mainSystem                   = 26
	RDSPPParserRULE_mainSystemA1                 = 27
	RDSPPParserRULE_mainSystemCounter            = 28
	RDSPPParserRULE_systemSubsystem              = 29
	RDSPPParserRULE_systemSubsystemPart          = 30
	RDSPPParserRULE_systemA1                     = 31
	RDSPPParserRULE_systemA2                     = 32
	RDSPPParserRULE_systemA3                     = 33
	RDSPPParserRULE_subsystem                    = 34
	RDSPPParserRULE_basicFunction                = 35
	RDSPPParserRULE_basicFunctionA1              = 36
	RDSPPParserRULE_basicFunctionA2              = 37
	RDSPPParserRULE_basicFunctionConcrete        = 38
	RDSPPParserRULE_product                      = 39
	RDSPPParserRULE_productClass                 = 40
	RDSPPParserRULE_productClassA1               = 41
	RDSPPParserRULE_productClassA2               = 42
	RDSPPParserRULE_productClassConcrete         = 43
	RDSPPParserRULE_operatingEquipment           = 44
	RDSPPParserRULE_pointOfInstallation          = 45
	RDSPPParserRULE_pointOfInstallationPlace     = 46
	RDSPPParserRULE_column                       = 47
	RDSPPParserRULE_row                          = 48
	RDSPPParserRULE_siteOfInstallation           = 49
	RDSPPParserRULE_siteOfInstallationPlace      = 50
	RDSPPParserRULE_signal                       = 51
	RDSPPParserRULE_signalClass                  = 52
	RDSPPParserRULE_signalMainClass              = 53
	RDSPPParserRULE_signalSubclass               = 54
	RDSPPParserRULE_signalClassCounter           = 55
	RDSPPParserRULE_signalInformation            = 56
	RDSPPParserRULE_signalDuration               = 57
	RDSPPParserRULE_signalType                   = 58
	RDSPPParserRULE_signalInformationCounter     = 59
	RDSPPParserRULE_terminal                     = 60
	RDSPPParserRULE_terminalPoint                = 61
	RDSPPParserRULE_document                     = 62
	RDSPPParserRULE_dcc                          = 63
	RDSPPParserRULE_technicalSector              = 64
	RDSPPParserRULE_dccMainClass                 = 65
	RDSPPParserRULE_dccSubclass                  = 66
	RDSPPParserRULE_dccConcrete                  = 67
	RDSPPParserRULE_pageCountNumber              = 68
	RDSPPParserRULE_pageNumberAlpha              = 69
	RDSPPParserRULE_pageNumber                   = 70
)

// IStartContext is an interface to support dynamic dispatch.
type IStartContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartContext differentiates from other interfaces.
	IsStartContext()
}

type StartContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartContext() *StartContext {
	var p = new(StartContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_start
	return p
}

func (*StartContext) IsStartContext() {}

func NewStartContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartContext {
	var p = new(StartContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_start

	return p
}

func (s *StartContext) GetParser() antlr.Parser { return s.parser }

func (s *StartContext) Designation() IDesignationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDesignationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDesignationContext)
}

func (s *StartContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStart(s)
	}
}

func (s *StartContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStart(s)
	}
}

func (p *RDSPPParser) Start() (localctx IStartContext) {
	localctx = NewStartContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, RDSPPParserRULE_start)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(142)
		p.Designation()
	}
	{
		p.SetState(143)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlyConjointDesignationContext is an interface to support dynamic dispatch.
type IStartOnlyConjointDesignationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlyConjointDesignationContext differentiates from other interfaces.
	IsStartOnlyConjointDesignationContext()
}

type StartOnlyConjointDesignationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlyConjointDesignationContext() *StartOnlyConjointDesignationContext {
	var p = new(StartOnlyConjointDesignationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlyConjointDesignation
	return p
}

func (*StartOnlyConjointDesignationContext) IsStartOnlyConjointDesignationContext() {}

func NewStartOnlyConjointDesignationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlyConjointDesignationContext {
	var p = new(StartOnlyConjointDesignationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlyConjointDesignation

	return p
}

func (s *StartOnlyConjointDesignationContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlyConjointDesignationContext) ConjointDesignation() IConjointDesignationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IConjointDesignationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IConjointDesignationContext)
}

func (s *StartOnlyConjointDesignationContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlyConjointDesignationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlyConjointDesignationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlyConjointDesignationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlyConjointDesignation(s)
	}
}

func (s *StartOnlyConjointDesignationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlyConjointDesignation(s)
	}
}

func (p *RDSPPParser) StartOnlyConjointDesignation() (localctx IStartOnlyConjointDesignationContext) {
	localctx = NewStartOnlyConjointDesignationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, RDSPPParserRULE_startOnlyConjointDesignation)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(145)
		p.ConjointDesignation()
	}
	{
		p.SetState(146)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlyFunctionContext is an interface to support dynamic dispatch.
type IStartOnlyFunctionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlyFunctionContext differentiates from other interfaces.
	IsStartOnlyFunctionContext()
}

type StartOnlyFunctionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlyFunctionContext() *StartOnlyFunctionContext {
	var p = new(StartOnlyFunctionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlyFunction
	return p
}

func (*StartOnlyFunctionContext) IsStartOnlyFunctionContext() {}

func NewStartOnlyFunctionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlyFunctionContext {
	var p = new(StartOnlyFunctionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlyFunction

	return p
}

func (s *StartOnlyFunctionContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlyFunctionContext) Function() IFunctionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFunctionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFunctionContext)
}

func (s *StartOnlyFunctionContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlyFunctionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlyFunctionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlyFunctionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlyFunction(s)
	}
}

func (s *StartOnlyFunctionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlyFunction(s)
	}
}

func (p *RDSPPParser) StartOnlyFunction() (localctx IStartOnlyFunctionContext) {
	localctx = NewStartOnlyFunctionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, RDSPPParserRULE_startOnlyFunction)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(148)
		p.Function()
	}
	{
		p.SetState(149)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlyOperatingEquipmentContext is an interface to support dynamic dispatch.
type IStartOnlyOperatingEquipmentContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlyOperatingEquipmentContext differentiates from other interfaces.
	IsStartOnlyOperatingEquipmentContext()
}

type StartOnlyOperatingEquipmentContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlyOperatingEquipmentContext() *StartOnlyOperatingEquipmentContext {
	var p = new(StartOnlyOperatingEquipmentContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlyOperatingEquipment
	return p
}

func (*StartOnlyOperatingEquipmentContext) IsStartOnlyOperatingEquipmentContext() {}

func NewStartOnlyOperatingEquipmentContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlyOperatingEquipmentContext {
	var p = new(StartOnlyOperatingEquipmentContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlyOperatingEquipment

	return p
}

func (s *StartOnlyOperatingEquipmentContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlyOperatingEquipmentContext) OperatingEquipment() IOperatingEquipmentContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOperatingEquipmentContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOperatingEquipmentContext)
}

func (s *StartOnlyOperatingEquipmentContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlyOperatingEquipmentContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlyOperatingEquipmentContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlyOperatingEquipmentContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlyOperatingEquipment(s)
	}
}

func (s *StartOnlyOperatingEquipmentContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlyOperatingEquipment(s)
	}
}

func (p *RDSPPParser) StartOnlyOperatingEquipment() (localctx IStartOnlyOperatingEquipmentContext) {
	localctx = NewStartOnlyOperatingEquipmentContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, RDSPPParserRULE_startOnlyOperatingEquipment)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(151)
		p.OperatingEquipment()
	}
	{
		p.SetState(152)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlyPointOfInstallationContext is an interface to support dynamic dispatch.
type IStartOnlyPointOfInstallationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlyPointOfInstallationContext differentiates from other interfaces.
	IsStartOnlyPointOfInstallationContext()
}

type StartOnlyPointOfInstallationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlyPointOfInstallationContext() *StartOnlyPointOfInstallationContext {
	var p = new(StartOnlyPointOfInstallationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlyPointOfInstallation
	return p
}

func (*StartOnlyPointOfInstallationContext) IsStartOnlyPointOfInstallationContext() {}

func NewStartOnlyPointOfInstallationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlyPointOfInstallationContext {
	var p = new(StartOnlyPointOfInstallationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlyPointOfInstallation

	return p
}

func (s *StartOnlyPointOfInstallationContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlyPointOfInstallationContext) PointOfInstallation() IPointOfInstallationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPointOfInstallationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPointOfInstallationContext)
}

func (s *StartOnlyPointOfInstallationContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlyPointOfInstallationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlyPointOfInstallationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlyPointOfInstallationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlyPointOfInstallation(s)
	}
}

func (s *StartOnlyPointOfInstallationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlyPointOfInstallation(s)
	}
}

func (p *RDSPPParser) StartOnlyPointOfInstallation() (localctx IStartOnlyPointOfInstallationContext) {
	localctx = NewStartOnlyPointOfInstallationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, RDSPPParserRULE_startOnlyPointOfInstallation)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(154)
		p.PointOfInstallation()
	}
	{
		p.SetState(155)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlySiteOfInstallationContext is an interface to support dynamic dispatch.
type IStartOnlySiteOfInstallationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlySiteOfInstallationContext differentiates from other interfaces.
	IsStartOnlySiteOfInstallationContext()
}

type StartOnlySiteOfInstallationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlySiteOfInstallationContext() *StartOnlySiteOfInstallationContext {
	var p = new(StartOnlySiteOfInstallationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlySiteOfInstallation
	return p
}

func (*StartOnlySiteOfInstallationContext) IsStartOnlySiteOfInstallationContext() {}

func NewStartOnlySiteOfInstallationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlySiteOfInstallationContext {
	var p = new(StartOnlySiteOfInstallationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlySiteOfInstallation

	return p
}

func (s *StartOnlySiteOfInstallationContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlySiteOfInstallationContext) SiteOfInstallation() ISiteOfInstallationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISiteOfInstallationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISiteOfInstallationContext)
}

func (s *StartOnlySiteOfInstallationContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlySiteOfInstallationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlySiteOfInstallationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlySiteOfInstallationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlySiteOfInstallation(s)
	}
}

func (s *StartOnlySiteOfInstallationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlySiteOfInstallation(s)
	}
}

func (p *RDSPPParser) StartOnlySiteOfInstallation() (localctx IStartOnlySiteOfInstallationContext) {
	localctx = NewStartOnlySiteOfInstallationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, RDSPPParserRULE_startOnlySiteOfInstallation)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(157)
		p.SiteOfInstallation()
	}
	{
		p.SetState(158)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlyProductContext is an interface to support dynamic dispatch.
type IStartOnlyProductContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlyProductContext differentiates from other interfaces.
	IsStartOnlyProductContext()
}

type StartOnlyProductContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlyProductContext() *StartOnlyProductContext {
	var p = new(StartOnlyProductContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlyProduct
	return p
}

func (*StartOnlyProductContext) IsStartOnlyProductContext() {}

func NewStartOnlyProductContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlyProductContext {
	var p = new(StartOnlyProductContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlyProduct

	return p
}

func (s *StartOnlyProductContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlyProductContext) Product() IProductContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProductContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProductContext)
}

func (s *StartOnlyProductContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlyProductContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlyProductContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlyProductContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlyProduct(s)
	}
}

func (s *StartOnlyProductContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlyProduct(s)
	}
}

func (p *RDSPPParser) StartOnlyProduct() (localctx IStartOnlyProductContext) {
	localctx = NewStartOnlyProductContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, RDSPPParserRULE_startOnlyProduct)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(160)
		p.Product()
	}
	{
		p.SetState(161)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlyTerminalContext is an interface to support dynamic dispatch.
type IStartOnlyTerminalContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlyTerminalContext differentiates from other interfaces.
	IsStartOnlyTerminalContext()
}

type StartOnlyTerminalContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlyTerminalContext() *StartOnlyTerminalContext {
	var p = new(StartOnlyTerminalContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlyTerminal
	return p
}

func (*StartOnlyTerminalContext) IsStartOnlyTerminalContext() {}

func NewStartOnlyTerminalContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlyTerminalContext {
	var p = new(StartOnlyTerminalContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlyTerminal

	return p
}

func (s *StartOnlyTerminalContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlyTerminalContext) Terminal() ITerminalContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITerminalContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITerminalContext)
}

func (s *StartOnlyTerminalContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlyTerminalContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlyTerminalContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlyTerminalContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlyTerminal(s)
	}
}

func (s *StartOnlyTerminalContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlyTerminal(s)
	}
}

func (p *RDSPPParser) StartOnlyTerminal() (localctx IStartOnlyTerminalContext) {
	localctx = NewStartOnlyTerminalContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, RDSPPParserRULE_startOnlyTerminal)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(163)
		p.Terminal()
	}
	{
		p.SetState(164)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlySignalContext is an interface to support dynamic dispatch.
type IStartOnlySignalContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlySignalContext differentiates from other interfaces.
	IsStartOnlySignalContext()
}

type StartOnlySignalContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlySignalContext() *StartOnlySignalContext {
	var p = new(StartOnlySignalContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlySignal
	return p
}

func (*StartOnlySignalContext) IsStartOnlySignalContext() {}

func NewStartOnlySignalContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlySignalContext {
	var p = new(StartOnlySignalContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlySignal

	return p
}

func (s *StartOnlySignalContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlySignalContext) Signal() ISignalContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalContext)
}

func (s *StartOnlySignalContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlySignalContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlySignalContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlySignalContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlySignal(s)
	}
}

func (s *StartOnlySignalContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlySignal(s)
	}
}

func (p *RDSPPParser) StartOnlySignal() (localctx IStartOnlySignalContext) {
	localctx = NewStartOnlySignalContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, RDSPPParserRULE_startOnlySignal)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(166)
		p.Signal()
	}
	{
		p.SetState(167)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IStartOnlyDocumentContext is an interface to support dynamic dispatch.
type IStartOnlyDocumentContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartOnlyDocumentContext differentiates from other interfaces.
	IsStartOnlyDocumentContext()
}

type StartOnlyDocumentContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartOnlyDocumentContext() *StartOnlyDocumentContext {
	var p = new(StartOnlyDocumentContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_startOnlyDocument
	return p
}

func (*StartOnlyDocumentContext) IsStartOnlyDocumentContext() {}

func NewStartOnlyDocumentContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartOnlyDocumentContext {
	var p = new(StartOnlyDocumentContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_startOnlyDocument

	return p
}

func (s *StartOnlyDocumentContext) GetParser() antlr.Parser { return s.parser }

func (s *StartOnlyDocumentContext) Document() IDocumentContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDocumentContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDocumentContext)
}

func (s *StartOnlyDocumentContext) EOF() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEOF, 0)
}

func (s *StartOnlyDocumentContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartOnlyDocumentContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartOnlyDocumentContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterStartOnlyDocument(s)
	}
}

func (s *StartOnlyDocumentContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitStartOnlyDocument(s)
	}
}

func (p *RDSPPParser) StartOnlyDocument() (localctx IStartOnlyDocumentContext) {
	localctx = NewStartOnlyDocumentContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, RDSPPParserRULE_startOnlyDocument)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(169)
		p.Document()
	}
	{
		p.SetState(170)
		p.Match(RDSPPParserEOF)
	}

	return localctx
}

// IDesignationContext is an interface to support dynamic dispatch.
type IDesignationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDesignationContext differentiates from other interfaces.
	IsDesignationContext()
}

type DesignationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDesignationContext() *DesignationContext {
	var p = new(DesignationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_designation
	return p
}

func (*DesignationContext) IsDesignationContext() {}

func NewDesignationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DesignationContext {
	var p = new(DesignationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_designation

	return p
}

func (s *DesignationContext) GetParser() antlr.Parser { return s.parser }

func (s *DesignationContext) ConjointDesignation() IConjointDesignationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IConjointDesignationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IConjointDesignationContext)
}

func (s *DesignationContext) Document() IDocumentContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDocumentContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDocumentContext)
}

func (s *DesignationContext) Function() IFunctionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFunctionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFunctionContext)
}

func (s *DesignationContext) Signal() ISignalContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalContext)
}

func (s *DesignationContext) OperatingEquipment() IOperatingEquipmentContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOperatingEquipmentContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOperatingEquipmentContext)
}

func (s *DesignationContext) Terminal() ITerminalContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITerminalContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITerminalContext)
}

func (s *DesignationContext) PointOfInstallation() IPointOfInstallationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPointOfInstallationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPointOfInstallationContext)
}

func (s *DesignationContext) SiteOfInstallation() ISiteOfInstallationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISiteOfInstallationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISiteOfInstallationContext)
}

func (s *DesignationContext) Product() IProductContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProductContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProductContext)
}

func (s *DesignationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DesignationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DesignationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterDesignation(s)
	}
}

func (s *DesignationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitDesignation(s)
	}
}

func (p *RDSPPParser) Designation() (localctx IDesignationContext) {
	localctx = NewDesignationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, RDSPPParserRULE_designation)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(210)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 9, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(172)
			p.ConjointDesignation()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		p.SetState(174)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserHASH {
			{
				p.SetState(173)
				p.ConjointDesignation()
			}

		}
		{
			p.SetState(176)
			p.Document()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		p.SetState(178)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserHASH {
			{
				p.SetState(177)
				p.ConjointDesignation()
			}

		}
		{
			p.SetState(180)
			p.Function()
		}
		p.SetState(183)
		p.GetErrorHandler().Sync(p)

		switch p.GetTokenStream().LA(1) {
		case RDSPPParserSEMICOLON:
			{
				p.SetState(181)
				p.Signal()
			}

		case RDSPPParserAMPERSAND:
			{
				p.SetState(182)
				p.Document()
			}

		case RDSPPParserEOF:

		default:
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		p.SetState(186)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserHASH {
			{
				p.SetState(185)
				p.ConjointDesignation()
			}

		}
		{
			p.SetState(188)
			p.OperatingEquipment()
		}
		p.SetState(191)
		p.GetErrorHandler().Sync(p)

		switch p.GetTokenStream().LA(1) {
		case RDSPPParserCOLON:
			{
				p.SetState(189)
				p.Terminal()
			}

		case RDSPPParserAMPERSAND:
			{
				p.SetState(190)
				p.Document()
			}

		case RDSPPParserEOF:

		default:
		}

	case 5:
		p.EnterOuterAlt(localctx, 5)
		p.SetState(194)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserHASH {
			{
				p.SetState(193)
				p.ConjointDesignation()
			}

		}
		{
			p.SetState(196)
			p.PointOfInstallation()
		}
		p.SetState(198)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserAMPERSAND {
			{
				p.SetState(197)
				p.Document()
			}

		}

	case 6:
		p.EnterOuterAlt(localctx, 6)
		p.SetState(201)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserHASH {
			{
				p.SetState(200)
				p.ConjointDesignation()
			}

		}
		{
			p.SetState(203)
			p.SiteOfInstallation()
		}
		p.SetState(205)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserAMPERSAND {
			{
				p.SetState(204)
				p.Document()
			}

		}

	case 7:
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(207)
			p.Product()
		}

	case 8:
		p.EnterOuterAlt(localctx, 8)
		{
			p.SetState(208)
			p.Terminal()
		}

	case 9:
		p.EnterOuterAlt(localctx, 9)
		{
			p.SetState(209)
			p.Signal()
		}

	}

	return localctx
}

// IAlphaContext is an interface to support dynamic dispatch.
type IAlphaContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsAlphaContext differentiates from other interfaces.
	IsAlphaContext()
}

type AlphaContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyAlphaContext() *AlphaContext {
	var p = new(AlphaContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_alpha
	return p
}

func (*AlphaContext) IsAlphaContext() {}

func NewAlphaContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AlphaContext {
	var p = new(AlphaContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_alpha

	return p
}

func (s *AlphaContext) GetParser() antlr.Parser { return s.parser }
func (s *AlphaContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AlphaContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AlphaContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterAlpha(s)
	}
}

func (s *AlphaContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitAlpha(s)
	}
}

func (p *RDSPPParser) Alpha() (localctx IAlphaContext) {
	localctx = NewAlphaContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, RDSPPParserRULE_alpha)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(212)
		_la = p.GetTokenStream().LA(1)

		if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IUnderscore_alphaContext is an interface to support dynamic dispatch.
type IUnderscore_alphaContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsUnderscore_alphaContext differentiates from other interfaces.
	IsUnderscore_alphaContext()
}

type Underscore_alphaContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUnderscore_alphaContext() *Underscore_alphaContext {
	var p = new(Underscore_alphaContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_underscore_alpha
	return p
}

func (*Underscore_alphaContext) IsUnderscore_alphaContext() {}

func NewUnderscore_alphaContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *Underscore_alphaContext {
	var p = new(Underscore_alphaContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_underscore_alpha

	return p
}

func (s *Underscore_alphaContext) GetParser() antlr.Parser { return s.parser }
func (s *Underscore_alphaContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *Underscore_alphaContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *Underscore_alphaContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterUnderscore_alpha(s)
	}
}

func (s *Underscore_alphaContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitUnderscore_alpha(s)
	}
}

func (p *RDSPPParser) Underscore_alpha() (localctx IUnderscore_alphaContext) {
	localctx = NewUnderscore_alphaContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, RDSPPParserRULE_underscore_alpha)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(214)
		p.Match(RDSPPParserT__24)
	}

	return localctx
}

// IUnderscoreContext is an interface to support dynamic dispatch.
type IUnderscoreContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsUnderscoreContext differentiates from other interfaces.
	IsUnderscoreContext()
}

type UnderscoreContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUnderscoreContext() *UnderscoreContext {
	var p = new(UnderscoreContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_underscore
	return p
}

func (*UnderscoreContext) IsUnderscoreContext() {}

func NewUnderscoreContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *UnderscoreContext {
	var p = new(UnderscoreContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_underscore

	return p
}

func (s *UnderscoreContext) GetParser() antlr.Parser { return s.parser }
func (s *UnderscoreContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UnderscoreContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *UnderscoreContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterUnderscore(s)
	}
}

func (s *UnderscoreContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitUnderscore(s)
	}
}

func (p *RDSPPParser) Underscore() (localctx IUnderscoreContext) {
	localctx = NewUnderscoreContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, RDSPPParserRULE_underscore)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(216)
		p.Match(RDSPPParserT__24)
	}

	return localctx
}

// IConjointDesignationContext is an interface to support dynamic dispatch.
type IConjointDesignationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsConjointDesignationContext differentiates from other interfaces.
	IsConjointDesignationContext()
}

type ConjointDesignationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyConjointDesignationContext() *ConjointDesignationContext {
	var p = new(ConjointDesignationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_conjointDesignation
	return p
}

func (*ConjointDesignationContext) IsConjointDesignationContext() {}

func NewConjointDesignationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ConjointDesignationContext {
	var p = new(ConjointDesignationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_conjointDesignation

	return p
}

func (s *ConjointDesignationContext) GetParser() antlr.Parser { return s.parser }

func (s *ConjointDesignationContext) HASH() antlr.TerminalNode {
	return s.GetToken(RDSPPParserHASH, 0)
}

func (s *ConjointDesignationContext) ProjectSpecificInformation() IProjectSpecificInformationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProjectSpecificInformationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProjectSpecificInformationContext)
}

func (s *ConjointDesignationContext) GeographicLocation() IGeographicLocationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IGeographicLocationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IGeographicLocationContext)
}

func (s *ConjointDesignationContext) DOT() antlr.TerminalNode {
	return s.GetToken(RDSPPParserDOT, 0)
}

func (s *ConjointDesignationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ConjointDesignationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ConjointDesignationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterConjointDesignation(s)
	}
}

func (s *ConjointDesignationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitConjointDesignation(s)
	}
}

func (p *RDSPPParser) ConjointDesignation() (localctx IConjointDesignationContext) {
	localctx = NewConjointDesignationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, RDSPPParserRULE_conjointDesignation)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(218)
		p.Match(RDSPPParserHASH)
	}
	p.SetState(225)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23, RDSPPParserT__25, RDSPPParserT__26:
		{
			p.SetState(219)
			p.ProjectSpecificInformation()
		}

	case RDSPPParserNUMBER:
		{
			p.SetState(220)
			p.GeographicLocation()
		}
		p.SetState(223)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserDOT {
			{
				p.SetState(221)
				p.Match(RDSPPParserDOT)
			}
			{
				p.SetState(222)
				p.ProjectSpecificInformation()
			}

		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IGeographicLocationContext is an interface to support dynamic dispatch.
type IGeographicLocationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsGeographicLocationContext differentiates from other interfaces.
	IsGeographicLocationContext()
}

type GeographicLocationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyGeographicLocationContext() *GeographicLocationContext {
	var p = new(GeographicLocationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_geographicLocation
	return p
}

func (*GeographicLocationContext) IsGeographicLocationContext() {}

func NewGeographicLocationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *GeographicLocationContext {
	var p = new(GeographicLocationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_geographicLocation

	return p
}

func (s *GeographicLocationContext) GetParser() antlr.Parser { return s.parser }

func (s *GeographicLocationContext) Latitude() ILatitudeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ILatitudeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ILatitudeContext)
}

func (s *GeographicLocationContext) Longitude() ILongitudeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ILongitudeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ILongitudeContext)
}

func (s *GeographicLocationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *GeographicLocationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *GeographicLocationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterGeographicLocation(s)
	}
}

func (s *GeographicLocationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitGeographicLocation(s)
	}
}

func (p *RDSPPParser) GeographicLocation() (localctx IGeographicLocationContext) {
	localctx = NewGeographicLocationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, RDSPPParserRULE_geographicLocation)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(227)
		p.Latitude()
	}
	{
		p.SetState(228)
		p.Longitude()
	}

	return localctx
}

// ILatitudeContext is an interface to support dynamic dispatch.
type ILatitudeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsLatitudeContext differentiates from other interfaces.
	IsLatitudeContext()
}

type LatitudeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLatitudeContext() *LatitudeContext {
	var p = new(LatitudeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_latitude
	return p
}

func (*LatitudeContext) IsLatitudeContext() {}

func NewLatitudeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LatitudeContext {
	var p = new(LatitudeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_latitude

	return p
}

func (s *LatitudeContext) GetParser() antlr.Parser { return s.parser }

func (s *LatitudeContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *LatitudeContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *LatitudeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LatitudeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LatitudeContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterLatitude(s)
	}
}

func (s *LatitudeContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitLatitude(s)
	}
}

func (p *RDSPPParser) Latitude() (localctx ILatitudeContext) {
	localctx = NewLatitudeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, RDSPPParserRULE_latitude)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(230)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(231)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(232)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(233)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(234)
		_la = p.GetTokenStream().LA(1)

		if !(_la == RDSPPParserT__12 || _la == RDSPPParserT__16) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// ILongitudeContext is an interface to support dynamic dispatch.
type ILongitudeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsLongitudeContext differentiates from other interfaces.
	IsLongitudeContext()
}

type LongitudeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLongitudeContext() *LongitudeContext {
	var p = new(LongitudeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_longitude
	return p
}

func (*LongitudeContext) IsLongitudeContext() {}

func NewLongitudeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LongitudeContext {
	var p = new(LongitudeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_longitude

	return p
}

func (s *LongitudeContext) GetParser() antlr.Parser { return s.parser }

func (s *LongitudeContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *LongitudeContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *LongitudeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LongitudeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LongitudeContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterLongitude(s)
	}
}

func (s *LongitudeContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitLongitude(s)
	}
}

func (p *RDSPPParser) Longitude() (localctx ILongitudeContext) {
	localctx = NewLongitudeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, RDSPPParserRULE_longitude)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(236)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(237)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(238)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(239)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(240)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(241)
		_la = p.GetTokenStream().LA(1)

		if !(_la == RDSPPParserT__4 || _la == RDSPPParserT__20) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IProjectSpecificInformationContext is an interface to support dynamic dispatch.
type IProjectSpecificInformationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsProjectSpecificInformationContext differentiates from other interfaces.
	IsProjectSpecificInformationContext()
}

type ProjectSpecificInformationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyProjectSpecificInformationContext() *ProjectSpecificInformationContext {
	var p = new(ProjectSpecificInformationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_projectSpecificInformation
	return p
}

func (*ProjectSpecificInformationContext) IsProjectSpecificInformationContext() {}

func NewProjectSpecificInformationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProjectSpecificInformationContext {
	var p = new(ProjectSpecificInformationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_projectSpecificInformation

	return p
}

func (s *ProjectSpecificInformationContext) GetParser() antlr.Parser { return s.parser }

func (s *ProjectSpecificInformationContext) Country() ICountryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICountryContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICountryContext)
}

func (s *ProjectSpecificInformationContext) Region() IRegionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRegionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRegionContext)
}

func (s *ProjectSpecificInformationContext) DOT() antlr.TerminalNode {
	return s.GetToken(RDSPPParserDOT, 0)
}

func (s *ProjectSpecificInformationContext) TownOrName() ITownOrNameContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITownOrNameContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITownOrNameContext)
}

func (s *ProjectSpecificInformationContext) ProjectCounter() IProjectCounterContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProjectCounterContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProjectCounterContext)
}

func (s *ProjectSpecificInformationContext) PlantType() IPlantTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPlantTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPlantTypeContext)
}

func (s *ProjectSpecificInformationContext) FacilityType() IFacilityTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFacilityTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFacilityTypeContext)
}

func (s *ProjectSpecificInformationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProjectSpecificInformationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProjectSpecificInformationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterProjectSpecificInformation(s)
	}
}

func (s *ProjectSpecificInformationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitProjectSpecificInformation(s)
	}
}

func (p *RDSPPParser) ProjectSpecificInformation() (localctx IProjectSpecificInformationContext) {
	localctx = NewProjectSpecificInformationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, RDSPPParserRULE_projectSpecificInformation)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(243)
		p.Country()
	}
	{
		p.SetState(244)
		p.Region()
	}
	{
		p.SetState(245)
		p.Match(RDSPPParserDOT)
	}
	{
		p.SetState(246)
		p.TownOrName()
	}
	{
		p.SetState(247)
		p.ProjectCounter()
	}
	{
		p.SetState(248)
		p.PlantType()
	}
	{
		p.SetState(249)
		p.FacilityType()
	}

	return localctx
}

// ICountryContext is an interface to support dynamic dispatch.
type ICountryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCountryContext differentiates from other interfaces.
	IsCountryContext()
}

type CountryContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCountryContext() *CountryContext {
	var p = new(CountryContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_country
	return p
}

func (*CountryContext) IsCountryContext() {}

func NewCountryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CountryContext {
	var p = new(CountryContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_country

	return p
}

func (s *CountryContext) GetParser() antlr.Parser { return s.parser }

func (s *CountryContext) AllAlpha() []IAlphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAlphaContext)(nil)).Elem())
	var tst = make([]IAlphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAlphaContext)
		}
	}

	return tst
}

func (s *CountryContext) Alpha(i int) IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *CountryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CountryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CountryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterCountry(s)
	}
}

func (s *CountryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitCountry(s)
	}
}

func (p *RDSPPParser) Country() (localctx ICountryContext) {
	localctx = NewCountryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, RDSPPParserRULE_country)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(254)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(251)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(252)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(253)
			p.Match(RDSPPParserT__26)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.SetState(259)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(256)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(257)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(258)
			p.Match(RDSPPParserT__26)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IRegionContext is an interface to support dynamic dispatch.
type IRegionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsRegionContext differentiates from other interfaces.
	IsRegionContext()
}

type RegionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRegionContext() *RegionContext {
	var p = new(RegionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_region
	return p
}

func (*RegionContext) IsRegionContext() {}

func NewRegionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RegionContext {
	var p = new(RegionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_region

	return p
}

func (s *RegionContext) GetParser() antlr.Parser { return s.parser }

func (s *RegionContext) AllAlpha() []IAlphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAlphaContext)(nil)).Elem())
	var tst = make([]IAlphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAlphaContext)
		}
	}

	return tst
}

func (s *RegionContext) Alpha(i int) IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *RegionContext) AllUnderscore_alpha() []IUnderscore_alphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IUnderscore_alphaContext)(nil)).Elem())
	var tst = make([]IUnderscore_alphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IUnderscore_alphaContext)
		}
	}

	return tst
}

func (s *RegionContext) Underscore_alpha(i int) IUnderscore_alphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUnderscore_alphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IUnderscore_alphaContext)
}

func (s *RegionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RegionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RegionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterRegion(s)
	}
}

func (s *RegionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitRegion(s)
	}
}

func (p *RDSPPParser) Region() (localctx IRegionContext) {
	localctx = NewRegionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, RDSPPParserRULE_region)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(265)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(261)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(262)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(263)
			p.Match(RDSPPParserT__26)
		}

	case RDSPPParserT__24:
		{
			p.SetState(264)
			p.Underscore_alpha()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.SetState(271)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(267)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(268)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(269)
			p.Match(RDSPPParserT__26)
		}

	case RDSPPParserT__24:
		{
			p.SetState(270)
			p.Underscore_alpha()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.SetState(276)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(273)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(274)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(275)
			p.Match(RDSPPParserT__26)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// ITownOrNameContext is an interface to support dynamic dispatch.
type ITownOrNameContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTownOrNameContext differentiates from other interfaces.
	IsTownOrNameContext()
}

type TownOrNameContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTownOrNameContext() *TownOrNameContext {
	var p = new(TownOrNameContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_townOrName
	return p
}

func (*TownOrNameContext) IsTownOrNameContext() {}

func NewTownOrNameContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TownOrNameContext {
	var p = new(TownOrNameContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_townOrName

	return p
}

func (s *TownOrNameContext) GetParser() antlr.Parser { return s.parser }

func (s *TownOrNameContext) AllAlpha() []IAlphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAlphaContext)(nil)).Elem())
	var tst = make([]IAlphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAlphaContext)
		}
	}

	return tst
}

func (s *TownOrNameContext) Alpha(i int) IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *TownOrNameContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TownOrNameContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TownOrNameContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterTownOrName(s)
	}
}

func (s *TownOrNameContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitTownOrName(s)
	}
}

func (p *RDSPPParser) TownOrName() (localctx ITownOrNameContext) {
	localctx = NewTownOrNameContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 42, RDSPPParserRULE_townOrName)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(281)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(278)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(279)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(280)
			p.Match(RDSPPParserT__26)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.SetState(286)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(283)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(284)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(285)
			p.Match(RDSPPParserT__26)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.SetState(291)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(288)
			p.Alpha()
		}

	case RDSPPParserT__25:
		{
			p.SetState(289)
			p.Match(RDSPPParserT__25)
		}

	case RDSPPParserT__26:
		{
			p.SetState(290)
			p.Match(RDSPPParserT__26)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IProjectCounterContext is an interface to support dynamic dispatch.
type IProjectCounterContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsProjectCounterContext differentiates from other interfaces.
	IsProjectCounterContext()
}

type ProjectCounterContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyProjectCounterContext() *ProjectCounterContext {
	var p = new(ProjectCounterContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_projectCounter
	return p
}

func (*ProjectCounterContext) IsProjectCounterContext() {}

func NewProjectCounterContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProjectCounterContext {
	var p = new(ProjectCounterContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_projectCounter

	return p
}

func (s *ProjectCounterContext) GetParser() antlr.Parser { return s.parser }

func (s *ProjectCounterContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *ProjectCounterContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *ProjectCounterContext) Underscore_alpha() IUnderscore_alphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUnderscore_alphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IUnderscore_alphaContext)
}

func (s *ProjectCounterContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProjectCounterContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProjectCounterContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterProjectCounter(s)
	}
}

func (s *ProjectCounterContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitProjectCounter(s)
	}
}

func (p *RDSPPParser) ProjectCounter() (localctx IProjectCounterContext) {
	localctx = NewProjectCounterContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 44, RDSPPParserRULE_projectCounter)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(295)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserNUMBER:
		{
			p.SetState(293)
			p.Match(RDSPPParserNUMBER)
		}

	case RDSPPParserT__24:
		{
			p.SetState(294)
			p.Underscore_alpha()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	{
		p.SetState(297)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// IPlantTypeContext is an interface to support dynamic dispatch.
type IPlantTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPlantTypeContext differentiates from other interfaces.
	IsPlantTypeContext()
}

type PlantTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPlantTypeContext() *PlantTypeContext {
	var p = new(PlantTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_plantType
	return p
}

func (*PlantTypeContext) IsPlantTypeContext() {}

func NewPlantTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PlantTypeContext {
	var p = new(PlantTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_plantType

	return p
}

func (s *PlantTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *PlantTypeContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *PlantTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PlantTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PlantTypeContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterPlantType(s)
	}
}

func (s *PlantTypeContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitPlantType(s)
	}
}

func (p *RDSPPParser) PlantType() (localctx IPlantTypeContext) {
	localctx = NewPlantTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 46, RDSPPParserRULE_plantType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(299)
		p.Alpha()
	}

	return localctx
}

// IFacilityTypeContext is an interface to support dynamic dispatch.
type IFacilityTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFacilityTypeContext differentiates from other interfaces.
	IsFacilityTypeContext()
}

type FacilityTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFacilityTypeContext() *FacilityTypeContext {
	var p = new(FacilityTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_facilityType
	return p
}

func (*FacilityTypeContext) IsFacilityTypeContext() {}

func NewFacilityTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FacilityTypeContext {
	var p = new(FacilityTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_facilityType

	return p
}

func (s *FacilityTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *FacilityTypeContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *FacilityTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FacilityTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FacilityTypeContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterFacilityType(s)
	}
}

func (s *FacilityTypeContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitFacilityType(s)
	}
}

func (p *RDSPPParser) FacilityType() (localctx IFacilityTypeContext) {
	localctx = NewFacilityTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 48, RDSPPParserRULE_facilityType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(301)
		p.Alpha()
	}

	return localctx
}

// IFunctionContext is an interface to support dynamic dispatch.
type IFunctionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFunctionContext differentiates from other interfaces.
	IsFunctionContext()
}

type FunctionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFunctionContext() *FunctionContext {
	var p = new(FunctionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_function
	return p
}

func (*FunctionContext) IsFunctionContext() {}

func NewFunctionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FunctionContext {
	var p = new(FunctionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_function

	return p
}

func (s *FunctionContext) GetParser() antlr.Parser { return s.parser }

func (s *FunctionContext) EQUALS() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEQUALS, 0)
}

func (s *FunctionContext) MainSystemA1() IMainSystemA1Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMainSystemA1Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMainSystemA1Context)
}

func (s *FunctionContext) MainSystem() IMainSystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMainSystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMainSystemContext)
}

func (s *FunctionContext) SystemSubsystemPart() ISystemSubsystemPartContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemSubsystemPartContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemSubsystemPartContext)
}

func (s *FunctionContext) BasicFunction() IBasicFunctionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBasicFunctionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBasicFunctionContext)
}

func (s *FunctionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FunctionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FunctionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterFunction(s)
	}
}

func (s *FunctionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitFunction(s)
	}
}

func (p *RDSPPParser) Function() (localctx IFunctionContext) {
	localctx = NewFunctionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 50, RDSPPParserRULE_function)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(315)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 23, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(303)
			p.Match(RDSPPParserEQUALS)
		}
		{
			p.SetState(304)
			p.MainSystemA1()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(305)
			p.Match(RDSPPParserEQUALS)
		}
		{
			p.SetState(306)
			p.MainSystem()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(307)
			p.Match(RDSPPParserEQUALS)
		}
		p.SetState(309)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 21, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(308)
				p.MainSystem()
			}

		}
		{
			p.SetState(311)
			p.SystemSubsystemPart()
		}
		p.SetState(313)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
			{
				p.SetState(312)
				p.BasicFunction()
			}

		}

	}

	return localctx
}

// IMainSystemContext is an interface to support dynamic dispatch.
type IMainSystemContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsMainSystemContext differentiates from other interfaces.
	IsMainSystemContext()
}

type MainSystemContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMainSystemContext() *MainSystemContext {
	var p = new(MainSystemContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_mainSystem
	return p
}

func (*MainSystemContext) IsMainSystemContext() {}

func NewMainSystemContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MainSystemContext {
	var p = new(MainSystemContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_mainSystem

	return p
}

func (s *MainSystemContext) GetParser() antlr.Parser { return s.parser }

func (s *MainSystemContext) MainSystemA1() IMainSystemA1Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMainSystemA1Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMainSystemA1Context)
}

func (s *MainSystemContext) MainSystemCounter() IMainSystemCounterContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMainSystemCounterContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMainSystemCounterContext)
}

func (s *MainSystemContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MainSystemContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MainSystemContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterMainSystem(s)
	}
}

func (s *MainSystemContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitMainSystem(s)
	}
}

func (p *RDSPPParser) MainSystem() (localctx IMainSystemContext) {
	localctx = NewMainSystemContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 52, RDSPPParserRULE_mainSystem)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(317)
		p.MainSystemA1()
	}
	{
		p.SetState(318)
		p.MainSystemCounter()
	}

	return localctx
}

// IMainSystemA1Context is an interface to support dynamic dispatch.
type IMainSystemA1Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsMainSystemA1Context differentiates from other interfaces.
	IsMainSystemA1Context()
}

type MainSystemA1Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMainSystemA1Context() *MainSystemA1Context {
	var p = new(MainSystemA1Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_mainSystemA1
	return p
}

func (*MainSystemA1Context) IsMainSystemA1Context() {}

func NewMainSystemA1Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MainSystemA1Context {
	var p = new(MainSystemA1Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_mainSystemA1

	return p
}

func (s *MainSystemA1Context) GetParser() antlr.Parser { return s.parser }

func (s *MainSystemA1Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *MainSystemA1Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MainSystemA1Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MainSystemA1Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterMainSystemA1(s)
	}
}

func (s *MainSystemA1Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitMainSystemA1(s)
	}
}

func (p *RDSPPParser) MainSystemA1() (localctx IMainSystemA1Context) {
	localctx = NewMainSystemA1Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 54, RDSPPParserRULE_mainSystemA1)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(320)
		p.Alpha()
	}

	return localctx
}

// IMainSystemCounterContext is an interface to support dynamic dispatch.
type IMainSystemCounterContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsMainSystemCounterContext differentiates from other interfaces.
	IsMainSystemCounterContext()
}

type MainSystemCounterContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMainSystemCounterContext() *MainSystemCounterContext {
	var p = new(MainSystemCounterContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_mainSystemCounter
	return p
}

func (*MainSystemCounterContext) IsMainSystemCounterContext() {}

func NewMainSystemCounterContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MainSystemCounterContext {
	var p = new(MainSystemCounterContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_mainSystemCounter

	return p
}

func (s *MainSystemCounterContext) GetParser() antlr.Parser { return s.parser }

func (s *MainSystemCounterContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *MainSystemCounterContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *MainSystemCounterContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MainSystemCounterContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MainSystemCounterContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterMainSystemCounter(s)
	}
}

func (s *MainSystemCounterContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitMainSystemCounter(s)
	}
}

func (p *RDSPPParser) MainSystemCounter() (localctx IMainSystemCounterContext) {
	localctx = NewMainSystemCounterContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 56, RDSPPParserRULE_mainSystemCounter)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(322)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(323)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(324)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// ISystemSubsystemContext is an interface to support dynamic dispatch.
type ISystemSubsystemContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSystemSubsystemContext differentiates from other interfaces.
	IsSystemSubsystemContext()
}

type SystemSubsystemContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySystemSubsystemContext() *SystemSubsystemContext {
	var p = new(SystemSubsystemContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_systemSubsystem
	return p
}

func (*SystemSubsystemContext) IsSystemSubsystemContext() {}

func NewSystemSubsystemContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SystemSubsystemContext {
	var p = new(SystemSubsystemContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_systemSubsystem

	return p
}

func (s *SystemSubsystemContext) GetParser() antlr.Parser { return s.parser }

func (s *SystemSubsystemContext) SystemA1() ISystemA1Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemA1Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemA1Context)
}

func (s *SystemSubsystemContext) SystemA2() ISystemA2Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemA2Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemA2Context)
}

func (s *SystemSubsystemContext) SystemA3() ISystemA3Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemA3Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemA3Context)
}

func (s *SystemSubsystemContext) Subsystem() ISubsystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISubsystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISubsystemContext)
}

func (s *SystemSubsystemContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SystemSubsystemContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SystemSubsystemContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSystemSubsystem(s)
	}
}

func (s *SystemSubsystemContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSystemSubsystem(s)
	}
}

func (p *RDSPPParser) SystemSubsystem() (localctx ISystemSubsystemContext) {
	localctx = NewSystemSubsystemContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 58, RDSPPParserRULE_systemSubsystem)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(326)
		p.SystemA1()
	}
	{
		p.SetState(327)
		p.SystemA2()
	}
	{
		p.SetState(328)
		p.SystemA3()
	}
	{
		p.SetState(329)
		p.Subsystem()
	}

	return localctx
}

// ISystemSubsystemPartContext is an interface to support dynamic dispatch.
type ISystemSubsystemPartContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSystemSubsystemPartContext differentiates from other interfaces.
	IsSystemSubsystemPartContext()
}

type SystemSubsystemPartContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySystemSubsystemPartContext() *SystemSubsystemPartContext {
	var p = new(SystemSubsystemPartContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_systemSubsystemPart
	return p
}

func (*SystemSubsystemPartContext) IsSystemSubsystemPartContext() {}

func NewSystemSubsystemPartContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SystemSubsystemPartContext {
	var p = new(SystemSubsystemPartContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_systemSubsystemPart

	return p
}

func (s *SystemSubsystemPartContext) GetParser() antlr.Parser { return s.parser }

func (s *SystemSubsystemPartContext) SystemSubsystem() ISystemSubsystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemSubsystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemSubsystemContext)
}

func (s *SystemSubsystemPartContext) SystemA1() ISystemA1Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemA1Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemA1Context)
}

func (s *SystemSubsystemPartContext) SystemA2() ISystemA2Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemA2Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemA2Context)
}

func (s *SystemSubsystemPartContext) SystemA3() ISystemA3Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemA3Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemA3Context)
}

func (s *SystemSubsystemPartContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SystemSubsystemPartContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SystemSubsystemPartContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSystemSubsystemPart(s)
	}
}

func (s *SystemSubsystemPartContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSystemSubsystemPart(s)
	}
}

func (p *RDSPPParser) SystemSubsystemPart() (localctx ISystemSubsystemPartContext) {
	localctx = NewSystemSubsystemPartContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 60, RDSPPParserRULE_systemSubsystemPart)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(340)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 24, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(331)
			p.SystemSubsystem()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(332)
			p.SystemA1()
		}
		{
			p.SetState(333)
			p.SystemA2()
		}
		{
			p.SetState(334)
			p.SystemA3()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(336)
			p.SystemA1()
		}
		{
			p.SetState(337)
			p.SystemA2()
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(339)
			p.SystemA1()
		}

	}

	return localctx
}

// ISystemA1Context is an interface to support dynamic dispatch.
type ISystemA1Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSystemA1Context differentiates from other interfaces.
	IsSystemA1Context()
}

type SystemA1Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySystemA1Context() *SystemA1Context {
	var p = new(SystemA1Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_systemA1
	return p
}

func (*SystemA1Context) IsSystemA1Context() {}

func NewSystemA1Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SystemA1Context {
	var p = new(SystemA1Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_systemA1

	return p
}

func (s *SystemA1Context) GetParser() antlr.Parser { return s.parser }

func (s *SystemA1Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SystemA1Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SystemA1Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SystemA1Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSystemA1(s)
	}
}

func (s *SystemA1Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSystemA1(s)
	}
}

func (p *RDSPPParser) SystemA1() (localctx ISystemA1Context) {
	localctx = NewSystemA1Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 62, RDSPPParserRULE_systemA1)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(342)
		p.Alpha()
	}

	return localctx
}

// ISystemA2Context is an interface to support dynamic dispatch.
type ISystemA2Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSystemA2Context differentiates from other interfaces.
	IsSystemA2Context()
}

type SystemA2Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySystemA2Context() *SystemA2Context {
	var p = new(SystemA2Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_systemA2
	return p
}

func (*SystemA2Context) IsSystemA2Context() {}

func NewSystemA2Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SystemA2Context {
	var p = new(SystemA2Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_systemA2

	return p
}

func (s *SystemA2Context) GetParser() antlr.Parser { return s.parser }

func (s *SystemA2Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SystemA2Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SystemA2Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SystemA2Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSystemA2(s)
	}
}

func (s *SystemA2Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSystemA2(s)
	}
}

func (p *RDSPPParser) SystemA2() (localctx ISystemA2Context) {
	localctx = NewSystemA2Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 64, RDSPPParserRULE_systemA2)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(344)
		p.Alpha()
	}

	return localctx
}

// ISystemA3Context is an interface to support dynamic dispatch.
type ISystemA3Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSystemA3Context differentiates from other interfaces.
	IsSystemA3Context()
}

type SystemA3Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySystemA3Context() *SystemA3Context {
	var p = new(SystemA3Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_systemA3
	return p
}

func (*SystemA3Context) IsSystemA3Context() {}

func NewSystemA3Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SystemA3Context {
	var p = new(SystemA3Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_systemA3

	return p
}

func (s *SystemA3Context) GetParser() antlr.Parser { return s.parser }

func (s *SystemA3Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SystemA3Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SystemA3Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SystemA3Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSystemA3(s)
	}
}

func (s *SystemA3Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSystemA3(s)
	}
}

func (p *RDSPPParser) SystemA3() (localctx ISystemA3Context) {
	localctx = NewSystemA3Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 66, RDSPPParserRULE_systemA3)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(346)
		p.Alpha()
	}

	return localctx
}

// ISubsystemContext is an interface to support dynamic dispatch.
type ISubsystemContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSubsystemContext differentiates from other interfaces.
	IsSubsystemContext()
}

type SubsystemContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySubsystemContext() *SubsystemContext {
	var p = new(SubsystemContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_subsystem
	return p
}

func (*SubsystemContext) IsSubsystemContext() {}

func NewSubsystemContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SubsystemContext {
	var p = new(SubsystemContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_subsystem

	return p
}

func (s *SubsystemContext) GetParser() antlr.Parser { return s.parser }

func (s *SubsystemContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *SubsystemContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *SubsystemContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SubsystemContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SubsystemContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSubsystem(s)
	}
}

func (s *SubsystemContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSubsystem(s)
	}
}

func (p *RDSPPParser) Subsystem() (localctx ISubsystemContext) {
	localctx = NewSubsystemContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 68, RDSPPParserRULE_subsystem)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(348)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(349)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// IBasicFunctionContext is an interface to support dynamic dispatch.
type IBasicFunctionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBasicFunctionContext differentiates from other interfaces.
	IsBasicFunctionContext()
}

type BasicFunctionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBasicFunctionContext() *BasicFunctionContext {
	var p = new(BasicFunctionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_basicFunction
	return p
}

func (*BasicFunctionContext) IsBasicFunctionContext() {}

func NewBasicFunctionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BasicFunctionContext {
	var p = new(BasicFunctionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_basicFunction

	return p
}

func (s *BasicFunctionContext) GetParser() antlr.Parser { return s.parser }

func (s *BasicFunctionContext) BasicFunctionA1() IBasicFunctionA1Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBasicFunctionA1Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBasicFunctionA1Context)
}

func (s *BasicFunctionContext) BasicFunctionA2() IBasicFunctionA2Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBasicFunctionA2Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBasicFunctionA2Context)
}

func (s *BasicFunctionContext) BasicFunctionConcrete() IBasicFunctionConcreteContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBasicFunctionConcreteContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBasicFunctionConcreteContext)
}

func (s *BasicFunctionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BasicFunctionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BasicFunctionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterBasicFunction(s)
	}
}

func (s *BasicFunctionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitBasicFunction(s)
	}
}

func (p *RDSPPParser) BasicFunction() (localctx IBasicFunctionContext) {
	localctx = NewBasicFunctionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 70, RDSPPParserRULE_basicFunction)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(351)
		p.BasicFunctionA1()
	}
	{
		p.SetState(352)
		p.BasicFunctionA2()
	}
	{
		p.SetState(353)
		p.BasicFunctionConcrete()
	}

	return localctx
}

// IBasicFunctionA1Context is an interface to support dynamic dispatch.
type IBasicFunctionA1Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBasicFunctionA1Context differentiates from other interfaces.
	IsBasicFunctionA1Context()
}

type BasicFunctionA1Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBasicFunctionA1Context() *BasicFunctionA1Context {
	var p = new(BasicFunctionA1Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_basicFunctionA1
	return p
}

func (*BasicFunctionA1Context) IsBasicFunctionA1Context() {}

func NewBasicFunctionA1Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BasicFunctionA1Context {
	var p = new(BasicFunctionA1Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_basicFunctionA1

	return p
}

func (s *BasicFunctionA1Context) GetParser() antlr.Parser { return s.parser }

func (s *BasicFunctionA1Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *BasicFunctionA1Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BasicFunctionA1Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BasicFunctionA1Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterBasicFunctionA1(s)
	}
}

func (s *BasicFunctionA1Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitBasicFunctionA1(s)
	}
}

func (p *RDSPPParser) BasicFunctionA1() (localctx IBasicFunctionA1Context) {
	localctx = NewBasicFunctionA1Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 72, RDSPPParserRULE_basicFunctionA1)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(355)
		p.Alpha()
	}

	return localctx
}

// IBasicFunctionA2Context is an interface to support dynamic dispatch.
type IBasicFunctionA2Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBasicFunctionA2Context differentiates from other interfaces.
	IsBasicFunctionA2Context()
}

type BasicFunctionA2Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBasicFunctionA2Context() *BasicFunctionA2Context {
	var p = new(BasicFunctionA2Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_basicFunctionA2
	return p
}

func (*BasicFunctionA2Context) IsBasicFunctionA2Context() {}

func NewBasicFunctionA2Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BasicFunctionA2Context {
	var p = new(BasicFunctionA2Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_basicFunctionA2

	return p
}

func (s *BasicFunctionA2Context) GetParser() antlr.Parser { return s.parser }

func (s *BasicFunctionA2Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *BasicFunctionA2Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BasicFunctionA2Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BasicFunctionA2Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterBasicFunctionA2(s)
	}
}

func (s *BasicFunctionA2Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitBasicFunctionA2(s)
	}
}

func (p *RDSPPParser) BasicFunctionA2() (localctx IBasicFunctionA2Context) {
	localctx = NewBasicFunctionA2Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 74, RDSPPParserRULE_basicFunctionA2)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(357)
		p.Alpha()
	}

	return localctx
}

// IBasicFunctionConcreteContext is an interface to support dynamic dispatch.
type IBasicFunctionConcreteContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBasicFunctionConcreteContext differentiates from other interfaces.
	IsBasicFunctionConcreteContext()
}

type BasicFunctionConcreteContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBasicFunctionConcreteContext() *BasicFunctionConcreteContext {
	var p = new(BasicFunctionConcreteContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_basicFunctionConcrete
	return p
}

func (*BasicFunctionConcreteContext) IsBasicFunctionConcreteContext() {}

func NewBasicFunctionConcreteContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BasicFunctionConcreteContext {
	var p = new(BasicFunctionConcreteContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_basicFunctionConcrete

	return p
}

func (s *BasicFunctionConcreteContext) GetParser() antlr.Parser { return s.parser }

func (s *BasicFunctionConcreteContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *BasicFunctionConcreteContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *BasicFunctionConcreteContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BasicFunctionConcreteContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BasicFunctionConcreteContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterBasicFunctionConcrete(s)
	}
}

func (s *BasicFunctionConcreteContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitBasicFunctionConcrete(s)
	}
}

func (p *RDSPPParser) BasicFunctionConcrete() (localctx IBasicFunctionConcreteContext) {
	localctx = NewBasicFunctionConcreteContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 76, RDSPPParserRULE_basicFunctionConcrete)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(359)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(360)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(361)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// IProductContext is an interface to support dynamic dispatch.
type IProductContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsProductContext differentiates from other interfaces.
	IsProductContext()
}

type ProductContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyProductContext() *ProductContext {
	var p = new(ProductContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_product
	return p
}

func (*ProductContext) IsProductContext() {}

func NewProductContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProductContext {
	var p = new(ProductContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_product

	return p
}

func (s *ProductContext) GetParser() antlr.Parser { return s.parser }

func (s *ProductContext) MINUS() antlr.TerminalNode {
	return s.GetToken(RDSPPParserMINUS, 0)
}

func (s *ProductContext) AllProductClass() []IProductClassContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IProductClassContext)(nil)).Elem())
	var tst = make([]IProductClassContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IProductClassContext)
		}
	}

	return tst
}

func (s *ProductContext) ProductClass(i int) IProductClassContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProductClassContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IProductClassContext)
}

func (s *ProductContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProductContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProductContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterProduct(s)
	}
}

func (s *ProductContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitProduct(s)
	}
}

func (p *RDSPPParser) Product() (localctx IProductContext) {
	localctx = NewProductContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 78, RDSPPParserRULE_product)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(363)
		p.Match(RDSPPParserMINUS)
	}
	p.SetState(365)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ok := true; ok; ok = (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0) {
		{
			p.SetState(364)
			p.ProductClass()
		}

		p.SetState(367)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IProductClassContext is an interface to support dynamic dispatch.
type IProductClassContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsProductClassContext differentiates from other interfaces.
	IsProductClassContext()
}

type ProductClassContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyProductClassContext() *ProductClassContext {
	var p = new(ProductClassContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_productClass
	return p
}

func (*ProductClassContext) IsProductClassContext() {}

func NewProductClassContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProductClassContext {
	var p = new(ProductClassContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_productClass

	return p
}

func (s *ProductClassContext) GetParser() antlr.Parser { return s.parser }

func (s *ProductClassContext) ProductClassA1() IProductClassA1Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProductClassA1Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProductClassA1Context)
}

func (s *ProductClassContext) ProductClassA2() IProductClassA2Context {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProductClassA2Context)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProductClassA2Context)
}

func (s *ProductClassContext) ProductClassConcrete() IProductClassConcreteContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProductClassConcreteContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProductClassConcreteContext)
}

func (s *ProductClassContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProductClassContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProductClassContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterProductClass(s)
	}
}

func (s *ProductClassContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitProductClass(s)
	}
}

func (p *RDSPPParser) ProductClass() (localctx IProductClassContext) {
	localctx = NewProductClassContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 80, RDSPPParserRULE_productClass)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(369)
		p.ProductClassA1()
	}
	{
		p.SetState(370)
		p.ProductClassA2()
	}
	{
		p.SetState(371)
		p.ProductClassConcrete()
	}

	return localctx
}

// IProductClassA1Context is an interface to support dynamic dispatch.
type IProductClassA1Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsProductClassA1Context differentiates from other interfaces.
	IsProductClassA1Context()
}

type ProductClassA1Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyProductClassA1Context() *ProductClassA1Context {
	var p = new(ProductClassA1Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_productClassA1
	return p
}

func (*ProductClassA1Context) IsProductClassA1Context() {}

func NewProductClassA1Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProductClassA1Context {
	var p = new(ProductClassA1Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_productClassA1

	return p
}

func (s *ProductClassA1Context) GetParser() antlr.Parser { return s.parser }

func (s *ProductClassA1Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *ProductClassA1Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProductClassA1Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProductClassA1Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterProductClassA1(s)
	}
}

func (s *ProductClassA1Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitProductClassA1(s)
	}
}

func (p *RDSPPParser) ProductClassA1() (localctx IProductClassA1Context) {
	localctx = NewProductClassA1Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 82, RDSPPParserRULE_productClassA1)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(373)
		p.Alpha()
	}

	return localctx
}

// IProductClassA2Context is an interface to support dynamic dispatch.
type IProductClassA2Context interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsProductClassA2Context differentiates from other interfaces.
	IsProductClassA2Context()
}

type ProductClassA2Context struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyProductClassA2Context() *ProductClassA2Context {
	var p = new(ProductClassA2Context)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_productClassA2
	return p
}

func (*ProductClassA2Context) IsProductClassA2Context() {}

func NewProductClassA2Context(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProductClassA2Context {
	var p = new(ProductClassA2Context)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_productClassA2

	return p
}

func (s *ProductClassA2Context) GetParser() antlr.Parser { return s.parser }

func (s *ProductClassA2Context) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *ProductClassA2Context) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProductClassA2Context) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProductClassA2Context) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterProductClassA2(s)
	}
}

func (s *ProductClassA2Context) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitProductClassA2(s)
	}
}

func (p *RDSPPParser) ProductClassA2() (localctx IProductClassA2Context) {
	localctx = NewProductClassA2Context(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 84, RDSPPParserRULE_productClassA2)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(375)
		p.Alpha()
	}

	return localctx
}

// IProductClassConcreteContext is an interface to support dynamic dispatch.
type IProductClassConcreteContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsProductClassConcreteContext differentiates from other interfaces.
	IsProductClassConcreteContext()
}

type ProductClassConcreteContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyProductClassConcreteContext() *ProductClassConcreteContext {
	var p = new(ProductClassConcreteContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_productClassConcrete
	return p
}

func (*ProductClassConcreteContext) IsProductClassConcreteContext() {}

func NewProductClassConcreteContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ProductClassConcreteContext {
	var p = new(ProductClassConcreteContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_productClassConcrete

	return p
}

func (s *ProductClassConcreteContext) GetParser() antlr.Parser { return s.parser }

func (s *ProductClassConcreteContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *ProductClassConcreteContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *ProductClassConcreteContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ProductClassConcreteContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ProductClassConcreteContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterProductClassConcrete(s)
	}
}

func (s *ProductClassConcreteContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitProductClassConcrete(s)
	}
}

func (p *RDSPPParser) ProductClassConcrete() (localctx IProductClassConcreteContext) {
	localctx = NewProductClassConcreteContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 86, RDSPPParserRULE_productClassConcrete)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(377)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(378)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(379)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// IOperatingEquipmentContext is an interface to support dynamic dispatch.
type IOperatingEquipmentContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOperatingEquipmentContext differentiates from other interfaces.
	IsOperatingEquipmentContext()
}

type OperatingEquipmentContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOperatingEquipmentContext() *OperatingEquipmentContext {
	var p = new(OperatingEquipmentContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_operatingEquipment
	return p
}

func (*OperatingEquipmentContext) IsOperatingEquipmentContext() {}

func NewOperatingEquipmentContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OperatingEquipmentContext {
	var p = new(OperatingEquipmentContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_operatingEquipment

	return p
}

func (s *OperatingEquipmentContext) GetParser() antlr.Parser { return s.parser }

func (s *OperatingEquipmentContext) EQUALS() antlr.TerminalNode {
	return s.GetToken(RDSPPParserEQUALS, 0)
}

func (s *OperatingEquipmentContext) SystemSubsystem() ISystemSubsystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemSubsystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemSubsystemContext)
}

func (s *OperatingEquipmentContext) BasicFunction() IBasicFunctionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBasicFunctionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBasicFunctionContext)
}

func (s *OperatingEquipmentContext) Product() IProductContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IProductContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IProductContext)
}

func (s *OperatingEquipmentContext) MainSystem() IMainSystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMainSystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMainSystemContext)
}

func (s *OperatingEquipmentContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OperatingEquipmentContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OperatingEquipmentContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterOperatingEquipment(s)
	}
}

func (s *OperatingEquipmentContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitOperatingEquipment(s)
	}
}

func (p *RDSPPParser) OperatingEquipment() (localctx IOperatingEquipmentContext) {
	localctx = NewOperatingEquipmentContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 88, RDSPPParserRULE_operatingEquipment)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(381)
		p.Match(RDSPPParserEQUALS)
	}
	p.SetState(383)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 26, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(382)
			p.MainSystem()
		}

	}
	{
		p.SetState(385)
		p.SystemSubsystem()
	}
	{
		p.SetState(386)
		p.BasicFunction()
	}
	{
		p.SetState(387)
		p.Product()
	}

	return localctx
}

// IPointOfInstallationContext is an interface to support dynamic dispatch.
type IPointOfInstallationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPointOfInstallationContext differentiates from other interfaces.
	IsPointOfInstallationContext()
}

type PointOfInstallationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPointOfInstallationContext() *PointOfInstallationContext {
	var p = new(PointOfInstallationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_pointOfInstallation
	return p
}

func (*PointOfInstallationContext) IsPointOfInstallationContext() {}

func NewPointOfInstallationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PointOfInstallationContext {
	var p = new(PointOfInstallationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_pointOfInstallation

	return p
}

func (s *PointOfInstallationContext) GetParser() antlr.Parser { return s.parser }

func (s *PointOfInstallationContext) PLUS() antlr.TerminalNode {
	return s.GetToken(RDSPPParserPLUS, 0)
}

func (s *PointOfInstallationContext) SystemSubsystem() ISystemSubsystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemSubsystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemSubsystemContext)
}

func (s *PointOfInstallationContext) MainSystem() IMainSystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMainSystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMainSystemContext)
}

func (s *PointOfInstallationContext) BasicFunction() IBasicFunctionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBasicFunctionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBasicFunctionContext)
}

func (s *PointOfInstallationContext) DOT() antlr.TerminalNode {
	return s.GetToken(RDSPPParserDOT, 0)
}

func (s *PointOfInstallationContext) PointOfInstallationPlace() IPointOfInstallationPlaceContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPointOfInstallationPlaceContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPointOfInstallationPlaceContext)
}

func (s *PointOfInstallationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PointOfInstallationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PointOfInstallationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterPointOfInstallation(s)
	}
}

func (s *PointOfInstallationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitPointOfInstallation(s)
	}
}

func (p *RDSPPParser) PointOfInstallation() (localctx IPointOfInstallationContext) {
	localctx = NewPointOfInstallationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 90, RDSPPParserRULE_pointOfInstallation)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(389)
		p.Match(RDSPPParserPLUS)
	}
	p.SetState(391)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 27, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(390)
			p.MainSystem()
		}

	}
	{
		p.SetState(393)
		p.SystemSubsystem()
	}
	p.SetState(395)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
		{
			p.SetState(394)
			p.BasicFunction()
		}

	}
	p.SetState(399)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RDSPPParserDOT {
		{
			p.SetState(397)
			p.Match(RDSPPParserDOT)
		}
		{
			p.SetState(398)
			p.PointOfInstallationPlace()
		}

	}

	return localctx
}

// IPointOfInstallationPlaceContext is an interface to support dynamic dispatch.
type IPointOfInstallationPlaceContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPointOfInstallationPlaceContext differentiates from other interfaces.
	IsPointOfInstallationPlaceContext()
}

type PointOfInstallationPlaceContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPointOfInstallationPlaceContext() *PointOfInstallationPlaceContext {
	var p = new(PointOfInstallationPlaceContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_pointOfInstallationPlace
	return p
}

func (*PointOfInstallationPlaceContext) IsPointOfInstallationPlaceContext() {}

func NewPointOfInstallationPlaceContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PointOfInstallationPlaceContext {
	var p = new(PointOfInstallationPlaceContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_pointOfInstallationPlace

	return p
}

func (s *PointOfInstallationPlaceContext) GetParser() antlr.Parser { return s.parser }

func (s *PointOfInstallationPlaceContext) Column() IColumnContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IColumnContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IColumnContext)
}

func (s *PointOfInstallationPlaceContext) Row() IRowContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRowContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRowContext)
}

func (s *PointOfInstallationPlaceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PointOfInstallationPlaceContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PointOfInstallationPlaceContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterPointOfInstallationPlace(s)
	}
}

func (s *PointOfInstallationPlaceContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitPointOfInstallationPlace(s)
	}
}

func (p *RDSPPParser) PointOfInstallationPlace() (localctx IPointOfInstallationPlaceContext) {
	localctx = NewPointOfInstallationPlaceContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 92, RDSPPParserRULE_pointOfInstallationPlace)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(401)
		p.Column()
	}
	{
		p.SetState(402)
		p.Row()
	}

	return localctx
}

// IColumnContext is an interface to support dynamic dispatch.
type IColumnContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsColumnContext differentiates from other interfaces.
	IsColumnContext()
}

type ColumnContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyColumnContext() *ColumnContext {
	var p = new(ColumnContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_column
	return p
}

func (*ColumnContext) IsColumnContext() {}

func NewColumnContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ColumnContext {
	var p = new(ColumnContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_column

	return p
}

func (s *ColumnContext) GetParser() antlr.Parser { return s.parser }

func (s *ColumnContext) AllAlpha() []IAlphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAlphaContext)(nil)).Elem())
	var tst = make([]IAlphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAlphaContext)
		}
	}

	return tst
}

func (s *ColumnContext) Alpha(i int) IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *ColumnContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ColumnContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ColumnContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterColumn(s)
	}
}

func (s *ColumnContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitColumn(s)
	}
}

func (p *RDSPPParser) Column() (localctx IColumnContext) {
	localctx = NewColumnContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 94, RDSPPParserRULE_column)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(404)
		p.Alpha()
	}
	{
		p.SetState(405)
		p.Alpha()
	}

	return localctx
}

// IRowContext is an interface to support dynamic dispatch.
type IRowContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsRowContext differentiates from other interfaces.
	IsRowContext()
}

type RowContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRowContext() *RowContext {
	var p = new(RowContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_row
	return p
}

func (*RowContext) IsRowContext() {}

func NewRowContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RowContext {
	var p = new(RowContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_row

	return p
}

func (s *RowContext) GetParser() antlr.Parser { return s.parser }

func (s *RowContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *RowContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *RowContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RowContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RowContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterRow(s)
	}
}

func (s *RowContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitRow(s)
	}
}

func (p *RDSPPParser) Row() (localctx IRowContext) {
	localctx = NewRowContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 96, RDSPPParserRULE_row)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(407)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(408)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(409)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// ISiteOfInstallationContext is an interface to support dynamic dispatch.
type ISiteOfInstallationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSiteOfInstallationContext differentiates from other interfaces.
	IsSiteOfInstallationContext()
}

type SiteOfInstallationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySiteOfInstallationContext() *SiteOfInstallationContext {
	var p = new(SiteOfInstallationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_siteOfInstallation
	return p
}

func (*SiteOfInstallationContext) IsSiteOfInstallationContext() {}

func NewSiteOfInstallationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SiteOfInstallationContext {
	var p = new(SiteOfInstallationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_siteOfInstallation

	return p
}

func (s *SiteOfInstallationContext) GetParser() antlr.Parser { return s.parser }

func (s *SiteOfInstallationContext) PLUSPLUS() antlr.TerminalNode {
	return s.GetToken(RDSPPParserPLUSPLUS, 0)
}

func (s *SiteOfInstallationContext) MainSystem() IMainSystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMainSystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMainSystemContext)
}

func (s *SiteOfInstallationContext) DOT() antlr.TerminalNode {
	return s.GetToken(RDSPPParserDOT, 0)
}

func (s *SiteOfInstallationContext) SiteOfInstallationPlace() ISiteOfInstallationPlaceContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISiteOfInstallationPlaceContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISiteOfInstallationPlaceContext)
}

func (s *SiteOfInstallationContext) GeographicLocation() IGeographicLocationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IGeographicLocationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IGeographicLocationContext)
}

func (s *SiteOfInstallationContext) SystemSubsystem() ISystemSubsystemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISystemSubsystemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISystemSubsystemContext)
}

func (s *SiteOfInstallationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SiteOfInstallationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SiteOfInstallationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSiteOfInstallation(s)
	}
}

func (s *SiteOfInstallationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSiteOfInstallation(s)
	}
}

func (p *RDSPPParser) SiteOfInstallation() (localctx ISiteOfInstallationContext) {
	localctx = NewSiteOfInstallationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 98, RDSPPParserRULE_siteOfInstallation)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(411)
		p.Match(RDSPPParserPLUSPLUS)
	}
	{
		p.SetState(412)
		p.MainSystem()
	}
	p.SetState(414)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
		{
			p.SetState(413)
			p.SystemSubsystem()
		}

	}
	{
		p.SetState(416)
		p.Match(RDSPPParserDOT)
	}
	p.SetState(419)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
		{
			p.SetState(417)
			p.SiteOfInstallationPlace()
		}

	case RDSPPParserNUMBER:
		{
			p.SetState(418)
			p.GeographicLocation()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// ISiteOfInstallationPlaceContext is an interface to support dynamic dispatch.
type ISiteOfInstallationPlaceContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSiteOfInstallationPlaceContext differentiates from other interfaces.
	IsSiteOfInstallationPlaceContext()
}

type SiteOfInstallationPlaceContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySiteOfInstallationPlaceContext() *SiteOfInstallationPlaceContext {
	var p = new(SiteOfInstallationPlaceContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_siteOfInstallationPlace
	return p
}

func (*SiteOfInstallationPlaceContext) IsSiteOfInstallationPlaceContext() {}

func NewSiteOfInstallationPlaceContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SiteOfInstallationPlaceContext {
	var p = new(SiteOfInstallationPlaceContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_siteOfInstallationPlace

	return p
}

func (s *SiteOfInstallationPlaceContext) GetParser() antlr.Parser { return s.parser }

func (s *SiteOfInstallationPlaceContext) AllAlpha() []IAlphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAlphaContext)(nil)).Elem())
	var tst = make([]IAlphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAlphaContext)
		}
	}

	return tst
}

func (s *SiteOfInstallationPlaceContext) Alpha(i int) IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SiteOfInstallationPlaceContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *SiteOfInstallationPlaceContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *SiteOfInstallationPlaceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SiteOfInstallationPlaceContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SiteOfInstallationPlaceContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSiteOfInstallationPlace(s)
	}
}

func (s *SiteOfInstallationPlaceContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSiteOfInstallationPlace(s)
	}
}

func (p *RDSPPParser) SiteOfInstallationPlace() (localctx ISiteOfInstallationPlaceContext) {
	localctx = NewSiteOfInstallationPlaceContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 100, RDSPPParserRULE_siteOfInstallationPlace)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(421)
		p.Alpha()
	}
	p.SetState(429)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
		{
			p.SetState(422)
			p.Alpha()
		}
		p.SetState(427)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
			{
				p.SetState(423)
				p.Alpha()
			}
			p.SetState(425)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)

			if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
				{
					p.SetState(424)
					p.Alpha()
				}

			}

		}

	}
	{
		p.SetState(431)
		p.Match(RDSPPParserNUMBER)
	}
	p.SetState(439)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RDSPPParserNUMBER {
		{
			p.SetState(432)
			p.Match(RDSPPParserNUMBER)
		}
		p.SetState(437)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserNUMBER {
			{
				p.SetState(433)
				p.Match(RDSPPParserNUMBER)
			}
			p.SetState(435)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)

			if _la == RDSPPParserNUMBER {
				{
					p.SetState(434)
					p.Match(RDSPPParserNUMBER)
				}

			}

		}

	}

	return localctx
}

// ISignalContext is an interface to support dynamic dispatch.
type ISignalContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalContext differentiates from other interfaces.
	IsSignalContext()
}

type SignalContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalContext() *SignalContext {
	var p = new(SignalContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signal
	return p
}

func (*SignalContext) IsSignalContext() {}

func NewSignalContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalContext {
	var p = new(SignalContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signal

	return p
}

func (s *SignalContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalContext) SEMICOLON() antlr.TerminalNode {
	return s.GetToken(RDSPPParserSEMICOLON, 0)
}

func (s *SignalContext) SignalClass() ISignalClassContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalClassContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalClassContext)
}

func (s *SignalContext) Underscore() IUnderscoreContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUnderscoreContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IUnderscoreContext)
}

func (s *SignalContext) SignalInformation() ISignalInformationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalInformationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalInformationContext)
}

func (s *SignalContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignal(s)
	}
}

func (s *SignalContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignal(s)
	}
}

func (p *RDSPPParser) Signal() (localctx ISignalContext) {
	localctx = NewSignalContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 102, RDSPPParserRULE_signal)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(441)
		p.Match(RDSPPParserSEMICOLON)
	}
	{
		p.SetState(442)
		p.SignalClass()
	}
	p.SetState(446)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RDSPPParserT__24 {
		{
			p.SetState(443)
			p.Underscore()
		}
		{
			p.SetState(444)
			p.SignalInformation()
		}

	}

	return localctx
}

// ISignalClassContext is an interface to support dynamic dispatch.
type ISignalClassContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalClassContext differentiates from other interfaces.
	IsSignalClassContext()
}

type SignalClassContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalClassContext() *SignalClassContext {
	var p = new(SignalClassContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalClass
	return p
}

func (*SignalClassContext) IsSignalClassContext() {}

func NewSignalClassContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalClassContext {
	var p = new(SignalClassContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalClass

	return p
}

func (s *SignalClassContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalClassContext) SignalMainClass() ISignalMainClassContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalMainClassContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalMainClassContext)
}

func (s *SignalClassContext) SignalSubclass() ISignalSubclassContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalSubclassContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalSubclassContext)
}

func (s *SignalClassContext) SignalClassCounter() ISignalClassCounterContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalClassCounterContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalClassCounterContext)
}

func (s *SignalClassContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalClassContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalClassContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalClass(s)
	}
}

func (s *SignalClassContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalClass(s)
	}
}

func (p *RDSPPParser) SignalClass() (localctx ISignalClassContext) {
	localctx = NewSignalClassContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 104, RDSPPParserRULE_signalClass)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(448)
		p.SignalMainClass()
	}
	{
		p.SetState(449)
		p.SignalSubclass()
	}
	{
		p.SetState(450)
		p.SignalClassCounter()
	}

	return localctx
}

// ISignalMainClassContext is an interface to support dynamic dispatch.
type ISignalMainClassContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalMainClassContext differentiates from other interfaces.
	IsSignalMainClassContext()
}

type SignalMainClassContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalMainClassContext() *SignalMainClassContext {
	var p = new(SignalMainClassContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalMainClass
	return p
}

func (*SignalMainClassContext) IsSignalMainClassContext() {}

func NewSignalMainClassContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalMainClassContext {
	var p = new(SignalMainClassContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalMainClass

	return p
}

func (s *SignalMainClassContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalMainClassContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SignalMainClassContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalMainClassContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalMainClassContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalMainClass(s)
	}
}

func (s *SignalMainClassContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalMainClass(s)
	}
}

func (p *RDSPPParser) SignalMainClass() (localctx ISignalMainClassContext) {
	localctx = NewSignalMainClassContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 106, RDSPPParserRULE_signalMainClass)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(452)
		p.Alpha()
	}

	return localctx
}

// ISignalSubclassContext is an interface to support dynamic dispatch.
type ISignalSubclassContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalSubclassContext differentiates from other interfaces.
	IsSignalSubclassContext()
}

type SignalSubclassContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalSubclassContext() *SignalSubclassContext {
	var p = new(SignalSubclassContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalSubclass
	return p
}

func (*SignalSubclassContext) IsSignalSubclassContext() {}

func NewSignalSubclassContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalSubclassContext {
	var p = new(SignalSubclassContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalSubclass

	return p
}

func (s *SignalSubclassContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalSubclassContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SignalSubclassContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalSubclassContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalSubclassContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalSubclass(s)
	}
}

func (s *SignalSubclassContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalSubclass(s)
	}
}

func (p *RDSPPParser) SignalSubclass() (localctx ISignalSubclassContext) {
	localctx = NewSignalSubclassContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 108, RDSPPParserRULE_signalSubclass)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(454)
		p.Alpha()
	}

	return localctx
}

// ISignalClassCounterContext is an interface to support dynamic dispatch.
type ISignalClassCounterContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalClassCounterContext differentiates from other interfaces.
	IsSignalClassCounterContext()
}

type SignalClassCounterContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalClassCounterContext() *SignalClassCounterContext {
	var p = new(SignalClassCounterContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalClassCounter
	return p
}

func (*SignalClassCounterContext) IsSignalClassCounterContext() {}

func NewSignalClassCounterContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalClassCounterContext {
	var p = new(SignalClassCounterContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalClassCounter

	return p
}

func (s *SignalClassCounterContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalClassCounterContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *SignalClassCounterContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *SignalClassCounterContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalClassCounterContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalClassCounterContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalClassCounter(s)
	}
}

func (s *SignalClassCounterContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalClassCounter(s)
	}
}

func (p *RDSPPParser) SignalClassCounter() (localctx ISignalClassCounterContext) {
	localctx = NewSignalClassCounterContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 110, RDSPPParserRULE_signalClassCounter)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(456)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(457)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(458)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// ISignalInformationContext is an interface to support dynamic dispatch.
type ISignalInformationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalInformationContext differentiates from other interfaces.
	IsSignalInformationContext()
}

type SignalInformationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalInformationContext() *SignalInformationContext {
	var p = new(SignalInformationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalInformation
	return p
}

func (*SignalInformationContext) IsSignalInformationContext() {}

func NewSignalInformationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalInformationContext {
	var p = new(SignalInformationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalInformation

	return p
}

func (s *SignalInformationContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalInformationContext) SignalDuration() ISignalDurationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalDurationContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalDurationContext)
}

func (s *SignalInformationContext) SignalType() ISignalTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalTypeContext)
}

func (s *SignalInformationContext) SignalInformationCounter() ISignalInformationCounterContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISignalInformationCounterContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISignalInformationCounterContext)
}

func (s *SignalInformationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalInformationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalInformationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalInformation(s)
	}
}

func (s *SignalInformationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalInformation(s)
	}
}

func (p *RDSPPParser) SignalInformation() (localctx ISignalInformationContext) {
	localctx = NewSignalInformationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 112, RDSPPParserRULE_signalInformation)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(460)
		p.SignalDuration()
	}
	{
		p.SetState(461)
		p.SignalType()
	}
	{
		p.SetState(462)
		p.SignalInformationCounter()
	}

	return localctx
}

// ISignalDurationContext is an interface to support dynamic dispatch.
type ISignalDurationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalDurationContext differentiates from other interfaces.
	IsSignalDurationContext()
}

type SignalDurationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalDurationContext() *SignalDurationContext {
	var p = new(SignalDurationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalDuration
	return p
}

func (*SignalDurationContext) IsSignalDurationContext() {}

func NewSignalDurationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalDurationContext {
	var p = new(SignalDurationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalDuration

	return p
}

func (s *SignalDurationContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalDurationContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SignalDurationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalDurationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalDurationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalDuration(s)
	}
}

func (s *SignalDurationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalDuration(s)
	}
}

func (p *RDSPPParser) SignalDuration() (localctx ISignalDurationContext) {
	localctx = NewSignalDurationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 114, RDSPPParserRULE_signalDuration)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(464)
		p.Alpha()
	}

	return localctx
}

// ISignalTypeContext is an interface to support dynamic dispatch.
type ISignalTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalTypeContext differentiates from other interfaces.
	IsSignalTypeContext()
}

type SignalTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalTypeContext() *SignalTypeContext {
	var p = new(SignalTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalType
	return p
}

func (*SignalTypeContext) IsSignalTypeContext() {}

func NewSignalTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalTypeContext {
	var p = new(SignalTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalType

	return p
}

func (s *SignalTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalTypeContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *SignalTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalTypeContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalType(s)
	}
}

func (s *SignalTypeContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalType(s)
	}
}

func (p *RDSPPParser) SignalType() (localctx ISignalTypeContext) {
	localctx = NewSignalTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 116, RDSPPParserRULE_signalType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(466)
		p.Alpha()
	}

	return localctx
}

// ISignalInformationCounterContext is an interface to support dynamic dispatch.
type ISignalInformationCounterContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSignalInformationCounterContext differentiates from other interfaces.
	IsSignalInformationCounterContext()
}

type SignalInformationCounterContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySignalInformationCounterContext() *SignalInformationCounterContext {
	var p = new(SignalInformationCounterContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_signalInformationCounter
	return p
}

func (*SignalInformationCounterContext) IsSignalInformationCounterContext() {}

func NewSignalInformationCounterContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SignalInformationCounterContext {
	var p = new(SignalInformationCounterContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_signalInformationCounter

	return p
}

func (s *SignalInformationCounterContext) GetParser() antlr.Parser { return s.parser }

func (s *SignalInformationCounterContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *SignalInformationCounterContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *SignalInformationCounterContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SignalInformationCounterContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SignalInformationCounterContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterSignalInformationCounter(s)
	}
}

func (s *SignalInformationCounterContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitSignalInformationCounter(s)
	}
}

func (p *RDSPPParser) SignalInformationCounter() (localctx ISignalInformationCounterContext) {
	localctx = NewSignalInformationCounterContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 118, RDSPPParserRULE_signalInformationCounter)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(468)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(469)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// ITerminalContext is an interface to support dynamic dispatch.
type ITerminalContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTerminalContext differentiates from other interfaces.
	IsTerminalContext()
}

type TerminalContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTerminalContext() *TerminalContext {
	var p = new(TerminalContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_terminal
	return p
}

func (*TerminalContext) IsTerminalContext() {}

func NewTerminalContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TerminalContext {
	var p = new(TerminalContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_terminal

	return p
}

func (s *TerminalContext) GetParser() antlr.Parser { return s.parser }

func (s *TerminalContext) COLON() antlr.TerminalNode {
	return s.GetToken(RDSPPParserCOLON, 0)
}

func (s *TerminalContext) AllTerminalPoint() []ITerminalPointContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*ITerminalPointContext)(nil)).Elem())
	var tst = make([]ITerminalPointContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(ITerminalPointContext)
		}
	}

	return tst
}

func (s *TerminalContext) TerminalPoint(i int) ITerminalPointContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITerminalPointContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(ITerminalPointContext)
}

func (s *TerminalContext) DOT() antlr.TerminalNode {
	return s.GetToken(RDSPPParserDOT, 0)
}

func (s *TerminalContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TerminalContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TerminalContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterTerminal(s)
	}
}

func (s *TerminalContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitTerminal(s)
	}
}

func (p *RDSPPParser) Terminal() (localctx ITerminalContext) {
	localctx = NewTerminalContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 120, RDSPPParserRULE_terminal)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(471)
		p.Match(RDSPPParserCOLON)
	}
	{
		p.SetState(472)
		p.TerminalPoint()
	}
	p.SetState(475)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RDSPPParserDOT {
		{
			p.SetState(473)
			p.Match(RDSPPParserDOT)
		}
		{
			p.SetState(474)
			p.TerminalPoint()
		}

	}

	return localctx
}

// ITerminalPointContext is an interface to support dynamic dispatch.
type ITerminalPointContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTerminalPointContext differentiates from other interfaces.
	IsTerminalPointContext()
}

type TerminalPointContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTerminalPointContext() *TerminalPointContext {
	var p = new(TerminalPointContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_terminalPoint
	return p
}

func (*TerminalPointContext) IsTerminalPointContext() {}

func NewTerminalPointContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TerminalPointContext {
	var p = new(TerminalPointContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_terminalPoint

	return p
}

func (s *TerminalPointContext) GetParser() antlr.Parser { return s.parser }

func (s *TerminalPointContext) AllAlpha() []IAlphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAlphaContext)(nil)).Elem())
	var tst = make([]IAlphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAlphaContext)
		}
	}

	return tst
}

func (s *TerminalPointContext) Alpha(i int) IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *TerminalPointContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *TerminalPointContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *TerminalPointContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TerminalPointContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TerminalPointContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterTerminalPoint(s)
	}
}

func (s *TerminalPointContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitTerminalPoint(s)
	}
}

func (p *RDSPPParser) TerminalPoint() (localctx ITerminalPointContext) {
	localctx = NewTerminalPointContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 122, RDSPPParserRULE_terminalPoint)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(479)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ok := true; ok; ok = (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23)|(1<<RDSPPParserNUMBER))) != 0) {
		p.SetState(479)
		p.GetErrorHandler().Sync(p)

		switch p.GetTokenStream().LA(1) {
		case RDSPPParserT__0, RDSPPParserT__1, RDSPPParserT__2, RDSPPParserT__3, RDSPPParserT__4, RDSPPParserT__5, RDSPPParserT__6, RDSPPParserT__7, RDSPPParserT__8, RDSPPParserT__9, RDSPPParserT__10, RDSPPParserT__11, RDSPPParserT__12, RDSPPParserT__13, RDSPPParserT__14, RDSPPParserT__15, RDSPPParserT__16, RDSPPParserT__17, RDSPPParserT__18, RDSPPParserT__19, RDSPPParserT__20, RDSPPParserT__21, RDSPPParserT__22, RDSPPParserT__23:
			{
				p.SetState(477)
				p.Alpha()
			}

		case RDSPPParserNUMBER:
			{
				p.SetState(478)
				p.Match(RDSPPParserNUMBER)
			}

		default:
			panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		}

		p.SetState(481)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IDocumentContext is an interface to support dynamic dispatch.
type IDocumentContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDocumentContext differentiates from other interfaces.
	IsDocumentContext()
}

type DocumentContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDocumentContext() *DocumentContext {
	var p = new(DocumentContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_document
	return p
}

func (*DocumentContext) IsDocumentContext() {}

func NewDocumentContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DocumentContext {
	var p = new(DocumentContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_document

	return p
}

func (s *DocumentContext) GetParser() antlr.Parser { return s.parser }

func (s *DocumentContext) AMPERSAND() antlr.TerminalNode {
	return s.GetToken(RDSPPParserAMPERSAND, 0)
}

func (s *DocumentContext) Dcc() IDccContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDccContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDccContext)
}

func (s *DocumentContext) SLASH() antlr.TerminalNode {
	return s.GetToken(RDSPPParserSLASH, 0)
}

func (s *DocumentContext) PageCountNumber() IPageCountNumberContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPageCountNumberContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPageCountNumberContext)
}

func (s *DocumentContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DocumentContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DocumentContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterDocument(s)
	}
}

func (s *DocumentContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitDocument(s)
	}
}

func (p *RDSPPParser) Document() (localctx IDocumentContext) {
	localctx = NewDocumentContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 124, RDSPPParserRULE_document)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(483)
		p.Match(RDSPPParserAMPERSAND)
	}
	{
		p.SetState(484)
		p.Dcc()
	}
	p.SetState(487)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RDSPPParserSLASH {
		{
			p.SetState(485)
			p.Match(RDSPPParserSLASH)
		}
		{
			p.SetState(486)
			p.PageCountNumber()
		}

	}

	return localctx
}

// IDccContext is an interface to support dynamic dispatch.
type IDccContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDccContext differentiates from other interfaces.
	IsDccContext()
}

type DccContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDccContext() *DccContext {
	var p = new(DccContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_dcc
	return p
}

func (*DccContext) IsDccContext() {}

func NewDccContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DccContext {
	var p = new(DccContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_dcc

	return p
}

func (s *DccContext) GetParser() antlr.Parser { return s.parser }

func (s *DccContext) TechnicalSector() ITechnicalSectorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITechnicalSectorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITechnicalSectorContext)
}

func (s *DccContext) DccMainClass() IDccMainClassContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDccMainClassContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDccMainClassContext)
}

func (s *DccContext) DccSubclass() IDccSubclassContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDccSubclassContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDccSubclassContext)
}

func (s *DccContext) DccConcrete() IDccConcreteContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDccConcreteContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDccConcreteContext)
}

func (s *DccContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DccContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DccContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterDcc(s)
	}
}

func (s *DccContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitDcc(s)
	}
}

func (p *RDSPPParser) Dcc() (localctx IDccContext) {
	localctx = NewDccContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 126, RDSPPParserRULE_dcc)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(489)
		p.TechnicalSector()
	}
	{
		p.SetState(490)
		p.DccMainClass()
	}
	{
		p.SetState(491)
		p.DccSubclass()
	}
	{
		p.SetState(492)
		p.DccConcrete()
	}

	return localctx
}

// ITechnicalSectorContext is an interface to support dynamic dispatch.
type ITechnicalSectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTechnicalSectorContext differentiates from other interfaces.
	IsTechnicalSectorContext()
}

type TechnicalSectorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTechnicalSectorContext() *TechnicalSectorContext {
	var p = new(TechnicalSectorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_technicalSector
	return p
}

func (*TechnicalSectorContext) IsTechnicalSectorContext() {}

func NewTechnicalSectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TechnicalSectorContext {
	var p = new(TechnicalSectorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_technicalSector

	return p
}

func (s *TechnicalSectorContext) GetParser() antlr.Parser { return s.parser }

func (s *TechnicalSectorContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *TechnicalSectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TechnicalSectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TechnicalSectorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterTechnicalSector(s)
	}
}

func (s *TechnicalSectorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitTechnicalSector(s)
	}
}

func (p *RDSPPParser) TechnicalSector() (localctx ITechnicalSectorContext) {
	localctx = NewTechnicalSectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 128, RDSPPParserRULE_technicalSector)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(494)
		p.Alpha()
	}

	return localctx
}

// IDccMainClassContext is an interface to support dynamic dispatch.
type IDccMainClassContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDccMainClassContext differentiates from other interfaces.
	IsDccMainClassContext()
}

type DccMainClassContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDccMainClassContext() *DccMainClassContext {
	var p = new(DccMainClassContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_dccMainClass
	return p
}

func (*DccMainClassContext) IsDccMainClassContext() {}

func NewDccMainClassContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DccMainClassContext {
	var p = new(DccMainClassContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_dccMainClass

	return p
}

func (s *DccMainClassContext) GetParser() antlr.Parser { return s.parser }

func (s *DccMainClassContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *DccMainClassContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DccMainClassContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DccMainClassContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterDccMainClass(s)
	}
}

func (s *DccMainClassContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitDccMainClass(s)
	}
}

func (p *RDSPPParser) DccMainClass() (localctx IDccMainClassContext) {
	localctx = NewDccMainClassContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 130, RDSPPParserRULE_dccMainClass)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(496)
		p.Alpha()
	}

	return localctx
}

// IDccSubclassContext is an interface to support dynamic dispatch.
type IDccSubclassContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDccSubclassContext differentiates from other interfaces.
	IsDccSubclassContext()
}

type DccSubclassContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDccSubclassContext() *DccSubclassContext {
	var p = new(DccSubclassContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_dccSubclass
	return p
}

func (*DccSubclassContext) IsDccSubclassContext() {}

func NewDccSubclassContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DccSubclassContext {
	var p = new(DccSubclassContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_dccSubclass

	return p
}

func (s *DccSubclassContext) GetParser() antlr.Parser { return s.parser }

func (s *DccSubclassContext) Alpha() IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *DccSubclassContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DccSubclassContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DccSubclassContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterDccSubclass(s)
	}
}

func (s *DccSubclassContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitDccSubclass(s)
	}
}

func (p *RDSPPParser) DccSubclass() (localctx IDccSubclassContext) {
	localctx = NewDccSubclassContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 132, RDSPPParserRULE_dccSubclass)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(498)
		p.Alpha()
	}

	return localctx
}

// IDccConcreteContext is an interface to support dynamic dispatch.
type IDccConcreteContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDccConcreteContext differentiates from other interfaces.
	IsDccConcreteContext()
}

type DccConcreteContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDccConcreteContext() *DccConcreteContext {
	var p = new(DccConcreteContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_dccConcrete
	return p
}

func (*DccConcreteContext) IsDccConcreteContext() {}

func NewDccConcreteContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DccConcreteContext {
	var p = new(DccConcreteContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_dccConcrete

	return p
}

func (s *DccConcreteContext) GetParser() antlr.Parser { return s.parser }

func (s *DccConcreteContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *DccConcreteContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *DccConcreteContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DccConcreteContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DccConcreteContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterDccConcrete(s)
	}
}

func (s *DccConcreteContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitDccConcrete(s)
	}
}

func (p *RDSPPParser) DccConcrete() (localctx IDccConcreteContext) {
	localctx = NewDccConcreteContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 134, RDSPPParserRULE_dccConcrete)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(500)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(501)
		p.Match(RDSPPParserNUMBER)
	}
	{
		p.SetState(502)
		p.Match(RDSPPParserNUMBER)
	}

	return localctx
}

// IPageCountNumberContext is an interface to support dynamic dispatch.
type IPageCountNumberContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPageCountNumberContext differentiates from other interfaces.
	IsPageCountNumberContext()
}

type PageCountNumberContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPageCountNumberContext() *PageCountNumberContext {
	var p = new(PageCountNumberContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_pageCountNumber
	return p
}

func (*PageCountNumberContext) IsPageCountNumberContext() {}

func NewPageCountNumberContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PageCountNumberContext {
	var p = new(PageCountNumberContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_pageCountNumber

	return p
}

func (s *PageCountNumberContext) GetParser() antlr.Parser { return s.parser }

func (s *PageCountNumberContext) PageNumber() IPageNumberContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPageNumberContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPageNumberContext)
}

func (s *PageCountNumberContext) PageNumberAlpha() IPageNumberAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPageNumberAlphaContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPageNumberAlphaContext)
}

func (s *PageCountNumberContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PageCountNumberContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PageCountNumberContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterPageCountNumber(s)
	}
}

func (s *PageCountNumberContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitPageCountNumber(s)
	}
}

func (p *RDSPPParser) PageCountNumber() (localctx IPageCountNumberContext) {
	localctx = NewPageCountNumberContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 136, RDSPPParserRULE_pageCountNumber)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(505)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
		{
			p.SetState(504)
			p.PageNumberAlpha()
		}

	}
	{
		p.SetState(507)
		p.PageNumber()
	}

	return localctx
}

// IPageNumberAlphaContext is an interface to support dynamic dispatch.
type IPageNumberAlphaContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPageNumberAlphaContext differentiates from other interfaces.
	IsPageNumberAlphaContext()
}

type PageNumberAlphaContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPageNumberAlphaContext() *PageNumberAlphaContext {
	var p = new(PageNumberAlphaContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_pageNumberAlpha
	return p
}

func (*PageNumberAlphaContext) IsPageNumberAlphaContext() {}

func NewPageNumberAlphaContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PageNumberAlphaContext {
	var p = new(PageNumberAlphaContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_pageNumberAlpha

	return p
}

func (s *PageNumberAlphaContext) GetParser() antlr.Parser { return s.parser }

func (s *PageNumberAlphaContext) AllAlpha() []IAlphaContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IAlphaContext)(nil)).Elem())
	var tst = make([]IAlphaContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IAlphaContext)
		}
	}

	return tst
}

func (s *PageNumberAlphaContext) Alpha(i int) IAlphaContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAlphaContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IAlphaContext)
}

func (s *PageNumberAlphaContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PageNumberAlphaContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PageNumberAlphaContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterPageNumberAlpha(s)
	}
}

func (s *PageNumberAlphaContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitPageNumberAlpha(s)
	}
}

func (p *RDSPPParser) PageNumberAlpha() (localctx IPageNumberAlphaContext) {
	localctx = NewPageNumberAlphaContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 138, RDSPPParserRULE_pageNumberAlpha)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(509)
		p.Alpha()
	}
	p.SetState(517)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
		{
			p.SetState(510)
			p.Alpha()
		}
		p.SetState(515)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
			{
				p.SetState(511)
				p.Alpha()
			}
			p.SetState(513)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)

			if ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RDSPPParserT__0)|(1<<RDSPPParserT__1)|(1<<RDSPPParserT__2)|(1<<RDSPPParserT__3)|(1<<RDSPPParserT__4)|(1<<RDSPPParserT__5)|(1<<RDSPPParserT__6)|(1<<RDSPPParserT__7)|(1<<RDSPPParserT__8)|(1<<RDSPPParserT__9)|(1<<RDSPPParserT__10)|(1<<RDSPPParserT__11)|(1<<RDSPPParserT__12)|(1<<RDSPPParserT__13)|(1<<RDSPPParserT__14)|(1<<RDSPPParserT__15)|(1<<RDSPPParserT__16)|(1<<RDSPPParserT__17)|(1<<RDSPPParserT__18)|(1<<RDSPPParserT__19)|(1<<RDSPPParserT__20)|(1<<RDSPPParserT__21)|(1<<RDSPPParserT__22)|(1<<RDSPPParserT__23))) != 0 {
				{
					p.SetState(512)
					p.Alpha()
				}

			}

		}

	}

	return localctx
}

// IPageNumberContext is an interface to support dynamic dispatch.
type IPageNumberContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPageNumberContext differentiates from other interfaces.
	IsPageNumberContext()
}

type PageNumberContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPageNumberContext() *PageNumberContext {
	var p = new(PageNumberContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RDSPPParserRULE_pageNumber
	return p
}

func (*PageNumberContext) IsPageNumberContext() {}

func NewPageNumberContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PageNumberContext {
	var p = new(PageNumberContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RDSPPParserRULE_pageNumber

	return p
}

func (s *PageNumberContext) GetParser() antlr.Parser { return s.parser }

func (s *PageNumberContext) AllNUMBER() []antlr.TerminalNode {
	return s.GetTokens(RDSPPParserNUMBER)
}

func (s *PageNumberContext) NUMBER(i int) antlr.TerminalNode {
	return s.GetToken(RDSPPParserNUMBER, i)
}

func (s *PageNumberContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PageNumberContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PageNumberContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.EnterPageNumber(s)
	}
}

func (s *PageNumberContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(RDSPPListener); ok {
		listenerT.ExitPageNumber(s)
	}
}

func (p *RDSPPParser) PageNumber() (localctx IPageNumberContext) {
	localctx = NewPageNumberContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 140, RDSPPParserRULE_pageNumber)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(519)
		p.Match(RDSPPParserNUMBER)
	}
	p.SetState(527)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RDSPPParserNUMBER {
		{
			p.SetState(520)
			p.Match(RDSPPParserNUMBER)
		}
		p.SetState(525)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == RDSPPParserNUMBER {
			{
				p.SetState(521)
				p.Match(RDSPPParserNUMBER)
			}
			p.SetState(523)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)

			if _la == RDSPPParserNUMBER {
				{
					p.SetState(522)
					p.Match(RDSPPParserNUMBER)
				}

			}

		}

	}

	return localctx
}
