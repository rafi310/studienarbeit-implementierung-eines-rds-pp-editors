// Code generated from RDSPP.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // RDSPP

import "github.com/antlr/antlr4/runtime/Go/antlr"

// RDSPPListener is a complete listener for a parse tree produced by RDSPPParser.
type RDSPPListener interface {
	antlr.ParseTreeListener

	// EnterStart is called when entering the start production.
	EnterStart(c *StartContext)

	// EnterStartOnlyConjointDesignation is called when entering the startOnlyConjointDesignation production.
	EnterStartOnlyConjointDesignation(c *StartOnlyConjointDesignationContext)

	// EnterStartOnlyFunction is called when entering the startOnlyFunction production.
	EnterStartOnlyFunction(c *StartOnlyFunctionContext)

	// EnterStartOnlyOperatingEquipment is called when entering the startOnlyOperatingEquipment production.
	EnterStartOnlyOperatingEquipment(c *StartOnlyOperatingEquipmentContext)

	// EnterStartOnlyPointOfInstallation is called when entering the startOnlyPointOfInstallation production.
	EnterStartOnlyPointOfInstallation(c *StartOnlyPointOfInstallationContext)

	// EnterStartOnlySiteOfInstallation is called when entering the startOnlySiteOfInstallation production.
	EnterStartOnlySiteOfInstallation(c *StartOnlySiteOfInstallationContext)

	// EnterStartOnlyProduct is called when entering the startOnlyProduct production.
	EnterStartOnlyProduct(c *StartOnlyProductContext)

	// EnterStartOnlyTerminal is called when entering the startOnlyTerminal production.
	EnterStartOnlyTerminal(c *StartOnlyTerminalContext)

	// EnterStartOnlySignal is called when entering the startOnlySignal production.
	EnterStartOnlySignal(c *StartOnlySignalContext)

	// EnterStartOnlyDocument is called when entering the startOnlyDocument production.
	EnterStartOnlyDocument(c *StartOnlyDocumentContext)

	// EnterDesignation is called when entering the designation production.
	EnterDesignation(c *DesignationContext)

	// EnterAlpha is called when entering the alpha production.
	EnterAlpha(c *AlphaContext)

	// EnterUnderscore_alpha is called when entering the underscore_alpha production.
	EnterUnderscore_alpha(c *Underscore_alphaContext)

	// EnterUnderscore is called when entering the underscore production.
	EnterUnderscore(c *UnderscoreContext)

	// EnterConjointDesignation is called when entering the conjointDesignation production.
	EnterConjointDesignation(c *ConjointDesignationContext)

	// EnterGeographicLocation is called when entering the geographicLocation production.
	EnterGeographicLocation(c *GeographicLocationContext)

	// EnterLatitude is called when entering the latitude production.
	EnterLatitude(c *LatitudeContext)

	// EnterLongitude is called when entering the longitude production.
	EnterLongitude(c *LongitudeContext)

	// EnterProjectSpecificInformation is called when entering the projectSpecificInformation production.
	EnterProjectSpecificInformation(c *ProjectSpecificInformationContext)

	// EnterCountry is called when entering the country production.
	EnterCountry(c *CountryContext)

	// EnterRegion is called when entering the region production.
	EnterRegion(c *RegionContext)

	// EnterTownOrName is called when entering the townOrName production.
	EnterTownOrName(c *TownOrNameContext)

	// EnterProjectCounter is called when entering the projectCounter production.
	EnterProjectCounter(c *ProjectCounterContext)

	// EnterPlantType is called when entering the plantType production.
	EnterPlantType(c *PlantTypeContext)

	// EnterFacilityType is called when entering the facilityType production.
	EnterFacilityType(c *FacilityTypeContext)

	// EnterFunction is called when entering the function production.
	EnterFunction(c *FunctionContext)

	// EnterMainSystem is called when entering the mainSystem production.
	EnterMainSystem(c *MainSystemContext)

	// EnterMainSystemA1 is called when entering the mainSystemA1 production.
	EnterMainSystemA1(c *MainSystemA1Context)

	// EnterMainSystemCounter is called when entering the mainSystemCounter production.
	EnterMainSystemCounter(c *MainSystemCounterContext)

	// EnterSystemSubsystem is called when entering the systemSubsystem production.
	EnterSystemSubsystem(c *SystemSubsystemContext)

	// EnterSystemSubsystemPart is called when entering the systemSubsystemPart production.
	EnterSystemSubsystemPart(c *SystemSubsystemPartContext)

	// EnterSystemA1 is called when entering the systemA1 production.
	EnterSystemA1(c *SystemA1Context)

	// EnterSystemA2 is called when entering the systemA2 production.
	EnterSystemA2(c *SystemA2Context)

	// EnterSystemA3 is called when entering the systemA3 production.
	EnterSystemA3(c *SystemA3Context)

	// EnterSubsystem is called when entering the subsystem production.
	EnterSubsystem(c *SubsystemContext)

	// EnterBasicFunction is called when entering the basicFunction production.
	EnterBasicFunction(c *BasicFunctionContext)

	// EnterBasicFunctionA1 is called when entering the basicFunctionA1 production.
	EnterBasicFunctionA1(c *BasicFunctionA1Context)

	// EnterBasicFunctionA2 is called when entering the basicFunctionA2 production.
	EnterBasicFunctionA2(c *BasicFunctionA2Context)

	// EnterBasicFunctionConcrete is called when entering the basicFunctionConcrete production.
	EnterBasicFunctionConcrete(c *BasicFunctionConcreteContext)

	// EnterProduct is called when entering the product production.
	EnterProduct(c *ProductContext)

	// EnterProductClass is called when entering the productClass production.
	EnterProductClass(c *ProductClassContext)

	// EnterProductClassA1 is called when entering the productClassA1 production.
	EnterProductClassA1(c *ProductClassA1Context)

	// EnterProductClassA2 is called when entering the productClassA2 production.
	EnterProductClassA2(c *ProductClassA2Context)

	// EnterProductClassConcrete is called when entering the productClassConcrete production.
	EnterProductClassConcrete(c *ProductClassConcreteContext)

	// EnterOperatingEquipment is called when entering the operatingEquipment production.
	EnterOperatingEquipment(c *OperatingEquipmentContext)

	// EnterPointOfInstallation is called when entering the pointOfInstallation production.
	EnterPointOfInstallation(c *PointOfInstallationContext)

	// EnterPointOfInstallationPlace is called when entering the pointOfInstallationPlace production.
	EnterPointOfInstallationPlace(c *PointOfInstallationPlaceContext)

	// EnterColumn is called when entering the column production.
	EnterColumn(c *ColumnContext)

	// EnterRow is called when entering the row production.
	EnterRow(c *RowContext)

	// EnterSiteOfInstallation is called when entering the siteOfInstallation production.
	EnterSiteOfInstallation(c *SiteOfInstallationContext)

	// EnterSiteOfInstallationPlace is called when entering the siteOfInstallationPlace production.
	EnterSiteOfInstallationPlace(c *SiteOfInstallationPlaceContext)

	// EnterSignal is called when entering the signal production.
	EnterSignal(c *SignalContext)

	// EnterSignalClass is called when entering the signalClass production.
	EnterSignalClass(c *SignalClassContext)

	// EnterSignalMainClass is called when entering the signalMainClass production.
	EnterSignalMainClass(c *SignalMainClassContext)

	// EnterSignalSubclass is called when entering the signalSubclass production.
	EnterSignalSubclass(c *SignalSubclassContext)

	// EnterSignalClassCounter is called when entering the signalClassCounter production.
	EnterSignalClassCounter(c *SignalClassCounterContext)

	// EnterSignalInformation is called when entering the signalInformation production.
	EnterSignalInformation(c *SignalInformationContext)

	// EnterSignalDuration is called when entering the signalDuration production.
	EnterSignalDuration(c *SignalDurationContext)

	// EnterSignalType is called when entering the signalType production.
	EnterSignalType(c *SignalTypeContext)

	// EnterSignalInformationCounter is called when entering the signalInformationCounter production.
	EnterSignalInformationCounter(c *SignalInformationCounterContext)

	// EnterTerminal is called when entering the terminal production.
	EnterTerminal(c *TerminalContext)

	// EnterTerminalPoint is called when entering the terminalPoint production.
	EnterTerminalPoint(c *TerminalPointContext)

	// EnterDocument is called when entering the document production.
	EnterDocument(c *DocumentContext)

	// EnterDcc is called when entering the dcc production.
	EnterDcc(c *DccContext)

	// EnterTechnicalSector is called when entering the technicalSector production.
	EnterTechnicalSector(c *TechnicalSectorContext)

	// EnterDccMainClass is called when entering the dccMainClass production.
	EnterDccMainClass(c *DccMainClassContext)

	// EnterDccSubclass is called when entering the dccSubclass production.
	EnterDccSubclass(c *DccSubclassContext)

	// EnterDccConcrete is called when entering the dccConcrete production.
	EnterDccConcrete(c *DccConcreteContext)

	// EnterPageCountNumber is called when entering the pageCountNumber production.
	EnterPageCountNumber(c *PageCountNumberContext)

	// EnterPageNumberAlpha is called when entering the pageNumberAlpha production.
	EnterPageNumberAlpha(c *PageNumberAlphaContext)

	// EnterPageNumber is called when entering the pageNumber production.
	EnterPageNumber(c *PageNumberContext)

	// ExitStart is called when exiting the start production.
	ExitStart(c *StartContext)

	// ExitStartOnlyConjointDesignation is called when exiting the startOnlyConjointDesignation production.
	ExitStartOnlyConjointDesignation(c *StartOnlyConjointDesignationContext)

	// ExitStartOnlyFunction is called when exiting the startOnlyFunction production.
	ExitStartOnlyFunction(c *StartOnlyFunctionContext)

	// ExitStartOnlyOperatingEquipment is called when exiting the startOnlyOperatingEquipment production.
	ExitStartOnlyOperatingEquipment(c *StartOnlyOperatingEquipmentContext)

	// ExitStartOnlyPointOfInstallation is called when exiting the startOnlyPointOfInstallation production.
	ExitStartOnlyPointOfInstallation(c *StartOnlyPointOfInstallationContext)

	// ExitStartOnlySiteOfInstallation is called when exiting the startOnlySiteOfInstallation production.
	ExitStartOnlySiteOfInstallation(c *StartOnlySiteOfInstallationContext)

	// ExitStartOnlyProduct is called when exiting the startOnlyProduct production.
	ExitStartOnlyProduct(c *StartOnlyProductContext)

	// ExitStartOnlyTerminal is called when exiting the startOnlyTerminal production.
	ExitStartOnlyTerminal(c *StartOnlyTerminalContext)

	// ExitStartOnlySignal is called when exiting the startOnlySignal production.
	ExitStartOnlySignal(c *StartOnlySignalContext)

	// ExitStartOnlyDocument is called when exiting the startOnlyDocument production.
	ExitStartOnlyDocument(c *StartOnlyDocumentContext)

	// ExitDesignation is called when exiting the designation production.
	ExitDesignation(c *DesignationContext)

	// ExitAlpha is called when exiting the alpha production.
	ExitAlpha(c *AlphaContext)

	// ExitUnderscore_alpha is called when exiting the underscore_alpha production.
	ExitUnderscore_alpha(c *Underscore_alphaContext)

	// ExitUnderscore is called when exiting the underscore production.
	ExitUnderscore(c *UnderscoreContext)

	// ExitConjointDesignation is called when exiting the conjointDesignation production.
	ExitConjointDesignation(c *ConjointDesignationContext)

	// ExitGeographicLocation is called when exiting the geographicLocation production.
	ExitGeographicLocation(c *GeographicLocationContext)

	// ExitLatitude is called when exiting the latitude production.
	ExitLatitude(c *LatitudeContext)

	// ExitLongitude is called when exiting the longitude production.
	ExitLongitude(c *LongitudeContext)

	// ExitProjectSpecificInformation is called when exiting the projectSpecificInformation production.
	ExitProjectSpecificInformation(c *ProjectSpecificInformationContext)

	// ExitCountry is called when exiting the country production.
	ExitCountry(c *CountryContext)

	// ExitRegion is called when exiting the region production.
	ExitRegion(c *RegionContext)

	// ExitTownOrName is called when exiting the townOrName production.
	ExitTownOrName(c *TownOrNameContext)

	// ExitProjectCounter is called when exiting the projectCounter production.
	ExitProjectCounter(c *ProjectCounterContext)

	// ExitPlantType is called when exiting the plantType production.
	ExitPlantType(c *PlantTypeContext)

	// ExitFacilityType is called when exiting the facilityType production.
	ExitFacilityType(c *FacilityTypeContext)

	// ExitFunction is called when exiting the function production.
	ExitFunction(c *FunctionContext)

	// ExitMainSystem is called when exiting the mainSystem production.
	ExitMainSystem(c *MainSystemContext)

	// ExitMainSystemA1 is called when exiting the mainSystemA1 production.
	ExitMainSystemA1(c *MainSystemA1Context)

	// ExitMainSystemCounter is called when exiting the mainSystemCounter production.
	ExitMainSystemCounter(c *MainSystemCounterContext)

	// ExitSystemSubsystem is called when exiting the systemSubsystem production.
	ExitSystemSubsystem(c *SystemSubsystemContext)

	// ExitSystemSubsystemPart is called when exiting the systemSubsystemPart production.
	ExitSystemSubsystemPart(c *SystemSubsystemPartContext)

	// ExitSystemA1 is called when exiting the systemA1 production.
	ExitSystemA1(c *SystemA1Context)

	// ExitSystemA2 is called when exiting the systemA2 production.
	ExitSystemA2(c *SystemA2Context)

	// ExitSystemA3 is called when exiting the systemA3 production.
	ExitSystemA3(c *SystemA3Context)

	// ExitSubsystem is called when exiting the subsystem production.
	ExitSubsystem(c *SubsystemContext)

	// ExitBasicFunction is called when exiting the basicFunction production.
	ExitBasicFunction(c *BasicFunctionContext)

	// ExitBasicFunctionA1 is called when exiting the basicFunctionA1 production.
	ExitBasicFunctionA1(c *BasicFunctionA1Context)

	// ExitBasicFunctionA2 is called when exiting the basicFunctionA2 production.
	ExitBasicFunctionA2(c *BasicFunctionA2Context)

	// ExitBasicFunctionConcrete is called when exiting the basicFunctionConcrete production.
	ExitBasicFunctionConcrete(c *BasicFunctionConcreteContext)

	// ExitProduct is called when exiting the product production.
	ExitProduct(c *ProductContext)

	// ExitProductClass is called when exiting the productClass production.
	ExitProductClass(c *ProductClassContext)

	// ExitProductClassA1 is called when exiting the productClassA1 production.
	ExitProductClassA1(c *ProductClassA1Context)

	// ExitProductClassA2 is called when exiting the productClassA2 production.
	ExitProductClassA2(c *ProductClassA2Context)

	// ExitProductClassConcrete is called when exiting the productClassConcrete production.
	ExitProductClassConcrete(c *ProductClassConcreteContext)

	// ExitOperatingEquipment is called when exiting the operatingEquipment production.
	ExitOperatingEquipment(c *OperatingEquipmentContext)

	// ExitPointOfInstallation is called when exiting the pointOfInstallation production.
	ExitPointOfInstallation(c *PointOfInstallationContext)

	// ExitPointOfInstallationPlace is called when exiting the pointOfInstallationPlace production.
	ExitPointOfInstallationPlace(c *PointOfInstallationPlaceContext)

	// ExitColumn is called when exiting the column production.
	ExitColumn(c *ColumnContext)

	// ExitRow is called when exiting the row production.
	ExitRow(c *RowContext)

	// ExitSiteOfInstallation is called when exiting the siteOfInstallation production.
	ExitSiteOfInstallation(c *SiteOfInstallationContext)

	// ExitSiteOfInstallationPlace is called when exiting the siteOfInstallationPlace production.
	ExitSiteOfInstallationPlace(c *SiteOfInstallationPlaceContext)

	// ExitSignal is called when exiting the signal production.
	ExitSignal(c *SignalContext)

	// ExitSignalClass is called when exiting the signalClass production.
	ExitSignalClass(c *SignalClassContext)

	// ExitSignalMainClass is called when exiting the signalMainClass production.
	ExitSignalMainClass(c *SignalMainClassContext)

	// ExitSignalSubclass is called when exiting the signalSubclass production.
	ExitSignalSubclass(c *SignalSubclassContext)

	// ExitSignalClassCounter is called when exiting the signalClassCounter production.
	ExitSignalClassCounter(c *SignalClassCounterContext)

	// ExitSignalInformation is called when exiting the signalInformation production.
	ExitSignalInformation(c *SignalInformationContext)

	// ExitSignalDuration is called when exiting the signalDuration production.
	ExitSignalDuration(c *SignalDurationContext)

	// ExitSignalType is called when exiting the signalType production.
	ExitSignalType(c *SignalTypeContext)

	// ExitSignalInformationCounter is called when exiting the signalInformationCounter production.
	ExitSignalInformationCounter(c *SignalInformationCounterContext)

	// ExitTerminal is called when exiting the terminal production.
	ExitTerminal(c *TerminalContext)

	// ExitTerminalPoint is called when exiting the terminalPoint production.
	ExitTerminalPoint(c *TerminalPointContext)

	// ExitDocument is called when exiting the document production.
	ExitDocument(c *DocumentContext)

	// ExitDcc is called when exiting the dcc production.
	ExitDcc(c *DccContext)

	// ExitTechnicalSector is called when exiting the technicalSector production.
	ExitTechnicalSector(c *TechnicalSectorContext)

	// ExitDccMainClass is called when exiting the dccMainClass production.
	ExitDccMainClass(c *DccMainClassContext)

	// ExitDccSubclass is called when exiting the dccSubclass production.
	ExitDccSubclass(c *DccSubclassContext)

	// ExitDccConcrete is called when exiting the dccConcrete production.
	ExitDccConcrete(c *DccConcreteContext)

	// ExitPageCountNumber is called when exiting the pageCountNumber production.
	ExitPageCountNumber(c *PageCountNumberContext)

	// ExitPageNumberAlpha is called when exiting the pageNumberAlpha production.
	ExitPageNumberAlpha(c *PageNumberAlphaContext)

	// ExitPageNumber is called when exiting the pageNumber production.
	ExitPageNumber(c *PageNumberContext)
}
