# ANTLR 4 Prototype

### Generate Parser
```bash
$ sh antlr.sh
```

### Run Test Script
```bash
# usage
$ go run main.go "<RDS-PP-KENNZEICHEN>"

# example
$ go run main.go "=G001 MDA30 GP001 —MA001:L1"
```
