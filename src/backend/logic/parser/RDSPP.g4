grammar RDSPP;

/* *******************************************************************
 *
 * LEXER RULES / TOKENS
 *
 * **************************************************************** */

WHITESPACE: [ \r\n\t]+ -> skip;

NUMBER: '0'..'9';

DOT: '.';
SLASH: '/';

HASH: '#';
EQUALS: '=';
PLUS: '+';
PLUSPLUS: '++';
MINUS: '-';
COLON: ':';
SEMICOLON: ';';
AMPERSAND: '&';




/* *******************************************************************
 *
 * PARSER RULES / DERIVATIONS
 *
 * references:
 *  - [1] VGB-S-823-32-2014-03-EN-DE (Application Guide Wind Turbines)
 *  - [2] DIN EN 61355-1 (DCC)
 *  - [3] The Definitive ANLTR4 Reference
 *
 * **************************************************************** */

/*
 * default start rule
 * => input is designation
 */
start: designation EOF;

/*
 * specific start rules for only one designation kind each
 *   => better recognition since they are less vague/similar
 *   => less NoViableAlternativExceptions
 * the rules (function, etc.) can't be used directly as start rule since without EOF
 * error recovery/listening ins't working properly ([3] page 271)
 */
startOnlyConjointDesignation: conjointDesignation EOF;
startOnlyFunction: function EOF;
startOnlyOperatingEquipment: operatingEquipment EOF;
startOnlyPointOfInstallation: pointOfInstallation EOF;
startOnlySiteOfInstallation: siteOfInstallation EOF;
startOnlyProduct: product EOF;
startOnlyTerminal: terminal EOF;
startOnlySignal: signal EOF;
startOnlyDocument: document EOF;

/*
 * designations and their allowed concatenations
 * '''''''''''''''''''''''''''''''''''''''''''''
 * sources:
 *  - [1] pages 54, table 4
 *  - [1] pages 90, table 10
 */
designation:
	conjointDesignation
	| conjointDesignation? document
	| conjointDesignation? function (signal | document)?
	| conjointDesignation? operatingEquipment (terminal | document)?
	| conjointDesignation? pointOfInstallation document?
	| conjointDesignation? siteOfInstallation document?
	| product
	| terminal
	| signal;

/*
 * common rules for all designations
 */
alpha: 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'J' | 'K' | 'L' | 'M' | 'N' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z';
// allows skipping _ when building the data structure if _ is used as an alphabetical character rather than a separator
underscore_alpha: '_';
// when _ is a separator
underscore: '_';

/*
 * conjoint desingation
 * ''''''''''''''''''''
 * # NNNNANNNNNA . AAAAA.AAANNAA
 *
 * sources:
 *  - [1] page 56, figure 8
 *  - [1] page 98, figure 36
 *  - [1] page 287
 */
conjointDesignation: HASH (projectSpecificInformation | geographicLocation (DOT projectSpecificInformation)?);

geographicLocation:	latitude longitude;
latitude: NUMBER NUMBER NUMBER NUMBER ('N' | 'S');
longitude: NUMBER NUMBER NUMBER NUMBER NUMBER ('E' | 'W');

projectSpecificInformation: country region DOT townOrName projectCounter plantType facilityType;
country: (alpha | 'I' | 'O') (alpha | 'I' | 'O');
region: (alpha | 'I' | 'O' | underscore_alpha) (alpha | 'I' | 'O' | underscore_alpha) (alpha | 'I' | 'O');
townOrName: (alpha | 'I' | 'O') (alpha | 'I' | 'O') (alpha | 'I' | 'O');
projectCounter: (NUMBER | underscore_alpha) NUMBER;
plantType: alpha;
facilityType: alpha;

/*
 * function designation
 * ''''''''''''''''''''
 * = ANNN AAANN AANNN
 *
 * sources:
 *  - [1] page 57, figure 9
 *  - [1] page 288f
 */
function:
	EQUALS mainSystemA1
	| EQUALS mainSystem
	| EQUALS mainSystem? systemSubsystemPart basicFunction?;

mainSystem: mainSystemA1 mainSystemCounter;
mainSystemA1: alpha;
mainSystemCounter: NUMBER NUMBER NUMBER;

systemSubsystem: systemA1 systemA2 systemA3 subsystem;

systemSubsystemPart:
	systemSubsystem
	| systemA1 systemA2 systemA3
	| systemA1 systemA2
	| systemA1;
systemA1: alpha;
systemA2: alpha;
systemA3: alpha;
subsystem: NUMBER NUMBER;

basicFunction: basicFunctionA1 basicFunctionA2 basicFunctionConcrete;
basicFunctionA1: alpha;
basicFunctionA2: alpha;
basicFunctionConcrete: NUMBER NUMBER NUMBER;

/*
 * product class designation
 * '''''''''''''''''''''''''
 * - AANNN (AANNN)
 *
 * sources:
 *  - [1] page 63, figure 10
 *  - [1] page 288
 */
product: MINUS (productClass)+;
productClass: productClassA1 productClassA2 productClassConcrete;
productClassA1: alpha;
productClassA2: alpha;
productClassConcrete: NUMBER NUMBER NUMBER;

/*
 * operating equipment designation
 * '''''''''''''''''''''''''''''''
 * = ANNN AAANN AANNN - AANNN (AANNN)
 *
 * sources:
 *  - [1] page 65, figure 12
 *  - [1] page 289
 */
operatingEquipment: EQUALS mainSystem? systemSubsystem basicFunction product;

/*
 * point of installation designation
 * '''''''''''''''''''''''''''''''''
 * + ANNN AAANN AANNN AANNN
 *
 * sources:
 *  - [1] page 67ff
 *  - [1] page 69, figure 17
 */
pointOfInstallation: PLUS mainSystem? systemSubsystem basicFunction? (DOT pointOfInstallationPlace)?;
pointOfInstallationPlace: column row;
column: alpha alpha;
row: NUMBER NUMBER NUMBER;

/*
 * site of installation designation
 * ''''''''''''''''''''''''''''''''
 * ++ ANNN AAANN A...N
 *
 * sources:
 *  - [1] page 74ff
 *  - [1] page 76, figure 22
 *  - [1] page 46
 */
siteOfInstallation: PLUSPLUS mainSystem systemSubsystem? DOT (siteOfInstallationPlace | geographicLocation);
siteOfInstallationPlace: alpha (alpha (alpha (alpha)?)?)? NUMBER (NUMBER (NUMBER (NUMBER)?)?)?;

/*
 * signal designation
 * ''''''''''''''''''
 * ; AANNN(_AANNN)
 *
 * sources:
 *  - [1] page 80ff
 *  - [1] page 81, figure 28
 */
signal: SEMICOLON signalClass (underscore signalInformation)?;

signalClass: signalMainClass signalSubclass signalClassCounter;
signalMainClass: alpha;
signalSubclass: alpha;
signalClassCounter: NUMBER NUMBER NUMBER;

signalInformation: signalDuration signalType signalInformationCounter;
signalDuration: alpha;
signalType: alpha;
signalInformationCounter: NUMBER NUMBER;

/*
 * terminal designation
 * ''''''''''''''''''''
 * : A...N (. A...N)
 *
 * sources:
 *  - [1] page 85, figure 27
 */
terminal: COLON terminalPoint (DOT terminalPoint)?;
terminalPoint: (alpha | NUMBER)+;

/*
 * document designation
 * ''''''''''''''''''''
 * & AAANNN (/ A...N)
 *
 * sources:
 *  - [1] page 88, figure 30
 *  - [2]
 */
document: AMPERSAND dcc (SLASH pageCountNumber)?;

dcc: technicalSector dccMainClass dccSubclass dccConcrete;
technicalSector: alpha;
dccMainClass: alpha;
dccSubclass: alpha;
dccConcrete: NUMBER NUMBER NUMBER;

pageCountNumber: pageNumberAlpha? pageNumber;
pageNumberAlpha: alpha (alpha (alpha (alpha)?)?)?;
pageNumber: NUMBER (NUMBER (NUMBER (NUMBER)?)?)?;
