package parser

import (
	"fmt"
	"regexp"
	"sort"
	"strings"
)

func indentLispTree(lisp string) string {
	const indent = 4
	bracketsCounter := 0
	precedingWhitespace := false
	addedTerminalMark := false
	tree := ""
	lastSubstr := ""
	specialLiterals := []string{"(", ")", " "}
	for i := 0; i < len(lisp); i++ {
		substr := string(lisp[i])
		if !isIn(substr, specialLiterals) {
			for !isIn(string(lisp[i+1]), specialLiterals) {
				substr += string(lisp[i+1])
				i++
			}
		}
		switch substr {
		case "(":
			precedingWhitespace = false
			addedTerminalMark = false
			bracketsCounter++
		case ")":
			if !addedTerminalMark {
				tree += fmt.Sprintf("  %s> %s", strings.Repeat("─", 40-bracketsCounter*indent-len(lastSubstr)-2), lastSubstr)
				addedTerminalMark = true
			}
			bracketsCounter--
		case " ":
			tree += fmt.Sprintf("\n%s", strings.Repeat(" ", bracketsCounter*indent))
			precedingWhitespace = true
			addedTerminalMark = false
		default:
			tree += substr
			if precedingWhitespace && !addedTerminalMark && string(lisp[i+1]) == " " {
				tree += fmt.Sprintf("  %s> %s", strings.Repeat("─", 40-bracketsCounter*indent-len(lastSubstr)-2), substr)
				precedingWhitespace = false
				addedTerminalMark = true
			}
		}
		lastSubstr = substr
	}
	return tree
}

func isIn(s string, array []string) bool {
	for i := 0; i < len(array); i++ {
		if array[i] == s {
			return true
		}
	}
	return false
}

func isAlpha(a string) bool {
	if len(a) == 1 {
		match, err := regexp.MatchString("[A-Z]", a)
		if err == nil {
			return match
		}
	}
	return false
}

func sortAndAppendAlphas(alphas []string) string {
	if len(alphas) == 0 {
		return ""
	}
	if len(alphas) == 1 {
		return alphas[0]
	}
	if len(alphas) == 2 {
		return alphas[0] + ", " + alphas[1]
	}
	sorted := make([]string, len(alphas))
	copy(sorted, alphas)
	sort.Strings(sorted)

	ret := sorted[0]
	for i := 0; i < len(sorted)-1; i++ {
		if !isAlpha(sorted[i]) || !isAlpha(sorted[i+1]) {
			continue
		}
		a := rune(sorted[i][0])
		b := rune(sorted[i+1][0])
		if b-a > 1 {
			ret += " ... " + sorted[i] + ", " + sorted[i+1]
		}
	}
	ret += " ... " + sorted[len(sorted)-1]
	return ret
}
