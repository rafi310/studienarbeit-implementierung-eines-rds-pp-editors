package parser

import (
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"rdspp.de/src/logic/designation/denomination"
	"rdspp.de/src/logic/parser/parser"
)

// ErrorInformation struct
type ErrorInformation struct {
	Designation string   `json:"designation"`
	Message     string   `json:"message"`
	Index       int      `json:"index"`
	Text        string   `json:"text"`
	Type        int      `json:"type"`
	Suggestions []string `json:"suggestions"`
}

/* Implementation of antlr.ErrorListener:
 *
 *     type ErrorListener interface {
 *         SyntaxError(recognizer Recognizer, offendingSymbol interface{}, line, column int, msg string, e RecognitionException)
 *         ReportAmbiguity(recognizer Parser, dfa *DFA, startIndex, stopIndex int, exact bool, ambigAlts *BitSet, configs ATNConfigSet)
 *         ReportAttemptingFullContext(recognizer Parser, dfa *DFA, startIndex, stopIndex int, conflictingAlts *BitSet, configs ATNConfigSet)
 *         ReportContextSensitivity(recognizer Parser, dfa *DFA, startIndex, stopIndex, prediction int, configs ATNConfigSet)
 *     }
 *
 */

type errorListener struct {
	*antlr.DefaultErrorListener
	rdsppParser *RdsppParser
}

func (listener *errorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{}, line, column int, msg string, e antlr.RecognitionException) {
	listener.rdsppParser.allErrorPos = append(listener.rdsppParser.allErrorPos, column)
	
	if listener.rdsppParser.Error != nil {
		return
	}

	offendingToken := offendingSymbol.(antlr.Token)

	err := new(ErrorInformation)
	err.Index = offendingToken.GetStart()
	err.Designation = listener.rdsppParser.designation
	err.Message = msg
	err.Text = offendingToken.GetText()

	/*
		possible msg values:

		(1) "no viable alternative at input 'TOKEN'"						=> antlr.NoViableAltException
		(2) "mismatches input 'TOKEN' expecting {'TOKEN', 'TOKEN', ...}"	=> no exception
		(3) "rule TOKEN_NAME MESSAGE"										=> antlr.FailedPredicateException
		(4) "extraneous input 'TOKEN' expecting TOKEN_NAME"					=> no exception
		(5) "missing TOKEN_NAME at 'TOKEN'"									=> no exception
	*/

	extractChars := func(strWithBraces string) []string {
		ret := make([]string, 0)
		if strings.HasPrefix(strWithBraces, "{") && strings.HasSuffix(strWithBraces, "}") {
			strWithBraces = strings.Trim(strWithBraces, "{}")
			for _, e := range strings.Split(strWithBraces, ", ") {
				if strings.HasPrefix(e, "'") && strings.HasSuffix(e, "'") {
					e = strings.Trim(e, "'")
				}
				ret = append(ret, e)
			}
		} else {
			if strings.HasPrefix(strWithBraces, "'") && strings.HasSuffix(strWithBraces, "'") {
				strWithBraces = strings.Trim(strWithBraces, "'")
			}
			ret = append(ret, strWithBraces)
		}
		for _, e := range ret {
			if e == "NUMBER" {
				err.Suggestions = append(err.Suggestions, "<NUMBER>")
			} else if e == "<EOF>" {
				err.Suggestions = append(err.Suggestions, "<EOF>")
			} else {
				err.Suggestions = append(err.Suggestions, e)
			}
		}
		return ret
	}

	if strings.HasPrefix(msg, "no viable alternative") {

		err.Type = 0
		err.Suggestions = make([]string, 4)
		err.Suggestions[0] = "<NUMBER>" // 0 ... 9
		err.Suggestions[1] = "<ALPHA>"  // A ... Z
		err.Suggestions[2] = "<PREFIX>" // #, =, +, -, :, ;, &
		err.Suggestions[3] = "<EOF>"    // EOF

	} else if strings.HasPrefix(msg, "mismatched input") ||
		strings.HasPrefix(msg, "extraneous input") {

		if strings.HasPrefix(msg, "mismatched input") {
			err.Type = 1
		} else {
			err.Type = 2
		}
		expecting := msg[strings.Index(msg, "expecting ")+10:]
		extractChars(expecting)

	} else if strings.HasPrefix(msg, "missing") {

		err.Type = 3
		missing := msg[strings.Index(msg, "missing ")+8 : strings.Index(msg, " at ")]
		if missing == "NUMBER" {
			err.Suggestions = append(err.Suggestions, "<NUMBER>")
		} else if missing == "<EOF>" {
			err.Suggestions = append(err.Suggestions, "<EOF>")
		} else {
			extractChars(missing)
		}

	} else if strings.HasPrefix(msg, "rule") {
		/*
			not possible because there are no semantic predicates in the grammar
		*/
	} else {
		/*
			unrecognized exception type => can't really do anything in this case
		*/
	}

	suggestions := make([]string, 0)
	alphas := make([]string, 0)
	for _, s := range err.Suggestions {
		if isAlpha(s) {
			alphas = append(alphas, s)
			continue
		}
		suggestions = append(suggestions, s)
	}
	if len(alphas) > 0 {
		suggestions = append(suggestions, sortAndAppendAlphas(alphas))
	}
	err.Suggestions = suggestions

	listener.rdsppParser.Error = err
	parser := recognizer.(antlr.Parser)
	listener.rdsppParser.addHint(parser.GetParserRuleContext().GetRuleContext(), column)
}

func (rdsppParser *RdsppParser) addHint(ctx antlr.RuleContext, column int) {
	if rdsppParser.Hint != nil {
		return
	}

	ruleIndex := ctx.GetRuleContext().GetRuleIndex()
	hintIndex := -1
	switch ruleIndex {
	case parser.RDSPPParserRULE_conjointDesignation:
		hintIndex = 0
	case parser.RDSPPParserRULE_function:
		hintIndex = 1
	case parser.RDSPPParserRULE_productClass:
		hintIndex = 2
	case parser.RDSPPParserRULE_operatingEquipment:
		hintIndex = 3
	case parser.RDSPPParserRULE_pointOfInstallation:
		hintIndex = 4
	case parser.RDSPPParserRULE_siteOfInstallation:
		hintIndex = 5
	case parser.RDSPPParserRULE_terminal:
		hintIndex = 6
	case parser.RDSPPParserRULE_signal:
		hintIndex = 7
	case parser.RDSPPParserRULE_document:
		hintIndex = 8
	case parser.RDSPPParserRULE_start:
		// In this case the to level rule "start" is reached. If this happens, none of the specific
		// designation rules was entered before the error occured. But to be absolutely certain this is correct
		// we search for a designation prefix manually in order to make sure no hints are missing.
		for i := column - 1; i > -1; i-- {
			c := rdsppParser.designation[i : i+1]
			switch c {
			case "#":
				hintIndex = 0 // conjoint desingation
			case "=":
				hintIndex = 1 // function
				for k := column; k < len(rdsppParser.designation); k++ {
					if rdsppParser.designation[k:k+1] == "-" {
						hintIndex = 3 // operating equipment
						break
					}
				}
			case "-":
				hintIndex = 2 // product class
			case "+":
				hintIndex = 4 // point of installation
				for k := column; k < len(rdsppParser.designation); k++ {
					if rdsppParser.designation[k:k+1] == "+" {
						hintIndex = 5 // site of installation
						break
					}
				}
			case ":":
				hintIndex = 6 // terminal
			case ";":
				hintIndex = 7 // signal
			case "&":
				hintIndex = 8 // document
			default:
			}

			if hintIndex > -1 {
				break
			}
		}
	default:
		if ctx.GetParent() != nil {
			rdsppParser.addHint(ctx.GetParent().(antlr.RuleContext), column)
		}
	}

	if hintIndex > -1 {
		rdsppParser.Hint = &denomination.Hints.Hints[hintIndex]
	}
}
