package geocoding

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/jasonwinn/geocoder"
	"rdspp.de/src/crosscutting/config"
	"rdspp.de/src/crosscutting/logger"
)

var httpClient = &http.Client{Timeout: 10 * time.Second}

// GetAddress returns the address and/or country pointed to by the provided
// latitude (lat) and longitude (lng).
func GetAddress(lat float64, lng float64) string {
	address, err := geocoder.ReverseGeocode(lat, lng)
	if err != nil {
		logger.Error(err.Error())
		return ""
	}
	addressStr := address.City
	if address.State != "" {
		if addressStr != "" {
			addressStr += ", "
		}
		addressStr += address.State
	}
	if address.CountryCode != "" {
		if addressStr != "" {
			addressStr += " "
		}
		addressStr += "(" + address.CountryCode + ")"
	}
	return addressStr
}

// GetCountryCode_ISO3166_1 returns the 2-digit country code according to ISO 3166-1.
// It does so by using the provided latitude (lat) and longitude (lon) for reverse geocoding.
func GetCountryCode_ISO3166_1(lat float64, lng float64) (string, error) {
	// api doc: https://www.geonames.org/export/web-services.html#countrycode
	api := "http://api.geonames.org/countryCode"
	api += "?lat=" + fmt.Sprintf("%f", lat)
	api += "&lng=" + fmt.Sprintf("%f", lng)
	api += "&radius=0" // km
	api += "&username=" + config.Api.Geonames.Username
	response, err := httpClient.Get(api)
	if err != nil || response.StatusCode != http.StatusOK {
		logger.Error("API request failed: " + api + " [" + err.Error() + "]")
		return "", fmt.Errorf("API request failed")
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	code := string(body)
	code = strings.Trim(code, " \"\r\n")
	l := len(code)
	if l != 2 || strings.HasPrefix(code, "ERR") {
		return "", fmt.Errorf("no country found")
	}
	return code, nil
}

// auto generated from example json response
type regionResponse struct {
	Codes []struct {
		Code  string `json:"code"`
		Level string `json:"level"`
		Type  string `json:"type"`
	} `json:"codes"`
	AdminCode1  string `json:"adminCode1"`
	Distance    int    `json:"distance"`
	CountryCode string `json:"countryCode"`
	CountryName string `json:"countryName"`
	AdminName1  string `json:"adminName1"`
}

// GetCountryCode_ISO3166_1 returns the 1- to 3-digit region code according to ISO 3166-2.
// It does so by using the provided latitude (lat) and longitude (lon) for reverse geocoding.
func GetRegionCode_ISO3166_2(lat float64, lng float64) (string, error) {
	// api doc: https://www.geonames.org/export/web-services.html#countrysubdiv
	api := "http://api.geonames.org/countrySubdivisionJSON"
	api += "?lat=" + fmt.Sprintf("%f", lat)
	api += "&lng=" + fmt.Sprintf("%f", lng)
	api += "&maxRows=1"
	api += "&radius=0" // km
	api += "&username=" + config.Api.Geonames.Username
	response, err := httpClient.Get(api)
	if err != nil || response.StatusCode != http.StatusOK {
		logger.Error("API request failed: " + api + " [" + err.Error() + "]")
		return "", fmt.Errorf("API request failed")
	}
	defer response.Body.Close()
	jsonData := new(regionResponse)
	err = json.NewDecoder(response.Body).Decode(jsonData)
	if err != nil || len(jsonData.Codes) == 0 {
		return "", fmt.Errorf("API request: invalid json result")
	}
	code := jsonData.Codes[0].Code
	code = strings.Trim(code, " \"\r\n")
	l := len(code)
	if l == 0 || l > 3 {
		return "", fmt.Errorf("invalid country found")
	}
	return code, nil
}
