package main

import (
	"flag"

	"rdspp.de/src/api"
	"rdspp.de/src/crosscutting/config"
	"rdspp.de/src/crosscutting/dependencies"
	"rdspp.de/src/crosscutting/logger"
	"rdspp.de/src/logic/assistant"
	"rdspp.de/src/logic/designation/denomination"
)

func main() {
	// parse command line args
	isDebug := flag.Bool("debug", false, "flag inication debug mode. has multiple effect, e.g. higher verbosity level")
	flag.Parse()

	// init modules
	config.Init(*isDebug)
	logger.Init()
	if (*isDebug) {
		logger.SetLogOutputEnabled(true, true)
	}
	denomination.Init()
	assistant.Init()
	dependencies.SetAllApiKeys()
	api.Init() // <= must be last, because it is blocking
}
