module rdspp.de/prototype

go 1.13

require (
	github.com/antlr/antlr4 v0.0.0-20191031194250-3fcb6da1f690
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/leesper/couchdb-golang v1.2.1 // indirect
	github.com/segmentio/pointer v0.0.0-20160608003038-39719d2ea756 // indirect
	github.com/zemirco/couchdb v0.0.0-20170316052722-83ed906ea1f0 // indirect
	github.com/zemirco/uid v0.0.0-20160129141151-3763f3c45832 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
