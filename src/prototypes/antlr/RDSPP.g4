grammar RDSPP;

// PARSER RULES => AST

start: designation EOF;

designation:
	uniqueDesignation
	| conjointDesignation
	| function
	| function document
	| function signal
	| operatingEquipment
	| operatingEquipment document
	| operatingEquipment terminal
	| productClass
	| pointOfInstallation
	| pointOfInstallation document
	| siteOfInstallation
	| siteOfInstallation document
	| terminal
	| signal
	| document;

uniqueDesignation:
	conjointDesignation document
	| conjointDesignation function
	| conjointDesignation function document
	| conjointDesignation function signal
	| conjointDesignation operatingEquipment
	| conjointDesignation operatingEquipment document
	| conjointDesignation operatingEquipment terminal
	| conjointDesignation pointOfInstallation
	| conjointDesignation pointOfInstallation document
	| conjointDesignation siteOfInstallation
	| conjointDesignation siteOfInstallation document;








counter2: NUM NUM;
counter3: NUM NUM NUM;
counterI: NUM+;







conjointDesignation:
	HASH geographicLocation DOT projectSpecificInformation
	| HASH geographicLocation
	| HASH projectSpecificInformation;

geographicLocation:
	latitude (N | S) longitude (E | W);

latitude: NUM NUM NUM NUM;
longitude: NUM NUM NUM NUM NUM;

projectSpecificInformation: country region DOT townOrName counter2u plantType facilityType;

country: (alpha | I | O) (alpha | I | O);
region: (alpha | I | O | UNDERSCORE) (alpha | I | O | UNDERSCORE) (alpha | I | O);
townOrName: (alpha | I | O) (alpha | I | O) (alpha | I | O);
counter2u: (NUM | UNDERSCORE) NUM;
plantType: alpha;
facilityType: alpha;








function:
	EQUALS mainSystem? systemSubsystemPart basicFunction
	| EQUALS mainSystem? systemSubsystemPart
	| EQUALS mainSystem;

mainSystem: alpha counter3;

systemSubsystem: alpha alpha alpha counter2;

systemSubsystemPart:
	systemSubsystem
	| alpha alpha alpha
	| alpha alpha
	| alpha;

basicFunction: alpha alpha counter3;








operatingEquipment: EQUALS mainSystem? systemSubsystem basicFunction productClass;








productClass: MINUS (productClassCode counter3)+;

productClassCode: alpha alpha;








pointOfInstallation: PLUS mainSystem systemSubsystem basicFunction? (DOT pointOfInstallationPlace)?;

pointOfInstallationPlace: column row;

column: alpha alpha;

row: NUM NUM NUM;








siteOfInstallation:
	PLUSPLUS mainSystem systemSubsystem DOT rrrrr
	| PLUSPLUS mainSystem systemSubsystem DOT geographicLocation;

rrrrr: alpha+ NUM+;








terminal: COLON ((alpha | NUM)+ DOT)? (alpha | NUM)+;

signal: SEMICOLON xxxxx UNDERSCORE yyyy;

xxxxx: alpha alpha NUM NUM NUM;

yyyy: alpha alpha NUM NUM;

document: AMPERSAND dddddd (SLASH seitenzahl)?;

dddddd: alpha alpha alpha NUM NUM NUM;

seitenzahl: NUM+;

alpha:
	A
	| B
	| C
	| D
	| E
	| F
	| G
	| H
	| J
	| K
	| L
	| M
	| N
	| P
	| Q
	| R
	| S
	| T
	| U
	| V
	| W
	| X
	| Y
	| Z;

// LEXER RULES => TOKENS

A: 'A';
B: 'B';
C: 'C';
D: 'D';
E: 'E';
F: 'F';
G: 'G';
H: 'H';
I: 'I';
J: 'J';
K: 'K';
L: 'L';
M: 'M';
N: 'N';
O: 'O';
P: 'P';
Q: 'Q';
R: 'R';
S: 'S';
T: 'T';
U: 'U';
V: 'V';
W: 'W';
X: 'X';
Y: 'Y';
Z: 'Z';

NUM: '0'..'9';

DOT: '.';
HASH: '#';
EQUALS: '=';
PLUS: '+';
PLUSPLUS: '++';
MINUS: '-';
COLON: ':';
SEMICOLON: ';';
AMPERSAND: '&';
SLASH: '/';
UNDERSCORE: '_';

WHITESPACE: [ \r\n\t]+ -> skip;
