package main

//https://godoc.org/github.com/zemirco/couchdb
import (
	"github.com/zemirco/couchdb"
		"fmt"
	"net/url")

func add(x int, y int, z int)int{
	return x + y + z
}
func main(){
	fmt.Println(add(42, 13, 11))

	u, err := url.Parse("http://127.0.0.1:5800/")
	if err != nil{
		panic(err)
	}
	//login als Admin
	client, err := couchdb.NewAuthClient("Raphael", "RDS-PP", u)
	if err != nil{
		panic(err)
	}
	//Status von CouchDB abfragen
	info, err := client.Info()
	if err != nil{
		panic(err)
	}
	fmt.Println(info)
	//Datenbank "dummy" wird erzeugt (schmeißt aber immer einen error dass die Datenbank schon existiert, deshalb panic auskommentiert)
	_, err = client.Create("dummy")
	if err != nil {
		//panic(err)
	}
	//Variable die auf die "dummy"-Datenbank verweist
	db := client.Use("dummy")
	//Testdokument anlegen
	doc := &dummyDocument{
		Foo:  "bar",
		Beep: "bopp",
	}
	//dieses Testdokument über den DB-Verweis zu "dummy" in die Datenbank laden
	result, err := db.Post(doc)
	if err != nil{
		panic(err)
	}
	fmt.Println(result)
	fmt.Println(result.ID)
	//id und akutelle revision holen
	if err := db.Get(doc, result.ID); err != nil {
		panic(err)
	}
	//gibt security Dokumente zurück
	test, err := db.GetSecurity()
	if err != nil{
		panic(err)
	}
	fmt.Println(test)
	//Das erstellte Testdokument aus der DB löschen
	test1, err := db.Delete(doc)
	if err != nil {
		panic(err)
	}
	fmt.Println(test1)
	//die komplette DB löschen
	if _,err := client.Delete("dummy"); err != nil{
		panic(err)
	}
}
type dummyDocument struct {
	couchdb.Document
	Foo  string `json:"foo"`
	Beep string `json:"beep"`
}
