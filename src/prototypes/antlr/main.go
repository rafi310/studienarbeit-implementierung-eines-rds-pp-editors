package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"rdspp.de/prototype/antlr/parser"
)

type TreeShapeListener struct {
	*parser.BaseRDSPPListener
}

func NewTreeShapeListener() *TreeShapeListener {
	return new(TreeShapeListener)
}
func (this *TreeShapeListener) EnterStart(ctx *parser.StartContext) {
	recognizer := ctx.GetParser()
	lispTree := ctx.ToStringTree(recognizer.GetRuleNames(), recognizer)
	fmt.Println(indentLispTree(lispTree))
}
func (this *TreeShapeListener) EnterEveryRule(ctx antlr.ParserRuleContext) {
	// fmt.Printf("N -> %#v [child-count: %#v]\n", ctx.GetText(), strconv.Itoa(ctx.GetChildCount()))
}
func (this *TreeShapeListener) VisitTerminal(node antlr.TerminalNode) {
	// fmt.Printf("T -> %#v\n", node.GetText())
}

func indentLispTree(lisp string) string {
	const indent = 4
	bracketsCounter := 0
	precedingWhitespace := false
	addedTerminalMark := false
	tree := ""
	lastSubstr := ""
	specialLiterals := []string{"(", ")", " "}
	for i := 0; i < len(lisp); i++ {
		substr := string(lisp[i])
		if !isIn(substr, specialLiterals) {
			for !isIn(string(lisp[i+1]), specialLiterals) {
				substr += string(lisp[i+1])
				i++
			}
		}
		switch substr {
		case "(":
			precedingWhitespace = false
			addedTerminalMark = false
			bracketsCounter++
		case ")":
			if !addedTerminalMark {
				tree += fmt.Sprintf("  %s> %s", strings.Repeat("─", 40-bracketsCounter*indent-len(lastSubstr)-2), lastSubstr)
				addedTerminalMark = true
			}
			bracketsCounter--
		case " ":
			tree += fmt.Sprintf("\n%s", strings.Repeat(" ", bracketsCounter*indent))
			precedingWhitespace = true
			addedTerminalMark = false
		default:
			tree += substr
			if precedingWhitespace && !addedTerminalMark && string(lisp[i+1]) == " " {
				tree += fmt.Sprintf("  %s> %s", strings.Repeat("─", 40-bracketsCounter*indent-len(lastSubstr)-2), substr)
				precedingWhitespace = false
				addedTerminalMark = true
			}
		}
		lastSubstr = substr
	}
	return tree
}

func isIn(s string, array []string) bool {
	for i := 0; i < len(array); i++ {
		if array[i] == s {
			return true
		}
	}
	return false
}

/* [ RDS-PP examples ]

#5047N00496W.GBCON.BEA_1WN
=G001 MDA30 GP001
=G001 MDA30 GP001 -MA001
=G001 MDA30 GP001 -MA001:L1
=G001 MDA3O&MFB010

*/

func main() {
	// check args
	if len(os.Args) < 2 {
		fmt.Println("\nno enough args! usage:")
		fmt.Println()
		fmt.Println("'go run main.go \"<RDS-PP-KENNZEICHEN>\"', e.g.")
		fmt.Println("'go run main.go \"=G001 MDA30 GP001 —MA001:L1\"'")
		fmt.Println()
		os.Exit(1)
	}
	kennzeichen := os.Args[1]
	separator := strings.Repeat("=", 64)
	
	for i := 0; i < 20; i++ {
		fmt.Printf("\n%s\n%s\n%s\n\n", separator, "KENNZEICHEN", separator)
		fmt.Println(kennzeichen)

		// lexer
		is := antlr.NewInputStream(kennzeichen)
		lexer := parser.NewRDSPPLexer(is)
		fmt.Printf("\n%s\n%s\n%s\n\n", separator, "LIST OF TOKENS", separator)
		for {
			t := lexer.NextToken()
			if t.GetTokenType() == antlr.TokenEOF {
				break
			}
			fmt.Printf("%s ─> %s\n", t.GetText(), lexer.SymbolicNames[t.GetTokenType()])
		}

		// parser
		is = antlr.NewInputStream(kennzeichen)
		lexer = parser.NewRDSPPLexer(is)
		stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
		parser := parser.NewRDSPPParser(stream)
		fmt.Printf("\n%s\n%s\n%s\n\n", separator, "ABSTRACT SYNTAX TREE (AST)", separator)
		antlr.ParseTreeWalkerDefault.Walk(NewTreeShapeListener(), parser.Start())
	}
}
