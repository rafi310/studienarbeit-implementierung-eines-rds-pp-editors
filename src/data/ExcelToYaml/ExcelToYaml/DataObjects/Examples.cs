﻿namespace ExcelToYaml.DataObjects
{
    public class Examples
    {
        public string[] De { get; set; }
        public string[] En { get; set; }

        public Examples(string[] de, string[] en)
        {
            this.De = de;
            this.En = en;
        }
    }
}
