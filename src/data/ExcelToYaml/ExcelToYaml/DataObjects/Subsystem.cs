﻿using System.Collections.Generic;

namespace ExcelToYaml.DataObjects
{
    public class Subsystem
    {
        public string Code { get; set; }
        public Denomination Denomination { get; set; }
        public List<BasicFunction> BasicFunctions { get; set; }

        public Subsystem()
            : this("", new Denomination())
        {
        }

        public Subsystem(string code, Denomination denomination)
            : this(code, denomination, new List<BasicFunction>())
        {
        }

        public Subsystem(string code, Denomination denomination, List<BasicFunction> basicFunctions)
        {
            this.Code = code;
            this.Denomination = denomination;
            this.BasicFunctions = basicFunctions;
        }
    }
}
