﻿namespace ExcelToYaml.DataObjects
{
    public class Denomination
    {
        private string _de;
        private string _en;

        public string De
        {
            get => this._de;
            set => this._de = value.Trim();
        }

        public string En
        {
            get => this._en;
            set => this._en = value.Trim();
        }

        public Denomination()
            :this("", "")
        {
        }
        public Denomination(string de, string en)
        {
            this.De = de;
            this.En = en;
        }
    }
}
