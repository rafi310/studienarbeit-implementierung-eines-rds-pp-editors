﻿namespace ExcelToYaml.DataObjects
{
    public class MainSystem
    {
        public string Code { get; set; }
        public Subsystem[] Subsystems { get; set; }

        public MainSystem(string code, Subsystem[] subsystems)
        {
            this.Code = code;
            this.Subsystems = subsystems;
        }
    }
}
