﻿namespace ExcelToYaml.DataObjects
{
    public class Product
    {
        public string Code { get; set; }
        public Denomination Denomination { get; set; }

        public Product()
            : this("", new Denomination())
        {
        }
        public Product(string code, Denomination denomination)
        {
            this.Code = code;
            this.Denomination = denomination;
        }
    }
}
