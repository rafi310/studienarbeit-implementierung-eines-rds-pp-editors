﻿using System.Collections.Generic;

namespace ExcelToYaml.DataObjects
{
    public class BasicFunctionA1
    {
        public string Code { get; set; }
        public Denomination Denomination { get; set; }
        public BasicFunctionA2[] BasicFunctions { get; set; }

        public BasicFunctionA1(string code, Denomination denomination, BasicFunctionA2[] basicFunctions)
        {
            this.Code = code;
            this.Denomination = denomination;
            this.BasicFunctions = basicFunctions;
        }
    }

    public class BasicFunctionA2
    {
        public string Code { get; set; }
        public Denomination Denomination { get; set; }
        public Examples Examples { get; set; }

        public BasicFunctionA2(string code, Denomination denomination, Examples examples)
        {
            this.Code = code;
            this.Denomination = denomination;
            this.Examples = examples;
        }
    }

    public class BasicFunction
    {
        public string Code { get; set; }
        public Denomination Denomination { get; set; }
        public IList<Product> Products { get; set; }

        public BasicFunction()
            : this("", new Denomination(), new List<Product>())
        {
        }

        public BasicFunction(string code, Denomination denomination)
            : this(code, denomination, new List<Product>())
        {
        }
        public BasicFunction(string code, Denomination denomination, IList<Product> products)
        {
            this.Code = code;
            this.Denomination = denomination;
            this.Products = products;
        }
    }
}
