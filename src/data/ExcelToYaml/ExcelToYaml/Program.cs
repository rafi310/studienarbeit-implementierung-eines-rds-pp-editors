﻿using System;
using System.IO;

// ReSharper disable StringLiteralTypo

namespace ExcelToYaml
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Console.SetWindowSize(220, 50);

            ExcelToYamlConverter converter = new ExcelToYamlConverter(
                Path.GetFullPath("RDS-PP_nach_Ringhand.xlsx"),
                Path.GetFullPath("annex3_pages1-3.txt"),
                Path.GetFullPath("annex3_pages4-44.txt")
            );
            converter.Convert();

            Console.Write("\npress enter to exit ...");
            Console.ReadLine();
        }
    }
}
