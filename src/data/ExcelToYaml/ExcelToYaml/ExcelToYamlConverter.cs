﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ExcelToYaml.DataObjects;
using Microsoft.Office.Interop.Excel;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

// ReSharper disable CommentTypo

namespace ExcelToYaml
{
    public class ExcelToYamlConverter
    {
        #region Properties
        // ReSharper disable InconsistentNaming

        // excel file
        private readonly string _xlsxPath;
        private Application _xlsxApplication;
        private Workbook _xlsxWorkbook;

        // annex 3 files
        private string _annex3_1_to_3;
        private readonly string _annex3_1_to_3_path;
        private string _annex3_4_to_44;
        private readonly string _annex3_4_to_44_path;

        // yaml file paths
        // ReSharper disable once InconsistentNaming
        private const string _outDir = "yaml_out";
        private readonly string _basicFunctionGeneralYaml = Path.Combine(_outDir, "basic_function_general.yaml");
        private readonly string _basicFunctionProductYaml = Path.Combine(_outDir, "basic_function_product.yaml");

        // serializer
        private readonly ISerializer _serializer;

        // ReSharper restore InconsistentNaming
        #endregion

        #region Constructor

        public ExcelToYamlConverter(string xlsxPath, string annex3Path1To3, string annex3Path4To44)
        {
            this._xlsxPath = xlsxPath;
            this._annex3_1_to_3_path = annex3Path1To3;
            this._annex3_4_to_44_path = annex3Path4To44;

            this._serializer = new SerializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .DisableAliases()
                .Build();
        }

        #endregion

        #region Public Methods

        public void Convert()
        {
            Console.WriteLine("working directory:              " + AppContext.BaseDirectory + "\n");

            Console.WriteLine("start converting Excel file:    " + Path.GetFileName(this._xlsxPath));
            this.ConvertXlsx();
            Console.WriteLine(" -> done\n");

            Console.WriteLine("start converting Annex-3:       " + Path.GetFileName(this._annex3_1_to_3_path) + " and " + Path.GetFileName(this._annex3_4_to_44_path));
            this.ConvertAnnex3();
            Console.WriteLine(" -> done\n");

            Console.WriteLine("output directory:               " + _outDir);
        }

        #endregion

        #region Private Methods

        private void ConvertXlsx()
        {
            /*
             * IMPORTANT: Excel indexes are 1-based and not 0-based
             */
            try
            {
                this._xlsxApplication = new Application();
                this._xlsxWorkbook = this._xlsxApplication.Workbooks.Open(this._xlsxPath);

                if (!Directory.Exists(_outDir))
                {
                    Directory.CreateDirectory(_outDir);
                }

                List<BasicFunctionA1> basicFunctionA1List = this.ProcessBasicFunctionAndProductCodes(this._xlsxWorkbook.Sheets[4]);

                if (File.Exists(this._basicFunctionGeneralYaml))
                    File.Delete(this._basicFunctionGeneralYaml);
                TextWriter writer = new StreamWriter(new FileStream(this._basicFunctionGeneralYaml, FileMode.OpenOrCreate));
                this._serializer.Serialize(writer, new
                {
                    basicFunctions = basicFunctionA1List
                });
                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("--------------------------------------------------------------------------------");
                Console.WriteLine(e.StackTrace);
                // ignored
            }
            finally
            {
                this._xlsxWorkbook?.Close(false);
                this._xlsxApplication?.Workbooks.Close();
                this._xlsxApplication?.Quit();
            }
        }

        private void ConvertAnnex3()
        {
            try
            {
                this._annex3_1_to_3 = File.ReadAllText(this._annex3_1_to_3_path);
                this._annex3_4_to_44 = File.ReadAllText(this._annex3_4_to_44_path);

                List<Subsystem> subsystems = this.ProcessAnnex3A(); // pages 1 to 3
                subsystems = this.ProcessAnnex3B(subsystems); // pages 4 to 44

                if (File.Exists(this._basicFunctionProductYaml))
                    File.Delete(this._basicFunctionProductYaml);
                TextWriter writer = new StreamWriter(new FileStream(this._basicFunctionProductYaml, FileMode.OpenOrCreate));
                this._serializer.Serialize(writer, new
                {
                    MainSystems = new[]
                    {
                        new
                        {
                            Code = "G",
                            Subsystems = subsystems
                        }
                    }
                });
                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("--------------------------------------------------------------------------------");
                Console.WriteLine(e.StackTrace);
                // ignored
            }
        }

        private List<BasicFunctionA1> ProcessBasicFunctionAndProductCodes(Worksheet sheet)
        {
            int rowCount = sheet.UsedRange.Rows.Count;
            Range cells = sheet.UsedRange.Cells;

            List<BasicFunctionA1> basicFunctionA1List = new List<BasicFunctionA1>();
            List<BasicFunctionA2> basicFunctionA2List = new List<BasicFunctionA2>();
            string codeA1 = cells[3, 2].Text.Trim(); // B3
            string codeA2 = cells[3, 4].Text.Trim(); // D3
            codeA2 = codeA2.Substring(1);
            Denomination denominationA1 = new Denomination(cells[3, 1].Text.Trim(), cells[3, 1].Text.Trim());
            Denomination denominationA2 = new Denomination(cells[3, 3].Text.Trim(), cells[3, 3].Text.Trim());
            Examples examples = new Examples(new string[] { }, new string[] { });

            for (int row = 3; row <= rowCount; row++)
            {
                string cA1 = cells[row, 2].Text.Trim(); // B_
                if (cA1 != "" && cA1 != codeA1)
                {
                    basicFunctionA1List.Add(new BasicFunctionA1(codeA1, denominationA1, basicFunctionA2List.ToArray()));
                    codeA1 = cA1;
                    denominationA1 = new Denomination(cells[row, 1].Text.Trim(), cells[row, 1].Text.Trim()); // de: A_, en: A_
                    basicFunctionA2List.Clear();
                }

                Denomination dA2 = new Denomination(cells[row, 3].Text.Trim(), cells[row, 3].Text.Trim()); // de: C_, en: C_
                string cA2 = cells[row, 4].Text.Trim();
                cA2 = cA2.Substring(1);
                if (cA2 != "" && cA2 != codeA2 &&
                    dA2.De != "" && dA2.En != "" && dA2.De != denominationA2.De && dA2.En != denominationA2.En)
                {
                    codeA2 = cA2; // D_
                    denominationA2 = dA2;
                    string examplesString = cells[row, 5].Text.Trim(); // E_
                    string[] examplesArray = examplesString.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int e = 0; e < examplesArray.Length; e++)
                        examplesArray[e] = examplesArray[e].Trim();
                    examples = new Examples(examplesArray, examplesArray);
                }
                basicFunctionA2List.Add(new BasicFunctionA2(codeA2, denominationA2, examples));
            }
            basicFunctionA1List.Add(new BasicFunctionA1(codeA1, denominationA1, basicFunctionA2List.ToArray()));

            return basicFunctionA1List;
        }

        private List<Subsystem> ProcessAnnex3A()
        {
            List<Subsystem> subsystems = new List<Subsystem>();

            string[] lines = this._annex3_1_to_3.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
            {
                string[] parts = Regex.Split(line, @"\s{4,}").Where(s => s.Trim() != string.Empty).ToArray();
                if (parts.Length != 3) continue;

                string code = parts[0].Substring(1);
                string en = parts[1];
                string de = parts[2];

                if (code.Length < 5) continue;

                if (code.EndsWith("n")) // one digit counter e.g. MDA1n
                {
                    for (int n = 1; n < 10; n++)
                    {
                        subsystems.Add(
                            new Subsystem(
                                code.Substring(0, code.Length - 1) + n,
                                new Denomination(
                                    de.Replace("<n>", n.ToString()),
                                    en.Replace("<n>", n.ToString())
                                )
                            )
                        );
                    }
                }
                else if (Regex.IsMatch(code, @"m\[\d+;\d+\]")) // counter range e.g. MDAm[12;28]
                {
                    Match match = Regex.Match(code, @"m\[(\d+);(\d+)\]");
                    int lower = int.Parse(match.Groups[1].Value);
                    int upper = int.Parse(match.Groups[2].Value);
                    Match offsetMatch = Regex.Match(de, @"<m\[(\+|-)?(\d+)\]>");
                    int sign = offsetMatch.Groups[1].Value == "+" ? 1 : -1;
                    int offset = int.Parse(offsetMatch.Groups[2].Value);
                    for (int m = lower; m <= upper; m++)
                    {
                        subsystems.Add(
                            new Subsystem(
                                Regex.Replace(code, @"m\[\d+;\d+\]", m.ToString("D" + upper.ToString().Length)),
                                new Denomination(
                                    Regex.Replace(de, @"<m\[(\+|-)?\d+\]>", (m + sign * offset).ToString()),
                                    Regex.Replace(en, @"<m\[(\+|-)?\d+\]>", (m + sign * offset).ToString())
                                )
                            )
                        );
                    }
                }
                else // default; no counter
                {
                    subsystems.Add(new Subsystem(code, new Denomination(de, en)));
                }
            }

            return subsystems;
        }

        private List<Subsystem> ProcessAnnex3B(List<Subsystem> subsystems)
        {
            if (subsystems == null || subsystems.Count == 0) return subsystems;

            string[] lines = this._annex3_4_to_44.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            Subsystem subsystem = null;
            BasicFunction basicFunction = new BasicFunction();
            Product product = new Product();

            Regex sRegex = new Regex(@"^=(\w{5}) {4,}(([^\s]+ {0,2})+) {4,}(([^\s]+ {0,2})+$)");
            Regex sfpRegex = new Regex(@"^=(\w+) +(\w+) +-(\w+) {4,}(([^\s]+ {0,2})+) {4,}(([^\s]+ {0,2})+)$");
            Regex pRegex = new Regex(@"^-(\w+) {4,}(([^\s]+ {0,2})+) {4,}(([^\s]+ {0,2})+)$");
            Regex sfRegex = new Regex(@"^=(\w+) +(\w+) {4,}(([^\s]+ {0,2})+) {4,}(([^\s]+ {0,2})+)");

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Trim();

                int nParts = Regex.Split(line, @"\s{4,}").Where(s => s.Trim() != string.Empty).ToArray().Length;

                if (nParts < 3)
                {
                    // <de> <en>
                    // only in case the denomination of the previous line has a line break
                    continue;
                }

                int matchId = -1;
                if (nParts == 3 && sRegex.IsMatch(line))
                {
                    matchId = 0;
                    // =<subsystem> <de> <en>
                    if (subsystem != null)
                    {
                        foreach (Subsystem s in subsystems)
                        {
                            if (s.Code != subsystem.Code) continue;
                            s.BasicFunctions = subsystem.BasicFunctions;
                            break;
                        }
                    }
                    Match match = sRegex.Match(line);
                    string code = match.Groups[1].Value.Trim();
                    string de = match.Groups[4].Value.Trim();
                    string en = match.Groups[2].Value.Trim();
                    subsystem = new Subsystem(code, new Denomination(de, en));
                }
                else if (sfpRegex.IsMatch(line))
                {
                    matchId = 1;
                    // =<subsystem> <basic-function> -<product> <de> <en>
                    Match match = sfpRegex.Match(line);
                    string code = match.Groups[3].Value.Trim();
                    string de = match.Groups[6].Value.Trim();
                    string en = match.Groups[4].Value.Trim();
                    product = new Product(code, new Denomination(de, en));
                    basicFunction.Products.Add(product);
                }
                else if (pRegex.IsMatch(line))
                {
                    matchId = 2;
                    // -<product> <de> <en>
                    Match match = pRegex.Match(line);
                    string code = match.Groups[1].Value.Trim();
                    string de = match.Groups[4].Value.Trim();
                    string en = match.Groups[2].Value.Trim();
                    product = new Product(code, new Denomination(de, en));
                    basicFunction.Products.Add(product);
                }
                else if (sfRegex.IsMatch(line))
                {
                    matchId = 3;
                    // =<subsystem> <basic-function> <de> <en>
                    Match match = sfRegex.Match(line);
                    string code = match.Groups[2].Value.Trim();
                    string de = match.Groups[5].Value.Trim();
                    string en = match.Groups[3].Value.Trim();
                    if (basicFunction.Code != code)
                    {
                        basicFunction = new BasicFunction(code, new Denomination(de, en));
                        subsystem.BasicFunctions.Add(basicFunction);
                    }
                }

                // check if the next 3 lines contain overhanging text that belongs to the denomination of the current designation
                // if so: simply append it
                for (int k = i + 1; k < i + 4 && k < lines.Length; k++)
                {
                    string next = lines[k];
                    string[] parts = Regex.Split(next, @"\s{4,}").Where(s => s.Trim() != string.Empty).ToArray();
                    if (parts.Length > 2) break;
                    if (parts.Length == 2)
                    {
                        switch (matchId)
                        {
                            case 0:
                                subsystem.Denomination.De += (subsystem.Denomination.De.EndsWith("-") ? "" : ", ") + parts[1];
                                subsystem.Denomination.En += (subsystem.Denomination.En.EndsWith("-") ? "" : ", ") + parts[0];
                                break;
                            case 1:
                            case 2:
                                product.Denomination.De += (product.Denomination.De.EndsWith("-") ? "" : ", ") + parts[1];
                                product.Denomination.En += (product.Denomination.En.EndsWith("-") ? "" : ", ") + parts[0];
                                break;
                            case 3:
                                basicFunction.Denomination.De += (basicFunction.Denomination.De.EndsWith("-") ? "" : ", ") + parts[1];
                                basicFunction.Denomination.En += (basicFunction.Denomination.En.EndsWith("-") ? "" : ", ") + parts[0];
                                break;
                        }
                    }
                    else if (parts.Length == 1)
                    {
                        int nBlanks = next.TakeWhile(char.IsWhiteSpace).Count();
                        next = next.Trim();
                        if (nBlanks < 100)
                        {
                            switch (matchId)
                            {
                                case 0:
                                    subsystem.Denomination.En += (subsystem.Denomination.En.EndsWith("-") ? "" : ", ") + next;
                                    break;
                                case 1:
                                case 2:
                                    product.Denomination.En += (product.Denomination.En.EndsWith("-") ? "" : ", ") + next;
                                    break;
                                case 3:
                                    basicFunction.Denomination.En += (basicFunction.Denomination.En.EndsWith("-") ? "" : ", ") + next;
                                    break;
                            }
                        }
                        else
                        {
                            switch (matchId)
                            {
                                case 0:
                                    subsystem.Denomination.De += (subsystem.Denomination.De.EndsWith("-") ? "" : ", ") + next;
                                    break;
                                case 1:
                                case 2:
                                    product.Denomination.De += (product.Denomination.De.EndsWith("-") ? "" : ", ") + next;
                                    break;
                                case 3:
                                    basicFunction.Denomination.De += (basicFunction.Denomination.De.EndsWith("-") ? "" : ", ") + next;
                                    break;
                            }
                        }
                    }
                }
            }

            return subsystems;
        }

        #endregion
    }
}
