#!/bin/bash

# variables
UML_DIR=5_uml/out

# remove *.tex files in output dir
# rm -rf $UML_DIR/*.tex
rm -rf $UML_DIR/**/*.tex

# rename *.latex to *.tex
find . -name "*.latex" -exec bash -c 'mv "$1" "${1%.latex}".tex' - '{}' \;

# flatten directory
find $UML_DIR/ -iname "*.*" -exec cp \{\} $UML_DIR/ \;
rm -r $UML_DIR/*/

# crop transparent borders with ImageMagick
find $UML_DIR/ -iname "*.png" -exec magick convert -trim "{}" "{}" \;
