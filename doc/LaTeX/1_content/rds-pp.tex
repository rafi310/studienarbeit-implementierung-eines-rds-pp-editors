\chapter{RDS-PP}
\label{cpt:rds-pp}

Da das Thema dieser Studienarbeit sich um den Kennzeichnungsstandard \acrfull{RDS-PP} dreht, soll dieser im folgenden Kapitel näher erläutert werden. Die Grundlagen zu \gls{RDS-PP} wurden in ein separates Kapitel ausgelagert, da diese essentiell für das Verständnis der Arbeit sind und daher umfangreicher behandelt werden. Alle weiteren theoretischen Grundlagen folgen im nächsten Kapitel.

\section{Allgemeines}

Eine Anlagenkennzeichnung wie sie z.B. mithilfe von \gls{RDS-PP} erzielt wird, ist wie eine Sprache. Diese Sprache bildet die Basis für eine einheitliche Kommunikation zwischen allen an Planung, Bau und Betrieb der Anlage beteiligten Akteure. Um dies zu erreichen ist \gls{RDS-PP} herstellerneutral, d.h. individuelle Ausprägungen oder Dialekte sind quasi nicht möglich. Damit diese Einschränkung nicht zur Behinderung beim Verwenden der Kennzeichnung wird, wurde beim Verfassen des Standards besonderer Wert auf die vollständige Definition von Kennzeichen für möglichst alle Komponentenarten gelegt.

Neben der einheitlichen Kommunikation spielt aber auch die Informationsverwaltung bei der Kennzeichnung eine Rolle. So können über das Kennzeichen beispielsweise Dokumente oder technische Anlagendaten einer speziellen Komponente zugeordnet werden.

\gls{RDS-PP} ist ein Standard speziell für die Kennzeichnung von Energieerzeugungsanlagen. Insbesondere in der Windenergiebranche hat sich das Kennzeichensystem seit ca. 15 Jahren international etabliert, weshalb der Fokus dieser Arbeit auch auf der Kennzeichnung von Windkrafträdern liegt. \cite{vgb:heft:rdspp}

Entwickelt werden die Standards und Anwendungsrichtlinien für \gls{RDS-PP} von einem Arbeitskreis des VGB PowerTech e.V., einem internationalen Interessenverbund von Unternehmen der Energieanlagen-Branche. \cite{vgb:website}

Vorgänger von \gls{RDS-PP} ist das ebenfalls vom VGB entwickelte \gls{KKS}, welches seit Beginn der 70er Jahre weltweit erfolgreich eingesetzt wird. Mit dem Aufkommen neuer internationaler Normen für die Kennzeichnung und der Zitierung dieser Normen in europäischen Richtlinien und harmonisierten Normen ergab sich die Notwendigkeit, das \gls{KKS} entsprechend anzupassen und zu erweitern. Dies wurde vom Arbeitskreis \enquote{Anlagenkennzeichnung und Dokumentation} des VGBs erarbeitet und führte schließlich zur Veröffentlichung von \gls{RDS-PP}. \cite{vgb:heft:uebergang_vom_kks}

\begin{figure}[h]
	\centering
	\includegraphics[width=.9\linewidth]{rds-pp-normen}
	\caption[Zusammenhänge der Kennzeichnungs-Normen und Richtlinien für RDS-PP]{Zusammenhänge der Kennzeichnungs-Normen und Richtlinien für RDS-PP (Stand der Norm-Bezeichnungen: 2014)}
	\label{img:rdspp-normen-uebersicht-1}
\end{figure}

Die wichtigsten Normen, die im Zusammenhang mit \gls{RDS-PP} eine Rolle spielen, sind in der Übersicht in \autoref{img:rdspp-normen-uebersicht-1} dargestellt. Die Normen der Reihe IEC/ISO 81346 beschreiben die allgemeine Kennzeichnung von Industrieanlagen und dienen als Grundnormen für \gls{RDS-PP}. Das darauf basierende Kennzeichenkonzept wird allgemein als \gls{RDS} bezeichnet und ist prinzipiell für alle Arten von Industrieanlagen anwendbar. Erst die Norm DIN ISO/TS 81346-10, welche von der DIN ISO/TS 16952-10 übernommen wurde und diese abgelöst hat, spezialisiert sich auf Kraftwerksanlagen. Diese stellt die normative Grundlage für \gls{RDS-PP} dar. Zusätzlich zu dieser Fachnorm, die für alle Kraftwerkstypen gilt, gibt es vom VGB noch ein spezielles Regelwerk, welches in \autoref{img:rdspp-normen-uebersicht-2} dargestellt ist. Dieses Regelwerk umfasst die Definitionen von Kennbuchstaben sowie mehrere Anwendungsrichtlinien (eine allgemeine und vier Kraftwerks-spezifische). Von diesen beschreibt die Richtlinie VGB-S-823-32 \cite{norm:vgb_s_823_32_2014_03_en_de} die für Windkraftanlagen spezifische Anwendung von \gls{RDS-PP}. Diese Richtlinie dient daher als maßgebliche Grundlage für diese Studienarbeit. \cite{vgb:heft:rdspp, norm:vgb_s_823_32_2014_03_en_de, beuth:website:din-iso-ts-81346-10}

\begin{figure}[h]
	\includegraphics[width=\linewidth]{rds-pp-uebersicht}
	\caption{Übersicht über das RDS-PP Regelwerk}
	\label{img:rdspp-normen-uebersicht-2}
\end{figure}

\section{Grundlagen der Kennzeichnung nach RDS-PP}

In diesem Abschnitt sollen alle grundlegenden Prinzipien und Regeln vorgestellt werden, die für die Anwendung von \gls{RDS-PP} wichtig sind. Die Informationen hierzu wurden hauptsächlich der VGB Anwendungsrichtlinie für Windkraftanlagen (VGB-S-823-32-2014-03-EN-DE) entnommen.

\subsection{Begriffe}

Es sollen zunächst ein paar häufig verwendete Begriffe definiert werden \cite{norm:vgb_s_823_32_2014_03_en_de}:

\begin{itemize}
	\item \textbf{System}: Gesamtheit miteinander in Beziehung stehender Objekte, die in einem Zusammenhang als Ganzes gesehen und als von ihrer Umgebung abgegrenzt betrachtet werden.
	\item \textbf{Objekt}: Betrachtungseinheit, die in einem Prozess behandelt wird. Ein Objekt kann unter verschiedenen Aspekten betrachtet werden und hat ihm zugeordnete Informationen.
	\item \textbf{Aspekt}: Spezifische Betrachtungsweise eines Objekts.
	\item \textbf{Referenzkennzeichen}: Eindeutiger Identifikator eines spezifischen Objekts in einem System, der einen oder mehrere Aspekte des Objekts in Bezug auf das Systems beschreibt.
	\item \textbf{Funktion}: Geplanter oder vollendeter Zweck oder Aufgabe.
	\item \textbf{Betriebsmittel}: Ein Produkt, das alleine oder zusammen mit anderen Produkten zur Realisierung einer technischen Aufgabe verwendet wird.
	\item \textbf{Baueinheit}: Betrachtungseinheit, die durch ihren Aufbau von anderen Einheiten abgegrenzt wird. Eine Baueinheit kann eine oder mehrere Funktionseinheiten enthalten.
	\item \textbf{Produkt}: Geplantes oder fertiges Ergebnis einer Arbeit oder eines Prozesses.
\end{itemize}

\subsection{Ziel der Kennzeichnung}

Das Hauptziel einer Kennzeichnung ist es, einzelne Teile einer Anlage eindeutig identifizieren zu können. Die Kennzeichnung ist dabei wie eine Sprache, die eine unmissverständliche Kommunikation aller an Entwicklung, Bau und Betrieb der Anlage beteiligten Personen erlaubt. Hierzu muss ein Kennzeichen über den ganzen Lebenslauf einer Anlage gültig sein und optimalerweise branchenweit eine einheitliche Bedeutung haben. \cite{norm:vgb_s_823_32_2014_03_en_de}

\subsection{Umfang der Kennzeichnung}

Mit \gls{RDS-PP} können fast alle Teile und Aspekte eines Kraftwerks gekennzeichnet werden. Welche genau das sind, wird in \autoref{sec:kennzeichenarten} genauer erläutert. Da \gls{RDS-PP} branchenweit ausgerichtet ist, sind keine hersteller- oder betreiberspezifischen Kennzeichnungen (z.B. Produkttypen, Arbeitsaufträge, Kostenstellen, etc.) vorgesehen. \cite{norm:vgb_s_823_32_2014_03_en_de}

\subsection{Prinzipien der Kennzeichnung}
\label{sec:rdspp:grundlagen:prinzipien-der-kennzeichnung}

\gls{RDS-PP} orientiert sich an fünf Grundprinzipien, die bei der Kennzeichnung zu beachten sind \cite{norm:vgb_s_823_32_2014_03_en_de, vgb:heft:rdspp}:

\begin{enumerate}
	
	\item \textbf{Hierarchische Kennzeichnung \enquote{vom Großen zum Kleinen}}\\
	Die Kennzeichnung erfolgt in einer hierarchischen Struktur, sodass zwischen Objekten verschiedener Hierarchiestufen eine Bestandsbeziehung (\enquote{X ist Bestandteil von Y}) besteht. Dadurch erkennt man am Kennzeichen nicht nur welches Objekt gemeint ist, sondern auch in welcher hierarchischen Ebene es sich befindet. So kann man am Kennzeichen eines Motors beispielsweise erkennen, zu welcher Funktion, welchem Teilsystem, welchem System und welcher Windkraftanlage er gehört (vgl. \autoref{img:rdspp:hierarchie}).
	
	\begin{minipage}{\linewidth}
		\centering
		\includegraphics[width=.75\textwidth]{rdspp_prinzipien_hierarchie}
		\captionof{figure}[Hierarchieprinzip der Kennzeichnung mit RDS-PP]{Hierarchieprinzip der Kennzeichnung mit RDS-PP \cite{vgb:heft:rdspp}}
		\label{img:rdspp:hierarchie}
	\end{minipage}
	
	\item \textbf{Ein Objekt hat verschiedene Aspekte}
	
	\begin{minipage}{\linewidth}
		\centering
		\includegraphics[width=.75\linewidth]{rdspp_prinzipien_aspekte}
		\captionof{figure}[Die drei Aspekte der Kennzeichnung mit RDS-PP]{Die drei Aspekte der Kennzeichnung mit RDS-PP \cite{vgb:heft:rdspp}}
		\label{img:rdspp:aspekte}
	\end{minipage}

	Man kann ein Objekt aus drei verschiedenen Aspekten betrachten, wie in \autoref{img:rdspp:aspekte} zu sehen ist. Die Aspekte sind Folgende:
	\begin{itemize}
		\item Funktionsaspekt: \textit{Was macht ein Objekt? Welche Aufgabe hat es?}
		\item Produktaspekt: \textit{Wie ist das Objekt zusammengesetzt? Welche Komponenten?}
		\item Ortsaspekt: \textit{Wo befindet sich das Objekt?}
	\end{itemize}
	Der betrachtete Aspekt muss aus dem Kennzeichen hervorgehen. Hierzu verwendet \gls{RDS-PP} unterschiedliche Vorzeichen ($=$, $+$, $-$, usw.) bei der Kennzeichnung. Diese Vorzeichen werden in \autoref{sec:kennzeichenarten} näher erläutert.
	
	\item \textbf{Der Funktionsaspekt kann zum Produktaspekt übergehen}\\
	Der von einem (Teil-)System zu erledigenden Aufgabe (= Funktionsaspekt) können Produkte zugeordnet werden, die diese Aufgabe erfüllen. Daher kann von einem Funktionsaspekt auf einen Produktaspekt \enquote{übergegangen} werden. Beispiel: Die Funktion \enquote{Öl fördern} wird durch die Produktklasse \enquote{Pumpe} erfüllt.
	
	\item \textbf{Objekte mit ähnlichen Merkmalen werden in Klassen gebündelt}\\
	Objekte, die ähnliche Aufgaben erfüllen -- also ähnliche Grundfunktionen haben -- werden in Klassen zusammengefasst. Eine Basis für diese Klassifizierung liefert die allgemeine Norm DIN EN 81346-2, welche speziell für Kraftwerke durch die Richtlinie VGB B102 ergänzt wird.
	
	\item \textbf{Jedes System muss für sich funktionsfähig sein}\\
	Jedes System muss alle zur Erfüllung seiner Aufgabe erforderlichen Grundfunktionen und Produkte beinhalten. Dieses Prinzip ist für die Definition von Systemgrenzen hilfreich.
	
\end{enumerate}

\subsection{Grundregeln für den Kennzeichenaufbau}
\label{sec:rdspp:grundregeln}

Zum Kennzeichenaufbau gibt der VGB in seiner Anwendungsrichtlinie folgende acht Grundregeln fest, die sich aus der internationalen Normreihe DIN EN 81346 ableiten \cite{norm:vgb_s_823_32_2014_03_en_de}:

\textbf{Regel 1: Kennzeichenaufbau}\\
Jedes Kennzeichen setzt sich aus einem Vorzeichen zur Aspektidentifizierung und einem anschließenden Code aus Buchstaben und Ziffern zusammen (s. \autoref{img:rdspp:regeln:kennzeichenaufbau}).

\begin{figure}[h]
	\centering
	\includegraphics[width=.75\linewidth]{rdspp_regeln_kennzeichenaufbau}
	\caption{Kennzeichenaufbau}
	\label{img:rdspp:regeln:kennzeichenaufbau}
\end{figure}

\textbf{Regel 2: Kennzeichenabschnitt}\\
Jeder Kennzeichenabschnitt ist abwechselnd alphabetisch (A) und numerisch (N) aufgebaut und besteht aus maximal vier Datenstellen (s. \autoref{img:rdspp:regeln:kennzeichenabschnitt}).

\begin{figure}[h]
	\centering
	\includegraphics[width=.75\linewidth]{rdspp_regeln_kennzeichenabschnitt}
	\caption{Kennzeichenabschnitt}
	\label{img:rdspp:regeln:kennzeichenabschnitt}
\end{figure}

\textbf{Regel 3: Verkürzung des Kennzeichenblocks}\\
Jeder Kennzeichenblock kann von rechts und/oder links gekürzt werden. Die erlaubten Kürzungen sind von der Kennzeichenart abhängig (s. \ref{sec:kennzeichenarten}). Numerische Stellen dürfen nicht gekürzt werden.

\textbf{Regel 4: Vorzeichen}\\
Das Vorzeichen ist Bestandteil des Kennzeichens und darf nur weggelassen werden, wenn eine Verwechslung ohne Vorzeichen ausgeschlossen ist.

\textbf{Regel 5: Kombination von Kennzeichenblöcken}\\
Werden Kennzeichenblöcke miteinander kombiniert, bestimmt das Vorzeichen des am weitesten rechts stehenden Blocks den Aspekt des Gesamtkennzeichens.

\textbf{Regel 6: Alphabetische Datenstellen}\\
Alphabetische Datenstellen dürfen nur aus lateinischen Großbuchstaben ohne \enquote{I} und \enquote{O} verwendet werden. Die einzige Ausnahme stellt der Kennzeichenblock \enquote{Gemeinsame Zuordnung} dar. Hier dürfen (teilweise) \enquote{I}, \enquote{O} sowie \enquote{\_} (Unterstrich) verwendet werden.

\textbf{Regel 7: Numerische Datenstellen}\\
Für Numerische Datenstellen werden arabische Ziffern (0-9) verwendet. Diese Datenstellen werden verwendet, um zwischen Objekten mit gleichen Kennbuchstaben zu unterscheiden. Es wird dabei eine Zählsystematik mit folgenden Grundsätzen verwendet:
\begin{itemize}
	\item Bei Wechsel eines vorausgehenden Kennzeichenteils beginnt die Zählung von vorne (bei 0).
	\item Die Zählung kann fortlaufen (01, 02, 03, ...) oder gruppierend (10, 11, ..., 20, 21, ...) erfolgen.
	\item 00 sollte als Zählnummer vermieden werden.
	\item Die Zählung kann lückenhaft sein.
	\item Nummern ab 90 sind freigehalten, sodass eine vorläufige Kennzeichnung von Prototypen oder Testsystemen ermöglicht wird.
	\item Einmal festgelegte Zählnummern sind fix und sollten auch bei Planungsänderungen o.ä. nicht geändert werden.
\end{itemize}

\textbf{Regel 8: Benennung}\\
Benennungen erfolgen nach der Richtlinie VGB-B 108. Für in der Windindustrie etablierte Begriffe gibt es Ausnahmen.

\section{Kennzeichenarten}
\label{sec:kennzeichenarten}

Nachdem die allgemeinen Grundlagen für die Kennzeichnung im vorigen Kapitel geklärt wurde, kann nun auf die genauen Kennzeichnungsarten eingegangen werden.

Prinzipiell werden Kennzeichen durch verschiedene Kennzeichnungsaufgaben unterschieden. Die Unterscheidung der Aufgabe erfolgt dabei durch das Vorzeichen. Alle nach \gls{RDS-PP} möglichen Kennzeichenarten werden nun im folgenden vorgestellt und erläutert.

\subsection{Gemeinsame Zuordnung (\#)}
\label{sec:rdspp:gemeinsame_zuordnung}

Die Gemeinsame Zuordnung mit dem Vorzeichen \enquote{\#} stellt die oberste Strukturebene dar. Sie ist das Referenzkennzeichen einer Gesamtanlage, zu der neben den eigentlichen Windkraftanlagen auch Anlagen wie ein Kommunikationsnetz oder ein Umspannwerk gehören. Alle Objekte der Gesamtanlage besitzen eigene, untergeordnete Kennzeichen, denen die Gemeinsame Zuordnung vorangestellt wird.

Es erhalten alle Windkraftwerke eine Gemeinsame Zuordnung, egal ob es sich um ein einzelnes Windrad mit Anschluss an das örtliche Stromnetz oder einen großen Windpark mit eigener Infrastruktur handelt. Infrastrukturobjekte wie das Umspannwerk können ggf. eine eigene Gemeinsame Zuordnung erhalten, falls diese für mehrere Windkraftanlagen zuständig sind.

Da die Gemeinsame Zuordnung ein Kraftwerk eindeutig kennzeichnen soll, muss es global einmalig sein. Um diese Anforderung ohne zentrale Vergabestelle erfüllen zu können, ist eine wesentlicher Teil der Gemeinsamen Zuordnung eine geographische Position auf Basis eines Koordinatensystems. Der genaue Kennzeichenaufbau ist in \autoref{img:rdspp:gemeinsame-zuordnung} zu sehen.

\begin{figure}
	\centering
	\includegraphics[width=.8\linewidth]{rdspp_gemeinsame_zuordnung}
	\caption[Aufbau des Kennzeichens \enquote{Gemeinsame Zuordnung}]{Aufbau des Kennzeichens \enquote{Gemeinsame Zuordnung} \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:gemeinsame-zuordnung}
\end{figure}

Die erste Gliederungsstufe (BL1 in \autoref{img:rdspp:gemeinsame-zuordnung}) enthält die geografische Position auf Basis des internationalen Koordinatensystems \gls{WGS84}. Dieses weit verbreitete System wird beispielsweise auch von Google Maps verwendet. Koordinaten werden hierbei als geographische Längen- und Breitengrade in der dezimalen Schreibweise und nicht im Sexagesimalformat angegeben (also z.B. \textit{48.445264°N, 8.696817°E} statt \textit{48°26'43.0"N 8°41'48.5"{}E}). Das Dezimalzeichen wird hierbei jedoch nicht geschrieben, um eine Verwechslung mit dem Trennzeichen für Gliederungsstufen (\enquote{.}) zu vermeiden. Stattdessen ist per Norm festgelegt, dass der Längengrad 2 Vor- und 2 Nachkommastellen sowie der Längengrad 3 Vor- und 2 Nachkommastellen hat. Die Position einer Windkraftanlage kann dadurch auf bis zu 1,11km genau bestimmt werden \cite{wiki:openstreetmap:genauigkeit}. Beispiel: Die Gliederungsstufe 1 einer Windkraftanlage in Horb a.N. mit den Koordinaten 48,44°N 8,69°E sähe folgendermaßen aus: 4844N00869E.

Die Gliederungsstufe 2 besteht aus länderspezifischen Codes und projekt- bzw. anlagenspezifischen Informationen. Die ersten beiden Zeichen sind für das Länderkennungszeichen nach ISO 3166-1 (z.B. DE für Deutschland), gefolgt drei Zeichen für die Region nach ISO-3166-2 (z.B. BW für Baden-Württemberg). Nicht verwendete Stellen werden durch \enquote{\_} (Unterstrich) gefüllt (z.B. \_BW). Für Offshoreanlagen wird die Kennung der Region verwendet, in die das Seekabel anlandet. Offshoreanlagen, die an ein sog. \enquote{Supergrid} ohne unmittelbaren Bezug zu einer Region werden mit den Buchstaben \enquote{EEZ} gekennzeichnet. Der Regionalcode wird durch einen Punkt von weiteren projektspezifischen Zeichen getrennt: Drei frei wählbare alphabetische Stellen als Kürzel für die Anlagenbezeichnung und zwei numerische Stellen für einen Zähler. Die letzten beiden Stellen repräsentieren die Art der Anlage bzw. der Infrastruktureinheit und sind wieder durch die Norm festgelegt und nicht frei wählbar.

\subsection{Funktionsaspekt}

\subsubsection{Funktion (=)}
\label{sec:rdspp:funktion}

Die Funktion mit dem Vorzeichen \enquote{=} dient zur Kennzeichnung technischer Objekte nach ihrem Zweck, ohne dabei zu berücksichtigen, welches Produkt diese Funktion erfüllt bzw. wo dieses eingebaut ist.

Der Kennzeichenblock besteht aus drei Gliederungsstufen, die von links nach rechts immer kleinere Betrachtungseinheiten darstellen (s. \autoref{img:rdspp:funktion}).

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.75\linewidth]{rdspp_funktion}
	\caption[Aufbau des Kennzeichens \enquote{Funktion}]{Aufbau des Kennzeichens \enquote{Funktion} \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:funktion}
\end{figure}

Jede Gliederungsstufe hat dabei einen alphabetischen Teil zur Klassifizierung und einen numerischen Teil zur Zählung. Die erste Gliederungsstufe unterscheidet zwischen Hauptsystemen (= Zusammenfassung mehrerer Systeme). In der zweiten Gliederungsstufe wird die erste alphabetische Stelle zur Kennzeichnung des Systems und die zweite zur Unterteilung eines Systems in Teilsysteme verwendet. Die dritte Gliederungsstufe kennzeichnet die Grundfunktionen innerhalb eines (Teil-) Systems. Hierzu gibt es zwei alphabetische Stellen zu Unterteilung der Grundfunktionen in Klassen und Unterklassen sowie drei numerische Stellen zur Zählung.

Die genauen Buchstaben-Zahlen-Kombinationen sind vom \gls{VGB} genaustens festgelegt und erlauben kaum eigene Festlegungen.

\subsubsection{Funktionale Zuordnung (==)}

Die Funktionale Zuordnung mit dem Vorzeichen \enquote{==} dient zur Kennzeichnung von in sich geschlossenen Teilprozessen. Da die Funktionale Zuordnung derzeit (noch) keine Anwendung in der Windenergie findet, soll diese im Rahmen dieser Studienarbeit auch nicht berücksichtigt werden.

\subsection{Produktaspekt}

\subsubsection{Produktklasse (--)}

Die Produktklasse mit dem Vorzeichen \enquote{--} dient der Kennzeichnung des Produktaspekts (z.B. eine Pumpe), ohne dabei die genau Funktion oder den Einbauort dieses Produkts zu spezifizieren. Die Produktklasse berücksichtigt dabei nicht, von welchem Hersteller das spezifische Produkt bezogen wird.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.7\linewidth]{rdspp_produkt}
	\caption[Aufbau des Kennzeichens \enquote{Produkt}]{Aufbau des Kennzeichens \enquote{Produkt} \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:produkt}
\end{figure}

Der Kennzeichenaufbau ist in \autoref{img:rdspp:produkt} dargestellt. Das Kennzeichen besteht aus mindestens einer Gliederungsstufe mit zwei alphabetischen zur Klassifizierung und -- in der Windenergie üblichen -- drei numerischen Datenstellen zur Zählung. Bei Bedarf können zusätzliche gleichartige Gliederungsstufen angefügt werden, um eine weitere \enquote{besteht aus}-Unterteilung zu erreichen. In den meisten Anwendungsfällen reicht allerdings eine Gliederungsstufe völlig aus.

Der Produktaspekt wird nicht alleine zur eindeutigen Kennzeichnung von technischen Objekten verwendet, sondern immer als Teil eines Betriebsmittelkennzeichens. Die Gliederungsstufe 3 des Funktionskennzeichens ist dabei optional.

\subsubsection{Betriebsmittel (=--)}

Das Betriebsmittel dient der eindeutigen Kennzeichnung von technischen Objekten. Dieses erhält man durch das Verketten von Funktions- und Produktklassenkennzeichen.

\subsection{Ortsaspekt}

Der Ortsaspekt eines Objektes ist definiert als Platz, den dieses Objekt für andere Objekte bereitstellt. Der Ortsaspekt wird dabei allerdings diesen anderen Objekten als Einbau- bzw. Aufstellungsortkennzeichen zugeordnet und nicht dem Objekt, das den Platz zur Verfügung stellt, selbst.

\subsubsection{Einbauort (+)}

Der Einbauort mit dem Vorzeichen \enquote{+} wird gekennzeichnet, in welchen Einbaueinheiten (z.B. Schränke, Gehäuse) ein Betriebsmittel eingebaut ist. Das Kennzeichen wird dabei dem Betriebsmittel zugeordnet. Das Einbauortkennzeichen wird aus dem Funktionskennzeichen des Objektes gebildet, in das eingebaut wird. \autoref{img:rdspp:funktion_zu_einbauort} verdeutlicht dies.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\linewidth]{rdspp_funktion_zu_einbauort}
	\caption[Bildung des Einbaukennzeichens auf Basis des Funktionskennzeichens]{Bildung des Einbaukennzeichens auf Basis des Funktionskennzeichens \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:funktion_zu_einbauort}
\end{figure}

Der aus dem Funktionskennzeichen übernommene Teil wird als Einbaueinheit bezeichnet. Erweitert wird das übernommene Funktionskennzeichen dabei um die Gliederungsstufe Einbauplatz, welche den genauen Platz des Objektes im Einbauraum kennzeichnet. Die Gliederungsstufe Einbauplatz ist dabei so aufgebaut, dass die zwei alphabetischen Datenstellen zur vertikalen in Zeilen und die drei numerischen Stellen zur horizontalen Unterteilung in Spalten dienen.

Je nach Anwendungsfall können die Gliederungsstufen Einbauraum und/oder Einbauplatz entfallen. Dies ist vor allem dann der Fall, wenn ein Betriebsmittel die Einbaueinheit für ein anderes Betriebsmittel darstellt und keine Einbauplätze für andere Objekte bietet, wodurch eine weitere Unterteilung nach dem Einbauplatz keinen Sinn macht. Optional kann dann auch das Produktkennzeichen der Einbaueinheit als Einbauplatz verwendet werden.

\subsubsection{Aufstellungsort (++)}

Der Aufstellungsort mit dem Vorzeichen \enquote{++} ist ähnlich dem Einbauortkennzeichen: Es wird einem Betriebsmittel zugeordnet und wird aus den ersten beiden Gliederungsstufen des Funktionskennzeichens abgeleitet (vgl. \autoref{img:rdspp:funktion_zu_aufstellungsort}).

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\linewidth]{rdspp_funktion_zu_aufstellungsort}
	\caption[Bildung des Aufstellungsortskennzeichens auf Basis des Funktionskennzeichens]{Bildung des Aufstellungsortskennzeichens auf Basis des Funktionskennzeichens \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:funktion_zu_aufstellungsort}
\end{figure}

Der Unterschied ist, dass der Aufstellungsort makroskopischer ist. Statt beispielsweise dem Einbauort in einem Schaltschrank werden Räume in Gebäuden oder Abschnitte auf Flächen angegeben. Die Raum- bzw. Flächenabschnittsangabe erfolgt in der dritten Gliederungsstufe. Für keine der Angaben werden vom VGB verpflichtende Vorgaben gemacht. Es werden lediglich folgende Empfehlungen gemacht:

\begin{itemize}
	\item Für Raumkennzeichnungen erlaubt die Norm DIN ISO/TS 16952-10 beliebige maximal 12-stellige alphanumerische Kennzeichen. Der VGB empfiehlt 5-stellige Raumkennzeichen nach folgendem Schema:\\
	\begin{minipage}{\linewidth}
		\centering
		\begin{tikzpicture}
		\node (n1) {N};
		\node (n2) [right=0mm of n1] {N};
		\node (a1) [right=0mm of n2] {A};
		\node (a2) [right=0mm of a1] {A};
		\node (a3) [right=0mm of a2] {A};
		\draw (n1.240) -- (n2.300);
		\draw (a1.240) -- (a3.300);
		\node (textN) [below left=0mm and 5mm of n1] {Stockwerk Buchstabe};
		\node (textA) [below left=5mm and 5mm of n1] {Raum Nummer};
		\draw (n1.south east) |- (textN.east);
		\draw (a2.south) |- (textA.east);
		\end{tikzpicture}
	\end{minipage}
	\item Für die Aufteilung von Flächen wird ein Rasterplan empfohlen, bei dem die Fläche zeilenweise mit Buchstaben und spaltenweise mit Nummern unterteilt wird. Alternativ ist auch eine Verwendung des Koordinatensystems \gls{WGS84} wie in der Gemeinsamen Zuordnung denkbar. Hierbei wäre allerdings fünf Nachkommastellen für Längen- und Breitengrade erforderlich, um eine metergenaue Positionsbestimmung zu ermöglichen \cite{wiki:openstreetmap:genauigkeit}.
\end{itemize}

\subsection{Spezifische Kennzeichen}

\subsubsection{Signale (;)}
\label{sec:rdspp:signal}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\linewidth]{rdspp_signal}
	\caption[Aufbau des Kennzeichens \enquote{Signal}]{Aufbau des Kennzeichens \enquote{Signal} \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:signal}
\end{figure}

Signale als Informationsträger werden mit dem Vorzeichen \enquote{;} gekennzeichnet. Dabei wird nicht der Signalweg, sondern lediglich der Signalursprung gekennzeichnet. Eine eindeutige Signalkennzeichnung erfolgt mit vorangestelltem Funktionskennzeichen.

Der Aufbau des Kennzeichens ist in \autoref{img:rdspp:signal} zu sehen. Die  klassifizierende Gliederungsstufe 1 wird durch die Norm DIN ISO/TS 16952-10 festgelegt. Die Gliederungsstufe 2 dient dazu, zusätzliche Informationen wie Signaldauer und Signaltyp festzuhalten.

\subsubsection{Anschlüsse (:)}

Anschlüsse mit dem Vorzeichen \enquote{:} stellen die Schnittstelle zwischen Betriebsmitteln dar. Eine eindeutige Anschlusskennzeichnung ist daher auch nur in Kombination mit vorangehendem Betriebsmittelkennzeichen möglich. Anschlüsse können dabei sowohl elektrisch (z.B. Energiekabel, Datenkabel) oder mechanisch (z.B. Flanschverbindung) sein.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\linewidth]{rdspp_anschluss}
	\caption[Aufbau des Kennzeichens \enquote{Anschluss}]{Aufbau des Kennzeichens \enquote{Anschluss} \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:anshluss}
\end{figure}

\autoref{img:rdspp:anshluss} zeigt den allgemeinen Aufbau des Anschlusskennzeichens. Es besteht aus zwei Gliederungsstufen, wobei die erste Gliederungsstufe, die dazu genutzt werden kann, mehrere Anschlüsse zusammenzufassen, optional ist. Bei beiden Gliederungsstufen gibt es keine Festlegung bezüglich der Anzahl alphabetischer und numerischer Datenstellen, da diese von der Anschlusskennzeichnung auf dem Bauteil abhängig und damit herstellerspezifisch sind. Sollte es vom Hersteller diesbezüglich keine Angaben geben, ist die Anschlusskennzeichnung gemäß DIN EN 61666 durchzuführen.

\subsubsection{Dokumente (\&)}

Dokumente haben das Vorzeichen \enquote{\&} und werden gemäß der Richtlinie VGB-S-832-00 (ehemals VGB-B 103) gekennzeichnet. Um ein Dokument eindeutig zu identifizieren, muss dem Dokumentenkennzeichen ein Objektkennzeichen vorangehen. Diese Objektkennzeichen können entweder auch dem \gls{RDS-PP} Standard entstammen (z.B. Funktionskennzeichen) oder einen anderen Ursprung haben (z.B. Materialnummer, Projektpläne, etc.).

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\linewidth]{rdspp_dokumente}
	\caption[Aufbau des Kennzeichens \enquote{Dokumente}]{Aufbau des Kennzeichens \enquote{Dokumente} \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:dokumente}
\end{figure}

Das Dokumentenkennzeichen ist wie in \autoref{img:rdspp:dokumente} zu sehen in zwei Gliederungsstufen unterteilt. Die erste Gliederungsstufe kodiert nach einem sogenannten Dokumentenklassenschlüssel (DCC), der auf der internationalen Norm DIN EN 61355-1 basiert und zusätzlich Ergänzungen des VGB enthält, was dokumentiert wird. Die zweite Gliederungsstufe beginnt mit einem Schrägstrich (\enquote{/}) und wird mit der referenzierten Seitenzahl fortgesetzt.

\section{Kombination von Kennzeichenblöcken}

Je nach Anwendungsfall dürfen die im vorigen Abschnitt beschriebenen Kennzeichenblöcke alleinstehend oder kombiniert angewendet werden. Es wird dabei zwischen zwei Arten der Kombination unterschieden: Verkettung und Zuordnung.

\subsection{Verkettung}
\label{sec:rdspp:verkettung}

Durch die Verkettung von zwei oder mehr Kennzeichenblöcken erhält man ein eindeutiges Kennzeichen. Alle möglichen Verkettungen sind in \autoref{table:rdspp:verkettung} dargestellt.

\begin{table}[ht]
	\centering
	\begin{tabular}{llll}
		\toprule
		\# Gemeinsame Zuordnung	& \& Dokument		& 					& 				\\
		\# Gemeinsame Zuordnung	& = Funktion		& 					& 				\\
		\# Gemeinsame Zuordnung	& = Funktion		& \& Dokument		& 				\\
		\# Gemeinsame Zuordnung	& = Funktion		& ; Signalname		& 				\\
		\# Gemeinsame Zuordnung	& = Funktion		& - Produktklasse	& 				\\
		\# Gemeinsame Zuordnung	& = Funktion		& - Produktklasse	& \& Dokument	\\
		\# Gemeinsame Zuordnung	& = Funktion		& - Produktklasse	& : Anschluss	\\
		\# Gemeinsame Zuordnung	& == Funktionale Z.	& 					& 				\\
		\# Gemeinsame Zuordnung	& == Funktionale Z.	& \& Dokument		& 				\\
		\# Gemeinsame Zuordnung	& == Funktionale Z.	& ; Signalname		& 				\\
		\# Gemeinsame Zuordnung	& + Einbauort		& 					& 				\\
		\# Gemeinsame Zuordnung	& + Einbauort		& \& Dokument		& 				\\
		\# Gemeinsame Zuordnung	& ++ Aufstellungort	& 					& 				\\
		\# Gemeinsame Zuordnung	& ++ Aufstellungort	& \& Dokument		& 				\\
		\bottomrule
	\end{tabular}
	\caption[Zulässige Verkettung von Kennzeichenblöcken]{Zulässige Verkettung von Kennzeichenblöcken \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{table:rdspp:verkettung}
\end{table}

\subsection{Zuordnung}

Eine Zuordnung ist die Verknüpfung zweier Kennzeichen unterschiedlicher Aspekte (Funktion, Produkt, Ort). Solch eine Zuordnung ist nicht-hierarchisch und wird auch als Rollenrelation bezeichnet. Zwischen welchen Kennzeichen eine solche Rollenrelation bestehen kann, ist in \autoref{img:rdspp:relationen} zu sehen (gestrichelte Pfeile).

\begin{figure}[ht]
	\includegraphics[width=\linewidth]{rdspp_relationen}
	\caption[Beziehungen zwischen verschiedenen RDS-PP Kennzeichen]{Beziehungen zwischen verschiedenen RDS-PP Kennzeichen \cite{norm:vgb_s_823_32_2014_03_en_de}}
	\label{img:rdspp:relationen}
\end{figure}
