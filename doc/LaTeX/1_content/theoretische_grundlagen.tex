\chapter{Theoretische Grundlagen}
\label{cpt:theoretische_grundlagen}

In diesem Kapitel wird ein kurzer Überblick über allgemeine Themen gegeben, die dem technischen Verständnis dieser Arbeit dienen soll. Dabei wurden folgende Themen ausgewählt:

\begin{itemize}
	\item \textbf{\nameref{sec:theoretische_grundlagen:parser}}: Für die Hauptfunktion \textit{\hauptfunktionParser} wird ein Parser benötigt.
	\item \textbf{\nameref{sec:theoretische_grundlagen:datenvisualisierung}}: Die Anwendung soll \gls{RDS-PP}-Anwender bestmöglich unterstützen, wobei auch eine visuelle Aufbereitung der Daten eine Rolle spielt.
	\item \textbf{\nameref{sec:theoretische_grundlagen:systemintegration}}: Für den praktischen Einsatz sollte sich eine fertige Anwendung in bestehende Systeme integrieren und nicht alleinstehend sein.
	\item \textbf{\nameref{sub:NoSQL}}: Für die Datenhaltung werden \gls{NoSQL} Datenbanken in Betracht gezogen, weshalb man mit deren Eigenschaften vertraut sein soll.
\end{itemize}

Die Grundlagen zum Kennzeichensystem \gls{RDS-PP} wurden bereits im vorherigen Kapitel behandelt.

\section{Parser}
\label{sec:theoretische_grundlagen:parser}

Ein Parser ist ein Computerprogramm, das dazu verwendet wird, eine Eingabe auf korrekten Syntax zu überprüfen und im fehlerfreien Fall in eine geeignete Ausgabe umzuwandeln. Die Ausgabe ist in der Regel ein hierarchische Datenstruktur wie ein \gls{AST}. Die syntaktische Analyse basiert auf den Regeln einer Grammatik, die definiert, wie eine wohlgeformte Eingabe aussehen darf.

Häufig geht dem Parsen eine lexikalische Analyse voraus, bei der ein sogenannter Lexer die Eingabe in Tokens unterteilt. \cite{parser:grune_jacobs:parsing_techniques, compiler:dragon_book}
 
\subsection{Grammatiken}

Jedem Parser liegt in irgendeiner Art eine formale Grammatik zugrunde. Diese Grammatik beschreibt die formale Sprache, die durch den Parser akzeptiert und verarbeitet werden kann. Die formale mathematische Definition lautet wie folgt: \cite{parser:grune_jacobs:parsing_techniques}

\begin{definition}{(Grammatik)}\\
	Eine Grammatik $G$ ist ein 4-Tupel $G=(N,T,P,S)$ mit
	\begin{enumerate}[label={(\arabic*)}]
		\item $N$ - Endliche Menge an Nichtterminal-Symbolen
		\item $T$ - Endliche Menge an Terminal-Symbolen
		\item $P$ - Endliche Menge an Produktionsregeln, von denen jede die Form $(N \uplus T)^+ \rightarrow (N \uplus T)^*$ hat. ($^+$ und $^*$ sind wie bei regulären Ausdrücken zu interpretieren: einmal oder beliebig oft ($^+ $) bzw. kein Mal oder beliebig oft ($^*$))
		\item $S$ - Startsymbol ($S \in N$)
	\end{enumerate}
\end{definition}

Die Produktionen $P$ sind dabei der wichtigste Teil der Grammatik, denn sie beschreiben, welche Wörter Bestandteil der durch $G$ erzeugten Sprache ist. Anhand der Eigenschaften der Produktionen lassen sich Grammatiken in Klassen einteilen. Die bekannteste Klassifizierung ist die Chomsky-Hierarchie:
\begin{itemize}
	\item \textbf{Typ-0}: unbeschränkte Grammatik
	\item \textbf{Typ-1}: kontextsensitive Grammatik;\\
	nur Produktionen der Form $uAv \rightarrow u\beta v$ mit $|\beta| > 0$ oder $S \rightarrow \epsilon$
	\item \textbf{Typ-2}: kontextfreie Grammatik;\\
	nur Produktionen der Form $A \rightarrow \beta$ oder $S \rightarrow \epsilon$
	\item \textbf{Typ-3}: reguläre Grammatik;\\
	nur Produktionen der Form $A \rightarrow aB$, $A \rightarrow a$ oder $S \rightarrow \epsilon$
\end{itemize}

($\epsilon$ - leeres Symbol; $S$ - Startsymbol; $u,v \in (N \uplus T)^*$; $\beta \in (N \uplus T )* \setminus \{\epsilon\}$; $A,B \in N$; $a \in T$)

Für die Sprachklassen $L$ vom Chomsky Typ $n \in \{0, 1, 2, 3\}$ gilt dabei $L_3 \subset L_2 \subset L_1 \subset L_0$.

Für Parser werden praktisch ausschließlich kontextfreie Grammatiken (Typ-2) verwendet. Dies liegt daran, dass Typ-3 Grammatiken zu eingeschränkt und Typ-1 Grammatiken zu unperformant wären. Typ-3 Grammatiken beschreiben die gleiche Sprachmenge wie \glspl{RegEx}, die man aus den meisten Programmiersprachen kennt. Diese eignen sich nicht dazu, komplexe Sprachen zu definieren. Typ-1 Grammatiken dagegen beschreiben die gleiche Sprachmenge wie nicht-deterministische linear-beschränkte Turingmaschinen. Da ein nicht-deterministischer Parsealgorithmus nur durch Backtracking simuliert werden kann, wären kontextsensitive Parser für nicht-triviale Grammatiken zu ineffizient. \cite{parser:grune_jacobs:parsing_techniques}

\subsection{Arten von Parsern}
\label{sec:arten_von_parsern}

Parser können anhand ihrer Vorgehensweise grob in zwei Kategorien eingeteilt werden: Top-Down und Bottom-Up. Top-Down Parser versuchen beginnend beim Startsymbol über die Produktionsregeln die Eingabe zu rekonstruieren. Bottom-Up Parser gehen die entgegengesetzte Richtung und versuchen die Eingabe mithilfe der Produktionsregeln auf das Startsymbol zu reduzieren.

Beide Parserkategorien können anhand bestimmter Implementierungseigenschaften weiter unterteilt werden. Bei Top-Down Parser sind dies beispielsweise Resursive-Descent- und LL(1)-Parser. Bei Bottom-Up Parser sind meistens LR-Parser (oder spezielle Unterarten wie SLR, LALR, etc.) anzutreffen. Top-Down Parser sind in der Regel relativ einfach und lassen sich deshalb teilweise mit überschaubarem Aufwand von Hand implementieren. Bottom-Up Parser sind dagegen meist deutlich komplexer und werden daher fast ausnahmslos durch sogenannte Parsergeneratoren generiert. \cite{parser:grune_jacobs:parsing_techniques, tutorialspoint:compiler}

\subsection{Parsergeneratoren}

Ein Parsergenerator ist eine Software, die es erlaubt auf Grundlage einer Syntax-Spezifikation einen Parser automatisch zu generieren. Der generierte Parser liegt dann z.B. als Klasse in einer bestimmten (objektorientierten) Programmiersprache vor und kann beliebig in andere Programme eingebunden werden.

Als Eingabe erhält ein Parsergenerator eine Grammatik, die häufig als \gls{BNF} oder \gls{EBNF} vorliegt. Dies sind formale Meta-Sprachen zur Darstellung kontextfreier Grammatiken. Die Darstellung ähnelt sehr der mathematischen Definition. So wird z.B. die Ableitung \enquote{$addition \rightarrow NUM + NUM$} in \gls{EBNF} als \enquote{\code{addition = NUM '+' NUM;}} dargestellt.

\subsection{Anforderungen durch RDS-PP}
\label{sec:theoretische_grundlagen:parser_anforderungen_durch_rdspp}

Nun da die Grundlagen zu Parsern und Grammatiken bekannt sind, soll noch kurz geklärt werden, welche Anforderungen ein Parser für \gls{RDS-PP} Kennzeichen erfüllen muss.

Wie sich in \autoref{cpt:rds-pp} gezeigt hat, sind \gls{RDS-PP} Kennzeichen aus sprachtheoretischer Sicht nicht sonderlich Komplex. Im Prinzip ist ein Kennzeichen nur eine Aneinanderreihung von Zahlen, Buchstaben und einigen Sonderzeichen. Vorgeschrieben ist dabei lediglich die Reihenfolge und Länge einzelner Abschnitte (Abschnitt $\hat{=}$ nur Buchstaben, Zahlen oder Sonderzeichen) und ob diese optional oder notwendig sind. Für solche Konstrukte reicht eine reguläre Grammatik theoretisch aus. Um dies zu Beweisen, seien beispielhaft folgende Kennzeichen der Gemeinsamen Zuordnung (s. \ref{sec:rdspp:gemeinsame_zuordnung}) gegeben:

\begin{lstlisting}[caption={Beispiele für Gemeinsame Zuordnungen}]
#5163N00017W
#5163N00017W.GBCON.BEA_1WN
#4834N00823E.DE_BW.LOR12WN
#DE_BW.LOR12WN
\end{lstlisting}

Die Bedeutung der Kennzeichen ist zunächst irrelevant. Man erkennt, dass die Gemeinsame Zuordnung zwei Gliederungsstufen besitzt, von denen je eine optional ist. Zudem kann nachvollzogen werden, welche Abschnitte Zahlen, welche Buchstaben und welche Sonderzeichen zulassen. Ein möglicher regulärer Ausdruck hierfür sähe folgendermaßen aus:

\begin{lstlisting}[language=regex, label={code:regex_conjoint_designation}, caption={Regulärer Ausdruck für den Kennzeichenblock \enquote{Gemeinsame Zuordnung}}]
#(([A-Z]{2}[A-Z_]{2}[A-Z]\.[A-Z]{3}[0-9_][0-9][A-Z]{2})|([0-9]{4}(N|S)[0-9]{5}(E|W))(\.[A-Z]{2}[A-Z_]{2}[A-Z]\.[A-Z]{3}[0-9_][0-9][A-Z]{2})?)
\end{lstlisting}

Die anderen Kennzeichenarten sind vom Prinzip her gleich aufgebaut, auch wenn dabei die Anzahl, Länge und Reihenfolge der Abschnitte variiert. Daraus lässt sich schließen, dass \gls{RDS-PP} eine reguläre Sprache ist.

\autoref{code:regex_conjoint_designation} lässt jedoch schon erkennen, dass das Verwenden von regulären Ausdrücken schon bei nur einer Kennzeichenart äußert unhandlich ist. Würde man alle anderen Kennzeichenarten sowie deren Kombinationen und verkürzten Schreibweisen ebenfalls so abbilden, wären die dadurch entstehenden regulären Ausdrücke nur schwer verständlich. Daraus würde eine potenzielle Fehlerquelle sowie eine schlechte Wartbarkeit resultieren, weshalb es deutlich mehr Sinn macht, die RDS-PP-Grammatik in einer verständlicheren Notation wie \gls{EBNF} zu spezifizieren.

Was für eine Art von Parser hierfür genau verwendet wird, spiel praktisch keine Rolle. Zum einen, weil \gls{RDS-PP} wie gerade erklärt eine relativ einfache Sprache ist, zum anderen weil die Eingabe selbst bei langen Kennzeichen unter 100 Zeichen lang ist (ohne Leerzeichen). Die Effizienz des Parsers spielt daher keine Rolle, da dies selbst ein ziemlich ineffizienter, mit Backtracking implementierter Parser in vertretbarer Zeit handhaben könnte. Es macht daher mehr Sinn, den konkreten Parsertyp oder Parsergenerator so auszuwählen, dass es für die Implementierung am einfachsten sowie möglichst verständlich und wartbar ist.

Welcher Art von Parser letztendlich verwendet wird, wird in \autoref{cpt:parser} erörtert.

\section{Datenvisualisierung}
\label{sec:theoretische_grundlagen:datenvisualisierung}

Informationen bzw. Daten können auf die verschiedenste Art und Weise visualisiert werden. In der Literatur sind dutzende -- wenn nicht hunderte -- Darstellungsformen zu finden, von denen jede ihre eigenen Einsatzgebiete hat. Um davon die richtige Darstellungsform systematisch auszuwählen, sollen folgende Fragen beantwortet werden:

\begin{enumerate}
	\item Welche Art von Daten gibt es?
	\item Welche Relationen bestehen zwischen den Daten?
	\item Was sind die passenden Darstellungsformen?
\end{enumerate}

\subsection{Charakterisierung der Daten}

Prinzipiell drehen sich alle Hauptfunktionen der Anwendung um \gls{RDS-PP} Kennzeichen und deren Bedeutung bzw. Benennung. Ob dies nun bei der unterstützten Erstellung einer neuen Kennzeichnung ist oder der semantischen Interpretation eines vorhandenen Kennzeichens spielt keine Rolle. Die Kennzeichen haben dabei prinzipiell eine hierarchische Struktur, d.h. dass z.B. der Zusammenhang der Kennzeichen \code{=MDA11} und \code{=MDK51} als Baumstruktur wie in \autoref{img:visualisierung:kennzeichen_baum} dargestellt werden kann. Die Hierarchie spiegelt dabei eine Ist-Teil-von-Beziehung der gekennzeichneten Objekte wieder. Den einzelnen Abschnitten lässt sich dann eine Bedeutung und eine Beschreibung des gekennzeichneten Objekts zuordnen.

\begin{figure}[h]
	\centering
	\setlength{\fboxsep}{4pt}
	\setlength{\fboxrule}{1pt}
	\begin{tikzpicture}[
	grow=right,
	]
	\node {=}
	child {node {M}
		child {node {D}
			child {node {K}
				child {node {51}}
			}
			child {node {A}
				child {node {11}}
			}
		}
	};
	\end{tikzpicture}
	\caption{Hierarchischer Zusammenhang zwischen den Kennzeichen =MDA11 und =MDK51}
	\label{img:visualisierung:kennzeichen_baum}
\end{figure}

Die in einem Kennzeichen enthaltenen Informationen sind größtenteils kodierte Beschreibungen von konkreten Systemen bzw. Objekten einer Windkraftanlage, d.h. unter Zuhilfenahme der entsprechenden Normen kann z.B. ermittelt werden, dass \code{=MDA11} das Rotorblatt 1 kennzeichnet. Die einzige Ausnahme hiervon stellt die Gemeinsame Zuordnung dar, welche die komplette Energiekraftanalge kennzeichnet und hierzu u.a. geographische Koordinaten enthält.

Zusammenfassend können folgende Daten und \textit{Datenrelationen} identifiziert werden:

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[
	>={Latex},
	every node/.style={align=center},
	font={\small},
	]
	
	\node[draw] (designation) at (0,2) {Kennzeichenabschnitt};
	\node[draw] (denomination) at (7,2) {textuelle\\Bedeutung/Beschreibung};
	\node[draw] (coordinates) at (0,0) {geographische Koordinaten};
	
	\path[->] (denomination) edge[above] node {\textit{ist zugeordnet}} (designation);
	\path[->] (designation) edge[out=175, in=185, looseness=6, left] node {\textit{hierarchisch}} (designation);
	\path[->] (designation) edge[right] node {\textit{kann enthalten}} (coordinates);
	
	\end{tikzpicture}
	\caption{Verarbeitete Daten und deren Relationen}
	\label{img:visualisierung:daten_und_relationen}
\end{figure}

\subsection{Auswahl der passenden Darstellungsformen}
\label{sub:selectedVisualisierung}

Die naheliegende Art der Visualisierung für eine hierarchische Struktur ist ein Baumdiagramm, welches das Verhältnis zwischen Eltern- und Kindknoten durch Kanten abbildet. Da die Kennzeichenhierarchie jedoch sowohl sehr breit als auch sehr tief werden kann, bietet sich eine ausklappbare Baumstruktur an, wie man sie beispielsweise von gängigen Dateiexplorern kennt. \cite{handbook_of_data_visualization, data_visualization_catalogue}

Bei der hierarchischen Aufschlüsselung einzelner Kennzeichen (wie beim Parsen) kann auch eine Tree Map (\enquote{Kacheldiagramm}) wie in \autoref{img:visualisierung:treemap} (rechts) anstelle eines normalen Baumes verwendet werden. Dadurch lässt sich die Ist-Teil-von-Beziehung deutlicher darstellen. Die Anordnung der inneren Kacheln würde dabei jedoch lediglich in eine Dimension (horizontal oder vertikal) erfolgen, um die Reihenfolge der Kennzeichenabschnitte im Gesamtkennzeichen beizubehalten. Auf eine gewichtete Platzverteilung der Kacheln (wie bei Tree Maps normalerweise üblich) kann verzichtet werden. Stattdessen würde einer Kachel so viel Platz eingeräumt, dass diese noch die Beschreibung beinhalten kann. \cite{handbook_of_data_visualization, data_visualization_catalogue}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{treemap}
	\caption[Vergleich: Tree vs. Tree Map]{Vergleich: Tree vs. Tree Map \cite{data_visualization_catalogue}}
	\label{img:visualisierung:treemap}
\end{figure}

Die geographischen Daten in der Gemeinsamen Zuordnung können einfach durch eine Karte visualisiert werden. Die Position der Anlage kann dabei einfach über einen Pin, wie es auch bei weit verbreiteten Kartendiensten üblich ist, gekennzeichnet werden (vgl. \autoref{img:visualisierung:pinmap}). Hierbei kann dann auch auf entsprechende Kartendienste wie Google Maps zurückgegriffen werden, die das setzen solcher Pins auf einer interaktiven Karte ermöglichen.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.36\linewidth]{pinmap}
	\caption[Pin Map (schematische Abbildung)]{Pin Map (schematische Abbildung) \cite{data_viz_project}}
	\label{img:visualisierung:pinmap}
\end{figure}

Zur Visualisierung, welches Objekt ein Kennzeichen kennzeichnet bzw. wo sich dieses Objekt in der Anlage befindet, wäre der Einsatz von schematischen bzw. technischen Zeichnungen denkbar. Hierbei wären mehrere Zeichnungsarten möglich, z.B. \gls{CAD}-Zeichnungen, Explosionszeichnungen, Schnittzeichnungen, usw. (vgl. \autoref{img:visualisierung:zeichnungen}).

\begin{figure}[h]
	\begin{subfigure}{.475\linewidth}
		\centering
		\includegraphics[width=.8\linewidth]{exploded-view-drawing}
		\caption{Explosionszeichnung (schematisch)}
	\end{subfigure}
	\hspace*{\fill}
	\begin{subfigure}{.475\linewidth}
		\centering
		\includegraphics[width=.8\linewidth]{windrad-gondel-aufbau}
		\caption{Transparente 3D-Illustration einer Windradgondel}
	\end{subfigure}
	\caption{Visualisierung durch schematische/technische Zeichnungen}
	\label{img:visualisierung:zeichnungen}
\end{figure}

Diese Art der Visualisierung ist jedoch im Rahmen dieser Studienarbeit nicht umsetzbar, da zum einen die nötigen 3D-Zeichnungen bzw. Darstellungen nicht zur Verfügung stehen und zum anderen das nötige Fachwissen aus maschinenbautechnischer Sicht fehlt.

% Wo sind Daten?
% Was für eine Art Daten?
%  - Benennung/Beschreibung einzelner Anschnitte
%  - Hierarchische Anlagenstruktur
%  - Koordinaten
% Passende Darstellungsform?
%  - Part-to-a-hole (Treemap)
%  - Hierarchy (Tree)
%  - Karte (Pin Map)
%  - Schematische/Technische Zeichnung von Objekten

\section{Integration mit anderen Systemen}
\label{sec:theoretische_grundlagen:systemintegration}

Die zu entwickelnde Anwendung soll Menschen, die mit \gls{RDS-PP} zu tun haben, unterstützen. Zur üblichen Arbeitsumgebung dieser Menschen gehören jedoch auch noch andere Systeme. Es lohnt sich daher zu betrachten, mit welchen Systemen eine Interaktion denkbar ist und wie eine Integration von anderen Systemen in die Anwendung oder andersherum möglich wäre.

\subsection{Systemlandschaft}

Bevor über konkrete Möglichkeiten der Systemintegration diskutiert werden kann, muss zunächst geklärt werden, wie die Systemlandschaft in Unternehmen aussieht und wozu diese Systeme eingesetzt werden.

Computersysteme werden in Unternehmen vorwiegend dazu eingesetzt, Prozesse zu beschleunigen und Produkte effizienter zu entwickeln. Historisch betrachtet sind die Funktionen solcher Systeme ständig komplexer geworden, um den wachsenden Anforderungen zu entsprechen. Im technischen Umfeld waren dies zunächst \gls{CAD} Software (bzw. allgemein CAx Software), welche in den 1980er Jahren populär wurden. Um die schnell wachsende Anzahl an \gls{CAD} Zeichnungen besser verwalten zu können, kamen in den 90er Jahren \gls{PDM} Systeme auf (teilweise auch als \gls{EDM} bezeichnet). Diese Systeme sind darauf ausgelegt, alle Produktdaten zentral zu sammeln und zu verwalten. Neben der Dokumentenmanagementfunktion zur Verwaltung von Zeichnungen und sonstigen Dateien (Datenblätter, Protokolle, etc.) bieten die meisten \gls{PDM}s zudem die Möglichkeit beispielsweise Materiallisten, Änderungen, Freigaben oder Konfigurationen zu managen. Da etwa im selben Zeitraum auch \gls{ERP} Systeme aufkamen, haben \gls{PDM}s meist auch eine \gls{ERP}-Schnittstelle, um Daten wie Stücklisten auszutauschen. \cite{enzyklopaedie_winf_plm, procad_wiki, contact_software_pdm}

Heutzutage geht man vermehrt sogar noch einen Schritt weiter als rein entwicklungs- und konstruktionsbezogene \gls{PDM} Systeme und setzt stattdessen auf sogenanntes \gls{PLM}. Bei diesem Ansatz versucht man, alle Informationen und Daten eines Produktes sowie die dazugehörigen Prozesse abteilungsübergreifend und ganzheitlich zu verwalten. Dies geschieht dabei über den kompletten Lebenszyklus eines Produkt, von der Entwicklung über Produktion und Vertrieb bis hin zu Wartung und Demontage. Die \gls{PLM} Vision sieht dabei im Sinne eines \enquote{Single Source of Truth}-Gedanken eine unternehmensweite Quelle für Daten und Informationen vor. Dies wird oft als Product Data Backbone bezeichnet. Ziel davon ist es, im Zuge der Industrie 4.0 Entwicklung einen Digitalen Zwilling zu erzeugen, der ein Produkt digital abbildet und alle produktbezogenen Informationen miteinander verknüpft. Zu den Anwendungsgebieten solcher Digitaler Zwillinge zählen z.B. Simulation und Analyse oder Predictive Maintenance. Trotz dass solche \gls{PLM} Ansätze immer mehr anzutreffen sind, sind auch reine \gls{PDM} Systeme immer noch verbreitet. \cite{procad_wiki, contact_software_pdm, it_production:produktzyklus_neu_denken, it_production:der_informationszwilling}

Betreiber von Erneuerbare-Energie-Anlagen sind u.a. gesetzlich dazu verpflichtet eine umfassende Dokumentation für jede Anlage zu erstellen, die auf Anfrage durch beispielsweise Behörden oder Versicherungen bereitzustellen ist. Solch eine Dokumentation wird auch als Lebenslaufakte bezeichnet. \cite{researchgate:konzeption_einer_lebenslaufakte} Es ist daher davon auszugehen, dass Hersteller und Betreiber von Energiekraftanalgen in irgendeiner Form ein \gls{PDM}/\gls{PLM} System im Einsatz haben.

In der Praxis ist ein \gls{PLM} System keine eigenständige Software, sondern integriert vielmehr eine Vielzahl von bestehenden Systemen wie \gls{PDM}, \gls{ERP}, CAx oder \gls{CRM} Systeme. \cite{enzyklopaedie_winf_plm, procad_wiki} Die Frage, die sich daher stellt, ist, wie lässt sich die zu entwickelnde \gls{RDS-PP}-Anwendung mit bestehenden \gls{PDM}/\gls{PLM} Systeme integrieren?

\subsection{Integrationsansätze}

Eine einfache Lösung auf die Frage nach Integrationsansätzen wird sich wohl nicht finden lassen. Es gibt dutzende Anbieter für \gls{PDM}/\gls{PLM} Software, die wahrscheinlich alle über unterschiedliche Schnittstellen und Integrationsmöglichkeiten verfügen. Es ist schwer hier eine definitive Aussage zu treffen, da die meisten Systeme so proprietär sind, dass eine Dokumentation der \gls{API} oder ihrer Features erst nach einem Kauf des Produktes einsehbar ist. Im Folgenden sollen zunächst zwei Initiativen und zwei Systeme betrachtet werden, die versuchen dem entgegenzuwirken bzw. Lösungen für einheitliche Schnittstellen geben wollen.

\subsubsection{Code of Openness}

Unter all den proprietären Lösungen gibt es auch Bemühungen, Systeme offener und leichter integrierbar zu machen. \gls{CPO} ist eine 2012 gegründete Initiative des ProSTEP iViP Vereins unter der Schirmherrschaft des \gls{BMWi}, die die Förderung der Offenheit von IT-Systemen zum Ziel hat. An der Initiative haben sich mehrere große Anbieter von \gls{PDM}/\gls{PLM} Software wie z.B. Siemens, Dassault Systèmes, PTC, Contact Software, Aras oder Autodesk sowie eine Vielzahl an Automobilherstellern angeschlossen. Insgesamt nennt ProSTEP auf ihrer Website 77 Unternehmen, die sich für die Einhaltung der \gls{CPO} schriftlich ausgesprochen haben (Stand: April 2020). Dabei wurde ein Kriterienkatalog entwickelt, der die messbare Kriterien (\enquote{muss}, \enquote{kann} und \enquote{soll}) zu den Kategorien Interoperabilität, Infrastruktur, Erweiterbarkeit, Schnittstellen, Standards, Architekturen und Partnerbeziehungen umfasst. In Zusammenarbeit mit dem \gls{BMWi} wurde sogar ein Zertifizierungsverfahren ausgearbeitet. Der \gls{CPO} sowie die Zertifizierungskriterien wurden 2018 in der frei zugänglichen DIN SPEC 91372 Serie veröffentlicht. \cite{code_of_openness, norm:code_of_openness_teil_1}

Die durch den \gls{CPO} geforderten Kriterien sind allerdings eher allgemeiner Natur. Es werden zwar \gls{API}s für den Datenaustausch, deren Dokumentation und die Einhaltung von Standards als Muss-Kriterien gefordert, jedoch werden keine spezifischen Standards oder Datenaustauschformate genannt.

\subsubsection{OSLC}

\gls{OSLC} ist eine 2008 gegründete offene Initiative, die Spezifikationen für die Integration von Softwaretools veröffentlicht. \gls{OSLC} setzt dabei konkret auf folgende Webstandards:

\begin{itemize}
	\item \textbf{\gls{HTTP}} als Kommunikationsprotokoll
	\item \textbf{REST} als Interface-Paradigma um \acrshort{CRUD}-Operationen auf Ressourcen (Daten, Dokumenten etc.) auszuführen
	\item \textbf{Linked Data} für die Identifikation von Ressourcen per \gls{URI}
	\item \textbf{\gls{RDF}} zur Beschreibung der Ressourcen und ihrer Beziehungen untereinander
\end{itemize}

\gls{OSLC} kennt dabei zwei Rollen: Provider, die \gls{OSLC} Ressourcen zur Verfügung stellen, und Consumer, die diese Ressourcen nutzen. Die \gls{OSLC} bietet zur Implementierung Bibliotheken für Java, .NET und JavaScript an, wobei die Java-Variante am ausgereiftesten ist. Dies liegt u.a. daran, dass das Eclipse Lyo Projekt eine \gls{OSLC}-konformes OSLC4J SDK und einen graphischen Modellierer (Lyo Designer) bereitstellt. \cite{oslc_website, oslc_doc}

\subsubsection{OpenPDM}

\begin{figure}[h]
	\centering
	\includegraphics[width=.95\linewidth]{openpdm}
	\caption[OpenPDM System]{OpenPDM System \cite{prostep:openpdm_product_guide}}
	\label{img:systemintegration:openpdm}
\end{figure}

OpenPDM ist ein Produkt der PROSTEP AG. Es ist ein System für \gls{PLM} Integration, d.h. es ermöglicht einem verschiedene \gls{PLM}-relevante Systeme wie \gls{PDM}/\gls{PLM}, CAx oder \gls{ERP} Software zu integrieren. Dazu werden eine große Anzahl an Standard-Connectoren für verbreitete Systeme angeboten (vgl. \autoref{img:systemintegration:openpdm}).

Interessant ist dabei vor allem, dass OpenPDM hierbei über einheitliche Schnittstellen Zugriff auf all diese Systeme ermöglicht. Hierbei wird auf standardisierte Datenformate wie \gls{XML} oder \gls{STEP}, einem Standard (ISO 10303) für die Beschreibung von Produktdaten (z.B. \gls{CAD}-Daten), gesetzt. \glspl{API} stehen in Form von Web Services (\acrshort{REST}, \acrshort{SOAP}) oder \gls{OSLC} zur Verfügung. \cite{prostep:openpdm_product_guide, openpdm_website}

\subsubsection{PDM WebConnector}

\begin{figure}[h]
	\centering
	\includegraphics[width=.8\linewidth]{pdm-webconnector}
	\caption[PDM WebConnector]{PDM WebConnector \cite{t_systems:pdm_webconnector}}
	\label{img:systemintegration:pdm_websonnector}
\end{figure}

Der PDM WebConnector von T-Systems verfolgt ein ähnliches Ziel wie OpenPDM: Integration verschiedener Software für ein \gls{PLM}-Konzept (vgl. \autoref{img:systemintegration:pdm_websonnector}). Umgesetzt ist dies als \glsr{SOA} Middleware, sodass auch anwenderspezifische Applikationen integriert werden können. \cite{t_systems:pdm_webconnector, t_systems:pdm_webconnector:presentation}

Sowohl OpenPDM als auch der PDM WebConnector haben eine \gls{CPO} Zertifizierung.

\subsection{Rückschlüsse für diese Arbeit}

Eine Integration in ein \gls{PDM}/\gls{PLM} System wäre mit Sicherheit sinnvoll, da so beispielsweise ausgehend von einem \gls{RDS-PP} Kennzeichen zugehörige Dokumente für den Anwender verfügbar gemacht werden könnten. Eine solche Systemintegration ist jedoch im Rahmen dieser Studienarbeit unrealistisch. Die Gründe dafür sind vielfältig:

\begin{itemize}
	\item Die heterogene Systemlandschaft erschwert eine einheitliche Lösung.
	\item Es besteht kein Zugriff auf entsprechende Systeme, da teure \gls{PDM}/\gls{PLM} Software den Kostenrahmen dieser Studienarbeit überstrapazieren würden.
	\item Selbst wenn ein Anbieter eine Demoversion seiner \gls{PDM}/\gls{PLM} Software zur Verfügung stellen würde, würden eine entsprechende Datengrundlage und Kenntnisse über den praktischen Einsatz solcher Systeme fehlen.
\end{itemize}

Die einzige Möglichkeit wäre hierbei die Kooperation mit einem Hersteller/-betreiber von Windkraftanalgen, der sowohl ein \gls{PDM}/\gls{PLM} System im Einsatz hat als auch seine Anlagen nach \gls{RDS-PP} kennzeichnet, um für einen konkreten Anwendungsfall eine Integration zu untersuchen. Dies würde den Rahmen dieser Studienarbeit jedoch deutlich sprengen. Das Themen Integration mit anderen Systemen soll sich daher bei dieser theoretischen Betrachtung begrenzen und es soll keine praktische Implementierung umgesetzt werden.

% PLM
% Lebenslaufakte (DIN 77005-1 [Sep 2018])
% PDM als Basis für PLM
% Austauschformate: STEP, IGEX, JT, SysML, eCl@ss
% Ziel: Zugriff auf Daten aus diesen Systemen
% OpenPDM und PDM WebConnector als Plantformen für die Integration für PLM Systeme
%  - Anbindung verschiedener Systeme
%  - einheitliche Schnittstellen (z.B. OSLC oder WebService (REST, SAOP))
% Code of (PLM) Openness


\section{NoSQL Datenbanken}
\label{sub:NoSQL}
\gls{NoSQL} ist die Abkürzung für \enquote{not only SQL}, was für Datenbanksysteme steht, die mit den Eigenschaften typischer relationaler Datenbanken brechen und alternative Datenbankmodelle verwenden. Gerade die Datenspeicherung basiert nicht auf einem relationalen Datenmodell. Das bedeutet jedoch nicht, dass generell auf die Datenbanksprache \gls{SQL} verzichtet wird.

Statt relationalen Datenbanktabellen mit Spalten und Zeilen für die Datenspeicherung nutzt eine \gls{NoSQL}-Datenbank zur Organisation der Daten beispielsweise Wertepaare, Objekte, Listen und auch Dokumente. \\
\cite{BDI:NoSQL, CoWo13, amz:noSQL}

\subsection{Unterscheidung zu relationalen Datenbanksystemen}

Als Hauptunterscheidung gilt, dass bei \gls{NoSQL}-Datenbanken nicht-relationale Datenbankmodelle verwendet werden. Darüber hinaus gibt es noch viele weitere Merkmale.
Dazu zählt, dass das komplette \gls{NoSQL}-Datenhaltungssystem für eine effiziente Skalierung optimiert und für eine verteilte Architektur entwickelt wurde. So ist eine horizontale Skalierung recht einfach möglich. 

%Das bedeutet, dass Daten auf mehreren Systemen, die an verschiedenen Orten sind, verteilt werden können. \gls{NoSQL} arbeitet schemafrei und ermöglicht flexible Änderungen von Datendefinitionen mit Versionierung und Hintergrundkonvertierungen.

%Bei \gls{SQL}-Datenbanken wird meist nach dem \gls{ACID}-Modell gespeichert bzw. werden Transaktionen nach diesem Schema vollzogen. Bei \gls{NoSQL} wird dagegen das \gls{BASE}-Modell angewandt. Es ist meist auf die Verfügbarkeit der Daten ausgerichtet und nicht unbedingt auf deren Konsistenz zu jedem Zeitpunkt.\cite{BDI:NoSQLkurzuebersicht}
%\todo{prüfen ob das noch nötig ist}

Ein weiteres Unterscheidungskriterium bildet die einfach gestalteten \gls{API}s sowie die jeweiligen Konsistenzmodelle der \gls{NoSQL}-Datenbanken.\cite{BDI:NoSQL}

\subsection{Arten von NoSQL Datenbanken}

\gls{NoSQL}-Datenbanken gibt es grundsätzlich in folgenden vier Hauptkategorien:
\begin{itemize}
	\item Key-Value-Datenbanken
	\item Graphendatenbanken
	\item Dokumentenorientierte Datenbanken
	\item Spaltenorientierte Datenbanken
\end{itemize}

Deren Eigenschaften sollen im Folgenden jeweils kurz zusammengefasst werden.

\subsubsection{Key-Value Datenbanken}

Bei Key-Value-Datenbanken werden Daten als eine Sammlung von Schlüssel-Werte Paaren gespeichert. Dabei dienen Schlüssel zur eindeutigen Identifikation. Schlüssel können aber durchaus auch Elemente wie Objekte oder Ähnliches sein. Besonders hilfreich sind Key-Value Datenbanken im Hinblick auf Big-Data. Hier kann der Hauptteil der Daten auf Festplatten gehalten werden und die Keys dazu im Cache. Dadurch sind die Daten trotz ihrer Größe immer noch ziemlich schnell abrufbar und können leicht skaliert werden.\cite{opsrc17}

%https://www.predictiveanalyticstoday.com/top-sql-key-value-store-databases/ 

Anwendungsfälle hierbei sind Sitzungsspeicher (Webanwendung startet und speichert für den Benutzer alle wichtigen Sitzungsdaten. Diese werden ausschließlich über einen Primärschlüssel für den jeweiligen Benutzer abgefragt) oder Warenkörbe.\cite{amz:noSQL}

\subsubsection{Graphenorientierte Datenbanken}

Graphendatenbanken sind prinzipiell zum Speichern und Navigieren von Beziehungen
konzipiert. Sie bestehen aus Knoten zum Speichern von Datenentitäten und Edges, welche die Beziehungen zwischen den Entitäten darstellen. Die Edges haben immer genau einen Start- und Endknoten. Die Anzahl und Art der Beziehungen in einem Knoten ist nicht beschränkt. In Graphdatenbanken werden die Joins oder Beziehungen sehr schnell durchlaufen, da diese nicht erst bei der Abfrage erstellt werden, sondern in der Datenbank bestehen bleiben. Außerdem bieten diese Datenbanken Vorteile bei Anwendungsfällen wie Analysen oder anderen Fällen, bei denen Beziehungen zwischen Daten erstellt und schnell abgefragt werden müssen. Beispielsweise Social Networking und Betrugserkennung sind Anwendungsgebiete dieses Datebanktyps. \cite{amz:noSQL}

\subsubsection{Dokumentenorientierte Datenbank}

Dokumentenorientierte Datenbanken werden oft auch als Document Stores bezeichnet. Dabei werden die Daten in Form von Dokumenten gespeichert. Datensätze, die gespeichert werden, sind prinzipiell Key-Value-Paare. So ist dieser Datenbanktyp recht ähnlich zu Key-Value Datenbanken. Der einzige Unterschied liegt darin, dass bei dokumentenorientierten Datenbanken das Value, (semi-)strukturierte Daten in Form von ganzen Dokumenten beinhaltet. \cite{opsrc17, WiInf:documentstores}

Ein Dokument ist dabei eine Aggregation von mehr oder weniger strukturierter Daten ohne Schemavorgabe. Die einzelnen Dokumente besitzen jedoch keine Beziehungen zueinander und sind nicht normiert. Komplexere Abfragen werden deshalb durch selbst zu programmierende Map-Reduce-Funktionen unterstützt.\cite{CoWo13}

Wie bei einem normalen Key-Value-System werden Daten als Aggregate zusammengefasst was für gewöhnlich Objekte im \gls{JSON}-Format sind. Dieses ist ein weitverbreitetes Datenformat für Webanwendungen und Apps. Auch möglich sind Lösungen mit \gls{XML} oder \gls{YAML}. 

\gls{JSON}, \gls{XML} und \gls{YAML} verlangen dabei keine vordefinierte Felder und können selbst wiederum beliebig verschachtelte Elemente enthalten. Bei dokumentenorientierten Datenbanken sind die Values (Daten) transparent gehalten.
Das ermöglicht sowohl das Suchen nach Dokumenten, als auch das Suchen nach Abschnitten innerhalb des Dokuments. Mit der gleichen Technik ist auch das Updaten von Abschnitten innerhalb des Dokuments möglich. 
Oft wird dieser Datenbanktyp auch als Erweiterung der Key-Value-Gattung bezeichnet.
\cite{WiInf:documentstores}

Wie schon im \autoref{sub:NoSQL} angesprochen, wird kein Schema vorgegeben, um die semistrukturierten Daten abzulegen. So sind Änderungen in der Dokumentstruktur und auch in der Gestaltung dieser Dokumente ohne vorzeitige Bekanntmachung möglich. Durch diese Möglichkeit der Datenhaltung ist der Entwickler viel flexibler bei der Eingabe der Daten.
%In der Folgenden \autoref{img:JSONBestellung} sieht man beispielhaft ein einfaches \gls{JSON}-Dokument, bei dem eine Bestellung mit einem Artikel angezeigt wird. 
% \begin{figure}[h]
% 	\begin{minipage}[b]{.45\linewidth}
% 		\includegraphics[width=\linewidth]{3_img/JSONBestellung}
% 		\caption{Beispielausschnitt einer Bestellung in JSON}
% 		\label{img:JSONBestellung}		
% 	\end{minipage}
% 	\hspace{.1\linewidth}
% 	\begin{minipage}[b]{.45\linewidth}
% 		\includegraphics[width=\linewidth]{3_img/JSONBestellungUpdate}
% 		\caption{JSON-Bestellung mit angefügtem Feld}
% 		\label{img:JSONBestellungUpdate}		
% 	\end{minipage}
% \end{figure}
%Sollte es jetzt jedoch nötig sein, Anmerkungen zu einem bestimmten Artikel noch hinzuzufügen, kann man wie in \autoref{img:JSONBestellungUpdate} gezeigt noch ein Feld mit Anmerkungen hinzufügen. Sollte es zu einer Abfrage kommen, bei der dieses Feld eine Rolle spiel, würden auch nur die Dokumente berücksichtigt, die auch für ein solches Feld verfügen.

%In einer relationalen Datenbank wäre dies nicht ohne weiteres möglich.\newline
In der Realität werden jedoch Dokumente auch nicht mit wahllosen Feldern versehen. Meistens wird ein Schema entsprechend zur Anwendung verfolgt, um die Voraussetzung für sinnvolle Abfragen zu schaffen. \cite{WiInf:documentstores} 

%Problematisch ist bei diesem Verfahren nur, Beziehungen zwischen den Dokumenten herzustellen. Es ist zwar möglich, ID-Felder in die Dokumente zu schreiben und somit eine gewisse Beziehung herzustellen jedoch sind keine direkten JOINs möglich.\cite{WiInf:documentstores} 
%Das liegt daran, dass zunächst ein Dokument geladen und nach der ID gesucht wird. Im nächsten Schritt erneut die Datenbank zu befragen, um ein Dokument mit der passenden ID zu suchen und zu laden, wodurch erst eine Schnittmenge erstellt werden kann.\newline 
%Da es eine nicht-relationale Datenbank ist, sind auch keine Abfragemöglichkeiten mitgegeben und muss daher selbst programmiert werden.\cite{WiInf:documentstores} 

%Auch die Formfreiheit kann Schwierigkeiten mit sich bringen, da mehr Aufwand betrieben werden muss, um Abfragen zu programmieren. Das kommt daher, dass zuvor der Applikationscode studiert werden muss, um Einblicke über das indirekte Schema der Datenbank zu gewinnen.  \cite{amz:noSQL}

\subsubsection{Spaltenorientierte Datenbanken}

Der klassische relationale Ansatz im Datenbankdesign ist ein zeilenbasiertes Verfahren zur Verarbeitung von Daten. Es ist nicht auf die Analyse, sondern die Transaktionsverarbeitung spezialisiert.

Spaltenorientierter Speicher für Datenbanktabellen ist bei der analytischen Abfrage ein wichtiger Faktor, da sich die Ein- und Ausgabeanforderungen von Festplatten dadurch insgesamt drastisch reduzieren. Auch verringert sich die Datenmenge, die von der Festplatte zu laden ist.

Spaltenorientierte Datenbanken spielen insbesondere im Bereich der analytischen Datenbanksysteme eine wesentliche Rolle. Dabei geht es darum, so effektiv und effizient wie möglich große Datenmengen auszuwerten. 
Dadurch, dass Spaltenorientierte Datenbanken wie alle anderen \gls{NoSQL}-Datenbanken für horizontale Skalierung konzipiert wurden, sind sie optimal geeignet für \enquote{Data Warehousing} und die \enquote{Big Data}-Verarbeitung \cite{amz:noSQL}

\subsection{Rückschlüsse auf die Arbeit}

Wie noch in \autoref{sub:tech:NoSQL} genauer erläutert wird, bietet sich für das Speichern der \gls{RDS-PP}-Kennzeichen eine \gls{NoSQL}-Datenbank an. Die Auswahl wurde im Hinblick auf die konkreten Anforderungen der Kennzeichen getroffen. Wie in \autoref{cpt:rds-pp} gezeigt wurde, sind \gls{RDS-PP}-Kennzeichen hierarchisch aufgebaut. In \gls{NoSQL} ist es recht einfach, hierarchischen Strukturen zu realisieren. Im Gegensatz zu relationalen Datenbanken haben \gls{NoSQL}-Datenbanken zudem ein flexibleres Datenbankschema. Das ist ganz hilfreich, da die Kennzeichendaten zwar recht ähnlich sind, aber sich vor allem im Hinblick auf die jeweilige Kennzeichenart leicht unterscheiden. Durch das flexible Datenbankschema lassen sich diese kleinen Unterschiede recht einfach umsetzen.
% Relational
% NoSQL
%  - key value
%  - dokumentenorientiert
%  - ...
