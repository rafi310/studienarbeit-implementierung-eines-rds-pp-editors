\chapter{Kennzeichenassistent}
\label{cpt:assistent}

In diesem Kapitel geht es um die Konzeption der Hauptfunktion \enquote{Unterstützung bei der Erstellung korrekter Kennzeichen} (vgl. \autoref{sec:zieldefinition}). Dabei soll der Anwender die Möglichkeit haben, für ein beliebiges Bauteil eines Kraftwerks das korrekte Kennzeichen nach dem \gls{RDS-PP}-Standard der \gls{VGB} zu erstellen.

Für diesen Zusammenhang wird ein Assistenzsystem geplant, welches einem \enquote{Step-By-Step}-Ansatz folgt. Dadurch werden abhängig von den bisherigen Eingaben des Benutzers verschiedene Auswahl- und Eingabemöglichkeiten zur Verfügung gestellt. Mit dessen Hilfe wird der Anwender schrittweise durch die Kennzeichenerstellung hindurchgeführt, wodurch ein fehlerhaftes Kennzeichen nicht erstellt werden kann. 
%In diesem Zusammenhang wird erst die Umsetzung im Backend veranschaulicht. Danach folgt wird die Darstellung im Frontend kurz angesprochen wobei auch die Kommunikation zwischen Frontend und Backend erläutert wird.

\section{Mögliche Eingabetypen}
\label{sec:konkreteAnforderungen}

Wie in \autoref{cpt:rds-pp} erwähnt, gibt es unterschiedliche Kennzeichenarten wie z. B. die Gemeinsame Zuordnung. Jede Kennzeichenart ist wiederum aus verschiedenen Abschnitten aufgebaut, die sich in der Art der möglichen Eingaben unterscheiden. Die Hilfestellungen, die der Assistent dem Benutzer geben kann, sind dann vom jeweiligen Eingabetyp abhängig. Wie diese verschiedenen Eingabetypen aussehen, wird im Folgenden skizziert:

\begin{itemize}
	\item \textbf{Geografische Position:} Wie aus \autoref{sec:kennzeichenarten} hervorgeht, werden für die geografische Zuordnung Koordinaten benötigt. Diese werden in das \gls{WGS84}-Format gebracht und hintereinander angefügt.
	
	Für die Umsetzung in der Oberfläche ist hier eine Pin Map angebracht. Der Anwender kann über die Verschiebung des Markers auf der Weltkarte oder der direkten Eingabe der Koordinaten seine gewünschte Position bestimmen.
	\item \textbf{Freie Eingabe von Buchstaben:} Bei manchen Kennzeichenabschnitten ist es nötig, eine feste oder beliebig Anzahl an Buchstaben eingeben zu können.
	
	Für die Umsetzung ist ein Textfeld, welches nur Buchstaben als Eingabe zulässt geeignet. Dabei sollte auch noch die Anzahl der einzugebenden Buchstaben mit einer Unter- und Obergrenze limitiert sein.
	\item \textbf{Freie Eingabe von Zahlen:} Auch gibt es Abschnitte, bei denen nur eine feste oder beliebige Anzahl von Zahlen benötigt wird.
	
	Hier gilt bei der Umsetzung in der Oberfläche genau das Gleiche wie bei der Eingabe von Buchstaben. Lediglich, dass anstatt von Buchstaben ausschließlich Zahlen als Eingabe erlaubt sind.
	\item \textbf{Kombination von Buchstaben und Zahlen:} Es ist nötig, auch eine Kombination der beiden oberen Punkte (Eingabe mit nur Text und nur Nummern) zu bieten.
	
	In der Oberfläche bietet es sich bei diesem Typ an, die Eingabefelder für Buchstaben und Zahlen aneinanderzureihen.
	\item \textbf{Auswahl aus einer Liste:} Durch den Standard sind viele Buchstaben bei unterschiedlichen Kennzeichen mit festen Funktionen bzw. Begriffen belegt. 
	
	Wenn das der Fall ist, werden diese Begriffe als Liste angezeigt, wodurch der Anwender einfach die gewünschte Auswahl treffen kann. In der Liste wird der Code, welcher zu dem Kennzeichen führt, sowie ein verständlicher Klartext dazu dargestellt. Die Liste beinhaltet immer nur valide Kennzeichenkombinationen, was bedeutet, dass nur die Kennzeichenteile in der Liste angezeigt werden, die laut Standard hinter das bisherige Kennzeichen angefügt werden können.
\end{itemize}

Jede dieser Eingabemöglichkeiten muss in der Oberfläche, wie in diesen Stichpunkten schon skizziert, entsprechend angezeigt werden. Die Umsetzung davon wird in \autoref{sec:ui:assistent} veranschaulicht.

Bei der Eingabe der Daten ist wichtig, dass der Anwender durch den kompletten Kennzeichenprozess hindurchgeführt wird und somit nicht in der Lage ist, ein Kennzeichen zu erstellen, das nicht dem \gls{RDS-PP}-Standard entspricht.


\section{Assistent als Datenstruktur}

Jede Nutzereingabe wird an das Backend übermittelt, welches mit einer entsprechenden Rückgabe antwortet, um den nächsten Schritt zu kommunizieren. Der Inhalt dieser Nachrichten muss definiert sein, damit sowohl Frontend als auch Backend entsprechend darauf reagieren können. Wie dies aussehen kann, wird in diesem Abschnitt geklärt.

Das entsprechende Daten-Objekt wird in \autoref{uml:class:assistant_dataClass} mit allen verbundenen Abhängigkeiten dargestellt. In diesem Datenobjekt sind alle Daten des Assistenten zu dem aktuellen Kennzeichen, das erstellt werden soll, enthalten, was bei einer zustandslosen \gls{API} so sein muss. Die einzelnen Felder dieses Datenobjekts werden im Folgenden näher beschrieben:

\begin{itemize}
	\item \code{History}: Alle bisherigen \code{CommunicationData}-Objekte die für das aktuelle Kennzeichen schon ausgetauscht wurden. Dabei werden deren History auf \code{null} gesetzt, um redundante Daten zu vermeiden.
	\item \code{PossibleFinalState}: Gibt an, ob das bisherige Kennzeichen beendet werden kann und eine valide Kennzeichenkombination bildet.
	\item \code{NextStep}: Angabe über den Zustand, der als nächstes an der Reihe ist. 
	\item \code{DataInputType}: Enumwert zur Auswahl der richtigen Dateneingabe in der Oberfläche (vgl. \autoref{sec:konkreteAnforderungen})
	\item \code{DataInputLength}: Bei freien Buchstaben-/Zahleneingaben gibt es oft Beschränkungen über die Anzahl der Symbole, die eingegeben werden müssen.
	\item \code{Data}: Hinter diesem Feld befinden sich die Eingaben des Anwenders, die vom Frontend ans Backend übermittelt werden.
	\item \code{ActualDesignation}: In diesem String wird das komplette Kennzeichen gehalten, welches gerade erstellt wird.
	\item \code{ActualDesignationType}: Um immer eindeutig den nächsten Zustand bestimmen zu können, muss auch die Kennzeichenart (siehe \autoref{sec:kennzeichenarten}) des aktuellen Abschnittes bekannt sein. Die Kennzeichenart des aktuellen Kennzeichenabschnitts wird in diesem Feld gehalten.
	\item \code{ProposalList}: Falls als \code{DataInputType} eine Liste ausgewählt wurde, stehen in dieser Liste alle möglichen Vorschläge, von denen der Nutzer ein Element auswählen kann.
\end{itemize}

\begin{uml}[h]
\centering
\includegraphics[width=.9\linewidth]{assistant_dataClass}
\caption{Structs zur Kommunikation zwischen Front- und Backend}
\label{uml:class:assistant_dataClass}
\end{uml}
Den genauen Verlauf der Kommunikation zwischen Front- und Backend wird in \autoref{uml:class:assistant_communication} veranschaulicht. Dieses Schema ist zu jedem Zeitpunkt der Kennzeichnung immer gleich.

\begin{uml}[h]
	\centering
	\includegraphics[width=.9\linewidth]{assistant_communication}
	\caption{Kommunikation zwischen Front- und Backend}
	\label{uml:class:assistant_communication}
\end{uml}
Der Client (Frontend) teilt dem Server (Backend) mit, dass er neue Daten haben will. Dabei wird, falls vorhanden die aktuelle \code{CommunicationData}-Struktur mitgegeben. Beim ersten Aufruf ist dieser \code{null}, wodurch einfach eine initiale Struktur erzeugt wird.

Im zweiten Schritt aktualisiert der Server diese Daten, wobei die Eingaben des Clients als Basis dienen. Wie genau das passiert, wird in \autoref{sec:assistantBackend} erläutert.

Sobald die Daten aktualisiert wurden, werden sie als Response des POST-Request zurück an den Client gesendet. 

\section{Logik-Modul}
\label{sec:assistantBackend}

Hier wird das Verhalten des Assistenten definiert. Dabei geht es darum, den Kennzeichenprozess als Zustandsautomat abzubilden und je nach Zustand die entsprechenden Daten an die Oberfläche zurück zu geben.

Die Daten in Form von Kennzeichencodes und dazugehörigen Bedeutungen, die hierbei für die Vorschlagslisten benötigt werden, sind die gleichen wie bereits beim Parser verwendet (siehe \autoref{sec:datengrundlage}). 

Der Assistent benötigt zusätzlich noch eine Datei mit Textressourcen für einige Auswahlmöglichkeiten, die keine direkte Fortführung des aktuellen Kennzeichens als Konsequenz haben. Diese sind z. B. nötig, da der Anwender die Möglichkeit haben soll, mit einem Funktionskennzeichen bei seinem Hauptsystem oder bei seinem Teilsystem zu beginnen. Beide Möglichkeiten führen am Ende zu einem korrekten Kennzeichen.

Das Logik-Modul gliedert sich prinzipiell in drei Hauptfunktionen auf: 
\begin{enumerate}[label = {(\arabic*)}]
	\item \label{enum:handleInput} Eingabe verwalten: In dieser Hauptfunktion muss die Eingabe des Anwenders in den \gls{RDS-PP}-Kennzeichencode umgewandelt und an das bisherige Kennzeichen angehängt werden.
	\item \label{enum:offerData} Nächsten Schritt bestimmen: Dabei wird mithilfe einer Art Zustandsautomat der nächste Zustand sowie mögliche Weiterführungen des Kennzeichens ermittelt.
	\item \label{enum:restoreHistory} Assistenzverlauf nachbilden: Wenn die Kennzeichen in der Datenbank abgelegt wurden, gibt es die Möglichkeit, an einem beliebigen Punkt in der Kennzeichenhierarchie den Assistenten zu starten. Dieser bildet dann automatisch alle vorangegangene Assistenzschritte mithilfe des ausgewählten Kennzeichens nach.
\end{enumerate}
Die Umsetzung dieser Funktionalitäten wird in den folgenden Unterabschnitten genauer erklärt:

\subsection{Eingabe verwalten}

%das vllt nur mit worten schildern
Diese Funktion setzt im wesentlichen zwei Punkte um. Zuerst wird die aktuelle Assistents-Datenstruktur in seine eigene \code{History} geschrieben. Das dient dazu, dass die Datenmenge nicht exponentiell ansteigt.

Im zweiten Schritt wird die Eingabe des Benutzers in den entsprechenden \gls{RDS-PP}-Code umgewandelt und an das Ende des bisherigen Kennzeichens angehängt.

\subsection{Nächsten Schritt bestimmen}

%hier statediagramm rein
Um den nächsten Schritt bestimmen zu können, werden die aktuellen Daten des Assistent in einen Zustandsautomaten eingelesen. Dieser entscheidet dann, je nach gesetzten Attributen in der Datenstruktur des Assistenten, in welchem Zustand sich der Assistent befindet und kann über diesen Zustand die Daten zurück liefern, die für den nächsten Kennzeichenabschnitt gebraucht werden. 

Um das Funktionsprinzip des Zustandsautomaten besser verstehen zu können, wird dieser erst in eine abstrakte Form gebracht (\autoref{fig:statemachineComplete:assistant}). Dieser spiegelt die Aneinanderreihungen von Kennzeichenarten wieder. Das Zeichen in einem Zustand spiegelt die jeweilige Kennzeichenart wieder, d.h. steht beispielsweise ein \enquote{\#} im Zustand, handelt es sich dabei um die Gemeinsame Zuordnung. Da nach jeder Kennzeichenart theoretisch der Assistent beendet werden kann, ist auch jeder Zustand (außer dem Startzustand $S0$) ein Finalzustand. Die schwarzen Zustandsübergänge zeigen die Verkettung von Kennzeichenarten, die zu einem eindeutigen Kennzeichen führen. Solch ein Kennzeichen beginnt immer mit der Gemeinsamen Zuordnung. Nicht eindeutige, aber trotzdem valide Kennzeichenverkettungen können anhand der hellgrauen Übergange nachvollzogen werden.

\begin{figure}[h]
	\centering
	\scalebox{0.95}{
		\begin{tikzpicture}[
			->,>=stealth',
			shorten >=1pt,
			auto,
			node distance=1.75cm,
			semithick, 
			every text node part/.style={align=center}
		]
			% nodes
			\node[initial, state]	(start)								{$S0$};
			\node[state, accepting] (hash) 		[right=of start]		{$\#$};
			\node[state, accepting] (equal)		[above right=of hash]	{$=$};
			\node[state, accepting] (-)			[right=of equal]		{$-$};
			\node[state, accepting] (++)		[below right=of hash]	{$++$};
			\node[state, accepting] (+)			[below=of ++]			{$+$};
			\node[state, accepting] (;)			[above=of -]			{$;$};
			\node[state, accepting] (colon)		[right=of -]			{$:$};
			\node[state, accepting] (ampersand)	[right=of ++]			{$\&$};
			
			% edges
			\path (start) edge (hash);
			\path[draw=black!33!white] (start)
				edge[bend left=45] (;)
				edge[bend left=57.5] (colon)
				edge[bend left=47.5] (-)
				edge[bend left] (equal)
				edge[bend right] (++)
				edge[bend right] (+)
				edge[bend right=50] (ampersand);
			\path (hash)
				edge (equal)
				edge (+)
				edge (++)
				edge (ampersand);
			\path (equal)
				edge (-)
				edge (ampersand)
				edge (;);
			\path (+)
				edge (ampersand);
			\path (++)
				edge (ampersand);
			\path (-)
				edge (ampersand)
				edge (colon);
		\end{tikzpicture}
	}
	\caption{Zustandsautomat des Assistenten (vereinfacht)}
	\label{fig:statemachineComplete:assistant}
\end{figure}

In dem zweiten Zustandsautomaten wird einer dieser groben Zustände feiner dargestellt, was am Ende auch den Zuständen des implementierten Zustandsautomaten entspricht. Für das Beispiel in \autoref{fig:statemachine:assistant}  wurde als Kennzeichenart der Funktionsaspekt eines Kennzeichens(siehe \autoref{sec:rdspp:funktion}) ausgewählt. Der genau Ablauf des Zustandsautomaten wird im Folgenden näher beschrieben:

\begin{figure}[h]
\noindent
\begin{minipage}[h]{.65\linewidth}
	\centering
	\scalebox{0.8}{
		\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick, [every text node part/.style={align=center}]
		
		\node[initial,state, draw] (S0)                    	{$S0$};
		\node[state, draw]         (S1) [above right of=S0] {$S1$};
		\node[state, draw]         (S2) [right of=S1] 		{$S2$};
		\node[state, draw]         (S3) [below right of=S2] {$S3$};
		\node[state, draw]         (S4) [below of=S3]       {$S4$};
		\node[state, draw]         (S5) [left of=S4]       {$S5$};
		\node[state, draw]         (S6) [left of=S5]        {$S6$};
		\node[state, draw]         (S7) [below of=S6]       	{$S7$};
		\node[state, accepting]    (S8) [right of=S7]   		{$S8$};
		\path (S0) edge node {mit Hauptsystem beginnen} (S1)
		edge node {mit System/Teilsystem beginnen} (S3)
		(S1) edge node {}(S2)
		(S2) edge node {}(S3)
		(S3) edge node {}(S4)
		(S4) edge node {}(S5)
		(S5) edge node {}(S6)
		(S6) edge node {}(S7)
		(S7) edge node {}(S8);
		\end{tikzpicture}
	}
\end{minipage}%
\begin{minipage}[h]{.35\linewidth}
	\begin{scriptsize}
	$S0$ - Auswahlmöglichkeit zu Beginn\\
	$S1$ - Hauptsystem Datenstelle 1 \\
	$S2$ - Hauptsystem Datenstelle 2 \\
	$S3$ - System/Teilsystem Datenstelle 1 \\
	$S4$ - System/Teilsystem Datenstelle 2 \\
	$S5$ - System/Teilsystem Datenstelle 3 \\
	$S6$ - Subsystem \\
	$S7$ - Grundfunktion\\
	$S8$ - Auswahl nächste Kennzeichenart\\
\end{scriptsize}
\end{minipage}
\caption{Zustandsautomat des Assistenten für ein Funktionskennzeichen}
\label{fig:statemachine:assistant}
\end{figure}

Sollte der Anwender die Auswahl getroffen haben, dass er ein Funktionskennzeichen erstellen will, erreicht er $S0$ des Zustandsautomat von \autoref{fig:statemachine:assistant}. 
In jedem dieser Zustände wird die Assistenz-Datenstruktur je nach gewünschter Eingabe und der verfügbaren Vorschläge bearbeitet. Außerdem wird noch jedes Mal der Verweis auf den nächsten Zustand aktualisiert.

In $S0$ erhält der Anwender die Auswahl, ob er bei dem Hauptsystem starten will (geht dann zu $S1$), oder direkt mit dem System/Teilsystem (geht dann zu $S3$)beginnen will. Bei einer solchen Auswahl wird der Kennzeichen-Code, der durch den Assistenten erstellt werden soll, nicht erweitert. 

Sollte ein Hauptsystem ausgewählt worden sein, lässt sich über $S2$ noch ein entsprechender Counter hinzufügen. Danach wird dann über $S3$, $S4$, $S5$ und $S6$ jeweils eine Auswahl an Systemen/Teilsystemen in die Assistenz-Datenstruktur geschrieben. Dabei wird bei jedem neuen Zustand der bisherige Verlauf des Zustandsautomaten berücksichtigt, wodurch die Auswahl der Systeme durch die getroffenen Entscheidungen in früheren Zuständen beschränkt ist.

In $S7$ werden noch alle, für die bereits getroffene Auswahl legitimen, Grundfunktionen in die Vorschlagsliste der Assistenz-Datenstruktur geschrieben. Nachdem diese Auswahl getroffen wurde, ist das Funktionskennzeichen prinzipiell fertig. 

Es ist jedoch noch nötig, dass in $S8$ die Vorschlagsliste mit den Kennzeichenarten befüllt wird, mit denen das Kennzeichen potentiell weitergehen könnte.

\subsection{Eingabe verwalten}
Für die letzte Funktion des Logik-Moduls muss auf die in \autoref{cpt:parser} beschriebene Parserkomponente zugegriffen werden.
Dabei wird der String eingelesen, der das aktuelle Kennzeichen beinhaltet, welches erweitert werden soll.

Der Parser zerlegt das Kennzeichen und liefert einen Kennzeichenbaum zurück. Eine Fehlermeldung oder ein Hinweis zum Kennzeichenaufbau kann nicht mit zurückgegeben werden, da das eingegebene Kennzeichen korrekt sein muss. Um die Korrektheit des Kennzeichens zu garantieren gibt es eine Methode, die überprüft, ob das Kennzeichen fortgeführt werden kann oder nicht. Sollte es nicht fortgeführt werden können, ist die Operation für dieses Kennzeichen auch nicht verfügbar. 

Zu beachten ist, dass auch korrekte Kennzeichen möglicherweise nicht fortgeführt werden können. Ist das Kennzeichen beispielsweise schon so weit vorangeschritten, dass es nicht möglich ist, noch weitere Kennzeichenelemente hinzuzufügen, darf die Möglichkeit, das Kennzeichen fortzusetzen, gar nicht erst angezeigt werden. Auch dieser Zusammenhang wird mit der Methode, ob ein Kennzeichen fortgeführt werden darf oder nicht, überprüft.

Mithilfe dieses Kennzeichenbaums können dann die Eingaben des Anwenders nachgestellt werden. Das ist recht einfach zu realisieren, da der Kennzeichenbaum die gleichen Enum-Werte für die einzelnen Sektionen verwendet, wie auch der Assistent für die Auswahl des aktuellen Zustands.
So kann prinzipiell der Assistent normal durchlaufen werden, nur dass anstatt der Eingaben des Anwenders sequenziell die Kennzeichenteile aus dem Kennzeichenbaum eingelesen werden.

Einzige Herausforderung sind die Auswahlmöglichkeiten, die zu Beginn dieses Abschnittes bereits erwähnt wurden und zu keiner Fortführung des Kennzeichens führen, sondern lediglich für den Zustandsautomaten von Relevanz sind (z.B. ob das Funktionskennzeichen mit einem Hauptsystem oder einem Teilsystem beginnt).
Um diese Herausforderung zu lösen, muss erst der aktuelle Kennzeichenteil gefunden werden. Um nun die korrekte Auswahlmöglichkeit treffen zu können, muss der folgende Kennzeichenabschnitt analysiert werden, da sich über diesen die richtige Wahl treffen lässt.

\section{Zusammenfassung}

In diesem Kapitel wurde die Funktionalität des Kennzeichenassistenten beschrieben. Dabei kann der Anwender eine Auswahl treffen, welche vom Assistenten schon so eingeschränkt wird, dass nur korrekte Codefolgen möglich sind, die letztendlich ein Kennzeichen bilden. Die Eingabe des Anwenders wird dann in die Struktur geschrieben, die alle Daten des Assistenten enthält. Nachdem diese Struktur an den Server gesendet wurde, kommt sie mit neuen Daten zurück, wodurch dem Anwender eine neue Sicht angezeigt wird, die neue Vorschlägen enthält, wie das Kennzeichen weitergehen kann.

\begin{comment}
In \autoref{fig:kennzeichenAssistent} wird dieser Zusammenhang veranschaulicht:
\tikzset{
	block/.style={
		draw, 
		rectangle, 
		minimum height=1.5cm, 
		minimum width=3cm, align=center
	}, 
	line/.style={->,>=latex'}
}
\begin{figure}[h]
	\begin{center}
		\begin{tikzpicture}[
		block/.style={
			draw, 
			rectangle, 
			minimum height=1.5cm, 
			minimum width=3cm, 
			align=center,
			fill = red!20
		}, 
		line/.style={->,>=latex'},
		]
		%Nodes
	\node[block] (a) {User};
	\node[block, below =2cm of a] (b) {Frontend \\ (Darstellung der Daten)};
	\node[block, below =2cm of b] (c) {Backend};
	\draw[-{Latex[width=3mm]}] ([xshift=-0.5cm]a.south) -- ([xshift=-0.5cm]b.north) node [midway, left] {Eingabe des nächsten Kennzeichenteils};
	%\draw[<-] ([xshift=0.5cm]a.south) -- ([xshift=0.5cm]b.north) node [midway, right] {testBack};
	\draw[-{Latex[width=3mm]}] ([xshift=-0.5cm]b.south) -- ([xshift=-0.5cm]c.north) node [midway, left] {POST-Request mit Struct};
	\draw[{Latex[width=3mm]}-] ([xshift=0.5cm]b.south) -- ([xshift=0.5cm]c.north) node [midway, right] {Response mit aktualisiertem Struct};
		\end{tikzpicture}
	\end{center}
	\caption{Funktion Kennzeichen-Assistent}
	\label{fig:kennzeichenAssistent}
\end{figure}

Auch wird in diesem Kapitel beschrieben, wie die Funktionalität umgesetzt wurde, um ein bestehendes Kennzeichen, was in der Datenbank abgelegt wurde erweitern zu können.
%hier noch mehr schreiben??


Inhalt...
\end{comment}

% Vorgeplänkel über Assistenten
% Aufbau Backend
% - Klassendiagramm dazu
% - Kommunikationsdatentyp
% - Daten ?
% Kommunikation zur Oberfläche
% Darstellung einzelner Auswahlmöglichkeiten an der Oberfläche
% Fazit