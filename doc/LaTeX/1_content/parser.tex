\chapter{Kennzeichenparser}
\label{cpt:parser}

Eine der Hauptfunktionalitäten der Anwendung ist es, Kennzeichen zu parsen und anschließend semantisch zu deuten, um dem Benutzer die Bedeutung des Kennzeichens erkenntlich zu machen. In diesem Kapitel soll geklärt werden, wie dieses Parsen und Interpretieren eines Kennzeichens realisiert wird.

\section{Umsetzung der Grammatik}

Wie bereits in \autoref{sec:technologie:parser} geklärt, soll ANTLR4 als Parsergenerator verwendet werden. Das ANTLR-Programm kann als jar-Datei heruntergeladen werden und erwartet bei der Ausführung als Eingabe eine g4-Datei, welche die Grammatik enthält. Wie diese Grammatik aus den \gls{RDS-PP} Normen herzuleiten ist, wird in diesem Abschnitt erläutert.

Grundsätzlich wird eine ANTLR-Grammatik in \gls{EBNF} Notation geschrieben. Dabei können Parser- und Lexerregeln in einer g4-Datei beschrieben werden. Lexerregeln beginnen dabei mit einem Großbuchstaben, während Parserregeln mit einem Kleinbuchstaben beginnen. \cite{antlr_reference}

Der erste Teil der Grammatik ist in \autoref{code:grammar:lexer} zu sehen. In Zeile 1 wird zunächst der Name der Grammatik angegeben. Anschließend werden zunächst alle Tokens für den Lexer definiert. Über die spezielle \code{-> skip} Anweisung (Zeile 4) wird dem Lexer mitgeteilt, jegliche Leerzeichen und Zeilenumbrüche zu ignorieren \cite{antlr_reference}, da diese keine spezielle Bedeutung in \gls{RDS-PP} Kennzeichen haben, sondern nur optional zur Verbesserung der Lesbarkeit sind. Die restlichen Tokens repräsentieren Zahlen, Trenn- und Vorzeichen eines Kennzeichens. Hierbei wurden bewusst auf Tokens für  Unterstrich (\_) und alphabetische Datenstellen (A-Z) verzichtet. Das liegt daran, dass es nicht möglich ist, einem Unterstrich bzw. einem Buchstaben eine eindeutige Bedeutung und damit einen eindeutigen Token zuzuweisen:
\begin{itemize}
	\item Ein Unterstrich ist entweder ein Trennzeichen in einem Signalkennzeichen (s. \ref{sec:rdspp:signal}) oder ein Füllzeichen für nicht benötigte alphabetische Datenstellen in einer Gemeinsamen Zuordnung (s. \ref{sec:rdspp:gemeinsame_zuordnung}).
	\item Buchstaben werden in unterschiedlichen Teilmengen des Alphabets (A-Z) verwendet. Meistens sind die Buchstaben I und O aufgrund der Verwechslungsgefahr ausgeschlossen (s. \ref{sec:rdspp:grundregeln}), in der zweiten Gliederungsstufe der Gemeinsamen Zuordnung sind sie jedoch teilweise erlaubt, da sie beispielsweise für Länderkürzel benötigt werden. In der ersten Gliederungsstufe der Gemeinsamen Zuordnung dagegen sind nur die Buchstaben N oder S bzw. E oder W für die vier Himmelsrichtungen erlaubt (s. \ref{sec:rdspp:gemeinsame_zuordnung}).
\end{itemize}
Stattdessen werden einfach entsprechende Parserregeln verwendet. Für die in den Parserregeln verwendeten Literale (z.B. \code{'A'}, \code{'B'}, etc.) werden von ANTLR automatisch anonyme Tokens angelegt, sodass diese beim Lexen akzeptiert werden. \cite{antlr_reference} Ein explizites definieren von einzelnen Lexerregeln wie \code{A: 'A';} ist daher nicht nötig.

\begin{lstlisting}[language=ebnf, caption={RDS-PP Grammatik (1/3): Lexerregeln}, label={code:grammar:lexer}]
grammar RDSPP;

// lexer tokens
WHITESPACE: [ \r\n\t]+ -> skip;
NUMBER: '0'..'9';
DOT: '.';
SLASH: '/';
HASH: '#';
EQUALS: '=';
PLUS: '+';
PLUSPLUS: '++';
MINUS: '-';
COLON: ':';
SEMICOLON: ';';
AMPERSAND: '&';
\end{lstlisting}

Im Anschluss an die Definition der Tokens folgen Parserregeln (s. \autoref{code:grammar:parser:1}). Hierbei wird zunächst eine Startregel (Zeile 2) festgelegt, die quasi aussagt, dass jede Eingabe einem Kennzeichen entspricht. Wie dieses Kennzeichen zusammengesetzt sein darf, wird in der zweiten Regel ab Zeile 5 festgelegt. Diese Regel enthält die erlaubte Verkettung von Kennzeichen wie in \ref{sec:rdspp:verkettung} erläutert. Neben der durch die Verkettung eindeutigen Kennzeichen sind jedoch auch die nicht-verketteten, uneindeutigen Kennzeichenblöcke erlaubt, was entweder durch eine explizite Alternative (\code{|}) oder durch Markieren der Gemeinsamen Zuordnung in einer Verkettung als optional (\code{?}) erreicht wird.

\begin{lstlisting}[language=ebnf, caption={RDS-PP Grammatik (2/3): Allgemeine Parserregeln}, label={code:grammar:parser:1}]
// default start rule
start: designation EOF;

// possible designations and their allowed concatenations
designation:
	conjointDesignation
		| conjointDesignation? document
		| conjointDesignation? function (signal | document)?
		| conjointDesignation? operatingEquipment (terminal | document)?
		| conjointDesignation? pointOfInstallation document?
		| conjointDesignation? siteOfInstallation document?
		| product
		| terminal
		| signal;
\end{lstlisting}

Der weitere Teil der Grammatik besteht aus den Definitionen der in der \code{designation}-Regel verwendeten Regeln für die einzelnen Kennzeichen. Exemplarisch soll im Folgenden nur noch die Umwandlung der Spezifikation der Gemeinsamen Zuordnung in Parserregeln demonstriert werden. Das Umwandeln der anderen Kennzeichenarten erfolgt dann nach dem gleichen Schema.

Die Vorgaben zum Aufbau der Gemeinsamen Zuordnung sind in \autoref{img:rdspp:gemeinsame-zuordnung} abgebildet. Dort ist zu sehen, dass nach dem Vorzeichen (\#) zwei Gliederungsstufen (engl.: Breakdownlevel; Geographische Position + Projektspezifische Angaben) getrennt durch ein Trennzeichen (.) folgen. Dieser allgemeine Aufbau wird durch die erste Parserregel in \autoref{code:grammar:parser:2} abgebildet. Aufgrund von Festlegungen für verkürzte Schreibweisen im Anhang der Anwendungsrichtlinie \cite{norm:vgb_s_823_32_2014_03_en_de} gibt es in dieser Regel noch zwei Alternativen, die je nur eine Gliederungsstufe nach dem Vorzeichen enthalten.

Statt die Regeln für die beiden Gliederungsstufen direkt durch das Auflisten der Buchstaben-Zahlen-Reihenfolge zu definieren, werden diese zunächst in granularere Regeln (z.B. \code{latitude} und \code{longitude}) unterteilt. Dadurch ist die semantische Bedeutung (z.B. Längen- und Breitengrad) dieser Abschnitte später direkt aus dem \gls{AST} ersichtlich. Die einzelnen Abschnitte spiegeln dann die alphanumerische Reihenfolge der Datenstellen des Kennzeichens wieder. Hier ist nun auch zu erkennen, weshalb, wie anfänglich erläutert, für den Unterstrich und Buchstaben Parserregeln notwendig waren.

\begin{lstlisting}[language=ebnf, caption={RDS-PP Grammatik (3/3): Parserregeln der Gemeinsamen Zuordnung}, label={code:grammar:parser:2}]
// general designation structure
conjointDesignation:
	HASH geographicLocation DOT projectSpecificInformation
	| HASH geographicLocation
	| HASH projectSpecificInformation;

// structure of breakdown levels
geographicLocation:         latitude longitude;
projectSpecificInformation:	country region DOT townOrName projectCounter plantType facilityType;

// structure of each section
latitude:       NUMBER NUMBER NUMBER NUMBER ('N' | 'S');
longitude:      NUMBER NUMBER NUMBER NUMBER NUMBER ('E' | 'W');
country:        (alpha | 'I' | 'O') (alpha | 'I' | 'O');
region:         (alpha | 'I' | 'O' | underscore_alpha)
                (alpha | 'I' | 'O' | underscore_alpha) (alpha | 'I' | 'O');
townOrName:     (alpha | 'I' | 'O') (alpha | 'I' | 'O') (alpha | 'I' | 'O');
projectCounter: (NUMBER | underscore_alpha) NUMBER;
plantType:      alpha;
facilityType:   alpha;

// common rules
alpha: 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'J' | 'K' | 'L' | 'M' | 'N' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z';
underscore_alpha: '_';
\end{lstlisting}

Die vollständige Grammatik ist im Anhang \ref{appendix:sec:grammar} zu finden.

\section{Einsatz von ANTLR}
\label{sec:parser:einsatz_von_antlr}

Da nun geklärt wurde, wie die \gls{RDS-PP} Norm in eine Grammatik umzuwandeln ist, kann nun auf den konkrete Einsatz von ANTLR eingegangen werden.

Der Parser kann mithilfe der ANTLR-jar-Datei über folgende Bashbefehle aus der Grammatik \code{RDSPP.g4} generiert werden:
\begin{lstlisting}[style=bash, caption={Generierung eines Parsers mit ANTLR}]
#!/bin/bash
alias antlr4="java -jar antlr-4.8-complete.jar"
antlr4 -Dlanguage=Go -o parser RDSPP.g4
\end{lstlisting}
ANTLR generiert dabei folgende Dateien:
\begin{itemize}
	\item \textbf{rdspp\_lexer.go}: Lexer Implementierung
	\item \textbf{rdspp\_parser.go}: Parser Implementierung
	\item \textbf{rdspp\_listener.go} RDSPPListener-Interface (erläutert in \ref{sec:parser:antlr:ast_to_datastruct})
	\item \textbf{rdspp\_base\_listener.go} Leere Standard-Implementierung des RDSPPListener-Interface
	\item \textbf{RDSPP.tokens}, \textbf{RDSPPLexer.tokens}: Zuweisung einer Nummer (Tokentyp) zu jedem Token
\end{itemize}

Wie diese generierten Dateien nun verwendet werden, wird im Folgenden erklärt.

\subsection{Umwandeln des AST in eine Datenstruktur}
\label{sec:parser:antlr:ast_to_datastruct}

Der Aufruf des generierten Parsers ist relativ trivial. Im Grunde wird lediglich der Eingabestring an eine Lexerinstanz übergeben. Der Lexer erzeugt dann einen eindimensionalen Tokenvektor, der dem Parser übergeben wird. Die Frage dabei ist viel mehr: Wie kann mithilfe des Parsers die Eingabe bzw. der daraus generierte \gls{AST} in eine geeignete Datenstruktur, mit der weitergearbeitet werden kann, umgewandelt werden?

ANTLR bietet hierfür zwei Tree-Walking-Mechanismen, mit denen der generierte \gls{AST} über eine rekursive Tiefensuche abgelaufen wird: Visitors und Listeners. Der unterschied zwischen den beiden ist, dass ein Listener nur Callback-Methoden enthält, die von einem Standard-Parse-Tree-Walker aus der ANLTR Bibliothek aufgerufen werden, während ein Visitor Visit-Methoden zum expliziten besuchen von Kindknoten besitzt. Der Listener kann also nur passiv auf den Walk reagieren, während der Visitor der Walk aktiv steuern kann und muss. \cite{antlr_reference} Um den \gls{AST} in eine Datenstruktur zu übersetzten, spielt es keine Rolle Kontrolle über den Walk zu haben, weshalb das Listener-Pattern verwendet wird.

\autoref{uml:class:antlr_listener} zeigt die Vererbungshierarchie eines solches Listeners auf. Das generierte \code{RDSPPListener}-Interface enthält für jede Parserregel entsprechende Enter- und Exit-Methoden. Diese erweitert das allgemeine \code{ParseTreeListener}-Interface, welches eine Enter- und Exit-Methode enthält, die bei jeder Regel aufgerufen wird, sowie eine VisitTerminal-Methode, die bei jedem Token aufgerufen wird. Der generierte Struct \code{BaseRDSPPListener} implementiert das \code{RDSPPListener}-Interface mit leeren Methoden. Der eigene, auf den Anwendungsfall zugeschnittene \code{TreeListener} muss somit nur die Methoden überschreiben, die er benötigt. \cite{antlr_reference}

\begin{uml}[ht]
	\centering
	\includegraphics[width=\linewidth]{antlr_listener}
	\caption{ANTLR Listener: Vererbungshierarchie}
	\label{uml:class:antlr_listener}
\end{uml}

Bevor die genaue Umsetzung des Listeners erläutert werden kann, muss zunächst die zu erzeugende Datenstruktur bekannt sein. Diese ist in \autoref{uml:class:designationnode} zu sehen.

\begin{uml}[h]
	\centering
	\includegraphics[width=.65\linewidth]{designationnode}
	\caption{Datenstruktur für den Kennzeichenaufbau}
	\label{uml:class:designationnode}
\end{uml}

Der Struct \code{DesignationNode} ist der Knoten einer Baumstruktur, mit der das Kennzeichen hierarchisch abgebildet werden soll. Die Abbildung eine Kennzeichens als Baum sieht (vereinfacht) beispielsweise so wie in \autoref{img:parser:kennzeichen_als_baum} zu sehen aus. Vom Parser werden dabei nicht alle Fehler des \code{DesignationNode}s gesetzt, sondern lediglich Folgenden:
\begin{itemize}
	\item \code{Designation}: Der aktuelle Teil des kompletten Kennzeichen
	\item \code{Section}: Enumwert zur Identifizierung der einzelnen Kennzeichenabschnitte
	\item \code{Childs}: Alle untergeordneten Teilanschnitte
\end{itemize}

Das Setzen aller weiteren Felder (Benennungen, Beispiele) ist in ein anderes Package ausgelagert, da dies nicht Aufgabe des Parsers sein soll (Single Responsibility Principle). Dieser soll lediglich die Datenstruktur für weitere Verarbeitungsschritte liefern.

\begin{figure}[h]
	\centering
	\setlength{\fboxsep}{4pt}
	\setlength{\fboxrule}{1pt}
	\fcolorbox{lightgray}{white}{
		\begin{tikzpicture}[
			level distance=1.5cm,
			level 1/.style={sibling distance=6cm},
			level 2/.style={sibling distance=4cm},
			font=\scriptsize
		]
		\node[align=center] {\textbf{Designation}: \#5163N00017W.GBCON.BEA\_1WN =G001 MDK51 GP001\\\textbf{Section}: D\_Designation}
		child {node[align=center] {\textbf{Designation}: \#5163N00017W.GBCON.BEA\_1WN\\\textbf{Section}: CD\_ConjointDesignation}
			child {node[align=center] {\textbf{Designation}: \#\\\textbf{Section}: PRE\_Prefix}}
			child {node[align=center] {\textbf{Designation}: 5163N00017W\\\textbf{Section}: CD\_GeographicLocation}
				child {node[align=center] {...}}
			}
			child {node[align=center] {...}}
		}
		child {node[align=center] {\textbf{Designation}: =G001 MDK51 GP001\\\textbf{Section}: F\_Function}
			child {node[align=center] {...}}
		};
		\end{tikzpicture}
	}
	\caption{Kennzeichen als Baumstruktur}
	\label{img:parser:kennzeichen_als_baum}
\end{figure}

Um aus dem \gls{AST} die oben beschriebene Datenstruktur zu generieren, genügt es, wenn der Listener die Methoden \code{EnterEveryRule}, \code{ExitEveryRule} und \code{VisitTerminal} implementiert, da bis auf wenige Ausnahmen bei fast allen Parserregeln das gleiche gemacht werden kann. Die Logik des Listeners sei im Folgenden illustriert:

\begin{enumerate}
	\item Man benötigt einen Pointer \code{current}, der auf den aktuellen Knoten zeigt, und einen Stack \code{parents}, der Pointer zu allen Elternknoten von \code{current} enthält (mit dem direkten Elternknoten an oberster Stelle)
	\item Beim Aufruf von \code{EnterEveryRule} wird in
		\begin{enumerate}
			\item der start-Regel nichts gemacht
			\item der designation-Regel die Wurzel des Baums initialisiert und \code{current} auf die Wurzel gesetzt
			\item bei jeder anderen Regel wird ein neuer Knoten zu \code{current.Childs} hinzugefügt, \code{current} auf den \code{parents}-Stack gelegt (push) und anschließend \code{current} auf den neuen Knoten gesetzt
		\end{enumerate}
	\item Beim Aufruf von \code{ExitEveryRule} wird in
		\begin{enumerate}
			\item der start-Regel nichts gemacht
			\item der designation-Regel \code{current} auf \code{null} gesetzt
			\item in jeder anderen Regel das oberste Element des \code{parents}-Stacks heruntergenommen (pop) und \code{current} zugewiesen
		\end{enumerate}
	\item Beim Aufruf von \code{VisitTerminal} wird
		\begin{enumerate}
			\item im Falle, dass das Terminal ein Präfix- oder Separator-Token ist, nacheinander 2c und 3c gemacht
			\item in jedem anderen Fall nichts gemacht (Zahlen werden nicht einzeln berücksichtigt, sondern nur als ganzes (['012'] statt ['0', '1', '2']); andere Terminale wie EOF werden gänzlich ignoriert)
		\end{enumerate}
\end{enumerate}

Auf diese Weise wird das komplette Kennzeichen in eine hierarchische Datenstruktur umgewandelt, die weiterverarbeitet werden kann.

\subsection{Fehlerbehandlung}

Da es natürlich auch vorkommen kann, dass vom Benutzer eine fehlerhafte Eingabe kommt, muss geschaut werden, wie ANTLR darauf reagiert und welche Möglichkeiten es gibt, entsprechende Fehlermeldungen und/oder Hilfestellungen zu liefern.

\subsubsection{Error-Recovery}

ANTLR kommst bereits standardmäßig mit einer Error-Recovery-Stategie. Diese erlaubt es dem Parser auch bei einer fehlerhaften Eingabe weiterzumachen. In einem Fehlerfall wird auf jeden Fall ein \gls{AST} erzeugt, sodass der TreeListener aus dem vorigen Abschnitt trotzdem noch eine Teilstruktur ausgeben kann. Der Worst-Case ist hierbei, dass der Parser bei einer falschen Eingabe bereits bei der designation-Regel (s. \autoref{code:grammar:parser:1}) scheitert, wodurch die Ausgabe des TreeListeners ein einziger Knoten ist.

Die Error-Recovery von ANTLR ist ziemlich ausgereift und prinzipiell auch ziemlich gut. Das Problem ist lediglich, dass \gls{RDS-PP} Kennzeichen nur Buchstaben-Zahlen-Kombinationen sind und unterschiedliche Kennzeichenarten sich daher in ihrer Struktur relativ ähnlich sind. Der ANTLR-Parser weiß daher bei invaliden Eingaben häufig nicht, was er tun soll und in einer \code{NoViableAltException} und sehr geringen Erkennungsraten resultiert. Dies kann bereits bei verhältnismäßig kleinen Fehlern passieren. Beispielsweise das inkorrekte Kennzeichen \code{=G001 MD51 GP001} (korrekt: \code{=G001 MDK51 GP001}), bei dem nur ein Buchstabe vergessen wurde, ist so ein Fall, wie der dazugehörige Parsebaum in \autoref{img:parser:error-tree-1} zeigt.

Einem menschlichen Betrachter (mit tiefgründigem \gls{RDS-PP}-Wissen) würde bei dem Kennzeichen sofort auffallen, dass es sich um ein Funktionskennzeichen handelt, wodurch der Fehler relativ leicht lokalisierbar ist und das Kennzeichen trotzdem noch teilweise interpretierbar ist. Dies liegt daran, dass ein menschlicher Betrachter durch das Vorzeichen einen Rückschluss auf die Kennzeichenart zieht und dadurch die mögliche Syntax erheblich einschränkt.

Diese Vorgehensweise kann auch vom Kennzeichenerkenner nachgeahmt werden. Hierzu muss die Annahme getroffen werden, dass die Vorzeichen in einem Kennzeichen korrekt sind. Hierzu kann eine einfache Funktion geschrieben werden, die zusammengesetzte Kennzeichen anhand der Vorzeichen aufsplittet und für jedes Einzelkennzeichen die Kennzeichenart bestimmt. Jedes Kennzeichen kann dadurch mit einer separaten Startregel des Parser geparst werden. Diese zusätzlichen Startregeln werden in der Grammatik noch ergänzt (s. \autoref{code:grammar:parser:additional_start_rules}).

\begin{lstlisting}[language=ebnf, caption={RDS-PP Grammatik: Zusätzliche Startregeln für eine bessere Erkennung}, label={code:grammar:parser:additional_start_rules}]
// specific start rules for only one designation kind each
startOnlyConjointDesignation: conjointDesignation EOF;
startOnlyFunction: function EOF;
startOnlyOperatingEquipment: operatingEquipment EOF;
startOnlyPointOfInstallation: pointOfInstallation EOF;
startOnlySiteOfInstallation: siteOfInstallation EOF;
startOnlyProduct: product EOF;
startOnlyTerminal: terminal EOF;
startOnlySignal: signal EOF;
startOnlyDocument: document EOF
\end{lstlisting}

Die Regeln für die einzelnen Kennzeichen (z.B. \code{function}) können dabei nicht direkt verwendet werden, sondern es müssen diese separaten Startregeln eingeführt werden, da ANTLRs Error-Reporting via ErrorListener (wird im nächsten Abschnitt erklärt) nicht funktioniert, wenn die Eingabe nicht mit \code{EOF} terminiert. \cite{antlr_reference}

Wird nun das obige Beispiel \code{=G001 MD51 GP001} mit der Startregel für Funktionskennzeichen \code{startOnlyFunction} statt mit der Standardstartregel geparst, ergibt sich der Parsebaum in \autoref{img:parser:error-tree-2}. Im Vergleich zum vorherigen Resultat (\autoref{img:parser:error-tree-1}) ist eine deutliche Verbesserung zu erkennen. Der Großteil des Kennzeichen wird nun erkannt (grüne Blätter) und ein einzelner Fehler (rotes Blatt mit \code{<missing>}) deutet korrekterweise auf ein fehlendes Zeichen hin. Die Erkennungsrate und die Fehlermeldungen werden durch dieses Vorgehen also deutlich verbessert und das Ergebnis ähnelt sehr der Betrachtungsweise eines menschlichen \gls{RDS-PP}-Experten.

\begin{figure}[h!]
	\begin{subfigure}[b]{.37\linewidth}
		\centering
		\includegraphics[width=\linewidth]{parse-tree-1}
		\caption[mit Startregel start]{mit Startregel \code{start}}
		\label{img:parser:error-tree-1}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{.6\linewidth}
		\centering
		\includegraphics[width=\linewidth]{parse-tree-2}
		\caption[mit Startregel startOnlyFunction]{mit Startregel \code{startOnlyFunction}}
		\label{img:parser:error-tree-2}
	\end{subfigure}
	\caption[Vergleich der Parsebäume für das invalide Kennzeichen =G001 MD51 GP001 mit unterschiedlichen Startregeln]{Vergleich der Parsebäume für das invalide Kennzeichen \code{=G001 MD51 GP001} mit unterschiedlichen Startregeln}
	\label{img:parser:error-trees}
\end{figure}

\subsubsection{Fehlermeldungen}

Standardmäßig wird bei einem Fehler eine Konsolenausgabe mit einer Fehlermeldung erzeugt. ANTLR bietet jedoch ähnlich wie beim TreeListener auch einen ErrorListener-Mechanismus, mit dem Fehlermeldungen umgeleitet und nach Bedarf abgeändert/angereichert werden können.

\begin{uml}[h]
	\centering
	\includegraphics[width=.9\linewidth]{antlr_errorlistener}
	\caption{ANTLR ErrorListener: Vererbungshierarchie}
	\label{uml:class:antlr_errorlistener}
\end{uml}

Wie \autoref{uml:class:antlr_errorlistener} zeigt, liefert ANTLR bereits einen \code{DefaultErrorListener}, der das Interface \code{ErrorListener} mit leeren Methoden implementiert. \cite{antlr_reference} Der eigenen \code{errorListener} kann somit einfach von der Standardimplementierung erben und die benötigten Methoden überschreiben. Benötigt wird nämlich lediglich die \code{SyntaxError}-Methode, die bei jedem Syntaxfehler aufgerufen wird. In dieser Methode kann dann im Fehlerfall ein Datenobjekt erstellt werden, dass neben dem \code{DesignationNode}-Baum an die Benutzeroberfläche zurückgegeben werden kann. Das Datenobjekt ist vom Typ \code{ErrorInformation} (s. \autoref{uml:class:error_hint}). Dieser Struct enthält das komplette Kennzeichen (\code{Designation}), die ursprüngliche ANTLR-Fehlermeldung (\code{Message}), die Fehlerposition (\code{Index}), des fehlerhafte Zeichen (\code{Text}), den Fehlertyp (\code{Type}) sowie Vorschläge (\code{Suggestions}). Fehlertyp und Vorschläge können aus der Fehlermeldung von ANTLR gewonnen werden. So wird zum Beispiel bei der Fehlermeldung \code{mismatched input ':' expecting <EOF>} als Vorschlag \code{EOF} gesetzt. Alle weiteren Fehler können direkt aus des Parametern der \code{SyntaxError}-Methode erlangt werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=.9\linewidth]{error_hint}
	\caption{Datenklassen: HintDenomination und ErrorInformation}
	\label{uml:class:error_hint}
\end{figure}

\subsubsection{Hilfestellungen}

Durch ein \code{ErrorInformation}-Objekt kann dem Nutzer zwar genau die fehlerhafte Datenstelle aufgezeigt werden und je nachdem ein mehr oder weniger gute Fehlernachricht. Sie helfen jedoch wenig dabei, die korrekte Kennzeichenstruktur aufzuzeigen. Daher macht es Sinn, zum fehlerhaften Kennzeichen passende Hilfestellungen mitzuliefern. Diese sollen von der Form her ähnlich den Normspezifikationen wie beispielsweise \autoref{img:rdspp:funktion} (\autoref{sec:rdspp:funktion}, S. \pageref{sec:rdspp:funktion}) sein: Eine Liste mit Gliederungsstufen (Breakdownlevel), von denen jede eine Benennung, eine Liste mit Datenstellen und mindestens eine Erklärung/Beschreibung zu den Datenstellen enthält. \autoref{img:parser:hint} zeigt den schematischen Aufbau einer solchen Hilfestellung. Die entsprechende Datenstruktur (\code{HintDenomination}) ist in \autoref{uml:class:error_hint} zu sehen.

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[
		box/.style={draw, align=center},
	]
		% boxes
		\node (bl1) [box] {Gliederungsstufe 1};
		\node (bl2) [box, right=5mm of bl1] {Gliederungsstufe 2};
		\path let \p1=(bl1.west), \p2=(bl1.east) in
			node (dp1) [box, below=3mm of bl1, minimum width=\x2-\x1] {A N N N};
		\path let \p1=(bl2.west), \p2=(bl2.east) in
			node (dp2) [box, below=3mm of bl2, minimum width=\x2-\x1] {A A A N N};
		\node (sgn) [left=5mm of dp1.west] {?};
		
		% text (left)
		\node (txt1) [below left=8mm and 5mm of sgn] {Vorzeichen};
		\node (txt2) [below left=13mm and 5mm of sgn] {Beschreibung der Datenstelle};
		\node (txt3) [below left=18mm and 5mm of sgn] {Hinweise};
		\node (txt4) [below left=23mm and 5mm of sgn] {Verweise auf Normen};
		\node (txt5) [below left=28mm and 5mm of sgn] {etc.};
		
		% horizontal lines
		\draw ([yshift=-3mm]sgn.240) -- ([yshift=-3mm]sgn.300);
		\draw ([yshift=-3mm]dp1.200) -- ([yshift=-3mm, xshift=3.5mm]dp1.200);
		\draw ([yshift=-3mm]dp1.220) -- ([yshift=-3mm, xshift=12mm]dp1.220);
		\draw ([yshift=-3mm]dp2.197) -- ([yshift=-3mm, xshift=11mm]dp2.197);
		\draw ([yshift=-3mm]dp2.310) -- ([yshift=-3mm, xshift=7.5mm]dp2.310);
		
		% vertical lines
		\draw ([yshift=-3mm]sgn.270) |- (txt1.east);
		\draw ([yshift=-3mm, xshift=1.75mm]dp1.200) |- (txt2.east);
		\draw ([yshift=-3mm, xshift=6mm]dp1.220) |- (txt3.east);
		\draw ([yshift=-3mm, xshift=5.5mm]dp2.197) |- (txt4.east);
		\draw ([yshift=-3mm, xshift=3.75mm]dp2.310) |- (txt5.east);
	\end{tikzpicture}
	\caption{Hilfestellung des Parsers (schematisch)}
	\label{img:parser:hint}
\end{figure}

Die mögliche \code{HintDenomination}-Objekte sind statisch (eines je Kennzeichenart) und können daher persistent gespeichert sein und bei Bedarf neben einem \code{ErrorInformation}-Objekt zurückgegeben werden. Um nicht immer alle Hinweise sondern nur das nötige \code{HintDenomination}-Objekt zurückzugeben, kann die \code{SyntaxError}-Methode erweitert werden, sodass diese erkennt, in welcher Kennzeichenart der Syntaxfehler vorkommt. Dies kann auf zwei Wege erfolgen:
\begin{enumerate}
	\item Der \gls{AST} kann rekursiv nach oben in Richtung Wurzelknoten durchlaufen werden. Sobald man bei einer Parserregel eines Kennzeichens (z.B. \code{conjointDesignation}, \autoref{code:grammar:parser:2}) angekommen ist, weiß man, welche Kennzeichenart in der Eingabe einen Syntaxfehler hat und kann das entsprechende \code{HintDenomination}-Objekt auswählen.
	\item Falls man mit der ersten Methode auf das Startsymbol stößt, bevor die Kennzeichenart ermittelt wurde, ist der Fehler so gravierend, dass der \gls{AST} nicht spezifischer als die designation-Regel (s. \autoref{code:grammar:parser:1}) wurde. In diesem Fall kann alternativ im Kennzeichen von der fehlerhaften Datenstelle aus nach links nach dem nächsten Vorzeichen durchsucht werden, um die Kennzeichenart zu ermitteln.
\end{enumerate}

\subsection{Zusammenfassung}

Fasst man die Erkenntnisse aus diesem Abschnitt (\ref{sec:parser:einsatz_von_antlr}) zusammen, kann die Benutzung des Parsers so spezifiziert werden:

\begin{tabular}{p{1.8cm}p{\linewidth-1.8cm}}
	\textbf{Eingabe}: &	Kennzeichen: \code{string} \\
	\textbf{Ausgabe}: &	$-$ Kennzeichenbaum: \code{DesignationNode} \newline
						$-$ Fehlermeldung: \code{ErrorInformation} \textit{(optional)} \newline
						$-$ Hinweis zum Kennzeichenaufbau: \code{HintDenomination} \textit{(optional)}
\end{tabular}

Bei den Fehlermeldungen und Hinweisen macht es Sinn, diese auf je max. ein Objekt des am weitesten links auftretenden Fehler zu limitieren, da eine Korrektur der Fehler von rechts nicht immer zielführend ist. Zudem hat der Benutzer keinen Vorteil davon, mit mehreren Fehlermeldungen gleichzeitig erschlagen zu werden, nur um dann nicht zu wissen, wo er anfangen soll.

\section{Datengrundlage}
\label{sec:datengrundlage}

Um zu einem Kennzeichenabschnitt die Bedeutung angeben zu können, müssen diese Bedeutungen in irgendeiner Form vorliegen und zuordenbar sein. Wie diese strukturiert gespeichert werden und wie darauf zugegriffen werden kann, soll in diesem Abschnitt kurz erläutert werden.

\subsection{Vollständigkeit}

Die Daten selbst stammen aus der Anwendungsrichtlinie des \gls{VGB} für \gls{RDS-PP} oder entsprechend aus referenzierten Normen. Falls eine Norm nicht vorlag bzw. nicht über die Datenbank Perinorm einsehbar war, konnte eine entsprechende Datengrundlage nicht vollständig eingepflegt werden. In einem solchen Fall wurde stattdessen ein kleiner Teil an Daten exemplarisch aus Internetquellen bezogen. Die Daten sind somit nicht für alle Kennzeichenarten vollständig, jedoch auf jeden Fall in soweit vorhanden, dass eine prototypischer Einsatz möglich ist.

\subsection{Datenformat}

Da die Daten relativ statisch sind, besteht kein Bedarf, diese in einer Datenbank abzulegen. Stattdessen sollte ein geläufiges (hierarchisches) Datenformat wie \gls{XML}, \gls{JSON} oder \gls{YAML} verwendet werden. Konkret wurde sich für \gls{YAML} entschieden, da es eine einfachere Syntax hat und deutlich leserlicher ist als die anderen zwei. Das ist für den Anwendungsfall von Vorteil, da die meisten Daten nicht strukturiert vorlagen und das händische Einpflegen von Daten somit teilweise unumgänglich war.

Als Backend-Sprache wurde Go ausgewählt (s. \ref{sec:technologie:backend}). Da Go u.a. für Webanwendungen ausgelegt ist, gibt es gute Packages, die das \gls{YAML}-Format unterstützen. Beispielhaft hierfür sei die folgende Methodensignatur zum deserialisieren einer \gls{YAML}-Eingabe:

\begin{minipage}{\linewidth}
	\begin{lstlisting}[language=go, caption={Go: yaml package}]
	// package gopkg.in/yaml.v2
	func Unmarshal(in []byte, out interface{}) (err error)
	\end{lstlisting}
\end{minipage}

Hierzu muss lediglich ein Daten-Struct vorliegen, der die Struktur der \gls{YAML}-Datei abbildet. Die \gls{YAML}-Datei wird dann als Byte-Array der Methode übergeben (Parameter \code{in}). Eine Referenz auf eine Instanz des Daten-Structs wird über den Parameter \code{out} übergeben.

\subsection{Beispiel}

Um das oben erläuterte zu demonstrieren, sei hier ein kleines Beispiel gegeben, das die Benennung der Hauptsysteme (s. \ref{sec:rdspp:funktion}) umsetzt. \autoref{code:yaml:main_systems} zeigt ausschnittsweise den Inhalt der \gls{YAML}-Datei.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=yaml, caption={main\_systems.yaml}, label={code:yaml:main_systems}]
system:
- code: G
  denomination:
    de: Energieumwandlung, z.B. Windenergieanlage (WEA)
    en: Energy Conversion, e.g. Wind Turbine Generator System (VVTG)
- code: K
  denomination:
    de: Übergeordnete Kommunikation, z.B. Windkraftwerkmanagement, übergeordnetes Kommunikationsnetzwerk
    en: Common Communication, e.g. Wind power plant management, common communication network
# ...
\end{lstlisting}
\end{minipage}

\autoref{code:go:main_systems_struct} zeigt den dazugehörigen Daten-Struct.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=go, caption={Daten-Struct für Hauptsysteme}, label={code:go:main_systems_struct}]
type MainSystemDenomination struct {
	System []struct {
		Code string `yaml:"code" json:"code"`
		Denomination struct {
			De string `yaml:"de" json:"de"`
			En string `yaml:"en" json:"en"`
		} `yaml:"denomination" json:"denomination"`
	} `json:"system"`
}
\end{lstlisting}
\end{minipage}

Der Daten-Struct spiegelt dabei 1:1 die \gls{YAML}-Struktur wieder. Die Strings hinter jedem Structfeld sind Tags, die Meta-Informationen enthalten, die über Reflexion zugänglich sind. Diese werden beim Serialisieren bzw. Deserialisieren bei der Auswahl der Namen der \gls{JSON}- bzw. \gls{YAML}-Felder berücksichtigt. \cite{golang:wiki}
