# Intructions to convert Excel spreachsheets to LaTeX tables

1. Download the [Excel2LaTeX](https://github.com/krlmlr/Excel2LaTeX) add-in
2. Create a `.xlsx` file in this directory
3. Create your table and select the cells you want to convert
4. _Excel > Add-Ins > Convert Table to LaTeX_
   1. Disable _Create table environment_
   2. Click _Save to File_
5. Run `latin1_to_utf8.sh` to correct the encoding of all `*.tex` files in the directory
6. Include the converted table like this:
   ```tex
   \begin{table}
       \centering
       \input{path/to/converted_table.tex}
       \caption{My awesome converted table}
       \label{table:converted}
   \end{table}
   ```


> **Tipp**: Make sure all required packages are loaded via `\usepackage{...}`. See [Excel2LaTeX](https://github.com/krlmlr/Excel2LaTeX) documentation to know which packages you might need.
