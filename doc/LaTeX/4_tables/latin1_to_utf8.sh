for texfile in *.tex; do
    tmp="$(iconv --from-code ISO-8859-1 --to-code UTF-8 $texfile)"
    echo "$tmp" > "$texfile"
done
